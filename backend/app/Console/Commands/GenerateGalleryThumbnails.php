<?php

namespace Foodstarz\Console\Commands;

use Illuminate\Console\Command;
use Foodstarz\Models\ImageGalleries;
use Intervention\Image\Facades\Image;

class GenerateGalleryThumbnails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:gallery_thumbnails';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates thumbnails for all gallery images.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $images = ImageGalleries::get();

        if (!file_exists(storage_path('app/gallery/thumbnails'))) {
            mkdir(storage_path('app/gallery/thumbnails'), 0777, true);
        }

        foreach($images as $image) {
            if(!empty($image->image) && file_exists(storage_path('app/gallery/'.$image->$image)) && !file_exists(storage_path('app/gallery/thumbnails/'.$image->$image))) {
                print storage_path('app/gallery/'.$image->image).PHP_EOL;
                $thumbnail = Image::make(storage_path('app/gallery/'.$image->image));
                $thumbnail->resize(750, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $thumbnail->save(storage_path('app/gallery/thumbnails/'.$image->image), 100);

                print $image->image.' - DONE!';
            }
        }

        print "------------------";
        print sizeof($image).' images successfully thumbnailed.'.PHP_EOL;
    }
}
