<?php

namespace Foodstarz\Console\Commands;

use Foodstarz\Models\Videos;
use Vinkla\Vimeo\VimeoManager;
use Illuminate\Console\Command;

class GetVimeoImages extends Command
{

    protected $signature = 'command:GetVimeoImages';

    protected $description = 'Command description';
    
    public function __construct(VimeoManager $vimeo)
    {
        $this->vimeo = $vimeo;
        parent::__construct();
    }

    public function handle()
    {

        $videos = Videos::all();

        foreach($videos as $video) {
            $get = $this->vimeo->request($video->video.'/pictures', [], 'GET');
            $picture = empty($get['body']['data'][0]['sizes'][3]['link']) ? '' : $get['body']['data'][0]['sizes'][3]['link'];
            Videos::where('id', $video->id)->update(['image' => $picture]);
        }

    }
}
