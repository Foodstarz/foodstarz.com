<?php

namespace Foodstarz\Console\Commands;

use Illuminate\Console\Command;
use App;
use Mail;
use Config;
use Foodstarz\Models\User;
use Foodstarz\Models\UserActivation;

class ResendActivationEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:resend';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resend the activation emails to all unactivated registered users.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Resending activation emails...');

        $users = User::where('active', 0)->get();
        $progress_bar = $this->output->createProgressBar(count($users));

        foreach($users as $user) {
            $activation = UserActivation::where('user_id', $user->id)->first();

            if($activation) {
                Mail::queue(Config::get('custom.theme') . '.emails.'.App::getLocale().'.register', ['name' => $user->name, 'activation_link' => route('user_activate', ['key' => $activation->activation_key])], function ($message) use ($user) {
                        $message->to($user->email, $user->name)->subject(trans('emails/register.subject'));
                    });
            }

            $progress_bar->advance();
        }

        $progress_bar->finish();
        $this->info('Done!');
    }
}
