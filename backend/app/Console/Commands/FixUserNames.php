<?php

namespace Foodstarz\Console\Commands;

use Illuminate\Console\Command;
use Foodstarz\Models\User;
use Foodstarz\Helpers\UserHelpers;

class FixUserNames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:usernames';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command fixes all users\' names.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::get();
        $progress_bar = $this->output->createProgressBar(count($users));

        foreach($users as $user) {
            $user->name = UserHelpers::usernameFix($user->name);
            $user->save();

            $progress_bar->advance();
        }
        $progress_bar->finish();

        $this->info('Users\' names fixing process finished successfully!');    }
}
