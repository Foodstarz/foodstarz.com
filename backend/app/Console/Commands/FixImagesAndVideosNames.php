<?php

namespace Foodstarz\Console\Commands;

use Illuminate\Console\Command;
use Foodstarz\Models\ImageGalleries;
use Foodstarz\Models\Videos;
use Foodstarz\Helpers\CommonHelpers;
use DB;

class FixImagesAndVideosNames extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'fix:images_and_videos_names';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fixes all the videos and images names according the standards.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->info('Images names fixing process started...');

        /*
        $old_images = DB::table('old_image_galleries')->get();
        $old_images_progress_bar = $this->output->createProgressBar(count($old_images));

        foreach($old_images as $old_image) {
            $new_image = ImageGalleries::where('id', $old_image->id)->first();
            if($new_image) {
                $new_image->caption = $old_image->caption;
                $new_image->save();
            }
            $old_images_progress_bar->advance();
        }
        $old_images_progress_bar->finish();

        $old_images = DB::table('image_galleries')->get();
        $old_images_progress_bar = $this->output->createProgressBar(count($old_images));

        foreach($old_images as $old_image) {
            DB::table('image_galleries')->where('id', $old_image->id)->update(['updated_at' => $old_image->created_at]);
            $old_images_progress_bar->advance();
        }
        $old_images_progress_bar->finish();
        */

        $images = ImageGalleries::get();
        $images_progress_bar = $this->output->createProgressBar(count($images));

        foreach($images as $image) {
            if(empty(trim($image->caption))) {
                $image->caption = null;
            } else {
                $image->caption = CommonHelpers::titleCase($image->caption);
            }

            $image->updated_at = $image->created_at;
            $image->save();
            $images_progress_bar->advance();
        }
        $images_progress_bar->finish();

        $this->info('Images names fixing process finished successfully!');

        $this->info('Videos names fixing process started...');

        $videos = Videos::get();
        $videos_progress_bar = $this->output->createProgressBar(count($videos));

        foreach($videos as $video) {
            $video->title = CommonHelpers::titleCase($video->title);
            $video->save();
            $videos_progress_bar->advance();
        }
        $videos_progress_bar->finish();

        $this->info('Videos names fixing process finished successfully!');
    }
}
