<?php

namespace Foodstarz\Console\Commands;

use Foodstarz\Models\Videos;
use Vinkla\Vimeo\VimeoManager;
use Illuminate\Console\Command;

class VideosNull extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'videos:null';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(VimeoManager $vimeo)
    {
        $this->vimeo = $vimeo;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $videos = Videos::whereNull('description')->get();

        if( !$videos->isEmpty() ){
            \Mail::queue('admin_layout.emails.video_null', [], function ($message){
                $message->to('david@foodstarz.com', 'David Broich')->subject('New \"Untitled video\" appeared');
            });
        }
    }
}
