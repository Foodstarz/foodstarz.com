<?php

namespace Foodstarz\Console\Commands;

use Illuminate\Console\Command;
use Foodstarz\Models\User;
use Foodstarz\Models\Recipe;

class CheckUserAvatars extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:avatars';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks all users\' avatars.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('active', 1)->get();
        $i=0;

        foreach($users as $user) {
            if(!empty($user->profile->avatar) && !file_exists(storage_path('app/avatars/'.$user->profile->avatar))) {
                $i++;

                print storage_path('app/avatars/'.$user->profile->avatar).PHP_EOL;
            }
        }

        print sizeof($users).' - '.$i.PHP_EOL;
    }
}
