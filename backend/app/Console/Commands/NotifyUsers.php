<?php

namespace Foodstarz\Console\Commands;

use Illuminate\Console\Command;
use Foodstarz\Models\User;
use Foodstarz\Models\Recipe;
use Config;
use Mail;
use App;

class NotifyUsers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'notify:users_no_recipes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notifies all users without recipes posted.';

    private $theme;
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        
        $this->theme = Config::get('custom.theme');
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::where('active', 1)->get();
        
        foreach($users as $user) {
            if(!\Foodstarz\Helpers\UserHelpers::userActivity($user)) {
                Mail::send($this->theme . '.emails.'.App::getLocale().'.notify_no_recipes', ['user' => $user], function ($message) use ($user) {
                    $message->from("david@foodstarz.com");
                    $message->to($user->email);
                    $message->subject(trans('emails/notify_no_recipes.subject'));
                });
            }
        }
    }
}
