<?php

namespace Foodstarz\Console\Commands;

use File;
use Storage;
use Foodstarz\Models\Profile;
use Illuminate\Console\Command;

class DeleteNotUsedAvatars extends Command
{

    protected $signature = 'command:DeleteNotUsedAvatars';

    protected $description = 'Command description';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        if ($this->confirm('Limit 10? [y|N]')) {
            $users = Profile::limit(10)->get();
        } else $users = Profile::all();
        $avatars = [];
        foreach($users as $user) {
            if($user->avatar) {
                if(Storage::has('avatars/' . $user->avatar)) {
                    $filename = md5($user->user_id) . '.' . File::extension('avatars/' . $user->avatar);
                    if($user->avatar != $filename) Storage::move('avatars/' . $user->avatar, 'avatars/' . $filename);
                    Profile::where('user_id', $user->user_id)->update(['avatar' => $filename]);
                    echo $this->info('renamed ' . $user->avatar . '->' . $filename);
                    $avatars[] = $filename;
                } else {
                    Profile::where('user_id', $user->user_id)->update(['avatar' => null]);
                    echo $this->info('nulled ' . $user->avatar);
                }
            }
        }
        if(count($users) > 10) {
            foreach (Storage::files('avatars') as $file) {
                $fl = str_replace('avatars/', '', $file);
                if (!in_array(str_replace('avatars/', '', $file), $avatars)) {
                    Storage::move($file, 'avatars/notused/' . $fl);
                    //                Storage::delete($file);
                    echo $this->line('moved ' . $file);
                }
            }
        }
    }
}
