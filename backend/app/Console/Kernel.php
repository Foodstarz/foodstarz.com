<?php

namespace Foodstarz\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \Foodstarz\Console\Commands\Inspire::class,
        \Foodstarz\Console\Commands\NotifyUsers::class,
        \Foodstarz\Console\Commands\CheckUserAvatars::class,
        \Foodstarz\Console\Commands\GenerateGalleryThumbnails::class,
        \Foodstarz\Console\Commands\FixImagesAndVideosNames::class,
        \Foodstarz\Console\Commands\FixUserNames::class,
        \Foodstarz\Console\Commands\ResendActivationEmail::class,
        \Foodstarz\Console\Commands\GetVimeoImages::class,
        \Foodstarz\Console\Commands\DeleteNotUsedAvatars::class,
        \Foodstarz\Console\Commands\VideosNull::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
    }
}
