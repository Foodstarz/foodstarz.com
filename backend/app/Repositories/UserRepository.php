<?php

namespace Foodstarz\Repositories;

use Foodstarz\Models\User;
use Foodstarz\Models\Profile;
use Foodstarz\Models\UserActivation;
use Mail;
use Config;
use App;

class UserRepository {

    /**
     * This method registers a new user.
     *
     * @param array $data
     * @return bool|User
     * @throws \Exception
     */
    public static function registerUser(array $data) {

        if(!is_array($data)) {
            throw new \Exception('Data parameter is not an array.');
        } else {
            $expected_params = ['name', 'email', 'password', 'role', 'register_ip'];
            $count = 0;
            $insert = [];

            foreach($data as $param => $value) {
                if(in_array($param, $expected_params)) {
                    if($param == 'password') {
                        $insert[$param] = bcrypt($value);
                    } else {
                        $insert[$param] = $value;
                    }
                    $count++;
                }
            }

            if($count != sizeof($expected_params)) {
                throw new \Exception('Data array doesn\'t have all the needed data.');
            }
        }

        $user = User::create($insert);

        $profile = new Profile();
        $profile->user_id = $user->id;
        $profile->culinary_profession = $data['culinary_profession'];

        $profile_slug = str_slug($user->name, '-');
        $slug_number = 2;
        while(Profile::where('slug', $profile_slug)->count() > 0) {
            $profile_slug = str_slug($user->name.' '.$slug_number, '-');
            $slug_number++;
        }
        $profile->slug = $profile_slug;

        if($profile->save()) {
            $activation = new UserActivation();
            $activation->user_id = $user->id;
            $activation->activation_key = md5(uniqid('', true));

            if($activation->save()) {
                Mail::queue(Config::get('custom.theme') . '.emails.'.App::getLocale().'.register', ['name' => $user->name, 'activation_link' => route('user_activate', ['key' => $activation->activation_key])], function ($message) use ($user) {
                    $message->to($user->email, $user->name)->subject(trans('emails/register.subject'));
                });
                return $user;
            } else {
                $user->delete();
                $profile->delete();
                return false;
            }
        } else {
            $user->delete();
            return false;
        }

    }

}