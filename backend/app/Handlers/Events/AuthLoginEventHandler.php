<?php

namespace Foodstarz\Handlers\Events;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Foodstarz\Models\User;
use Illuminate\Http\Request;

use Event;
use Foodstarz\Events\UserProfileWasUpdated;

class AuthLoginEventHandler
{
    /**
     * Create the event handler.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param User $user
     * @param $remember
     */
    public function handle(User $user, $remember)
    {
        $user->last_ip = $this->request->ip();
        $user->save();

        Event::fire(new UserProfileWasUpdated($user));
    }
}
