<?php

namespace Foodstarz\Listeners;

use Foodstarz\Events\UserProfileWasUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class UserProfileUpdateListener
{
    private $request;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Handle the event.
     *
     * @param  UserProfileWasUpdated  $event
     * @return void
     */
    public function handle(UserProfileWasUpdated $event)
    {
        $user = $event->user;
        $profile = $user->profile;

        $this->request->session()->put('user', $user);
        $this->request->session()->put('profile', $profile);
    }
}
