<?php

namespace Foodstarz\Events;

use Foodstarz\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Foodstarz\Models\User;

class UserProfileWasUpdated extends Event
{
    use SerializesModels;

    public $user;

    /**
     * Create a new event instance.
     *
     * @param User $user
     * @param Profile $profile
     */
    public function __construct(User $user)
    {
        $this->user = $user->fresh();
        $this->profile = $user->profile();
    }
}
