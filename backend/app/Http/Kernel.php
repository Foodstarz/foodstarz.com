<?php

namespace Foodstarz\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \Foodstarz\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
        \Foodstarz\Http\Middleware\VerifyCsrfToken::class,
        \Foodstarz\Http\Middleware\Language::class
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \Foodstarz\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \Foodstarz\Http\Middleware\RedirectIfAuthenticated::class,
        'baseProfileData' => \Foodstarz\Http\Middleware\CheckBaseProfileData::class,
        'avatar' => \Foodstarz\Http\Middleware\CheckAvatar::class,
        'chefData' => \Foodstarz\Http\Middleware\ChefData::class,
        'chefProfile' => \Foodstarz\Http\Middleware\ChefProfile::class,
        'isAdmin' => \Foodstarz\Http\Middleware\CheckAdmin::class,
    ];
}
