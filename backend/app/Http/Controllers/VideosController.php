<?php

namespace Foodstarz\Http\Controllers;

use Illuminate\Http\Request;
use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\MessageBag;
use Config;
use Auth;
use Validator;
use File;
use Vinkla\Vimeo\VimeoManager;
use Foodstarz\Models\Videos;
use Foodstarz\Models\Profile;

use Facebook\Facebook;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Facebook\Exceptions\FacebookSDKException;

use Foodstarz\Helpers\CommonHelpers;
use Foodstarz\Models\Bookmark;

class VideosController extends Controller
{

    use DispatchesJobs,
        ValidatesRequests;

    private $viewData = array();
    private $theme;

    public function __construct(VimeoManager $vimeo) {
        $this->viewData['inner_page'] = false;
        $this->theme = Config::get('custom.theme');
        $this->vimeo = $vimeo;
    }

    public function videos(VimeoManager $vimeo) {
        $this->viewData['inner_page'] = true;

        $this->viewData['videos'] = Videos::getVideos();

        foreach($this->viewData['videos'] as $video) {
            $picture = $vimeo->request($video->video.'/pictures', [], 'GET');

            if ( !empty($picture['body']['data'][0]['sizes'][2]['link']) ){
                $pic_arr = explode('?', $picture['body']['data'][0]['sizes'][2]['link']);
                $this->viewData['videos_pictures'][$video->id] = $pic_arr[0];
            }
            else{
                $this->viewData['videos_pictures'][$video->id] = 'https://i.vimeocdn.com/video/111111111_295x0.jpg';
            }
        }

        return view($this->theme . '.pages.videos', $this->viewData);
    }

    public function userVideos($slug) {
        $profile = Profile::getProfileBySlug($slug);

        if($profile) {
            $this->viewData['inner_page'] = true;
            $this->viewData['user'] = $profile->user;
            $this->viewData['profile'] = $profile;
            $this->viewData['videos'] = Videos::getAllVideosByUser($profile->user->id);

            $this->viewData['videos_pictures'] = [];
            foreach($this->viewData['videos'] as $video) {
                $picture = $this->vimeo->request($video->video.'/pictures', [], 'GET');
                if (!empty($picture['body']['data'][0]['sizes'][3]['link']) ) {
                  $this->viewData['videos_pictures'][$video->id] = $picture['body']['data'][0]['sizes'][3]['link'];
                } else {
                  $this->viewData['videos_pictures'][$video->id] = 'https://i.vimeocdn.com/video/111111111_640x0.jpg';
                }
            }

            return view($this->theme . '/pages.profile_videos', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function getVimeoTicket() {
    	$jsonData = [];
        $jsonData['ok'] = false;

    	$ticket = $this->vimeo->request('/me/videos', ['type' => 'streaming', 'upgrade_to_1080' => true], 'POST');

    	$jsonData['vimeo_response'] = $ticket;

        if(isset($ticket['body']['upload_link'])) $jsonData['ok'] = true;

        return response()->json($jsonData);
    }

    public function completedVimeoUpload(Request $request) {
    	$jsonData = [];
        $jsonData['ok'] = false;

        $complete = $this->vimeo->request($request->get('complete_uri'), ['method' => 'DELETE'], 'DELETE');

        $jsonData['vimeo_response'] = $complete;

        if($complete['status'] == 201 && empty($complete['body']) && !empty($complete['headers']['Location'])) {
            $video = new Videos();
            $video->user_id = Auth::user()->id;
            $video->video = $complete['headers']['Location'];
            $picture = $this->vimeo->request($video->video . '/pictures', [], 'GET');
            $video->image = empty($picture['body']['data'][0]['sizes'][3]['link']) ? '' : $picture['body']['data'][0]['sizes'][3]['link'];
            $video->save();

            $jsonData['ok'] = true;
            $jsonData['redirect_edit'] = route('video_edit', $video->id);
        }

        return response()->json($jsonData);
    }

    public function editVideo($id, Request $request) {
        session_start();

        $videoData = Videos::getVideo($id);

        if($videoData) {
            if($videoData->user_id == Auth::user()->id || Auth::user()->role == 1) {
                $this->viewData['inner_page'] = true;
                $this->viewData['user'] = Auth::user();
                $this->viewData['profile'] = Auth::user()->profile;
                $this->viewData['videoData'] = $videoData;

                $fb = new Facebook([
                    'app_id' => env('FACEBOOK_APP_ID'),
                    'app_secret' => env('FACEBOOK_APP_SECRET'),
                    'default_graph_version' => 'v2.4',
                ]);

                $helper = $fb->getRedirectLoginHelper();

                if($request->get('code') !== null) {
                    try {
                        $accessToken = $helper->getAccessToken();
                    } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                        // When Graph returns an error
                        echo 'Graph returned an error: ' . $e->getMessage();
                        exit;
                    } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                        // When validation fails or other local issues
                        echo 'Facebook SDK returned an error: ' . $e->getMessage();
                        exit;
                    }

                    if (! isset($accessToken)) {
                        if ($helper->getError()) {
                            header('HTTP/1.0 401 Unauthorized');
                            echo "Error: " . $helper->getError() . "\n";
                            echo "Error Code: " . $helper->getErrorCode() . "\n";
                            echo "Error Reason: " . $helper->getErrorReason() . "\n";
                            echo "Error Description: " . $helper->getErrorDescription() . "\n";
                        } else {
                            header('HTTP/1.0 400 Bad Request');
                            echo 'Bad request';
                        }
                        exit;
                    }

                    $oAuth2Client = $fb->getOAuth2Client();
                    $tokenMetadata = $oAuth2Client->debugToken($accessToken);
                    $tokenMetadata->validateAppId(env('FACEBOOK_APP_ID'));
                    // If you know the user ID this access token belongs to, you can validate it here
                    // $tokenMetadata->validateUserId('123');
                    // $tokenMetadata->validateExpiration();

                    if (! $accessToken->isLongLived()) {
                        // Exchanges a short-lived access token for a long-lived one
                        try {
                            $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                            echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>";
                            exit;
                        }
                        echo '<h3>Long-lived</h3>';
                    }

                    $user = User::getUserByEmail(session('user')->email);
                    $profile = $user->profile();
                    $profile->update(['fb_access_token' => (string) $accessToken]);
                    Event::fire(new UserProfileWasUpdated($user));
                } else {
                    try {
                        $accessToken = $helper->getAccessToken();
                    } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                        $user = User::getUserByEmail(session('user')->email);
                        $profile = $user->profile();
                        $profile->update(['fb_access_token' => null]);
                        Event::fire(new UserProfileWasUpdated($user));
                    }

                    $permissions = ['publish_actions'];
                    $loginUrl = $helper->getLoginUrl(route('video_edit', $id), $permissions);
                    $this->viewData['login_link'] = htmlspecialchars($loginUrl);
                }

                return view($this->theme . '/pages.edit_video', $this->viewData);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function editVideoProcess($id, Request $request) {
        $videoData = Videos::getVideo($id);

        if($videoData) {
            if($videoData->user_id == Auth::user()->id || Auth::user()->role == 1) {
                $jsonData = [];
                $jsonData['ok'] = false;
                $jsonData['_token'] = csrf_token();

                $requestData = $request->only(['title', 'description']);
                $errors = new MessageBag();

                $validator = Validator::make($requestData, [
                    'title' => 'required|min:2|max:128',
                    'description' => 'required|min:10|max:128'
                ]);

                if ($validator->fails()) {
                    $jsonData['errors'] = $validator->errors();
                    return response()->json($jsonData);
                }

                $videoData->title = CommonHelpers::titleCase($requestData['title']);
                $videoData->description = $requestData['description'];
                $videoData->save();

                $patch = $this->vimeo->request($videoData->video, ['name' => $videoData->title.' - '.$videoData->user->name, 'description' => $videoData->description], 'PATCH');

                $jsonData['ok'] = true;
                $jsonData['msg'] = trans('pages/edit_video.messages.success');
                $jsonData['redirect'] = route('video_view', ['id' => $videoData->id]);

                if($request->has('facebook')) {
                    $access_token = Auth::user()->profile->fb_access_token;

                    $facebookData = array();
                    $facebookData['consumer_key'] = '814279045307343';
                    $facebookData['consumer_secret'] = 'fbb35c7b62f18360314aa4bdd73fc2dc';

                    $facebook = new FacebookPoster($facebookData);
                    $facebook->share($videoData->title, $jsonData['redirect'], asset('assets/images/logo.png'), $videoData->description, $access_token);
                }

                return response()->json($jsonData);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function viewVideo($id, Request $request) {
        $videoData = Videos::getVideo($id, 'title');

        if($videoData) {
            $this->viewData['inner_page'] = true;
            $this->viewData['user'] = $videoData->user;
            $this->viewData['profile'] = $videoData->user->profile;
            $this->viewData['video'] = $videoData;
            $this->viewData['social_url'] = route('video_view', $id);
            $this->viewData['social_description'] = $videoData->title;

            $picture = $this->vimeo->request($videoData->video.'/pictures', [], 'GET');
            $this->viewData['social_image'] = empty($picture['body']['data'][0]['sizes'][3]['link']) ? '' : $picture['body']['data'][0]['sizes'][3]['link'];

            $this->viewData['bookmarked'] = (Bookmark::getBookmark(3, $videoData->id)) ? true : false;

            return view($this->theme . '/pages.view_video', $this->viewData);
        }
        else if ($videoData = Videos::getVideo($id)){
            $this->viewData['inner_page'] = true;
            $this->viewData['user'] = $videoData->user;
            $this->viewData['profile'] = $videoData->user->profile;
            $this->viewData['video'] = $videoData;
            $this->viewData['social_url'] = route('video_view', $id);
            $this->viewData['social_description'] = 'Untitled';

            $picture = $this->vimeo->request($videoData->video.'/pictures', [], 'GET');
            $this->viewData['social_image'] = empty($picture['body']['data'][0]['sizes'][3]['link']) ? '' : $picture['body']['data'][0]['sizes'][3]['link'];

            $this->viewData['bookmarked'] = (Bookmark::getBookmark(3, $videoData->id)) ? true : false;

            return view($this->theme . '/pages.view_video', $this->viewData);
        }
        else{
            abort(404);
        }
    }

    public function deleteVideoProcess($id) {
        $videoData = Videos::getVideo($id);

        if($videoData) {
            if($videoData->user_id == Auth::user()->id || Auth::user()->role == 1) {
                $videoData->delete();

                $this->vimeo->request($videoData->video, [], 'DELETE');


                return redirect()->route('view_user_videos', ['slug' => Auth::user()->profile->slug]);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function hideVideoProcess($id) {
        if(Auth::check() && Auth::user()->role == 1) {
            $videoData = Videos::getVideo($id);

            if ($videoData) {
                if($videoData->hidden == 0) {
                    $videoData->hidden = 1;
                } else {
                    $videoData->hidden = 0;
                }
                $videoData->save();

                return redirect()->route('video_view', $videoData->id);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function videosNull(VimeoManager $vimeo){

        $this->viewData['videos'] = Videos::whereNull('description')->get();

        foreach($this->viewData['videos'] as $video) {
            $picture = $vimeo->request($video->video.'/pictures', [], 'GET');
            $this->viewData['videos_pictures'][$video->id] = empty($picture['body']['data'][0]['sizes'][3]['link']) ? '' : $picture['body']['data'][0]['sizes'][3]['link'];
        }

        return view($this->theme . '/pages.videos_null', $this->viewData);
    }

    public function addVideo(Request $request) {
        session_start();

        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;

        $fb = new Facebook([
          'app_id' => env('FACEBOOK_APP_ID'),
          'app_secret' => env('FACEBOOK_APP_SECRET'),
          'default_graph_version' => 'v2.4',
        ]);

        $helper = $fb->getRedirectLoginHelper();

        if($request->get('code') !== null) {
            try {
                $accessToken = $helper->getAccessToken();
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                // When Graph returns an error
                echo 'Graph returned an error: ' . $e->getMessage();
                exit;
            } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                // When validation fails or other local issues
                echo 'Facebook SDK returned an error: ' . $e->getMessage();
                exit;
            }

            if (! isset($accessToken)) {
                if ($helper->getError()) {
                    header('HTTP/1.0 401 Unauthorized');
                    echo "Error: " . $helper->getError() . "\n";
                    echo "Error Code: " . $helper->getErrorCode() . "\n";
                    echo "Error Reason: " . $helper->getErrorReason() . "\n";
                    echo "Error Description: " . $helper->getErrorDescription() . "\n";
                } else {
                    header('HTTP/1.0 400 Bad Request');
                    echo 'Bad request';
                }
                exit;
            }

            $oAuth2Client = $fb->getOAuth2Client();
            $tokenMetadata = $oAuth2Client->debugToken($accessToken);
            $tokenMetadata->validateAppId(env('FACEBOOK_APP_ID'));
            // If you know the user ID this access token belongs to, you can validate it here
            // $tokenMetadata->validateUserId('123');
            // $tokenMetadata->validateExpiration();

            if (! $accessToken->isLongLived()) {
                // Exchanges a short-lived access token for a long-lived one
                try {
                    $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                    echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>";
                    exit;
                }
                echo '<h3>Long-lived</h3>';
            }

            $user = User::getUserByEmail(session('user')->email);
            $profile = $user->profile();
            $profile->update(['fb_access_token' => (string) $accessToken]);
            Event::fire(new UserProfileWasUpdated($user));
        } else {
            try {
                $accessToken = $helper->getAccessToken();
            } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                $user = User::getUserByEmail(session('user')->email);
                $profile = $user->profile();
                $profile->update(['fb_access_token' => null]);
                Event::fire(new UserProfileWasUpdated($user));
            }

            $permissions = ['publish_actions'];
            $loginUrl = $helper->getLoginUrl(route('add_video'), $permissions);
            $this->viewData['login_link'] = htmlspecialchars($loginUrl);
        }

        return view($this->theme . '/pages.add_video', $this->viewData);

    }

    public function addVideoProcess(Request $request) {
        $videoData = new Videos();

        if($videoData) {
            if(Auth::user()->role >= 1) {
                $jsonData = [];
                $jsonData['ok'] = false;
                $jsonData['_token'] = csrf_token();

                $requestData = $request->only(['title', 'description']);
                $errors = new MessageBag();

                $validator = Validator::make($requestData, [
                  'video' => 'mimes:mp4,x-m4v',
                  'title' => 'required|min:2|max:128',
                  'description' => 'required|min:10|max:128'
                ]);
                $jsonData['error_video'] = '';
                if($_FILES['video']['size'] === 0 || $_FILES['video']['error'] > 0)
                {
                    $jsonData['error_video'] = trans('pages/video_add.form.video.replace');
                }

                if ($validator->fails() || $jsonData['error_video']) {
                    $jsonData['errors'] = $validator->errors();
                    return response()->json($jsonData);
                }

                $dataVideo = $this->vimeo->upload($_FILES['video']['tmp_name'], false);

                $videoData->video = $dataVideo;
                $videoData->title = CommonHelpers::titleCase($requestData['title']);
                $videoData->description = $requestData['description'];
                $videoData->user_id = Auth::user()->id;
                $videoData->save();


                $patch = $this->vimeo->request($videoData->video, ['name' => $videoData->title.' - '.$videoData->user->name, 'description' => $videoData->description], 'PATCH');

                $jsonData['ok'] = true;
                $jsonData['msg'] = trans('pages/edit_video.messages.success');
                $jsonData['redirect'] = route('video_view', ['id' => $videoData->id]);

                if($request->has('facebook')) {
                    $access_token = Auth::user()->profile->fb_access_token;

                    $facebookData = array();
                    $facebookData['consumer_key'] = '814279045307343';
                    $facebookData['consumer_secret'] = 'fbb35c7b62f18360314aa4bdd73fc2dc';

                    $facebook = new FacebookPoster($facebookData);
                    $facebook->share($videoData->title, $jsonData['redirect'], asset('assets/images/logo.png'), $videoData->description, $access_token);
                }

                return response()->json($jsonData);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

}