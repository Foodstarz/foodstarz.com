<?php

namespace Foodstarz\Http\Controllers;

use Foodstarz\Models\Jury;
use Foodstarz\Models\JuryVote;
use Foodstarz\Models\RecipePrivate;
use Foodstarz\Http\Controllers\Controller;
use Config;
use Auth;

class JuryController extends Controller {

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->viewData['inner_page'] = false;
        $this->theme = Config::get('custom.theme');
    }

    public function index() {
        $this->viewData['inner_page'] = true;

        $jury = Jury::get();
        $this->viewData["jury"] = $jury;

        return view($this->theme . '.pages.jury', $this->viewData);
    }

    public function vote($id, $v) {
        $recipe = RecipePrivate::getRecipe($id);
        if (!$recipe) {
            abort(404);
        }
        $jury = Jury::getJuryByUser(Auth::user()->id);
        $JuryVote = new JuryVote();
        $JuryVote->jury_id = $jury->id;
        $JuryVote->recipe_id = $recipe->id;
        if ($v == 1) {
            $JuryVote->vote = 1;
        } else {
            $JuryVote->vote = 0;
        }
        $JuryVote->save();
        return redirect(route('recipes'));
    }

}
