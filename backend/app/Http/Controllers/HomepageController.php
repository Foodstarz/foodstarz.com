<?php

namespace Foodstarz\Http\Controllers;

use Foodstarz\Models\Recipe;
use Foodstarz\Models\Partner;
use Foodstarz\Models\User;
use Foodstarz\Http\Controllers\Controller;
use Config;
use Session;
use Auth;
use Foodstarz\Models\FeaturedRecipes;
use Foodstarz\Models\ImageGalleries;
use Jenssegers\Agent\Agent;
use Foodstarz\Models\Videos;
use Vinkla\Vimeo\VimeoManager;

use SimpleXMLElement;

//use Foodstarz\Http\Controllers\VimeoController as VimeoManager;

class HomepageController extends Controller {

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->viewData['inner_page'] = false;
        $this->theme = Config::get('custom.theme');
    }

    public function index(VimeoManager $vimeo) {

        $rss = "http://blog.foodstarz.com/rss";

        $xmlstr = trim(@file_get_contents($rss));
        if($xmlstr===false)die('Error connect to RSS: '.$rss);
        $xml = new SimpleXMLElement($xmlstr);
        if($xml===false)die('Error parse RSS: '.$rss);

        $feed_items = array_slice($xml->xpath('//item'), 0, 3);

        //$recipes = Recipe::where('status', '=', '2')->limit(6)->orderBy('created_at', 'desc')->get();
        $this->viewData["recipes"] = FeaturedRecipes::getLastRecipe(6);
        $this->viewData["gold_partners"] = Partner::getHomepageParnters(1, 3);
        $this->viewData["charity_partners"] = Partner::getHomepageParnters(2, 3);
        $this->viewData["foodstarz"] = User::getHomePageUsers(4, 6);
        $this->viewData['images'] = ImageGalleries::getLastImages(9);
        $this->viewData['videos'] = Videos::getLastVideos(9);
        $this->viewData['feed_items'] = $feed_items;

        $agent = new Agent();

        foreach($this->viewData['videos'] as $video) {
            $picture = $vimeo->request($video->video.'/pictures', [], 'GET');
            $this->viewData['videos_pictures'][$video->id] = empty($picture['body']['data'][0]['sizes'][3]['link']) ? '' : $picture['body']['data'][0]['sizes'][3]['link'];
        }

        //dd(\Auth::user()->background);
        $this->viewData['agent'] = $agent;

        if($agent->isPhone() || $agent->isTablet()) {
            $this->viewData['inner_page'] = true;
        }

        return view($this->theme . '.pages.homepage', $this->viewData);
    }
    
    public function loginBack() {
        $old_user = Session::get("old_user");
        Session::flush();

        Auth::loginUsingId($old_user);
        
        $user = Auth::user();
        $profile = Auth::user()->profile;
        
        if (!empty($user->name) && !empty($profile->gender) && !empty($profile->country) && !empty($profile->city) && !empty($profile->culinary_profession)) {
            Session::put('profile_core_data', true);
        } else {
            Session::put('profile_core_data', false);
        }

        if (!empty($profile->biography) && !empty($profile->work_experience) && !empty($profile->cooking_style)) {
            Session::put('profile_chef_data', true);
        } else {
            Session::put('profile_chef_data', false);
        }
        
        return redirect(route("users_listing"));
    }

}
