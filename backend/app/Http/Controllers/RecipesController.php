<?php

namespace Foodstarz\Http\Controllers;

use Illuminate\Http\Request;
use Foodstarz\Http\Controllers\Controller;
use Config;
use Foodstarz\Models\RecipeCategory;
use Foodstarz\Models\User;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\MessageBag;
use Intervention\Image\Facades\Image;
use Foodstarz\Models\Recipe;
use Foodstarz\Models\RecipePrivate;
use File;
use Validator;
use Mail;
use Session;
use Auth;
use Foodstarz\Models\FeaturedRecipes;
use Foodstarz\Helpers\CommonHelpers;
use Foodstarz\Models\Bookmark;
use App;

class RecipesController extends Controller {

    use DispatchesJobs,
        ValidatesRequests;

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->viewData['inner_page'] = false;
        $this->theme = Config::get('custom.theme');
    }

    public function addRecipe() {
        $this->viewData['inner_page'] = true;
        $this->viewData['recipe_categories'] = RecipeCategory::getCategories();
        $this->viewData['ingredient_units'] = Config::get('custom.ingredient_units');
        $this->viewData['components'] = empty(old('components')) ? null : old('components');
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;

        return view($this->theme . '/pages.add_recipe', $this->viewData);
    }

    public function addRecipeProcess(Request $request) {

        $jsonData = [];
        $jsonData['ok'] = false;
        $jsonData['_token'] = csrf_token();
        
        $recipeData = $request->only(['title', 'category', 'portions', 'preparation_time', 'difficulty', 'components', 'image', 'status']);
        $errorsCheck = false;
        $errors = new MessageBag();

        $validator = Validator::make($recipeData, [
                    'title' => 'required|min:2|max:128',
                    'category' => 'required|array|min:1',
                    'portions' => 'required|min:1',
                    'preparation_time' => 'required|min:1',
                    'difficulty' => 'required|in:easy,medium,hard',
                    'components' => 'required|array|min:1',
                    'image' => 'required',
                    'status' => 'required|in:0,1'
        ]);

        if ($validator->fails()) {
            /*
            $request->flashExcept(['image', 'components']);

            foreach ($recipeData['components'] as $key => $value) {
                $recipeData['components'][$key]['image'] = null;
            }

            Session::flash('components', $recipeData['components']);
            return redirect(route('recipe_add'))->withErrors($validator);
             */
            
            $jsonData['errors'] = $validator->errors();
            return response()->json($jsonData);
        }

        $recipe_slug = str_slug($recipeData['title'], '-');
        $slug_number = 2;
        while (Recipe::countRecipeBySlug($recipe_slug) > 0) {
            $recipe_slug = str_slug($recipeData['title'] . ' ' . $slug_number, '-');
            $slug_number++;
        }

        $image_allowed_mime_types = Config::get('custom.recipe_images_mime_types');
        $image_max_size = Config::get('custom.recipe_images_max_size');

        $compNum = 1;
        foreach ($recipeData['components'] as $k => $component) {
            if (empty(trim($component['name']))) {
                $errors->add('component_name_' . $compNum, trans('pages/add_recipe.messages.errors.components.name'));
                $errorsCheck = true;
            }

            if (empty($component['ingredients'])) {
                $errors->add('component_ingredients_' . $compNum, trans('pages/add_recipe.messages.errors.components.no_ingredients'));
                $errorsCheck = true;
            } else {
                $ingredientsErrors = false;
                for ($i = 0; $i < sizeof($component['ingredients']['name']); $i++) {
                    $component['ingredients']['name'][$i] = trim($component['ingredients']['name'][$i]);
                    $component['ingredients']['quantity'][$i] = trim($component['ingredients']['quantity'][$i]);
                    if (isset($component['ingredients']['unit'][$i])) {
                        $component['ingredients']['unit'][$i] = trim($component['ingredients']['unit'][$i]);
                    } else {
                        $component['ingredients']['unit'][$i] = null;
                    }
                    if (empty($component['ingredients']['name'][$i])/* || empty($component['ingredients']['quantity'][$i]) || empty($component['ingredients']['unit'][$i])*/) {
                        $ingredientsErrors = true;
                    }
                }
                if ($ingredientsErrors) {
                    $errors->add('component_ingredients_' . $compNum, trans('pages/add_recipe.messages.errors.components.ingredients_empty_fields'));
                    $errorsCheck = true;
                }
            }

            if (empty($component['instructions'])) {
                $errors->add('component_instructions_' . $compNum, trans('pages/add_recipe.messages.errors.components.no_instructions'));
                $errorsCheck = true;
            } else {
                $instructionsErrors = false;
                foreach ($component['instructions'] as $instruction) {
                    $instruction = trim($instruction);
                    if (empty($instruction)) {
                        $instructionsErrors = true;
                    }
                }
                if ($instructionsErrors) {
                    $errors->add('component_instructions_' . $compNum, trans('pages/add_recipe.messages.errors.components.instructions_empty_fields'));
                    $errorsCheck = true;
                }
            }

            $recipe_images = [];

            if (!$errorsCheck) {
                $picture = trim($component['image']);
                if (!empty($picture)) {
                    $image = Image::make($picture);
                    $image_mime_type = $image->mime();

                    if (in_array($image_mime_type, $image_allowed_mime_types)) {

                        if (!$errorsCheck) {
                            $width = $image->width();
                            $height = $image->height();

                            if ($width > $height) {
                                $width = $height;
                            } else {
                                $height = $width;
                            }

                            $image->fit($width, $height);

                            switch ($image_mime_type) {
                                case 'image/jpeg':
                                    $ext = 'jpg';
                                    break;

                                case 'image/png':
                                    $ext = 'png';
                                    break;

                                default:
                                    $ext = 'jpg';
                                    break;
                            }


                            if (!file_exists(storage_path('app/recipes/original'))) {
                                mkdir(storage_path('app/recipes/original'), 0777, true);
                            }


                            $image_name = uniqid(time()) . '.' . $ext;

                            $image->save(storage_path('app/recipes/original/' . $image_name), 100);
                            chmod(storage_path('app/recipes/original/' . $image_name), 0777);

                            if ($image->filesize() > $image_max_size) {
                                File::delete(storage_path('app/recipes/original/' . $image_name));
                                $errors->add('component_image_' . $compNum, trans('pages/add_recipe.messages.errors.images.max_size'));
                                $errorsCheck = true;
                                break;
                            } else {
                                $recipe_images[] = $image_name;
                                $recipeData['components'][$k]['image'] = $image_name;
                            }
                        }
                    } else {
                        $errors->add('component_image_' . $compNum, trans('pages/add_recipe.messages.errors.images.mime_type'));
                        $errorsCheck = true;
                        break;
                    }
                }
            }

            $compNum++;
        }

        if (!$errorsCheck) {
            $mainImage = null;
            $picture = trim($recipeData['image']);
            if (!empty($picture)) {
                $image = Image::make($picture);
                $image_mime_type = $image->mime();

                if (in_array($image_mime_type, $image_allowed_mime_types)) {

                    if (!$errorsCheck) {
                        $width = $image->width();
                        $height = $image->height();

                        if ($width > $height) {
                            $width = $height;
                        } else {
                            $height = $width;
                        }

                        $image->fit($width, $height);

                        switch ($image_mime_type) {
                            case 'image/jpeg':
                                $ext = 'jpg';
                                break;

                            case 'image/png':
                                $ext = 'png';
                                break;

                            default:
                                $ext = 'jpg';
                                break;
                        }


                        if (!file_exists(storage_path('app/recipes/original'))) {
                            mkdir(storage_path('app/recipes/original'), 0777, true);
                        }


                        $image_name = uniqid(time()) . '.' . $ext;
                        $mainImage = $image_name;

                        $image->save(storage_path('app/recipes/original/' . $image_name), 100);
                        chmod(storage_path('app/recipes/original/' . $image_name), 0777);

                        if ($image->filesize() > $image_max_size) {
                            File::delete(storage_path('app/recipes/original/' . $image_name));
                            $errors->add('image' . $compNum, trans('pages/add_recipe.messages.errors.images.max_size'));
                            $errorsCheck = true;
                        } else {
                            $recipe_images[] = $image_name;
                        }
                    }
                } else {
                    $errors->add('image' . $compNum, trans('pages/add_recipe.messages.errors.images.mime_type'));
                    $errorsCheck = true;
                }
            } else {
                $errors->add('image' . $compNum, trans('pages/add_recipe.messages.errors.images.no_picture'));
                $errorsCheck = true;
            }
        }

        if (!$errorsCheck) {

            $new_recipe_data = [
                'slug' => $recipe_slug,
                'user_id' => session('user')->id,
                'title' => CommonHelpers::titleCase($recipeData['title']),
                'category' => json_encode($recipeData['category']),
                'portions' => $recipeData['portions'],
                'preparation_time' => $recipeData['preparation_time'],
                'difficulty' => $recipeData['difficulty'],
                'components' => json_encode($recipeData['components']),
                'main_image_original' => $mainImage,
                'main_image_watermarked' => null,
                'status' => $recipeData['status']
            ];

            $recipe = Recipe::create($new_recipe_data);
            $new_recipe_data['id'] = $recipe->id;
            $recipePrivate = RecipePrivate::create($new_recipe_data);

            $users = User::getUserByRole('<', 3);
            if($recipeData['status'] != 0) {
                foreach ($users AS $user) {
                    Mail::send($this->theme . '.emails.'.App::getLocale().'.new_recipe', ['data' => $new_recipe_data, 'user' => Auth::user()], function ($message) use ($user) {
                        $message->from(Config::get('custom.default_email'));
                        $message->to($user->email);
                        $message->subject(trans('emails/recipes.new_recipe'));
                    });
                }
            }
            
            Mail::send($this->theme . '.emails.'.App::getLocale().'.recipe_confirmation', ['data' => $new_recipe_data, 'user' => Auth::user()], function ($message) {
                $message->from(Config::get('custom.default_email'));
                $message->to(Auth::user()->email);
                $message->subject(trans('emails/recipe_confirmation.subject'));
            });

            //return redirect(route('recipe_add'))->with('success', trans('pages/add_recipe.messages.success'));
            $jsonData['ok'] = true;
            $jsonData['msg'] = trans('pages/add_recipe.messages.success');
            return response()->json($jsonData);
        } else {
            if (!empty($recipe_images)) {
                foreach ($recipe_images as $im) {
                    File::delete(storage_path('app/recipes/original/' . $im));
                }
            }

            /*
            $request->flashExcept(['image', 'components']);

            foreach ($recipeData['components'] as $key => $value) {
                $recipeData['components'][$key]['image'] = null;
            }

            Session::flash('components', $recipeData['components']);
            
            return redirect(route('recipe_add'))->withErrors($errors);
            */
            
            $jsonData['errors'] = $errors;
            return response()->json($jsonData);

            
        }
    }

    public function listCategories() {
        $this->viewData['inner_page'] = true;

        $this->viewData['categories'] = RecipeCategory::getCategories('title');

        return view($this->theme . '.pages.recipe_categories', $this->viewData);
    }

    public function viewRecipe($slug, Request $request) {

        $this->viewData['inner_page'] = true;
        $this->viewData['social_url'] = route('recipe_view', ['slug' => $slug]);
        $recipe_private = RecipePrivate::getRecipeBySlug($slug);

        $this->viewData['social_description'] = $recipe_private->title;
        $this->viewData['social_image'] = empty($recipe_private->main_image_watermarked) ? asset('storage/recipes/original/'.$recipe_private->main_image_original) : asset('storage/recipes/watermark/'.$recipe_private->main_image_watermarked);

        $this->viewData['bookmarked'] = (Bookmark::getBookmark(1, $recipe_private->id)) ? true : false;

        if($recipe_private) {
            if(Auth::check() && $recipe_private->user_id == Auth::user()->id) {
                $recipe_private->category = json_decode($recipe_private->category, true);
                $recipe_private->components = json_decode($recipe_private->components, true);

                $this->viewData['recipe'] = $recipe_private;
                $this->viewData['recipe_author'] = $recipe_private->user;
                $this->viewData['components'] = $recipe_private->components;

                return view($this->theme . '/pages.view_recipe', $this->viewData);
            } else {
                if (Auth::guest() || Auth::user()->role > 2) {
                    $recipe = Recipe::getRecipeBySlug($slug);
                    if ($recipe) {
                        if ($recipe->status == 2) {
                            $recipe->category = json_decode($recipe->category, true);
                            $recipe->components = json_decode($recipe->components, true);

                            $this->viewData['recipe'] = $recipe;
                            $this->viewData['recipe_author'] = $recipe->user;
                            if(!is_array($recipe->components)) {
                                $this->viewData['components'] = json_decode($recipe->components, true);
                            }

                            return view($this->theme . '/pages.view_recipe', $this->viewData);
                        } else {
                            //The recipe is not yet approved
                            abort(404);
                        }
                    } else {
                        //No such recipe
                        abort(404);
                    }
                } else {
                    $recipe = $recipe_private;
                    if ($recipe) {
                        $recipe->category = json_decode($recipe->category, true);
                        $recipe->components = json_decode($recipe->components, true);

                        $this->viewData['recipe'] = $recipe;
                        $this->viewData['recipe_author'] = $recipe->user;

                        return view($this->theme . '/pages.view_recipe_private', $this->viewData);
                    } else {
                        abort(404);
                    }
                }
            }
        } else {
            abort(404);
        }

    }

    public function recipes(Request $request) {
        $this->viewData['inner_page'] = true;

        //$recipes = Recipe::where('status', 2)->orderBy('created_at', 'desc')->paginate(9);

        $this->viewData["recipes"] = FeaturedRecipes::getRecipes();

        return view($this->theme . '.pages.recipes', $this->viewData);
    }

    public function categoryRecipes($id) {
        $this->viewData['inner_page'] = true;

        $id = (int) $id;
        $category = RecipeCategory::getCategory($id);
        $this->viewData["category"] = $category;
        if (!$category) {
            abort(404);
        }

        $recipes = Recipe::getRecipesByCat($id);
        $this->viewData["recipes"] = $recipes;

        return view($this->theme . '.pages.category_recipes', $this->viewData);
    }

    public function editRecipe($id, Request $request) {

        if (Auth::user()->role != 1) {
            $recipe =  RecipePrivate::getRecipe($id, Auth::user()->id);
        } else {
            $recipe =  RecipePrivate::getRecipe($id);
        }

        if ($recipe) {
            $this->viewData['inner_page'] = true;
            $this->viewData['recipe_categories'] = RecipeCategory::getCategories();
            $this->viewData['ingredient_units'] = Config::get('custom.ingredient_units');

            $recipe->category = json_decode($recipe->category, true);
            $recipe->components = json_decode($recipe->components, true);

            $this->viewData['recipe'] = [];
            $this->viewData['recipe']['id'] = $id;
            $this->viewData['recipe']['title'] = empty(old('title')) ? $recipe->title : old('title');
            $this->viewData['recipe']['category'] = empty(old('category')) ? $recipe->category : old('category');
            $this->viewData['recipe']['portions'] = empty(old('portions')) ? $recipe->portions : old('portions');
            $this->viewData['recipe']['preparation_time'] = empty(old('preparation_time')) ? $recipe->preparation_time : old('preparation_time');
            $this->viewData['recipe']['difficulty'] = empty(old('difficulty')) ? $recipe->difficulty : old('difficulty');
            $this->viewData['recipe']['main_image_original'] = $recipe->main_image_original;
            if (session('components')) {
                $this->viewData['recipe']['components'] = session('components');
            } else {
                $this->viewData['recipe']['components'] = empty(old('components')) ? $recipe->components : old('components');
            }

            $this->viewData['recipe']['status'] = empty(old('status')) ? $recipe->status : old('status');

            return view($this->theme . '/pages.edit_recipe', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function editRecipeProcess($id, Request $request) {
        $jsonData = [];
        $jsonData['ok'] = false;
        $jsonData['_token'] = csrf_token();

        if (Auth::user()->role > 2) {
            $recipe = RecipePrivate::getRecipe($id, Auth::user()->id);
        } else {
            $recipe = RecipePrivate::getRecipe($id);
        }

        if (!$recipe) {
            abort(404);
        }

        $recipeData = $request->only(['title', 'category', 'portions', 'preparation_time', 'difficulty', 'components', 'image', 'status']);
        $errorsCheck = false;
        $errors = new MessageBag();

        $validator = Validator::make($recipeData, [
                    'title' => 'required|min:2|max:128',
                    'category' => 'required|array|min:1',
                    'portions' => 'required|min:1',
                    'preparation_time' => 'required|min:1',
                    'difficulty' => 'required|in:easy,medium,hard',
                    'components' => 'required|array|min:1',
                    'image' => 'sometimes',
                    'status' => 'required|in:0,1'
        ]);

        if ($validator->fails()) {
            /*
            $request->flashExcept(['image', 'components']);

            foreach ($recipeData['components'] as $key => $value) {
                $recipeData['components'][$key]['image'] = null;
            }

            Session::flash('components', $recipeData['components']);
            
            return redirect(route('recipe_edit', ['id' => $id]))->withErrors($validator);
            */
            
            $jsonData['errors'] = $validator->errors();
            return response()->json($jsonData);
        }

        $recipe_slug = str_slug($recipeData['title'], '-');
        $slug_number = 2;
        while (Recipe::countRecipeBySlug($recipe_slug) > 0) {
            $recipe_slug = str_slug($recipeData['title'] . ' ' . $slug_number, '-');
            $slug_number++;
        }

        $image_allowed_mime_types = Config::get('custom.recipe_images_mime_types');
        $image_max_size = Config::get('custom.recipe_images_max_size');

        $compNum = 1;
        foreach ($recipeData['components'] as $k => $component) {
            if (empty(trim($component['name']))) {
                $errors->add('component_name_' . $compNum, trans('pages/add_recipe.messages.errors.components.name'));
                $errorsCheck = true;
            }

            if (empty($component['ingredients'])) {
                $errors->add('component_ingredients_' . $compNum, trans('pages/add_recipe.messages.errors.components.no_ingredients'));
                $errorsCheck = true;
            } else {
                $ingredientsErrors = false;
                for ($i = 0; $i < sizeof($component['ingredients']['name']); $i++) {
                    $component['ingredients']['name'][$i] = trim($component['ingredients']['name'][$i]);
                    $component['ingredients']['quantity'][$i] = trim($component['ingredients']['quantity'][$i]);
                    if (isset($component['ingredients']['unit'][$i])) {
                        $component['ingredients']['unit'][$i] = trim($component['ingredients']['unit'][$i]);
                    } else {
                        $component['ingredients']['unit'][$i] = null;
                    }
                    if (empty($component['ingredients']['name'][$i]) /*|| empty($component['ingredients']['quantity'][$i]) || empty($component['ingredients']['unit'][$i])*/) {
                        $ingredientsErrors = true;
                    }
                }
                if ($ingredientsErrors) {
                    $errors->add('component_ingredients_' . $compNum, trans('pages/add_recipe.messages.errors.components.ingredients_empty_fields'));
                    $errorsCheck = true;
                }
            }

            if (empty($component['instructions'])) {
                $errors->add('component_instructions_' . $compNum, trans('pages/add_recipe.messages.errors.components.no_instructions'));
                $errorsCheck = true;
            } else {
                $instructionsErrors = false;
                foreach ($component['instructions'] as $instruction) {
                    $instruction = trim($instruction);
                    if (empty($instruction)) {
                        $instructionsErrors = true;
                    }
                }
                if ($instructionsErrors) {
                    $errors->add('component_instructions_' . $compNum, trans('pages/add_recipe.messages.errors.components.instructions_empty_fields'));
                    $errorsCheck = true;
                }
            }

            $recipe_images = [];

            if (!$errorsCheck) {
                $picture = trim($component['image']);
                if (!empty($picture)) {
                    $image = Image::make($picture);
                    $image_mime_type = $image->mime();

                    if (in_array($image_mime_type, $image_allowed_mime_types)) {

                        if (!$errorsCheck) {
                            $width = $image->width();
                            $height = $image->height();

                            if ($width > $height) {
                                $width = $height;
                            } else {
                                $height = $width;
                            }

                            $image->fit($width, $height);

                            switch ($image_mime_type) {
                                case 'image/jpeg':
                                    $ext = 'jpg';
                                    break;

                                case 'image/png':
                                    $ext = 'png';
                                    break;

                                default:
                                    $ext = 'jpg';
                                    break;
                            }


                            if (!file_exists(storage_path('app/recipes/original'))) {
                                mkdir(storage_path('app/recipes/original'), 0777, true);
                            }


                            $image_name = uniqid(time()) . '.' . $ext;

                            $image->save(storage_path('app/recipes/original/' . $image_name), 100);
                            chmod(storage_path('app/recipes/original/' . $image_name), 0777);

                            if ($image->filesize() > $image_max_size) {
                                File::delete(storage_path('app/recipes/original/' . $image_name));
                                $errors->add('component_image_' . $compNum, trans('pages/add_recipe.messages.errors.images.max_size'));
                                $errorsCheck = true;
                                break;
                            } else {
                                $recipe_images[] = $image_name;
                                $recipeData['components'][$k]['image'] = $image_name;
                            }
                        }
                    } else {
                        $errors->add('component_image_' . $compNum, trans('pages/add_recipe.messages.errors.images.mime_type'));
                        $errorsCheck = true;
                        break;
                    }
                }
            }

            $compNum++;
        }

        if (!$errorsCheck) {
            $mainImage = null;
            $picture = trim($recipeData['image']);
            if (!empty($picture)) {
                $image = Image::make($picture);
                $image_mime_type = $image->mime();

                if (in_array($image_mime_type, $image_allowed_mime_types)) {

                    if (!$errorsCheck) {
                        $width = $image->width();
                        $height = $image->height();

                        if ($width > $height) {
                            $width = $height;
                        } else {
                            $height = $width;
                        }

                        $image->fit($width, $height);

                        switch ($image_mime_type) {
                            case 'image/jpeg':
                                $ext = 'jpg';
                                break;

                            case 'image/png':
                                $ext = 'png';
                                break;

                            default:
                                $ext = 'jpg';
                                break;
                        }


                        if (!file_exists(storage_path('app/recipes/original'))) {
                            mkdir(storage_path('app/recipes/original'), 0777, true);
                        }


                        $image_name = uniqid(time()) . '.' . $ext;
                        $mainImage = $image_name;

                        $image->save(storage_path('app/recipes/original/' . $image_name), 100);
                        chmod(storage_path('app/recipes/original/' . $image_name), 0777);

                        if ($image->filesize() > $image_max_size) {
                            File::delete(storage_path('app/recipes/original/' . $image_name));
                            $errors->add('image' . $compNum, trans('pages/add_recipe.messages.errors.images.max_size'));
                            $errorsCheck = true;
                        } else {
                            $recipe_images[] = $image_name;
                        }
                    }
                } else {
                    $errors->add('image' . $compNum, trans('pages/add_recipe.messages.errors.images.mime_type'));
                    $errorsCheck = true;
                }
            }
        }

        if (!$errorsCheck) {

            $current_rev = RecipePrivate::getRecipe($id);
            $current_rev->components = json_decode($current_rev->components, true);

            foreach ($recipeData['components'] as $key => $value) {
                if (empty($recipeData['components'][$key]['image'])) {
                    foreach ($current_rev->components as $k => $v) {
                        if ($v['name'] == $recipeData['components'][$key]['name'] || $k == $key) {
                            $recipeData['components'][$key]['image'] = $v['image'];
                        }
                    }
                }
            }

            $update_data = [
                'slug' => $recipe_slug,
                'title' => CommonHelpers::titleCase($recipeData['title']),
                'category' => json_encode($recipeData['category']),
                'portions' => $recipeData['portions'],
                'preparation_time' => $recipeData['preparation_time'],
                'difficulty' => $recipeData['difficulty'],
                'components' => json_encode($recipeData['components']),
                'status' => $recipeData['status']
            ];

            if (!empty($mainImage)) {
                $update_data['main_image_original'] = $mainImage;
            }

            RecipePrivate::updateRecipe($id, $update_data);

            //return redirect(route('recipe_edit', ['id' => $id]))->with('success', trans('pages/edit_recipe.messages.success'));
            $jsonData['ok'] = true;
            $jsonData['msg'] = trans('pages/edit_recipe.messages.success');
            return response()->json($jsonData);
        } else {
            if (!empty($recipe_images)) {
                foreach ($recipe_images as $im) {
                    File::delete(storage_path('app/recipes/original/' . $im));
                }
            }

            /*
            $request->flashExcept(['image', 'components']);

            foreach ($recipeData['components'] as $key => $value) {
                $recipeData['components'][$key]['image'] = null;
            }

            Session::flash('components', $recipeData['components']);
            
            return redirect(route('recipe_edit', ['id' => $id]))->withErrors($errors);
            */
            
            $jsonData['errors'] = $errors;
            return response()->json($jsonData);
        }
    }

    public function getRecipes(Request $request) {
        $result = [];
        $result['status'] = true;
        if ($request->get("key") == NULL) {
            $result['status'] = false;
            $result['message'] = trans('pages/main.error_occurred');
            return json_encode($result);
        }
        switch ($request->get("key")) {
            case "approved":
                $q = RecipePrivate::getRecipesByUser(Auth::user()->id, 2);
                break;
            case "in-progress":
                $q = RecipePrivate::getRecipesByUser(Auth::user()->id, 0);
                break;
            case "waiting":
                $q = RecipePrivate::getRecipesByUser(Auth::user()->id, 1);
                break;
        }
        $result['result'] = view($this->theme . '.ajax.get_recipes')->with('recipes', $q)->render();
        return response()->json($result);
    }

    public function delete($id) {
        $r = Recipe::find((int) $id);
        $r->delete();

        $rp = RecipePrivate::find((int) $id);
        $rp->delete();

        return redirect(route('recipes_listing'));
    }
    
    public function deleteRecipeComponentPicture($id, $component) {
        $recipe = RecipePrivate::getRecipe($id);
        
        if($recipe) {
            if($recipe->user_id == Auth::user()->id || Auth::user()->role == 1) {
                $components = json_decode($recipe->components, true);
                $components[$component]['image'] = '';
                $components[$component]['image_watermarked'] = '';
                $recipe->components = json_encode($components);
                $recipe->status = 1;
                $recipe->save();

                return response()->json(['ok' => true]);
            } else {
                return response()->json(['ok' => false]);
            }
        } else {
            return response()->json(['ok' => false]);
        }
    }

}
