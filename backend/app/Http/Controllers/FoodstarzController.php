<?php

namespace Foodstarz\Http\Controllers;

use Foodstarz\Models\User;
use Foodstarz\Http\Controllers\Controller;
use Config;

class FoodstarzController extends Controller {

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->viewData['inner_page'] = true;
        $this->theme = Config::get('custom.theme');
    }
    
    public function listing() {

        $this->viewData["foodstarz"] = User::getUsers([1,2,4], 'name');

        return view($this->theme . '.pages.foodstarz', $this->viewData);
    }

}
