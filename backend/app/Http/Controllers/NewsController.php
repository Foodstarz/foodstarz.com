<?php

namespace Foodstarz\Http\Controllers;

use Illuminate\Http\Request;

use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;

use Config;
use Foodstarz\Models\News;

class NewsController extends Controller
{
    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->viewData['inner_page'] = true;
        $this->theme = Config::get('custom.theme');
    }
    
    public function news() {

        $this->viewData['news'] = News::getNews();
        
        return view($this->theme . '.pages.news_listing', $this->viewData);
    }
    
    public function viewNews($slug) {
        $news = News::getNewsBySlug($slug);
        
        if($news) {
            $this->viewData["news"] = $news;
            $this->viewData['social_url'] = route('view_news', $news->slug);

            return view($this->theme . '.pages.view_news', $this->viewData);
        } else {
            abort(404);
        }
    }
}
