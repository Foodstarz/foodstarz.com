<?php

namespace Foodstarz\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Config;
use Foodstarz\Models\News;

class NewsController extends Controller
{
    use DispatchesJobs,
        ValidatesRequests;
    
    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }
    
    public function listing() {
        $news = News::getNews();
        $this->viewData['news'] = $news;
        
        return view($this->theme . '.pages.news_listing', $this->viewData);
    }
    
    public function add() {
        return view($this->theme . '.pages.add_news', $this->viewData);
    }
    
    public function addProcess(Request $request) {
        $this->validate($request, [
            'title' => 'required|min:3',
            'content' => 'required|min:3',
        ]);
        
        $news_slug = str_slug($request->get('title'), '-');
        $slug_number = 2;
        while (News::countNewsBySlug($news_slug) > 0) {
            $news_slug = str_slug($request->get('title') . ' ' . $slug_number, '-');
            $slug_number++;
        }
        
        $news = new News();
        $news->title = $request->get('title');
        $news->slug = $news_slug;
        $news->content = $request->get('content');
        $news->save();
        
        return redirect(route('add_news'))->with('success', trans('admin/pages/add_news.messages.success'))->with('slug', $news_slug);
    }
    
    public function edit($id) {
        $new = News::getNewsById($id);
        
        if($new) {
            $item = [];
            $item['id'] = $id;
            $item['slug'] = $new->slug;
            $item['title'] = empty(old('title')) ? $new->title : old('title');
            $item['content'] = empty(old('content')) ? $new->content : old('content');
            
            $this->viewData['item'] = $item;
            
            return view($this->theme . '.pages.edit_news', $this->viewData);
        } else {
            abort(404);
        }
    }
    
    public function editProcess($id, Request $request) {
        $new = News::getNewsById($id);
        
        if($new) {
            $this->validate($request, [
                'title' => 'required|min:3',
                'content' => 'required|min:3',
            ]);
            
            $news_slug = str_slug($request->get('title'), '-');
            $slug_number = 2;
            while (News::countNewsBySlug($news_slug) > 0) {
                $news_slug = str_slug($request->get('title') . ' ' . $slug_number, '-');
                $slug_number++;
            }
            
            $new->title = $request->get('title');
            $new->slug = $news_slug;
            $new->content = $request->get('content');
            
            $new->save();
            
            return redirect(route('edit_news', $id))->with('success', trans('admin/pages/edit_news.messages.success'));
        } else {
            abort(404);
        }
        
    }
    
    public function deleteProcess($id) {
        $new = News::getNewsById($id);
        
        if($new) {
            $new->delete();
            return redirect(route('news_listing'))->with('success', trans('admin/pages/news_listing.messages.delete'));
        } else {
            return redirect(route('news_listing'));
        }
    }
    
}
