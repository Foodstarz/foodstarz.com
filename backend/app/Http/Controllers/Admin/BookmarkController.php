<?php

namespace Foodstarz\Http\Controllers\Admin;

use Foodstarz\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;
use Foodstarz\Models\Bookmark;
use Foodstarz\Models\Recipe as Recipe;
use Foodstarz\Models\Image as Image;
use Foodstarz\Models\Video as Video;

class BookmarkController extends Controller {
	private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
        $this->types = [
    		'1' => 'Foodstarz\Models\Recipe',
    		'2' => 'Foodstarz\Models\ImageGalleries',
    		'3' => 'Foodstarz\Models\Video'
    	];
    }

    public function bookmark($type, $id) {
    	$types = $this->types;

    	if(array_key_exists($type, $types)) {
    		$resource = $types[$type]::where('id', $id)->first();

    		if($resource) {
    			$bookmark = new Bookmark();
    			$bookmark->resource_type = $type;
    			$bookmark->resource_id = $id;
                $bookmark->save();

    			return redirect()->back();
    		} else {
    			abort(404);
    		}
    	} else {
    		abort(404);
    	}
    }

    public function remove($type, $id) {
    	$types = $this->types;

    	if(array_key_exists($type, $types)) {
    		$bookmark = Bookmark::getBookmark($type, $id);
    		if($bookmark) {
    			$bookmark->delete();

    			return redirect()->back();
    		} else {
    			abort(404);
    		}
    	} else {
    		abort(404);
    	}
    }

    public function view_bookmarks($type) {
        $types = $this->types;

        if(array_key_exists($type, $types)) {
            $bookmarks = Bookmark::getBookmarksByType($type);

            $this->viewData['type'] = last(explode('\\', $types[$type]));
            $this->viewData['paginator'] = $bookmarks;
            $this->viewData['bookmarks'] = [];

            foreach ($bookmarks as $bookmark) {
                $resource = $types[$type]::where('id', $bookmark->resource_id)->first();

                if($resource) {
                    $this->viewData['bookmarks'][] = [
                        'data' => $resource,
                        'bookmark' => $bookmark
                    ];
                }
            }

            return view($this->theme . '.pages.bookmarks_listing', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function share($id) {
        $bookmark = Bookmark::getBookmarkById($id);
        if($bookmark) {
            $bookmark->shared = $bookmark->shared ? false : true;
            $bookmark->save();

            return redirect()->back();
        } else {
            abort(404);
        }
    }
}