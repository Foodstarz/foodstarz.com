<?php

namespace Foodstarz\Http\Controllers\Admin;

use Cache;
use Config;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Foodstarz\Models\Features;
use Foodstarz\Models\UserGroup;
use Foodstarz\Http\Controllers\Controller;

class FeaturesController extends Controller
{
    private $viewData = array();
    private $theme;
    private $return;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function index() {

        $this->viewData['features'] = Features::getFeatures();

        $this->viewData['user_groups'] = UserGroup::getGroups();

        return view($this->theme . '.pages.features.index', $this->viewData);

    }

    public function switchData(Request $request) {
        
        $feature = Features::find($request['id']);
        $feature->$request['data'] = $request[$request['data']];

        $this->return = [];
        
        if($feature->save()) {
            $this->return['status'] = 1;
            $this->return['title'] = trans('admin/pages/features.success.title');
            $this->return['message'] = trans('admin/pages/features.success.message');
            $this->return['data'] = $feature->$request['data'];
        } else {
            $this->return['status'] = 0;
            $this->return['title'] = trans('admin/pages/features.danger.title');
            $this->return['message'] = trans('admin/pages/features.danger.message');
        }
        
        return response()->json($this->return);
        
    }

}