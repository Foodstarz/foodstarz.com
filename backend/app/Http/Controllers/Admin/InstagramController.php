<?php

namespace Foodstarz\Http\Controllers\Admin;

use Config;
use Illuminate\Http\Request;
use Vinkla\Instagram\Facades\Instagram;
use Vinkla\Instagram\InstagramManager;
use Foodstarz\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class InstagramController extends Controller {

    private $instagramManager;

    public function __construct(InstagramManager $instagramManager)
    {
        $this->instagramManager = $instagramManager;
    }

    public function index()
    {
        $name = 'borough_cc';
        $result = $this->instagramManager->getTagMedia($name, 3);

        dd($result);
    }

}

