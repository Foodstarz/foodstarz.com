<?php

namespace Foodstarz\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;

use Config;
use Foodstarz\Models\User;
use Hash;
use Mail;

class EmailController extends Controller
{
    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function index()
    {
        return view($this->theme . '.pages.email_index', $this->viewData);
    }
    
    public function process() {
//        $users = ['daniel@danielfletchersweet.co.uk', 'd.febrianto18@yahoo.com', 'crivello.domenico@gmail.com', 'bernuycocinero@gmail.com', 'vkstromstad@gmail.com', 'ryanskywatkins@gmail.com', 'thetransformer-@hotmail.com', 'Whitemike7789@gmail.com', 'zmills417@gmail.com'];
//        //$users = User::where('id', '>', '254')->get();
//
//        foreach ($users as $user) {
//            $user = User::getUserByEmail($user);
//
//            if($user->email == Config::get('custom.default_email')) {
//                continue;
//            }
//
//            $password_plain = str_random(6);
//            $user->password = Hash::make($password_plain);
//            $user->save();
//
//            Mail::send($this->theme.'/emails.invite', ['name' => $user->name, 'email' => $user->email, 'password' => $password_plain], function($message) use ($user) {
//                $message->from(Config::get('custom.default_email'), 'David');
//                $message->to($user->email, $user->name);
//                $message->subject('Welcome to the NEW Foodstarz!');
//            });
//        }
        
        return redirect(route('mass_email'))->with('success', 'You successfully Mass Emailed all the users! Now please leave this page!');
    }
}
