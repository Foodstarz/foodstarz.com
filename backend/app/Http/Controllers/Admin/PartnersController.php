<?php

namespace Foodstarz\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Foodstarz\Models\Partner;
use Foodstarz\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Config;
use Image;

class PartnersController extends Controller {

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function listing() {
        $partners = Partner::getPartners();
        $this->viewData['partners'] = $partners;
        return view($this->theme . '.pages.partners_listing', $this->viewData);
    }

    public function edit($id) {
        $id = (int) $id;
        $c_text = Partner::getPartner($id);
        $this->viewData['partner'] = $c_text;

        if ($c_text) {
            return view($this->theme . '.pages.partners_edit', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function add() {
        return view($this->theme . '.pages.partners_add', $this->viewData);
    }

    public function editProcess($id, Request $r) {
        $id = (int) $id;

        $update = [
            'name' => $r->get("name"),
            'url' => $r->get("url"),
            'width' => $r->get("width"),
            'height' => $r->get("height"),
            'type' => $r->get('type')
        ];
        
        $picture = $r->file("image");
        if (!empty($picture)) {
            $image = Image::make($picture);
            $image_mime_type = $image->mime();
            switch ($image_mime_type) {
                case 'image/jpeg':
                    $ext = 'jpg';
                    break;

                case 'image/png':
                    $ext = 'png';
                    break;

                default:
                    $ext = 'jpg';
                    break;
            }


            if (!file_exists(storage_path('app/partners/'))) {
                mkdir(storage_path('app/partners/'), 0777, true);
            }


            $image_name = uniqid(time()) . '.' . $ext;

            $image->save(storage_path('app/partners/' . $image_name), 100);
            chmod(storage_path('app/partners/' . $image_name), 0777);
            $update['image'] = $image_name;
        }

        Partner::updatePartner($id, $update);
        return redirect(route("partners_listing"))->with('success', trans('admin/pages/partners.edit_success'));
    }

    public function delete($id) {
        $id = (int) $id;
        $c = Partner::delPartner($id);

        return redirect(route("partners_listing"))->with('success', trans('admin/pages/partners.delete_success'));
    }

    public function addProcess(Request $r) {
        $picture = $r->file("image");
        if (!empty($picture)) {
            $image = Image::make($picture);
            $image_mime_type = $image->mime();
            switch ($image_mime_type) {
                case 'image/jpeg':
                    $ext = 'jpg';
                    break;

                case 'image/png':
                    $ext = 'png';
                    break;

                default:
                    $ext = 'jpg';
                    break;
            }


            if (!file_exists(storage_path('app/partners/'))) {
                mkdir(storage_path('app/partners/'), 0777, true);
            }


            $image_name = uniqid(time()) . '.' . $ext;

            $image->save(storage_path('app/partners/' . $image_name), 100);
            chmod(storage_path('app/partners/' . $image_name), 0777);
        }
        $cp = new Partner();
        $cp->name = $r->get("name");
        $cp->url = $r->get("url");
        $cp->image = $image_name;
        $cp->width = $r->get("width");
        $cp->height = $r->get("height");

        $cp->save();
        return redirect(route("partners_listing"))->with('success', trans('admin/pages/partners.new_success'));
    }

}
