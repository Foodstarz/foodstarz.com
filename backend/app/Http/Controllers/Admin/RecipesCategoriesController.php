<?php

namespace Foodstarz\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Config;
use Foodstarz\Models\RecipeCategory;

class RecipesCategoriesController extends Controller
{

    use DispatchesJobs,
        ValidatesRequests;

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function listing() {
        $this->viewData['categories'] = RecipeCategory::getCategories('id', 'ASC', 20);

        return view($this->theme . '.pages.recipes_categories_listing', $this->viewData);
    }

    public function add() {
        return view($this->theme . '.pages.add_recipes_categories', $this->viewData);
    }

    public function addProcess(Request $request) {
        $this->validate($request, [
            'title' => 'required|array',
            'image' => 'required'
        ]);

        $category = new RecipeCategory();
        $category->title = json_encode($request->get('title'));
        $category->image = $request->get('image');
        $category->save();

        return redirect(route('recipes_categories_add'))->with('success', trans('admin/pages/add_recipes_categories.messages.success'));
    }

    public function edit($id) {
        $category = RecipeCategory::getCategory($id);

        if($category) {
            $item = [];
            $item['id'] = $id;
            $item['title'] = empty(old('title')) ? json_decode($category->title, true) : old('title');
            $item['image'] = empty(old('image')) ? $category->image : old('image');

            $this->viewData['item'] = $item;

            return view($this->theme . '.pages.edit_recipes_categories', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function editProcess($id, Request $request) {
        $category = RecipeCategory::getCategory($id);

        if($category) {
            $this->validate($request, [
                'title' => 'required|array',
                'image' => 'required'
            ]);

            $category->title = json_encode($request->get('title'));
            $category->image = $request->get('image');
            $category->save();

            return redirect(route('recipes_categories_edit', $id))->with('success', trans('admin/pages/edit_recipes_categories.messages.success'));
        } else {
            abort(404);
        }
    }

    public function delete($id) {
        $category = RecipeCategory::getCategory($id);

        if($category) {
            $category->delete();
            return redirect(route('recipes_categories_listing'))->with('success', trans('admin/pages/recipes_categories_listing.messages.delete'));
        } else {
            return redirect(route('recipes_categories_listing'));
        }
    }

}