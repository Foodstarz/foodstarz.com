<?php

namespace Foodstarz\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Config;
use Foodstarz\Models\Interview;

class InterviewController extends Controller
{

    use DispatchesJobs,
        ValidatesRequests;

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function listing() {
        $this->viewData['questions'] = Interview::getInterviews();

        return view($this->theme . '.pages.questions_listing', $this->viewData);
    }

    public function add() {
        return view($this->theme . '.pages.add_questions', $this->viewData);
    }

    public function addProcess(Request $request) {
        $this->validate($request, [
            'question' => 'required|array',
            'answer_type' => 'required|in:input,textarea',
            'required' => 'required|in:0,1',
        ]);

        $interview = new Interview();
        $interview->question = json_encode($request->get('question'));
        $interview->answer_type = $request->get('answer_type');
        $interview->required = $request->get('required');
        $interview->save();

        return redirect(route('interview_questions_add'))->with('success', trans('admin/pages/add_questions.messages.success'));
    }

    public function edit($id, Request $request) {
        $question = Interview::getInterview($id);

        if($question) {
            $item = [];
            $item['id'] = $id;
            $item['question'] = empty(old('question')) ? json_decode($question->question, true) : old('question');
            $item['answer_type'] = empty(old('answer_type')) ? $question->title : old('answer_type');
            $item['required'] = empty(old('required')) ? $question->required : old('required');

            $this->viewData['item'] = $item;

            return view($this->theme . '.pages.edit_questions', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function editProcess($id, Request $request) {
        $interview = Interview::getInterview($id);

        if($interview) {
            $this->validate($request, [
                'question' => 'required|array',
                'answer_type' => 'required|in:input,textarea',
                'required' => 'required|in:0,1',
            ]);

            $interview->question = json_encode($request->get('question'));
            $interview->answer_type = $request->get('answer_type');
            $interview->required = $request->get('required');
            $interview->save();

            return redirect(route('interview_questions_edit', $id))->with('success', trans('admin/pages/edit_questions.messages.success'));
        } else {
            abort(404);
        }
    }

    public function delete($id) {
        $interview = Interview::getInterview($id);

        if($interview) {
            $interview->delete();
            return redirect(route('interview_questions_listing'))->with('success', trans('admin/pages/questions_listing.messages.delete'));
        } else {
            return redirect(route('interview_questions_listing'));
        }
    }

}