<?php

namespace Foodstarz\Http\Controllers\Admin;

use Cache;
use Config;
use Carbon\Carbon;
use Foodstarz\Models\User;
use Foodstarz\Models\Videos;
use Foodstarz\Models\Recipe;
use Illuminate\Http\Request;
use LaravelAnalytics;
use Foodstarz\Models\Profile;
use Foodstarz\Models\RecipePrivate;
use Foodstarz\Models\ImageGalleries;
use Foodstarz\Http\Controllers\Controller;

class OverviewController extends Controller
{
    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function index() {

        $this->generateStatistic();

        $this->viewData['most_viewed_pages'] = LaravelAnalytics::getMostVisitedPages(365, 50);

        return view($this->theme . '.pages.overview.index', $this->viewData);

    }

    public function users() {

        $this->viewData['users'] = User::getStatistics();

        return view($this->theme . '.pages.overview.users', $this->viewData);

    }

    public function countries() {

        if(Cache::has('stat_countries')) {

            $this->viewData['countries'] = Cache::get('stat_countries');

        } else {

            $countries = Config::get('custom.countryList');

            foreach ($countries as $key => $value) {

                $users = User::getUsersByCountry($key);

                $this->viewData['countries'][$key]['name'] = $value;
                $this->viewData['countries'][$key]['code'] = $key;
                $this->viewData['countries'][$key]['videos'] = 0;
                $this->viewData['countries'][$key]['images'] = 0;
                $this->viewData['countries'][$key]['recipes'] = 0;

                foreach ($users as $user) {

                    $this->viewData['countries'][$key]['videos'] = $this->viewData['countries'][$key]['videos'] + count($user->videos);
                    $this->viewData['countries'][$key]['images'] = $this->viewData['countries'][$key]['images'] + count($user->images);
                    $this->viewData['countries'][$key]['recipes'] = $this->viewData['countries'][$key]['recipes'] + count($user->recipes);

                }
            }

            Cache::put('stat_countries', $this->viewData['countries'], Carbon::now()->addMinutes(10));

        }

        return view($this->theme . '.pages.overview.countries', $this->viewData);

    }

    public function compare(Request $request) {

        $this->viewData['first_date'] = $request->get('first_date');
        $this->viewData['second_date'] = $request->get('second_date');
        $this->viewData['first_period_1'] = $request->get('first_period_1');
        $this->viewData['second_period_1'] = $request->get('second_period_1');
        $this->viewData['first_period_2'] = $request->get('first_period_2');
        $this->viewData['second_period_2'] = $request->get('second_period_2');

        return view($this->theme . '.pages.overview.compare', $this->viewData);

    }

    public function compare_process(Request $request) {

        if($request->get('first_period_1')) {

            $this->generateStatisticPeriod($request->get('first_period_1'), $request->get('second_period_1'), 'first_');
            $this->generateStatisticPeriod($request->get('first_period_2'), $request->get('second_period_2'), 'second_');

            return view($this->theme . '.pages.overview.compare_period', $this->viewData);

        } else {

            $this->generateStatistic($request->get('first_date'), 'first_');
            $this->generateStatistic($request->get('second_date'), 'second_');

            return view($this->theme . '.pages.overview.compare_process', $this->viewData);

        }

    }

    function generateStatistic($date = '', $pre = '')
    {

        if ($date == '') $date = Carbon::now()->format('Y-m-d');

        $ident = Carbon::parse($date)->format('Ymd');
        $subDay = Carbon::parse($date)->subDay();
        $subWeek = Carbon::parse($date)->subWeek();
        $subMonth = Carbon::parse($date)->subMonth();
        $date = Carbon::parse($date);

        $this->viewData[$pre . 'users_countries'] = [];

        $this->viewData[$pre . 'day'] = Profile::countByDate($subDay, '>', $date, '<=');
        $this->viewData[$pre . 'week'] = Profile::countByDate($subWeek, '>', $date, '<=');
        $this->viewData[$pre . 'month'] = Profile::countByDate($subMonth, '>', $date, '<=');

        $this->viewData[$pre . 'avatars'] = Profile::countProfilesWithData($date, ['avatar']);
        $this->viewData[$pre . 'core_data'] = Profile::countProfilesWithData($date, ['culinary_profession', 'gender', 'city', 'country']);

        $videos = Videos::getUserIds($date);
        $images = ImageGalleries::getUserIds($date);
        $recipes = Recipe::getUserIds($date);
        $recipes_private = RecipePrivate::getUserIds($date);

        $this->viewData[$pre . 'any_content'] = count($videos->toArray() + $images->toArray() + $recipes->toArray() + $recipes_private->toArray());

        if (Cache::has('stat_users_countries' . $ident) && Cache::has('stat_total' . $ident)) {

            $this->viewData[$pre . 'users_countries'] = Cache::get('stat_users_countries' . $ident);

            $this->viewData[$pre . 'total'] = Cache::get('stat_total' . $ident);

        } else {

            $countries = Config::get('custom.countryList');

            $users_by_country = Profile::getUsersGroupCountry($date);

            $all_users = $users_by_country->count();

            $this->viewData[$pre . 'total'] = $all_users;

            if ($all_users != 0) {

                foreach ($countries as $key => $value) {

                    $count = $users_by_country->where('country', $key)->count();

                    $this->viewData[$pre . 'users_countries'][$key]['percent'] = round($count * 100 / $all_users, 2);
                    $this->viewData[$pre . 'users_countries'][$key]['count'] = $count;
                    $this->viewData[$pre . 'users_countries'][$key]['name'] = $value;

                }

                arsort($this->viewData[$pre . 'users_countries']);
//                $this->viewData[$pre . 'users_countries'] = array_slice($this->viewData[$pre . 'users_countries'], 0, 20);
                $this->viewData[$pre . 'users_countries'] = $this->viewData[$pre . 'users_countries'];
            }

            Cache::put('stat_users_countries' . $ident, $this->viewData[$pre . 'users_countries'], Carbon::now()->addMinutes(10));
            Cache::put('stat_total' . $ident, $this->viewData[$pre . 'total'], Carbon::now()->addMinutes(10));

        }

    }

        function generateStatisticPeriod($date1 = '', $date2 = '', $pre = '') {

            if($date1 == '') $date1 = Carbon::now()->format('Y-m-d');
            if($date2 == '') $date2 = Carbon::now()->format('Y-m-d');

            $date1 = Carbon::parse($date1);
            $date2 = Carbon::parse($date2);

            $ident = Carbon::parse($date1)->format('Ymd') . Carbon::parse($date2)->format('Ymd');

            $this->viewData[$pre . 'users_countries'] = [];

            $this->viewData[$pre . 'users'] = Profile::countByPeriod($date1, $date2);


            $this->viewData[$pre . 'avatars'] = Profile::countProfilesWithPeriod($date1, $date2, ['avatar']);
            $this->viewData[$pre . 'core_data'] = Profile::countProfilesWithPeriod($date1, $date2, ['culinary_profession', 'gender', 'city', 'country']);

            $videos = Videos::getUserIds($date1, $date2);
            $images = ImageGalleries::getUserIds($date1, $date2);
            $recipes = Recipe::getUserIds($date1, $date2);
            $recipes_private = RecipePrivate::getUserIds($date1, $date2);

            $this->viewData[$pre . 'any_content'] = count($videos->toArray()+$images->toArray()+$recipes->toArray()+$recipes_private->toArray());

            if(Cache::has('stat_users_countries'.$ident) && Cache::has('stat_total'.$ident)) {

                $this->viewData[$pre . 'users_countries'] = Cache::get('stat_users_countries'.$ident);

                $this->viewData[$pre . 'total'] = Cache::get('stat_total'.$ident);

            } else {

                $countries = Config::get('custom.countryList');

                $users_by_country = Profile::getUsersGroupCountryPeriod($date1, $date2);

                $all_users = $users_by_country->count();

                $this->viewData[$pre . 'total'] = $all_users;

                if($all_users != 0) {

                    foreach ($countries as $key => $value) {

                        $count = $users_by_country->where('country', $key)->count();

                        $this->viewData[$pre . 'users_countries'][$key]['percent'] = round($count * 100 / $all_users, 2);
                        $this->viewData[$pre . 'users_countries'][$key]['count'] = $count;
                        $this->viewData[$pre . 'users_countries'][$key]['name'] = $value;

                    }

                    arsort($this->viewData[$pre . 'users_countries']);
//                $this->viewData[$pre . 'users_countries'] = array_slice($this->viewData[$pre . 'users_countries'], 0, 20);
//                    $this->viewData[$pre . 'users_countries'] = $this->viewData['users_countries'];
                }

                Cache::put('stat_users_countries'.$ident, $this->viewData[$pre . 'users_countries'], Carbon::now()->addMinutes(10));
                Cache::put('stat_total'.$ident, $this->viewData[$pre . 'total'], Carbon::now()->addMinutes(10));

            }

    }

}