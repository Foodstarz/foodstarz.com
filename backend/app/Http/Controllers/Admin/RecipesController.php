<?php

namespace Foodstarz\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Foodstarz\Http\Requests;
use Foodstarz\Models\RecipePrivate;
use Foodstarz\Models\Recipe;
use Foodstarz\Models\User;
use Foodstarz\Models\RecipeCategory;
use Foodstarz\Models\JuryVote;
use Foodstarz\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use File;
use Validator;
use Session;
use Auth;
use Config;
use Image;
use Foodstarz\Models\FeaturedRecipes;

class RecipesController extends Controller {

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function listing(Request $request) {
        $requestData = $request->only(['sort', 'sort_type']);
        
        $sort = $requestData['sort'];
        $sort_type = $requestData['sort_type'];
        
        if(!empty($sort) && !empty($sort_type)) {
            $this->viewData['sort'] = $sort;
            $this->viewData['sort_type'] = $sort_type;
            $recipes = RecipePrivate::getRecipes(20, 1, $sort, $sort_type);
        } else {
            $recipes = RecipePrivate::getRecipes(20, 1);
        }
        
        $this->viewData['recipes'] = $recipes;
        return view($this->theme . '.pages.recipes_listing', $this->viewData);
    }
    
    public function search(Request $request) {
        $requestData = $request->only(['search_title', 'search_author']);
        
        $search_title = $requestData['search_title'];
        $search_author = $requestData['search_author'];
        
        $recipes = RecipePrivate::searchRecipes($search_title, $search_author);

        if(!empty($search_title)) $this->viewData['search_title'] = $search_title;
        if(!empty($search_author)) $this->viewData['search_author'] = $search_author;
        
        $this->viewData['recipes'] = $recipes->appends($requestData);
        return view($this->theme . '.pages.recipes_search', $this->viewData);
    }

    public function review($id) {
        $id = (int) $id;
        $recipe = RecipePrivate::getRecipe($id);
        $votes = JuryVote::where("recipe_id", "=", $id)->get();
        $this->viewData['recipe'] = $recipe;
        $this->viewData['votes'] = $votes;

        if ($recipe) {
            return view($this->theme . '.pages.recipe_review', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function approve($id, $status) {
        $recipe = RecipePrivate::getRecipe($id);
        if (!$recipe) {
            abort(404);
        }
        if ($status == 1) {
            $update = [
                'status' => 2
            ];
            $recipe->update($update);

            $update_data = [
                'slug' => $recipe->slug,
                'title' => $recipe->title,
                'category' => json_encode($recipe->category),
                'portions' => $recipe->portions,
                'preparation_time' => $recipe->preparation_time,
                'difficulty' => $recipe->difficulty,
                'components' => json_encode($recipe->components),
                'main_image_watermarked' => $recipe->main_image_watermarked,
                'status' => $recipe->status
            ];

            Recipe::updateRecipe($id, $update_data);

            /*
            $count = Recipe::where("user_id", $recipe->user->id)->where('status', 2)->count();
            if ($count == 1) {
                $update = [
                    'role' => 4,
                    'role_changed' => date('Y-m-d H:i:s')
                ];
                User::where("id", $recipe->user->id)->update($update);
            }
            */

            return redirect(route('recipes_listing'));
        } else if($status == 0) {
            $update = [
                'status' => 0
            ];

            $recipe->update($update);

            return redirect(route('recipes_listing'));
        }
    }

    public function watermark($id, Request $r) {
        $recipe = RecipePrivate::getRecipe($id);
        if (!$recipe) {
            abort(404);
        }
        if ($r->get("main_image") != "") {
            if ($recipe->main_image_watermarked != NULL && file_exists(storage_path('app/recipes/watermark/' . $recipe->main_image_watermarked))) {
                File::delete(storage_path('app/recipes/watermark/' . $recipe->main_image_watermarked));
            }
            //Original image
            $img = Image::make(storage_path('app/recipes/original/' . $recipe->main_image_original));

            //Watermark image
            $ratio = 3 / 1;
            $watermark = Image::make(storage_path('app/recipes/watermark/' . $r->get("main_image") . ".png"));
            $watermark->resize(intval($img->width() / $ratio), null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $watermark->opacity(30);


            $file = explode(".", $recipe->main_image_original);
            $image_name = uniqid(time()) . '.' . $file[1];

            $img->insert($watermark, "bottom-left", 20, 15);
            $img->save(storage_path('app/recipes/watermark/' . $image_name));

            $update = [
                'main_image_watermarked' => $image_name
            ];

            RecipePrivate::updateRecipe($id, $update);
        }
        $components = json_decode($recipe->components, true);
        if(!isset($components['html']) && !empty($r->get("image_component"))) {
            foreach ($r->get("image_component") AS $key => $value) {
                if ($value != "") {
                    $current_component = $components[$key];

                    if (array_key_exists("image_watermarked", $current_component) && file_exists(storage_path('app/recipes/watermark/' . $current_component['image_watermarked']))) {
                        File::delete(storage_path('app/recipes/watermark/' . $current_component['image_watermarked']));
                    }
                    //Main image
                    $img = Image::make(storage_path('app/recipes/original/' . $current_component['image']));

                    //Watermark image
                    $ratio = 3 / 1;
                    $watermark = Image::make(storage_path('app/recipes/watermark/' . $value . ".png"));
                    $watermark->resize(intval($img->width() / $ratio), null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $watermark->opacity(30);

                    $file = explode(".", $current_component['image']);
                    $image_name = uniqid(time()) . '.' . $file[1];

                    $img->insert($watermark, "bottom-left", 20, 15);
                    $img->save(storage_path('app/recipes/watermark/' . $image_name));
                    $current_component['image_watermarked'] = $image_name;
                    $components[$key] = $current_component;
                }
            }
        }
        $update = [
            'components' => json_encode($components)
        ];

        RecipePrivate::updateRecipe($id, $update);

        return redirect(route('recipe_review', $recipe->id));
    }
    
    public function featured() {
        $featured = FeaturedRecipes::getAllRecipes();
        
        $featured_array = [];
        
        foreach($featured as $item) {
            $featured_array[] = $item->recipe_id;
        }
        
        $recipes = Recipe::getRecipesByNotIn($featured_array);
        //$recipes->setPageName('page_recipes');
        
        $this->viewData['recipes'] = $recipes;
        $this->viewData['featured'] = $featured;
        
        return view($this->theme . '.pages.featured_recipes', $this->viewData);
    }
    
    public function addFeatured($id) {
        $recipe = Recipe::getRecipe($id);
        $featured = FeaturedRecipes::getRecipe($id);
        
        if($recipe && !$featured) {
            $featured_recipe = new FeaturedRecipes();
            $featured_recipe->recipe_id = $id;
            $featured_recipe->save();
            
            return redirect(route('featured_recipes'))->with('success', trans('admin/pages/featured_recipes.messages.add'));
        } else {
            abort(404);
        }
    }
    
    public function deleteFeatured($id) {
        $featured = FeaturedRecipes::getRecipe($id);
        
        if($featured) {
            $featured->delete();
            
            return redirect(route('featured_recipes'))->with('success', trans('admin/pages/featured_recipes.messages.delete'));
        } else {
            abort(404);
        }
    }
    
    public function fillFeatured() {
        $recipes = Recipe::getRecipes();
        
        foreach($recipes as $recipe) {
            $featured = new FeaturedRecipes();
            $featured->recipe_id = $recipe->id;
            $featured->save();
        }
    }

}
