<?php

namespace Foodstarz\Http\Controllers\Admin;

use File;
use Cache;
use Config;
use LaravelAnalytics;
use Foodstarz\Models\User;
use Foodstarz\Models\Videos;
use Foodstarz\Models\Recipe;
use Illuminate\Http\Request;
use Foodstarz\Models\Profile;
use Foodstarz\Models\RecipePrivate;
use Foodstarz\Models\ImageGalleries;
use Foodstarz\Models\Bookmark;
use Foodstarz\Http\Controllers\Controller;

class ManageController extends Controller
{
    public $result = [];
    public $types = [
        'recipe' => 'Foodstarz\Models\Recipe',
        'image' => 'Foodstarz\Models\ImageGalleries',
        'video' => 'Foodstarz\Models\Videos'
    ];
    public $resources = [
        'recipe' => '1',
        'image' => '2',
        'video' => '3'
    ];

    function __construct()
    {
        $this->result['status'] = 0;
        $this->result['message'] = '';
    }

    public function index($action, $type, $id)
    {

        if($action == 'delete') $method = $action.ucfirst($type);
        else $method = $action;

        if(method_exists($this, $method)) $this->$method($id, $type);
        else return 0;

        return response()->json($this->result);

    }

    function hide($id, $type) {
        $types = $this->types;
        if($types[$type]::where('id', $id)->update(['hidden' => '1'])) {
            $this->result['status'] = 1;
            $this->result['instruct'] = 'class';
            $this->result['class'] = 'unhide';
            $this->result['href'] = route('manage_content', ['action' => 'unhide', 'type' => $type, 'id' => $id]);
        }
    }
    function unhide($id, $type) {
        $types = $this->types;
        if($types[$type]::where('id', $id)->update(['hidden' => '0'])) {
            $this->result['status'] = 1;
            $this->result['instruct'] = 'class';
            $this->result['class'] = 'hide';
            $this->result['href'] = route('manage_content', ['action' => 'hide', 'type' => $type, 'id' => $id]);
        }
    }

    function deleteImage($id) {
        $imageData = ImageGalleries::getGalleryById($id);
        if($imageData) {
            File::delete(storage_path('app/gallery/' . $imageData->image));
            File::delete(storage_path('app/gallery/thumbnails/' . $imageData->image));
            $imageData->delete();

            $this->result['status'] = 1;
            $this->result['instruct'] = 'reload';
        }
    }

    function deleteVideo($id) {
        $videoData = Videos::getVideo($id);
        if($videoData) {
            $videoData->delete();

            $this->vimeo->request($videoData->video, [], 'DELETE');

            $this->result['status'] = 1;
            $this->result['instruct'] = 'reload';
        }
    }

    public function deleteRecipe($id) {
        $r = Recipe::find((int) $id);
        $r->delete();

        $rp = RecipePrivate::find((int) $id);
        $rp->delete();

        $this->result['status'] = 1;
        $this->result['instruct'] = 'reload';
    }

    function bookmark($id, $type) {
        $types = $this->types;
        $resource_id = $this->resources[$type];

        if(array_key_exists($type, $types)) {
            $resource = $types[$type]::where('id', $id)->first();
            if($resource) {
                $bookmark = new Bookmark();
                $bookmark->resource_type = $resource_id;
                $bookmark->resource_id = $id;
                $bookmark->save();

                $this->result['status'] = 1;
                $this->result['instruct'] = 'class';
                $this->result['class'] = 'unbookmark';
                $this->result['href'] = route('manage_content', ['action' => 'unbookmark', 'type' => $type, 'id' => $id]);
            }
        }
    }

    function unbookmark($id, $type) {
        $types = $this->types;
        $resource_id = $this->resources[$type];

        if(array_key_exists($type, $types)) {
            $bookmark = Bookmark::getBookmark($resource_id, $id);
            if($bookmark) {
                $bookmark->delete();

                $this->result['status'] = 1;
                $this->result['instruct'] = 'class';
                $this->result['class'] = 'bookmark';
                $this->result['href'] = route('manage_content', ['action' => 'bookmark', 'type' => $type, 'id' => $id]);
            }
        }
    }
    
}