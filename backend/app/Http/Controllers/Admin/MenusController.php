<?php

namespace Foodstarz\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Config;
use Foodstarz\Models\Menus;

class MenusController extends Controller
{
    
    use DispatchesJobs,
        ValidatesRequests;
    
    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }
    
    public function listing($menu_id) {
        $menu_id = (int)$menu_id;
        
        $menus = Menus::getMenu($menu_id);
        $this->viewData['menu_id'] = $menu_id;
        $this->viewData['links'] = $menus;
        
        return view($this->theme . '.pages.menus_listing', $this->viewData);
    }
    
    public function add($menu_id) {
        $this->viewData['menu_id'] = $menu_id;
        return view($this->theme . '.pages.add_menu_item', $this->viewData);
    }
    
    public function addProcess($menu_id, Request $request) {
        $this->validate($request, [
            'title' => 'required|array',
            'url' => 'required',
            'order' => 'required|numeric'
        ]);
        
        $link = new Menus();
        $link->menu = $menu_id;
        $link->title = json_encode($request->get('title'));
        $link->url = $request->get('url');
        $link->order = $request->get('order');
        $link->save();
        
        return redirect(route('menus_add', $menu_id))->with('success', trans('admin/pages/add_menu_item.messages.success'));
    }
    
    public function edit($menu_id, $id) {
        $link = Menus::getMenuById($id);
        
        if($link) {
            $this->viewData['menu_id'] = $menu_id;
            $item = [];
            $item['id'] = $id;
            $item['title'] = empty(old('title')) ? json_decode($link->title, true) : old('title');
            $item['url'] = empty(old('url')) ? $link->url : old('url');
            $item['order'] = empty(old('order')) ? $link->order : old('order');
            
            $this->viewData['item'] = $item;
            
            return view($this->theme . '.pages.edit_menu_item', $this->viewData);
        } else {
            abort(404);
        }
    }
    
    public function editProcess($menu_id, $id, Request $request) {
        $link = Menus::getMenuById($id);
        
        if($link) {
            $this->validate($request, [
                'title' => 'required|min:2',
                'url' => 'required',
                'order' => 'required|numeric'
            ]);
            
            $link->title = json_encode($request->get('title'));
            $link->url = $request->get('url');
            $link->order = $request->get('order');
            
            $link->save();
            
            return redirect(route('menus_edit', ['menu_id' => $menu_id, 'id' => $id]))->with('success', trans('admin/pages/edit_menu_item.messages.success'));
        } else {
            abort(404);
        }
    }
    
    public function deleteProcess($menu_id, $id) {
        $link = Menus::getMenuById($id);
        
        if($link) {
            $link->delete();
            return redirect(route('menus_listing', $menu_id))->with('success', trans('admin/pages/menus_listing.messages.delete'));
        } else {
            return redirect(route('menus_listing', $menu_id));
        }
    }
    
}
