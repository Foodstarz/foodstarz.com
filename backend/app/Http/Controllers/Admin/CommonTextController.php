<?php

namespace Foodstarz\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Foodstarz\Models\CommonText;
use Foodstarz\Http\Controllers\Controller;
use Illuminate\Support\MessageBag;
use Config;

class CommonTextController extends Controller {

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function listing() {
        $texts = CommonText::paginate(20);
        $this->viewData['texts'] = $texts;
        return view($this->theme . '.pages.c_pages_listing', $this->viewData);
    }

    public function edit($id) {
        $id = (int) $id;
        $c_text = CommonText::getTextById($id);
        $this->viewData['text'] = $c_text;

        if ($c_text) {
            return view($this->theme . '.pages.c_pages_edit', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function add() {
        return view($this->theme . '.pages.c_pages_add', $this->viewData);
    }

    public function editProcess($id, Request $r) {
        $id = (int) $id;
        
        $update = [
            'title' => $r->get("title"),
            'slug' => $r->get("slug"),
            'text' => $r->get("text")
        ];

        CommonText::updateTextById($id, $update);
        return redirect(route("c_pages_listing"))->with('success', trans('admin/pages/c_pages.success'));
    }

    public function delete($id) {
        $id = (int) $id;
        CommonText::delTextById($id);

        return redirect(route("c_pages_listing"))->with('success', trans('admin/pages/c_pages.delete_success'));
    }
    
    public function addProcess(Request $r) {
        $cp = new CommonText();
        $cp->title = $r->get("title");
        $cp->text = $r->get("text");
        $cp->slug = $r->get("slug");
        
        $cp->save();
        return redirect(route("c_pages_listing"))->with('success', trans('admin/pages/c_pages.new_success'));
    }

}
