<?php

namespace Foodstarz\Http\Controllers\Admin;

use Auth;
use Config;
use Session;
use Validator;
use Foodstarz\Models\User;
use Illuminate\Http\Request;
use Foodstarz\Http\Requests;
use Foodstarz\Models\UserGroup;
use Foodstarz\Helpers\UserHelpers;
use Foodstarz\Http\Controllers\Controller;

class UsersController extends Controller {

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function listing(Request $request) {
        $requestData = $request->only(['sort', 'sort_type']);
        
        $sort = $requestData['sort'];
        $sort_type = $requestData['sort_type'];
        
        if(!empty($sort) && !empty($sort_type)) {
            $this->viewData['sort'] = $sort;
            $this->viewData['sort_type'] = $sort_type;
            $this->viewData['users'] = User::getListing($sort, $sort_type);
        } else {
            $this->viewData['users'] = User::getListing();
        }
      
        return view($this->theme . '.pages.users_listing', $this->viewData);
    }
    
    public function search(Request $request) {
        $requestData = $request->only(['search_name', 'search_email']);
        
        $users = User::searchUser($requestData['search_name'], $requestData['search_email']);

        if(!empty($requestData['search_name'])) $this->viewData['search_name'] = $requestData['search_name'];
        if(!empty($requestData['search_email'])) $this->viewData['search_email'] = $requestData['search_email'];
        
        $this->viewData['users'] = $users->appends($requestData);
        return view($this->theme . '.pages.users_search', $this->viewData);
    }

    public function delete($id) {
        $id = (int) $id;
        User::delUser($id);

        return redirect(route("users_listing"))->with('success', trans('admin/pages/users.delete_success'));
    }

    public function inactivate($id) {

        $id = (int) $id;
        $user = User::getUser($id);

        if($user->active == 0) {
            $user->active = 1;
            if(UserHelpers::isChefProfileDone($user)) {
                $user->role = 4;
            }

            $user->save();

            return redirect(route("users_listing"))->with('success', trans('admin/pages/users.activate_success'));
        } else {
            $user->active = 0;
            $user->role = 3;

            $user->save();

            return redirect(route("users_listing"))->with('success', trans('admin/pages/users.inactivate_success'));
        }
    }

    public function loginAs($id) {
        $old_user = Auth::user()->id;
        Session::flush();
        Session::put('old_user', $old_user);
        
        Auth::loginUsingId((int) $id);
        $user = Auth::user();
        $profile = Auth::user()->profile;
        
        
        if (!empty($user->name) && !empty($profile->gender) && !empty($profile->country) && !empty($profile->city) && !empty($profile->culinary_profession)) {
            Session::put('profile_core_data', true);
        } else {
            Session::put('profile_core_data', false);
        }

        if (!empty($profile->biography) && !empty($profile->work_experience) && !empty($profile->cooking_style)) {
            Session::put('profile_chef_data', true);
        } else {
            Session::put('profile_chef_data', false);
        }
        
        return redirect(route("homepage"));
    }

    public function groups() {

        $this->viewData['groups'] = UserGroup::getGroups();

        return view($this->theme . '.pages.users.groups', $this->viewData);
    }

    public function groups_edit($id) {

        $this->viewData['group'] = UserGroup::getGroup($id);

        return view($this->theme . '.pages.users.groups_edit', $this->viewData);
    }

    public function groups_edit_process($id, Request $request) {

        $groupData = $request->only(['name', 'status']);

        $validator = Validator::make($groupData, [
            'name' => 'required|min:5|max:128'
        ]);

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        } else {
            UserGroup::updateGroup($id, $groupData);
        }

        return redirect(route('users_groups'));
    }

    public function group_create() {

        return view($this->theme . '.pages.users.groups_create', $this->viewData);
    }

}
