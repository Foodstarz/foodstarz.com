<?php

namespace Foodstarz\Http\Controllers\Admin;

use Foodstarz\Models\News;
use Illuminate\Http\Request;

use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use Config;
use Foodstarz\Models\User;
use Foodstarz\Models\Newsletter;
use Response;

class NewsletterController extends Controller
{

    use DispatchesJobs,
        ValidatesRequests;

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function export() {
        $users = User::getList(['email', 'name']);

        $list = [];
        foreach($users as $user) {
            $user_names = explode(' ', $user->name);
            $list[] = [$user_names[0], !empty($user_names[1]) ? $user_names[1] : '', $user->email];
        }

        /*$newsletter_users = Newsletter::all();
        foreach($newsletter_users as $user) {
            if($user->subscribe == 1) {
                if(!in_array($user->email, $users_emails)) {
                    $users_emails[] = $user->email;
                    $users_names[] = "Foodstarz User";
                }
            } else {
                if(($key = array_search($user->email, $users_emails)) !== false) {
                    unset($users_emails[$key]);
                    unset($users_names[$key]);
                }
            }
        }*/

        $headers = [
            'Cache-Control'       => 'must-revalidate, post-check=0, pre-check=0',
            'Content-type'        => 'text/csv',
            'Content-Disposition' => 'attachment; filename=NewsletterUsers.csv',
            'Expires'             => '0',
            'Pragma'              => 'public'
        ];

        $callback = function() use ($list)
        {
            $FH = fopen('php://output', 'w');
            fputcsv($FH, array('First Name', 'Last Name', 'E-Mail'));
            foreach ($list as $row) {
                fputcsv($FH, $row);
            }
            fclose($FH);
        };

        return Response::stream($callback, 200, $headers);

        /*
        $this->viewData['emails'] = $users_emails;
        $this->viewData['names'] = $users_names;

        return view($this->theme . '.pages.newsletter_export', $this->viewData);
        */
    }

    public function change() {
        return view($this->theme . '.pages.newsletter_change', $this->viewData);
    }

    public function changeProcess(Request $request) {
        $this->validate($request, [
            'emails' => 'required',
            'subscribe' => 'required|in:0,1',
        ]);

        $emails = $request->get('emails');
        $emails = explode(PHP_EOL, $emails);
        $subscribe = $request->get('subscribe');

        foreach($emails as $email) {
            $user = Newsletter::firstOrCreate(['email' => $email]);
            $user->subscribe = $subscribe;
            $user->save();
        }

        return redirect(route('newsletter_change'))->with('success', trans('admin/pages/newsletter_change.messages.success'));
    }

}