<?php

namespace Foodstarz\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Foodstarz\Http\Controllers\Controller;
use Config;
use Foodstarz\Models\InfoWidget;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\Facades\File;

class HomepageController extends Controller {

    use DispatchesJobs,
        ValidatesRequests;
    
    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->theme = Config::get('custom.admin_theme');
    }

    public function index(Request $r) {
        $item = [];
        $item['heading'] = empty(old('heading')) ? json_decode(InfoWidget::getWidgetByItem('homepage_heading')['content'], true) : old('heading');
        $item['subheading'] = empty(old('subheading')) ? json_decode(InfoWidget::getWidgetByItem('homepage_subheading')['content'], true) : old('subheading');
        $item['subheading_auth'] = empty(old('subheading_auth')) ? json_decode(InfoWidget::getWidgetByItem('homepage_subheading_auth')['content'], true) : old('subheading_auth');
        $this->viewData['item'] = $item;
        
        return view($this->theme . '.pages.homepage', $this->viewData);
    }
    
    public function homepageHeaderProcess(Request $request) {
        $this->validate($request, [
            'heading' => 'required|array',
            'subheading' => 'required|array',
            'subheading_auth' => 'required|array',
        ]);
        
        $heading = InfoWidget::getWidgetByItem('homepage_heading');
        $heading->content = json_encode($request->get('heading'));
        $heading->save();
        
        $subheading = InfoWidget::getWidgetByItem('homepage_subheading');
        $subheading->content = json_encode($request->get('subheading'));
        $subheading->save();

        $subheading_auth = InfoWidget::getWidgetByItem('homepage_subheading_auth');
        $subheading_auth->content = json_encode($request->get('subheading_auth'));
        $subheading_auth->save();
        
        return redirect(route('admin_homepage'))->with('success', trans('admin/pages/homepage.messages.success'));
    }

    public function clear_cache() {
        File::cleanDirectory(storage_path('framework/sessions'));
    }

}
