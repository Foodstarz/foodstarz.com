<?php

namespace Foodstarz\Http\Controllers;

use Foodstarz\Models\CommonText;
use Foodstarz\Http\Controllers\Controller;
use Config;

class CommonTextController extends Controller {

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->viewData['inner_page'] = true;
        $this->theme = Config::get('custom.theme');
    }

    public function view($slug) {
        $c_text = CommonText::getTextBySlug($slug);
        
        if($c_text) {
            $this->viewData["c_text"] = $c_text;

            return view($this->theme . '.pages.c_page', $this->viewData);
        } else {
            abort(404);
        }
    }

}
