<?php

namespace Foodstarz\Http\Controllers;

use Foodstarz\Models\User;
use Illuminate\Http\Request;
use Foodstarz\Http\Requests;
use Foodstarz\Models\Profile;
use Foodstarz\Helpers\Php7Helper;
use Foodstarz\Http\Controllers\Controller;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

use App;
use Auth;
use Mail;
use Hash;
use Event;
use Config;
use Session;
use Validator;
use Foodstarz\Models\Interview;
use Foodstarz\Models\Newsletter;
use Illuminate\Support\MessageBag;
use Foodstarz\Models\UserActivation;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use Foodstarz\Events\UserProfileWasUpdated;

use Vinkla\Vimeo\VimeoManager;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

use Foodstarz\Models\Videos;
use Foodstarz\Models\Recipe;
use Foodstarz\Models\Features;
use Foodstarz\Models\ImageGalleries;

class UserProfileController extends Controller {

    use DispatchesJobs,
        ValidatesRequests;

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->viewData['inner_page'] = false;
        $this->theme = Config::get('custom.theme');
    }

    public function coreDataForm() {
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;
        return view($this->theme . '/pages.profile_core_data', $this->viewData);
    }

    public function coreDataFormProcess(Request $request) {
        $countries = '';

        $i = 0;
        foreach (Config::get('custom.countryList') as $countryCode => $countryName) {
            if (sizeof(Config::get('custom.countryList')) == $i) {
                $countries.=$countryCode;
            } else {
                $countries.=$countryCode . ',';
            }

            $i++;
        }

        if(Auth::check() && !empty(Session::get("old_user"))) {
            $validator = Validator::make($request->all(), [
                'name' => 'min:3|max:64',
                'gender' => 'in:male,female',
                'country' => 'in:' . $countries,
                'city' => 'max:64',
                'culinary_profession' => 'in:amateur,blogger,professional'
            ]);
        } else {
            $validator = Validator::make($request->all(), [
                'name' => 'required|min:3|max:64',
                'gender' => 'required|in:male,female',
                'country' => 'required|in:' . $countries,
                'city' => 'required|max:64',
                'culinary_profession' => 'required|in:amateur,blogger,professional'
            ]);
        }


        if ($validator->fails()) {
            $this->viewData['inner_page'] = true;
            $this->viewData['user'] = Auth::user();
            $this->viewData['profile'] = Auth::user()->profile;
            return view($this->theme . '/pages.profile_core_data', array('errors' => $validator->errors()),$this->viewData);
        }

        $user = User::where('email', session('user')->email)->first();
        $userData = $request->only(['name']);
        $user->update($userData);

        $profile = $user->profile();
        $profileData = $request->only(['gender', 'country', 'city', 'culinary_profession']);

        if ($userData['name'] != session('user')->name) {
            $profile_slug = str_slug($userData['name'], '-');
            $slug_number = 2;
            while (Profile::where('slug', $profile_slug)->count() > 0) {
                $profile_slug = str_slug($userData['name'] . ' ' . $slug_number, '-');
                $slug_number++;
            }
            $profileData['slug'] = $profile_slug;
        }

        $profile->update($profileData);
        $request->session()->put('profile_core_data', true);


        Event::fire(new UserProfileWasUpdated($user));

        return redirect(route('upload_avatar'));
        //return redirect(route('profile_core_data'))->with('success', trans('pages/profile_core_data.messages.success') . '<br>' . trans('pages/profile_core_data.messages.proceed') . ' <a href="' . route('upload_avatar') . '">' . trans('pages/profile_core_data.messages.proceed_to') . '</a>.');
    }

    public function uploadAvatar() {
        if (!session('profile_core_data')) {
            return redirect(route('profile_core_data'));
        }
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;
        return view($this->theme . '/pages.upload_avatar', $this->viewData);
    }

    public function changeBackground() {
        if(!Features::getPermition('backgrounds', Auth::user()->role)) return back();
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;
        return view($this->theme . '/pages.change_background', $this->viewData);
    }

    public function recipes($slug) {
        $this->viewData['inner_page'] = true;
        
        $profile = Profile::getProfileBySlug($slug);
        
        $this->viewData['user'] = $profile->user;
        $this->viewData['profile'] = $profile;
        return view($this->theme . '/pages.user_profile_recipes', $this->viewData);
    }

    public function uploadAvatarProcess(Request $request) {

        $this->viewData['_token'] = csrf_token();
        $this->viewData['ok'] = false;

        if(!empty($request->get('avatar'))) {
            if(Image::make($request->get('avatar'))) {
                $avatar = Image::make($request->get('avatar'));
                $avatar_mime_type = $avatar->mime();
                $avatar_allowed_mime_types = Config::get('custom.avatar_mime_types');

                if (in_array($avatar_mime_type, $avatar_allowed_mime_types)) {
                    $width = $avatar->width();
                    $height = $avatar->height();
                    if ($width != 750 && $height != 750) {
                        if ($width < 750 || $height < 750) {
                            $this->viewData['errors'][] = trans('pages/upload_avatar.messages.image_dimensions_error');
                        } else {
                            $avatar->fit(750, 750);
                        }
                    }

                    if (empty($this->viewData['errors'])) {
                        switch ($avatar_mime_type) {
                            case 'image/jpeg':
                                $ext = 'jpg';
                                break;

                            case 'image/png':
                                $ext = 'png';
                                break;

                            default:
                                $ext = 'jpg';
                                break;
                        }

                        $avatar_name = md5(session('user')->id) . '.' . $ext;

                        $max_size = Config::get('custom.avatar_max_size');
                        if ($avatar->filesize() > $max_size) {
                            $this->viewData['errors'][] = trans('pages/upload_avatar.messages.size_error', ['size' => $max_size / 1024 / 1024]);
                        } else {
                            $user = User::getUserByEmail(session('user')->email);

                            if (!empty($user->profile->avatar) && Storage::has('avatars/' . $user->profile->avatar)) {
                                Storage::delete('avatars/' . $user->profile->avatar);
                            }

                            $avatar->save(storage_path('app/avatars/' . $avatar_name), 75);

                            if(empty($user->profile->avatar)) {
                                $update = [
                                    'role' => 4,
                                    'role_changed' => date('Y-m-d H:i:s')
                                ];
                                User::updateUser($user->id, $update);
                            }

                            $profile = $user->profile();
//
                            $profile->update(['avatar' => $avatar_name]);

                            Event::fire(new UserProfileWasUpdated($user));

                            $this->viewData['ok'] = true;
                        }
                    }
                } else {
                    $this->viewData['errors'][] = trans('pages/upload_avatar.messages.mime_type_error');
                }
            } else {
                $this->viewData['errors'][] = trans('pages/upload_avatar.messages.valid_image_error');
            }
        } else {
            $this->viewData['errors'][] = trans('pages/upload_avatar.messages.valid_image_error');
        }

        return response()->json($this->viewData);
    }

    public function changeBackgroundProcess(Request $request) {

        $this->viewData['_token'] = csrf_token();
        $this->viewData['ok'] = false;

        $user = User::getUserByEmail(session('user')->email);
        $profile = $user->profile();

        $bgs = json_decode($user->profile->background);

        if(!empty($request->get('background'))) {
            if(Image::make($request->get('background'))) {
                $background = Image::make($request->get('background'));
                $background_mime_type = $background->mime();
                $background_allowed_mime_types = Config::get('custom.background_mime_types');

                if (in_array($background_mime_type, $background_allowed_mime_types)) {
                    $width = $background->width();
                    $height = $background->height();
                    if ($width != 750 && $height != 750) {
                        if ($width < 750 || $height < 750) {
                            $this->viewData['errors'][] = trans('pages/change_background.messages.image_dimensions_error');
                        } else {
                            $background->fit(750, 750);
                        }
                    }

                    if (empty($this->viewData['errors'])) {
                        switch ($background_mime_type) {
                            case 'image/jpeg':
                                $ext = 'jpg';
                                break;

                            case 'image/png':
                                $ext = 'png';
                                break;

                            default:
                                $ext = 'jpg';
                                break;
                        }

                        $background_name = md5(session('user')->id . '_1') . '.' . $ext;

                        $max_size = Config::get('custom.background_max_size');

                        if ($background->filesize() > $max_size) {
                            $this->viewData['errors'][] = trans('pages/change_background.messages.size_error', ['size' => $max_size / 1024 / 1024]);
                        } else {

                            if (isset($bgs[0]) && $bgs[0] != '') {
                                Storage::delete('backgrounds/' . $user->profile->background);
                            }

                            $background->save(storage_path('app/backgrounds/' . $background_name), 100);

                            $bgs[0] = $background_name;

                            Event::fire(new UserProfileWasUpdated($user));

                            $this->viewData['ok'] = true;
                        }
                    }
                } else {
                    $this->viewData['errors'][] = trans('pages/change_background.messages.mime_type_error');
                }
            } else {
                $this->viewData['errors'][] = trans('pages/change_background.messages.valid_image_error');
            }
        }  else {
            $bgs[0] = '';
        }

        if(!empty($request->get('background2'))) {
            if(Image::make($request->get('background2'))) {
                $background = Image::make($request->get('background2'));
                $background_mime_type = $background->mime();
                $background_allowed_mime_types = Config::get('custom.background_mime_types');

                if (in_array($background_mime_type, $background_allowed_mime_types)) {
                    $width = $background->width();
                    $height = $background->height();
                    if ($width != 750 && $height != 750) {
                        if ($width < 750 || $height < 750) {
                            $this->viewData['errors'][] = trans('pages/change_background.messages.image_dimensions_error');
                        } else {
                            $background->fit(750, 750);
                        }
                    }

                    if (empty($this->viewData['errors'])) {
                        switch ($background_mime_type) {
                            case 'image/jpeg':
                                $ext = 'jpg';
                                break;

                            case 'image/png':
                                $ext = 'png';
                                break;

                            default:
                                $ext = 'jpg';
                                break;
                        }

                        $background_name = md5(session('user')->id . '_2') . '.' . $ext;

                        $max_size = Config::get('custom.background_max_size');

                        if ($background->filesize() > $max_size) {
                            $this->viewData['errors'][] = trans('pages/change_background.messages.size_error', ['size' => $max_size / 1024 / 1024]);
                        } else {

                            if (isset($bgs[1]) && $bgs[1] != '') {
                                Storage::delete('backgrounds/' . $user->profile->background);
                            }

                            $background->save(storage_path('app/backgrounds/' . $background_name), 100);

                            $bgs[1] = $background_name;

                            Event::fire(new UserProfileWasUpdated($user));

                            $this->viewData['ok'] = true;
                        }
                    }
                } else {
                    $this->viewData['errors'][] = trans('pages/change_background.messages.mime_type_error');
                }
            } else {
                $this->viewData['errors'][] = trans('pages/change_background.messages.valid_image_error');
            }
        } else {
            $bgs[1] = '';
        }

        if(!empty($request->get('background3'))) {
            if(Image::make($request->get('background3'))) {
                $background = Image::make($request->get('background3'));
                $background_mime_type = $background->mime();
                $background_allowed_mime_types = Config::get('custom.background_mime_types');

                if (in_array($background_mime_type, $background_allowed_mime_types)) {
                    $width = $background->width();
                    $height = $background->height();
                    if ($width != 750 && $height != 750) {
                        if ($width < 750 || $height < 750) {
                            $this->viewData['errors'][] = trans('pages/change_background.messages.image_dimensions_error');
                        } else {
                            $background->fit(750, 750);
                        }
                    }

                    if (empty($this->viewData['errors'])) {
                        switch ($background_mime_type) {
                            case 'image/jpeg':
                                $ext = 'jpg';
                                break;

                            case 'image/png':
                                $ext = 'png';
                                break;

                            default:
                                $ext = 'jpg';
                                break;
                        }

                        $background_name = md5(session('user')->id . '_3') . '.' . $ext;

                        $max_size = Config::get('custom.background_max_size');

                        if ($background->filesize() > $max_size) {
                            $this->viewData['errors'][] = trans('pages/change_background.messages.size_error', ['size' => $max_size / 1024 / 1024]);
                        } else {

                            if (isset($bgs[2]) && $bgs[2] != '') {
                                Storage::delete('backgrounds/' . $user->profile->background);
                            }

                            $background->save(storage_path('app/backgrounds/' . $background_name), 100);

                            $bgs[2] = $background_name;

                            Event::fire(new UserProfileWasUpdated($user));

                            $this->viewData['ok'] = true;
                        }
                    }
                } else {
                    $this->viewData['errors'][] = trans('pages/change_background.messages.mime_type_error');
                }
            } else {
                $this->viewData['errors'][] = trans('pages/change_background.messages.valid_image_error');
            }
        } else {
            $bgs[2] = '';
        }

        $profile->update(['background' => json_encode($bgs)]);

        return response()->json($this->viewData);
    }

    public function chefDataForm() {
        $this->viewData['inner_page'] = true;
        $this->viewData['biography'] = empty(old('biography')) ? session('profile')->biography : old('biography');
        $this->viewData['work_experience'] = empty(old('work_experience')) ? json_decode(session('profile')->work_experience, true) : old('work_experience');
        $this->viewData['awards'] = empty(old('awards')) ? json_decode(session('profile')->awards, true) : old('awards');
        $this->viewData['organisations'] = empty(old('organisations')) ? json_decode(session('profile')->organisations, true) : old('organisations');
        $this->viewData['role_models'] = empty(old('role_models')) ? json_decode(session('profile')->role_models, true) : old('role_models');
        $this->viewData['cooking_style'] = empty(old('cooking_style')) ? session('profile')->cooking_style : old('cooking_style');
        $this->viewData['kitchen_skills'] = Config::get('profile.kitchen_skills');
        $this->viewData['kitchen_skills_post'] = empty(old('kitchen_skills')) ? json_decode(session('profile')->kitchen_skills, true) : old('kitchen_skills');
        $this->viewData['business_skills'] = Config::get('profile.business_skills');
        $this->viewData['business_skills_post'] = empty(old('business_skills')) ? json_decode(session('profile')->business_skills, true) : old('business_skills');
        $this->viewData['personal_skills'] = Config::get('profile.personal_skills');
        $this->viewData['personal_skills_post'] = empty(old('personal_skills')) ? json_decode(session('profile')->personal_skills, true) : old('personal_skills');
        $this->viewData['search'] = empty(old('search')) ? session('profile')->search : old('search');
        $this->viewData['offer'] = empty(old('offer')) ? session('profile')->offer : old('offer');
        $this->viewData['website'] = empty(old('website')) ? session('profile')->website : old('website');
        $this->viewData['instagram'] = empty(old('instagram')) ? session('profile')->instagram : old('instagram');
        $this->viewData['facebook'] = empty(old('facebook')) ? session('profile')->facebook : old('facebook');
        $this->viewData['twitter'] = empty(old('twitter')) ? session('profile')->twitter : old('twitter');
        $this->viewData['pinterest'] = empty(old('pinterest')) ? session('profile')->pinterest : old('pinterest');
        $this->viewData['tumblr'] = empty(old('tumblr')) ? session('profile')->tumblr : old('tumblr');
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;
        return view($this->theme . '/pages.profile_chef_data', $this->viewData);
    }

    public function chefDataFormProcess(Request $request) {

        $chefData = $request->only(['biography',
                                    'work_experience',
                                    'awards',
                                    'organisations',
                                    'role_models',
                                    'cooking_style',
                                    'kitchen_skills',
                                    'business_skills',
                                    'personal_skills',
                                    'search',
                                    'offer',
                                    'website',
                                    'instagram',
                                    'facebook',
                                    'twitter',
                                    'pinterest',
                                    'tumblr']);
        $errorsCheck = false;
        $errors = new MessageBag();

        $wexp = [];
        for ($i = 0; $i < sizeof($chefData['work_experience']['position']); $i++) {
            $wexp[$i]['position'] = $chefData['work_experience']['position'][$i];
            $wexp[$i]['employer'] = $chefData['work_experience']['employer'][$i];
            $wexp[$i]['period']['from'] = $chefData['work_experience']['period']['from'][$i];
            $wexp[$i]['period']['to'] = isset($chefData['work_experience']['period']['to'][$i]) ? $chefData['work_experience']['period']['to'][$i] : null;
            $wexp[$i]['period']['now'] = isset($chefData['work_experience']['period']['now'][$i]) ? $chefData['work_experience']['period']['now'][$i] : false;
            $wexp[$i]['job_description'] = $chefData['work_experience']['job_description'][$i];
        }

        $chefData['work_experience'] = $wexp;
        $request->merge(['work_experience' => $wexp]);

        if(Auth::check() && !empty(Session::get("old_user"))) {
            $validator = Validator::make($chefData, [
                'biography' => 'min:30',
                'work_experience' => 'array|min:1',
                'awards' => 'sometimes|array',
                'organisations' => 'sometimes|array',
                'role_models' => 'sometimes|array',
                'cooking_style' => 'min:3|max:255',
                'kitchen_skills' => 'sometimes|array|max:6|arrayValues:' . implode('/', Config::get('profile.kitchen_skills')),
                'business_skills' => 'sometimes|array|max:6|arrayValues:' . implode('/', Config::get('profile.business_skills')),
                'personal_skills' => 'sometimes|array|max:6|arrayValues:' . implode('/', Config::get('profile.personal_skills')),
                'search' => 'sometimes|min:30',
                'offer' => 'sometimes|min:30',
                'website' => 'sometimes|url',
                'instagram' => 'sometimes|url',
                'facebook' => 'sometimes|url',
                'twitter' => 'sometimes|url',
                'pinterest' => 'sometimes|url',
                'tumblr' => 'sometimes|url'
            ]);
        } else {
            $validator = Validator::make($chefData, [
              'biography' => 'min:30',
              'work_experience' => 'array|min:1',
              'awards' => 'sometimes|array',
              'organisations' => 'sometimes|array',
              'role_models' => 'sometimes|array',
              'cooking_style' => 'min:3|max:255',
              'kitchen_skills' => 'sometimes|array|max:6|arrayValues:' . implode('/', Config::get('profile.kitchen_skills')),
              'business_skills' => 'sometimes|array|max:6|arrayValues:' . implode('/', Config::get('profile.business_skills')),
              'personal_skills' => 'sometimes|array|max:6|arrayValues:' . implode('/', Config::get('profile.personal_skills')),
              'search' => 'sometimes|min:30',
              'offer' => 'sometimes|min:30',
              'website' => 'sometimes|url',
              'instagram' => 'sometimes|url',
              'facebook' => 'sometimes|url',
              'twitter' => 'sometimes|url',
              'pinterest' => 'sometimes|url',
              'tumblr' => 'sometimes|url'
            ]);
        }

        for ($i = 0; $i < sizeof($chefData['work_experience']); $i++) {
            $experience = $chefData['work_experience'][$i];
            if (empty($experience['position']) || empty($experience['employer']) || empty($experience['period']['from']) || empty($experience['job_description'])) {
                if(empty(Session::get("old_user"))) {
                    $validator->errors()->add('work_experience', trans('pages/profile_chef_data.messages.errors.work_experience'));
                    $errorsCheck = true;
                }
            }

            if (empty($experience['period']['to'])) {
                if (empty($experience['period']['now']) && empty(Session::get("old_user"))) {
                    $validator->errors()->add('work_experience', trans('pages/profile_chef_data.messages.errors.work_experience'));
                    $errorsCheck = true;
                }
            }
        }

        $awLength = sizeof($chefData['awards']);
        for ($i = 0; $i < $awLength; $i++) {
            $award = $chefData['awards'][$i];
            if (empty($award) && $awLength != 1) {
                $validator->errors()->add('awards', trans('pages/profile_chef_data.messages.errors.awards'));
                $errorsCheck = true;
            }
        }

        $orgLength = sizeof($chefData['organisations']);
        for ($i = 0; $i < $orgLength; $i++) {
            $organisation = $chefData['organisations'][$i];
            if (empty($organisation) && $orgLength != 1) {
                $validator->errors()->add('organisations', trans('pages/profile_chef_data.messages.errors.organisations'));
                $errorsCheck = true;
            }
        }

        $rmLength = sizeof($chefData['role_models']);
        for ($i = 0; $i < $rmLength; $i++) {
            $role_model = $chefData['role_models'][$i];
            if (empty($role_model) && $rmLength != 1) {
                $validator->errors()->add('role_models', trans('pages/profile_chef_data.messages.errors.awards'));
                $errorsCheck = true;
            }
        }

        if ($errorsCheck || $validator->fails()) {
            /*$request->flash();
            return redirect(route('profile_chef_data'))->withErrors($errors);*/
            $this->viewData['inner_page'] = true;
            $this->viewData['biography'] = empty(old('biography')) ? session('profile')->biography : old('biography');
            $this->viewData['work_experience'] = empty(old('work_experience')) ? json_decode(session('profile')->work_experience, true) : old('work_experience');
            $this->viewData['awards'] = empty(old('awards')) ? json_decode(session('profile')->awards, true) : old('awards');
            $this->viewData['organisations'] = empty(old('organisations')) ? json_decode(session('profile')->organisations, true) : old('organisations');
            $this->viewData['role_models'] = empty(old('role_models')) ? json_decode(session('profile')->role_models, true) : old('role_models');
            $this->viewData['cooking_style'] = empty(old('cooking_style')) ? session('profile')->cooking_style : old('cooking_style');
            $this->viewData['kitchen_skills'] = Config::get('profile.kitchen_skills');
            $this->viewData['kitchen_skills_post'] = empty(old('kitchen_skills')) ? json_decode(session('profile')->kitchen_skills, true) : old('kitchen_skills');
            $this->viewData['business_skills'] = Config::get('profile.business_skills');
            $this->viewData['business_skills_post'] = empty(old('business_skills')) ? json_decode(session('profile')->business_skills, true) : old('business_skills');
            $this->viewData['personal_skills'] = Config::get('profile.personal_skills');
            $this->viewData['personal_skills_post'] = empty(old('personal_skills')) ? json_decode(session('profile')->personal_skills, true) : old('personal_skills');
            $this->viewData['search'] = empty(old('search')) ? session('profile')->search : old('search');
            $this->viewData['offer'] = empty(old('offer')) ? session('profile')->offer : old('offer');
            $this->viewData['website'] = empty(old('website')) ? session('profile')->website : old('website');
            $this->viewData['instagram'] = empty(old('instagram')) ? session('profile')->instagram : old('instagram');
            $this->viewData['facebook'] = empty(old('facebook')) ? session('profile')->facebook : old('facebook');
            $this->viewData['twitter'] = empty(old('twitter')) ? session('profile')->twitter : old('twitter');
            $this->viewData['pinterest'] = empty(old('pinterest')) ? session('profile')->pinterest : old('pinterest');
            $this->viewData['tumblr'] = empty(old('tumblr')) ? session('profile')->tumblr : old('tumblr');
            $this->viewData['user'] = Auth::user();
            $this->viewData['profile'] = Auth::user()->profile;

            return view($this->theme . '/pages.profile_chef_data', $this->viewData, array('errors' => $validator->errors()));
        } else {
            $chefData['work_experience'] = json_encode($chefData['work_experience']);
            $chefData['awards'] = json_encode($chefData['awards']);
            $chefData['organisations'] = json_encode($chefData['organisations']);
            $chefData['role_models'] = json_encode($chefData['role_models']);
            $chefData['kitchen_skills'] = json_encode($chefData['kitchen_skills']);
            $chefData['business_skills'] = json_encode($chefData['business_skills']);
            $chefData['personal_skills'] = json_encode($chefData['personal_skills']);

            $user = User::where('email', session('user')->email)->first();
            $profile = $user->profile();
            $profile->update($chefData);
            $request->session()->put('profile_chef_data', true);
            Event::fire(new UserProfileWasUpdated($user));

            return redirect(route('profile_chef_data'))->with('success', trans('pages/profile_chef_data.messages.success') . '<br>' . trans('pages/profile_chef_data.messages.proceed') . ' <a href="'.route('interview').'">' . trans('pages/profile_chef_data.messages.proceed_to') . '</a>.');
        }
    }

    public function interview() {
        $this->viewData['inner_page'] = true;
        $this->viewData['questions'] = Interview::all();
        $this->viewData['interview'] = empty(old('interview')) ? json_decode(session('profile')->interview, true) : old('interview');
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;
        return view($this->theme . '/pages.interview', $this->viewData);
    }

    public function interviewProcess(Request $request) {

        $interview = $request->input('interview');
        $errorsCheck = false;
        $errors = new MessageBag();

        $this->validate($request, array(
            'interview' => 'required|array'
        ));

        $questions = Interview::all();
        foreach ($questions as $question) {
            if (!isset($interview[$question->id]) || empty($interview[$question->id])) {
                $errors->add('interview', trans('pages/profile_interview.errors.required'));
                $errorsCheck = true;
                break;
            }
        }

        if ($errorsCheck) {
            $request->flash();
            return redirect(route('interview'))->withErrors($errors);
        } else {
            $interview = json_encode($interview);
            $user = User::getUserByEmail(session('user')->email);
            $profile = $user->profile();
            $justActivated = false;
            if(empty($user->profile->interview)) {
                $update = [
                    'role' => 4,
                ];
                User::updateUser($user->id, $update);
                $justActivated = true;
            }
            $profile->update(['interview' => $interview]);
            Event::fire(new UserProfileWasUpdated($user));
            return redirect(route('interview'))->with('success', $justActivated ? trans('pages/profile_interview.success_just_activated') : trans('pages/profile_interview.success_normal'));
        }
    }

    public function viewUserProfile($slug, Request $request) {
        $profile = Profile::getProfileBySlug($slug);

        if ($profile) {
            $this->viewData['bio'] = $profile->biography;
            $biography = '';
            $biography_paragraphs = explode(PHP_EOL, $profile->biography);
            $profile->biography = $biography_paragraphs;
            $profile->work_experience = json_decode($profile->work_experience, true);
            $profile->awards = json_decode($profile->awards, true);
            $profile->organisations = json_decode($profile->organisations, true);
            $profile->role_models = json_decode($profile->role_models, true);

            $kitchen_skills = json_decode($profile->kitchen_skills, true);
            $business_skills = json_decode($profile->business_skills, true);
            $personal_skills = json_decode($profile->personal_skills, true);

            $kitchen_skills_new = [];
            $business_skills_new = [];
            $personal_skills_new = [];

            if(!empty($kitchen_skills)) {
                foreach ($kitchen_skills as $k => $v) {
                    if (in_array($v, Config::get('profile.kitchen_skills'))) {
                        $kitchen_skills_new[] = $v;
                    }
                }
            }

            if(!empty($business_skills)) {
                foreach ($business_skills as $k => $v) {
                    if (in_array($v, Config::get('profile.business_skills'))) {
                        $business_skills_new[] = $v;
                    }
                }
            }

            if(!empty($personal_skills)) {
                foreach ($personal_skills as $k => $v) {
                    if (in_array($v, Config::get('profile.personal_skills'))) {
                        $personal_skills_new[] = $v;
                    }
                }
            }

            $profile->kitchen_skills = $kitchen_skills_new;
            $profile->business_skills = $business_skills_new;
            $profile->personal_skills = $personal_skills_new;

            $this->viewData['inner_page'] = true;
            $this->viewData['user'] = $profile->user;
            $this->viewData['profile'] = $profile;
            $this->viewData['social_url'] = route('view_user_profile', ['slug' => $slug]);
            $this->viewData['social_description'] = $profile->user->name;
            $this->viewData['social_image'] = asset('storage/avatars/'.$profile->avatar);

            /*
              if(!empty($profile->user->name) && !empty($profile->gender) && !empty($profile->country) && !empty($profile->city) && !empty($profile->culinary_profession)
              && !empty($profile->avatar) && !empty($profile->biography) && !empty($profile->work_experience) && !empty($profile->cooking_style)) {
              $this->viewData['profile_done'] = true;
              } else {
              $this->viewData['profile_done'] = false;
              }
             */

            return view($this->theme . '/pages.user_profile', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function viewUserChronic($slug, Request $request, VimeoManager $vimeo) {
        $profile = Profile::getProfileBySlug($slug);
        $user = $profile->user;

        $items = [];

        $recipes = Recipe::getRecipesByUser($user->id, 2);
        $images = ImageGalleries::getGalleriesWithCaption($user->id);
        $videos = Videos::getVideosByUser($user->id);
        foreach($recipes as $recipe) {
            $item = [];
            $item['type'] = trans('pages/recipe.recipes');
            $item['category_url'] = route('recipes');
            $item['title'] = $recipe->title;
            $item['image'] = empty($recipe->main_image_watermarked) ? asset('storage/recipes/original/'.$recipe->main_image_original) : asset('storage/recipes/watermark/'.$recipe->main_image_watermarked);
            $item['url'] = route('recipe_view', ['slug' => $recipe->slug]);
            $item['created_at'] = $recipe->created_at;

            $items[] = $item;
        }

        foreach($images as $image) {
            $item = [];
            $item['type'] = trans('pages/gallery.page_title');
            $item['category_url'] = route('gallery');
            $item['title'] = $image->caption;
            $item['image'] = asset('storage/gallery/thumbnails/'.$image->image);
            $item['url'] = route('gallery_view_image', $image->id);
            $item['created_at'] = $image->created_at;

            $items[] = $item;
        }

        foreach($videos as $video) {
            $item = [];
            $item['type'] = trans('pages/videos.page_title');
            $item['category_url'] = route('videos');
            $item['title'] = $video->title;

            $picture = $vimeo->request($video->video.'/pictures', [], 'GET');
            if(!isset($picture['body']['error'])) $item['image'] = empty($picture['body']['data'][0]['sizes'][3]['link']) ? '' : $picture['body']['data'][0]['sizes'][3]['link'];
            else $this->viewData['videos_pictures'][$video->id] = asset('assets/images/vimeo.jpg');

            $item['url'] = route('video_view', $video->id);
            $item['created_at'] = $video->created_at;

            $items[] = $item;
        }

        usort($items, function($a, $b) {
            $t1 = strtotime($a['created_at']);
            $t2 = strtotime($b['created_at']);
            if((int)phpversion() == 5) {
                if($t1==$t2) return 0;
                elseif($t1<$t2) return -1;
                else return 1;
            } else {
                return Php7Helper::operator($t1, $t2);
            }

        });

        $currentPage = $request->get('page', 1);
        $total = count($items);

        $paginator = new LengthAwarePaginator(
            array_slice($items, ($currentPage - 1) * 9, 9),
            $total,
            9,
            $currentPage,
            ['path' => Paginator::resolveCurrentPath()]
        );

        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = $user;
        $this->viewData['profile'] = $profile;
        $this->viewData['items'] = $paginator;

        return view($this->theme . '/pages.user_profile_chronic', $this->viewData);
    }

    public function viewUserProfileInterview($slug, Request $request) {

        $profile = Profile::getProfileBySLug($slug);

        if ($profile) {
            $profile->interview = json_decode($profile->interview, true);

            $this->viewData['inner_page'] = true;
            $this->viewData['user'] = $profile->user;
            $this->viewData['profile'] = $profile;
            $this->viewData['interview_questions'] = Interview::all();

            return view($this->theme . '/pages.user_profile_interview', $this->viewData);
        } else {
            abort(404);
        }
    }
    
    public function settings() {
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;
        $this->viewData['backgrounds'] = false;//Features::getPermition('backgrounds', Auth::user()->role);
        return view($this->theme . '/pages.profile_settings', $this->viewData);
    }
    
    public function changePassword() {
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;
        return view($this->theme . '/pages.profile_change_password', $this->viewData);
    }
    
    public function changePasswordProcess(Request $request) {
        Validator::extend('passcheck', function ($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->getAuthPassword());
        });
        
        $this->validate($request, [
            'old_password' => 'required|passcheck',
            'new_password' => 'required|confirmed|min:6',
            'new_password_confirmation' => 'required'
        ]);
        
        $user = User::getUserByEmail(session('user')->email);
        $user->update(['password' => Hash::make($request->input('new_password'))]);
        
        Event::fire(new UserProfileWasUpdated($user));

        return redirect(route('change_password'))->with('success', trans('pages/profile_change_password.messages.success'));
    }
    
    public function closeAccount() {
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;
        return view($this->theme . '/pages.profile_close_account', $this->viewData);
    }
    
    public function closeAccountProcess(Request $request) {
        Validator::extend('passcheck', function ($attribute, $value, $parameters) {
            return Hash::check($value, Auth::user()->getAuthPassword());
        });
        
        $this->validate($request, [
            'agreement' => 'accepted',
            'password' => 'required|passcheck'
        ]);
        
        $user = User::getUserByEmail(session('user')->email);
        $user->delete();
        
        Auth::logout();
        Session::flush();
        
        return redirect(route('homepage'));
    }
    
    public function changeEmail() {
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;
        $this->viewData['email'] = empty(old('email')) ? Auth::user()->email : old('email');
        return view($this->theme . '/pages.profile_change_email', $this->viewData);
    }
    
    public function changeEmailProcess(Request $request) {
        $this->validate($request, [
            'email' => 'required|email|max:255|unique:users',
        ]);
        
        $user = User::getUserByEmail(Auth::user()->email);
        $user->email = $request->get('email');
        $user->active = 0;
        $user->save();
        
        $activation = new UserActivation();
        $activation->user_id = $user->id;
        $activation->activation_key = md5(uniqid('', true));
        $activation->save();
        
        Mail::queue(Config::get('custom.theme') . '.emails.'.App::getLocale().'.change_email', ['name' => $user->name, 'email' => $user->email, 'activation_link' => route('user_activate', ['key' => $activation->activation_key])], function ($message) use ($user) {
            $message->to($user->email, $user->name)->subject(trans('emails/change_email.subject'));
        });
        
        Auth::logout();
        Session::flush();

        return redirect(route('homepage'));
    }

    public function changeLanguage() {
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;

        return view($this->theme . '/pages.profile_change_language', $this->viewData);
    }

    public function changeLanguageProcess(Request $request) {
        $chosenLang = $request->get('language');
        $available_languages = Config::get('custom.languages');

        if(!empty($chosenLang)) {
            $langStr = '';
            foreach ($available_languages as $code => $lang) {
                $langStr .= $code . ',';
            }

            $this->validate($request, [
                'language' => 'required|in:' . $langStr,
            ]);

            Profile::where('user_id', Auth::user()->id)->update(['lang' => $chosenLang]);

            return redirect(route('change_language'))->with('success', trans('pages/profile_change_language.messages.success'));
        } else {
            Profile::where('user_id', Auth::user()->id)->update(['lang' => null]);

            return redirect(route('change_language'))->with('success', trans('pages/profile_change_language.messages.success'));
        }

    }

    public function newsletter_subscription() {
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;

        $newsletter = Newsletter::getNewsLetterByEmail(Auth::user()->email);
        if($newsletter) {
            $this->viewData['subscribe'] = $newsletter->subscribe;
        } else {
            $this->viewData['subscribe'] = 1;
        }

        return view($this->theme . '/pages.profile_email_subscription', $this->viewData);
    }

    public function newsletter_subscriptionProcess(Request $request) {
        $subscribe = $request->get('subscribe');

        $this->validate($request, [
            'subscribe' => 'required|in:0,1',
        ]);

        $user = Newsletter::firstOrCreate(['email' => Auth::user()->email]);
        $user->subscribe = $subscribe;
        $user->save();

        return redirect(route('newsletter_subscription'))->with('success', trans('pages/profile_email_subscription.messages.success'));
    }

}
