<?php

namespace Foodstarz\Http\Controllers;

use Facebook\Facebook;
use Facebook\Helpers\FacebookRedirectLoginHelper;
use Facebook\Exceptions\FacebookSDKException;

use Illuminate\Http\Request;
use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Support\MessageBag;
use Config;
use Auth;
use Validator;
use File;
use Foodstarz\Models\ImageGalleries;
use Intervention\Image\Facades\Image;
use Foodstarz\Models\User;
use Foodstarz\Models\Profile;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Foodstarz\Helpers\FacebookPoster;
use Illuminate\Support\Facades\App;
use Event;
use Foodstarz\Events\UserProfileWasUpdated;
use Foodstarz\Helpers\CommonHelpers;
use Foodstarz\Models\Bookmark;

class GalleryController extends Controller
{

    use DispatchesJobs,
        ValidatesRequests;

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->viewData['inner_page'] = false;
        $this->viewData['admin'] = false;
        $this->theme = Config::get('custom.theme');
    }

    public function gallery() {
        $this->viewData['inner_page'] = true;

        $this->viewData['images'] = ImageGalleries::getGallery();

        return view($this->theme . '.pages.gallery', $this->viewData);
    }

    public function userGallery($slug) {
        $profile = Profile::getProfileBySlug($slug);

        if($profile) {
            $this->viewData['inner_page'] = true;
            $this->viewData['user'] = $profile->user;
            $this->viewData['profile'] = $profile;
            $this->viewData['gallery'] = ImageGalleries::getGalleriesByUser($profile->user->id);

            return view($this->theme . '/pages.profile_gallery', $this->viewData);
        } else {
            abort(404);
        }
    }

    public function addImage() {
        $this->viewData['inner_page'] = true;
        $this->viewData['user'] = Auth::user();
        $this->viewData['profile'] = Auth::user()->profile;

        return view($this->theme . '/pages.add_gallery_image', $this->viewData);
    }

    public function addImageProcess(Request $request) {
        $jsonData = [];
        $jsonData['ok'] = false;
        $jsonData['_token'] = csrf_token();

        $requestData = $request->only(['caption', 'image']);
        $errors = new MessageBag();

        $validator = Validator::make($requestData, [
            'caption' => 'required|min:2|max:128',
            'image' => 'required|image|mimes:jpeg,png'
        ]);

        if ($validator->fails()) {
            $jsonData['errors'] = $validator->errors();
            return response()->json($jsonData);
        }

        $image = $request->file('image');
        if($image->isValid()) {
            if (!file_exists(storage_path('app/gallery'))) {
                mkdir(storage_path('app/gallery'), 0777, true);
            }

            if (!file_exists(storage_path('app/gallery/thumbnails'))) {
                mkdir(storage_path('app/gallery/thumbnails'), 0777, true);
            }

            $image_name = uniqid(time()) . '.' . $image->getClientOriginalExtension();
            $image->move(storage_path('app/gallery'), $image_name);
            chmod(storage_path('app/gallery/' . $image_name), 0777);

            $thumbnail = Image::make(storage_path('app/gallery/'.$image_name));
            $thumbnail->resize(750, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $thumbnail->save(storage_path('app/gallery/thumbnails/'.$image_name), 100);
            chmod(storage_path('app/gallery/thumbnails/' . $image_name), 0777);

            $imageGallery = new ImageGalleries();
            $imageGallery->user_id = Auth::user()->id;
            $imageGallery->caption = CommonHelpers::titleCase(array_key_exists('caption', $requestData) ? $requestData['caption'] : '');
            $imageGallery->image = $image_name;
            $imageGallery->save();

            $jsonData['ok'] = true;
            $jsonData['image_id'] = $imageGallery->id;
            $jsonData['msg'] = trans('pages/add_gallery_image.messages.success');
            $jsonData['redirect'] = route('gallery_view_image', ['id' => $imageGallery->id]);
            $jsonData['redirect_edit'] = route('gallery_edit_image', ['id' => $imageGallery->id]);
            return response()->json($jsonData);
        } else {
            $errors->add('image', trans('pages/add_gallery_image.messages.errors.invalid_image'));
            $jsonData['errors'] = $errors;
            return response()->json($jsonData);
        }
    }

    public function editImage($id, Request $request) {
        session_start();

        $imageData = ImageGalleries::where('id', $id)->first();

        if($imageData) {
            if($imageData->user_id == Auth::user()->id || Auth::user()->role == 1) {
                $this->viewData['inner_page'] = true;
                $this->viewData['user'] = Auth::user();
                $this->viewData['profile'] = Auth::user()->profile;
                $this->viewData['imageData'] = $imageData;

                $fb = new Facebook([
                    'app_id' => env('FACEBOOK_APP_ID'),
                    'app_secret' => env('FACEBOOK_APP_SECRET'),
                    'default_graph_version' => 'v2.4',
                ]);

                $helper = $fb->getRedirectLoginHelper();

                if($request->get('code') !== null) {
                    try {
                        $accessToken = $helper->getAccessToken();
                    } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                        // When Graph returns an error
                        echo 'Graph returned an error: ' . $e->getMessage();
                        exit;
                    } catch(\Facebook\Exceptions\FacebookSDKException $e) {
                        // When validation fails or other local issues
                        echo 'Facebook SDK returned an error: ' . $e->getMessage();
                        exit;
                    }

                    if (! isset($accessToken)) {
                        if ($helper->getError()) {
                            header('HTTP/1.0 401 Unauthorized');
                            echo "Error: " . $helper->getError() . "\n";
                            echo "Error Code: " . $helper->getErrorCode() . "\n";
                            echo "Error Reason: " . $helper->getErrorReason() . "\n";
                            echo "Error Description: " . $helper->getErrorDescription() . "\n";
                        } else {
                            header('HTTP/1.0 400 Bad Request');
                            echo 'Bad request';
                        }
                        exit;
                    }

                    $oAuth2Client = $fb->getOAuth2Client();
                    $tokenMetadata = $oAuth2Client->debugToken($accessToken);
                    $tokenMetadata->validateAppId(env('FACEBOOK_APP_ID'));
                    // If you know the user ID this access token belongs to, you can validate it here
                    // $tokenMetadata->validateUserId('123');
                    // $tokenMetadata->validateExpiration();

                    if (! $accessToken->isLongLived()) {
                        // Exchanges a short-lived access token for a long-lived one
                        try {
                            $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
                        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
                            echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>";
                            exit;
                        }
                        echo '<h3>Long-lived</h3>';
                    }

                    $user = User::getUserByEmail(session('user')->email);
                    $profile = $user->profile();
                    $profile->update(['fb_access_token' => (string) $accessToken]);
                    Event::fire(new UserProfileWasUpdated($user));
                } else {
                    try {
                        $accessToken = $helper->getAccessToken();
                    } catch(\Facebook\Exceptions\FacebookResponseException $e) {
                        $user = User::getUserByEmail(session('user')->email);
                        $profile = $user->profile();
                        $profile->update(['fb_access_token' => null]);
                        Event::fire(new UserProfileWasUpdated($user));
                    }

                    $permissions = ['publish_actions'];
                    $loginUrl = $helper->getLoginUrl(route('gallery_edit_image', $id), $permissions);
                    $this->viewData['login_link'] = htmlspecialchars($loginUrl);
                }

                return view($this->theme . '/pages.edit_gallery_image', $this->viewData);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function editImageProcess($id, Request $request) {
        $imageData = ImageGalleries::getGalleryById($id);

        if($imageData) {
            if($imageData->user_id == Auth::user()->id || Auth::user()->role == 1) {
                $jsonData = [];
                $jsonData['ok'] = false;
                $jsonData['_token'] = csrf_token();

                $requestData = $request->only(['caption', 'image']);
                $errors = new MessageBag();

                $validator = Validator::make($requestData, [
                    'caption' => 'required|min:2|max:128',
                    'image' => 'image|mimes:jpeg,png'
                ]);

                if ($validator->fails()) {
                    $jsonData['errors'] = $validator->errors();
                    return response()->json($jsonData);
                }

                if($request->hasFile('image')) {
                    $image = $request->file('image');
                    if ($image->isValid()) {
                        if (!file_exists(storage_path('app/gallery'))) {
                            mkdir(storage_path('app/gallery'), 0777, true);
                        }

                        $image_name = uniqid(time()) . '.' . $image->getClientOriginalExtension();
                        $image->move(storage_path('app/gallery'), $image_name);
                        chmod(storage_path('app/gallery/' . $image_name), 0777);

                        $thumbnail = Image::make(storage_path('app/gallery/'.$image_name));
                        $thumbnail->resize(750, null, function ($constraint) {
                            $constraint->aspectRatio();
                        });
                        $thumbnail->save(storage_path('app/gallery/thumbnails/'.$image_name), 100);
                        chmod(storage_path('app/gallery/thumbnails/' . $image_name), 0777);

                        File::delete(storage_path('app/gallery/' . $imageData->image));
                        File::delete(storage_path('app/gallery/thumbnails/' . $imageData->image));

                        $imageData->caption = CommonHelpers::titleCase($requestData['caption']);
                        $imageData->image = $image_name;
                        $imageData->save();

                        $jsonData['ok'] = true;
                        $jsonData['msg'] = trans('pages/edit_gallery_image.messages.success');
                        $jsonData['redirect'] = route('gallery_view_image', ['id' => $imageData->id]);
                        return response()->json($jsonData);
                    } else {
                        $errors->add('image', trans('pages/edit_gallery_image.messages.errors.invalid_image'));
                        $jsonData['errors'] = $errors;
                        return response()->json($jsonData);
                    }
                } else {
                    $imageData->caption = CommonHelpers::titleCase($requestData['caption']);
                    $imageData->save();
                    $jsonData['ok'] = true;
                    $jsonData['msg'] = trans('pages/edit_gallery_image.messages.success');
                    $jsonData['redirect'] = route('gallery_view_image', ['id' => $imageData->id]);

                    if($request->has('facebook')) {
                        $access_token = Auth::user()->profile->fb_access_token;

                        $facebookData = array();
                        $facebookData['consumer_key'] = env('FACEBOOK_APP_ID');
                        $facebookData['consumer_secret'] = env('FACEBOOK_APP_SECRET');

                        $facebook = new FacebookPoster($facebookData);
                        $facebook->share($imageData->caption, $jsonData['redirect'], asset('storage/gallery/'.$imageData->image), trans('pages/edit_gallery_image.share_text', ['name' => Auth::user()->name]), $access_token);
                    }

                    return response()->json($jsonData);
                }
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function deleteImageProcess($id) {
        $imageData = ImageGalleries::getGalleryById($id);

        if($imageData) {
            if($imageData->user_id == Auth::user()->id || Auth::user()->role == 1) {
                File::delete(storage_path('app/gallery/' . $imageData->image));
                File::delete(storage_path('app/gallery/thumbnails/' . $imageData->image));
                $imageData->delete();

                return redirect()->route('view_user_gallery', ['slug' => Auth::user()->profile->slug]);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function hideImageProcess($id) {
        if(Auth::check() && Auth::user()->role == 1) {
            $imageData = ImageGalleries::where('id', $id)->first();

            if ($imageData) {
                if($imageData->hidden == 0) {
                    $imageData->hidden = 1;
                } else {
                    $imageData->hidden = 0;
                }
                $imageData->save();

                return redirect()->route('gallery_view_image', $imageData->id);
            } else {
                abort(404);
            }
        } else {
            abort(404);
        }
    }

    public function viewImage($id, Request $request) {
        $imageData = ImageGalleries::getGalleryNotNullCaption($id);

        if($imageData) {
            $this->viewData['inner_page'] = true;
            $this->viewData['user'] = $imageData->user;
            $this->viewData['profile'] = $imageData->user->profile;
            $this->viewData['image'] = $imageData;
            $this->viewData['social_url'] = route('gallery_view_image', $id);
            $this->viewData['social_description'] = $imageData->caption;
            $this->viewData['social_image'] = asset('storage/gallery/'.$imageData->image);
            $this->viewData['bookmarked'] = (Bookmark::getBookmark(2, $imageData->id)) ? true : false;

            return view($this->theme . '/pages.view_gallery_image', $this->viewData);
        } else {
            abort(404);
        }
    }

}
