<?php

namespace Foodstarz\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Foodstarz\Http\Requests;
use Foodstarz\Http\Controllers\Controller;
use Foodstarz\Helpers\CommonHelpers;
use Illuminate\Support\MessageBag;

use Foodstarz\Models\Background;
use Foodstarz\Models\User;

class BackgroundController extends Controller
{
    public function addImage(Request $request)
    {

        $jsonData = [];
        $jsonData['ok'] = false;
        $jsonData['_token'] = csrf_token();

        $requestData = $request->only(['image-background']);
        $errors = new MessageBag();

        $validator = \Validator::make($requestData, [
            'image-background' => 'required|image|mimes:jpeg,png'
        ]);

        if ($validator->fails()) {
            $jsonData['errors'] = $validator->errors();
            return response()->json($jsonData);
        }

        $image = $request->file('image-background');
        if ($image->isValid()) {

            $image_name = Background::saveImage($image);

            $background = User::find( Auth::user()->id)->background;

            if ($background) {
                $old_image = $background->image;

                $background->image = $image_name;

                if ($background->save()) {
                    \File::delete(storage_path( CommonHelpers::pathBackground() . $old_image));
                }
            } else {
                Background::create([
                    'user_id' => Auth::user()->id,
                    'image' => $image_name
                ]);
            }

            $jsonData['ok'] = true;

            return response()->json($jsonData);

        } else {
            $errors->add('image', trans('pages/add_gallery_image.messages.errors.invalid_image'));
            $jsonData['errors'] = $errors;

            return response()->json($jsonData);
        }
    }
}
