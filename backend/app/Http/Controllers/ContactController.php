<?php

namespace Foodstarz\Http\Controllers;

use Illuminate\Http\Request;
use Foodstarz\Http\Controllers\Controller;
use Config;
use Mail;
use Illuminate\Support\MessageBag;
use Validator;
use App;

class ContactController extends Controller {

    private $viewData = array();
    private $theme;

    public function __construct() {
        $this->viewData['inner_page'] = true;
        $this->theme = Config::get('custom.theme');
    }

    public function index() {
        return view($this->theme . '.pages.contact', $this->viewData);
    }

    public function send(Request $request) {
        $data = $request->only(['name', 'email', 'subject', 'message', 'g-recaptcha-response']);
        $errorsCheck = false;
        $errors = new MessageBag();
        $validator = Validator::make($data, [
                    'name' => 'required|min:3',
                    'email' => 'required|email',
                    'message' => 'required|min:3',
                    'g-recaptcha-response' => 'required|recaptcha'
        ]);

        if ($validator->fails()) {
            $request->flash();
            return redirect(route('contact_us'))->withErrors($validator);
        }

        Mail::send($this->theme . '.emails.'.App::getLocale().'.contacts', ['data' => $data], function ($message) use ($data) {
            $message->from($data['email'], $data['name']);
            $message->to(Config::get('custom.default_email'));
            $message->subject($data['subject']);
        });

        return redirect(route('contact_us'))->with('success', trans('pages/contacts.success_message'));
    }

}
