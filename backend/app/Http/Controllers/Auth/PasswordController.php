<?php

namespace Foodstarz\Http\Controllers\Auth;

use Foodstarz\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Foodstarz\Models\PasswordReset;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Validator;

class PasswordController extends Controller
{
    use DispatchesJobs,
        ValidatesRequests;
    
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectTo;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
        $this->redirectTo = route('homepage');
    }

    /**
     * Send a reset link to the given user.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postEmail(Request $request)
    {
        $this->validate($request, ['email' => 'required|email', 'g-recaptcha-response' => 'recaptcha']);

        $email = $request->only('email');
        $passwordReset = PasswordReset::where('email', $email)->first();

        if($passwordReset) {
            $response = 'already_sent';
        } else {
            $response = Password::sendResetLink($email, function (Message $message) {
                $message->subject($this->getEmailSubject());
            });
        }

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return redirect()->back()->with('status', trans('pages/password.msg.success'));

            case Password::INVALID_USER:
                return redirect()->back()->withErrors(['email' => trans($response)]);

            case 'already_sent':
                return redirect()->back()->with('error', trans('pages/password.msg.already_sent'));
        }
    }
}
