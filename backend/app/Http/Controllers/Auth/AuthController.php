<?php

namespace Foodstarz\Http\Controllers\Auth;

use Foodstarz\Models\User;
use Validator;
use Config;
use Auth;
use Foodstarz\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Http\Request;
use Foodstarz\Repositories\UserRepository;
use Foodstarz\Models\UserActivation;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;

class AuthController extends Controller {

    use DispatchesJobs,
        ValidatesRequests;
    /*
      |--------------------------------------------------------------------------
      | Registration & Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles the registration of new users, as well as the
      | authentication of existing users. By default, this controller uses
      | a simple trait to add these behaviors. Why don't you explore it?
      |
     */

use AuthenticatesAndRegistersUsers;

    protected $homepagePath;
    protected $registerPath;
    protected $loginPath;

    public function __construct(Request $request) {
        $this->middleware('guest', ['except' => 'getLogout']);

        $this->homepagePath = route('homepage');
        $this->registerPath = route('register');
        $this->loginPath = route('login');
        $this->request = $request;
    }

    protected function validator(array $data) {
        return Validator::make($data, [
                    'name' => 'required|min:3|max:64',
                    'email' => 'required|email|max:255|unique:users',
                    'password' => 'required|confirmed|min:6',
                    'culinary_profession' => 'required|in:amateur,blogger,professional',
                    'agreement' => 'accepted',
                    'g-recaptcha-response' => 'recaptcha'
        ]);
    }

    protected function create(array $data) {
        $data['role'] = Config::get('custom.default_user_role');
        $data['register_ip'] = $this->request->ip();

        return UserRepository::registerUser($data);
    }
    
    public function postRegister(Request $request) {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return view('/auth.register', array('errors' => $validator->errors()));
        }

        $user = $this->create($request->all());

        if ($user instanceof User) {
            return view('/auth.register', array('success' => trans('pages/register.messages.success')));
            //return redirect($this->registerPath)->with('success', trans('pages/register.messages.success'));
        } else {
            //return redirect($this->registerPath)->with('error', trans('pages/register.messages.error'));
            return view('/auth.register', array('error' => trans('pages/register.messages.error')));
        }

    }

    public function userActivate($key, Request $request) {
        $activation = UserActivation::where('activation_key', $key)->first();
        $error = false;
        if (!empty($activation)) {
            $user = $activation->user;
            $user->active = 1;

            if ($user->save()) {
                UserActivation::where('activation_key', $key)->delete();
            } else {
                $error = trans('pages/activation.messages.error');
            }
        } else {
            $error = trans('pages/activation.messages.error');
        }

        if ($error) {
            return redirect($this->registerPath)->with('error', $error);
        } else {
            //return redirect()->route('login')->with('success', trans('pages/activation.messages.success'))->with('email', $user->email);
            Auth::loginUsingId($user->id);
            
            $profile = $user->profile;

            if (!empty($user->name) && !empty($profile->gender) && !empty($profile->country) && !empty($profile->city) && !empty($profile->culinary_profession)) {
                $request->session()->put('profile_core_data', true);
            } else {
                $request->session()->put('profile_core_data', false);
            }

            if (!empty($profile->biography) && !empty($profile->work_experience) && !empty($profile->cooking_style)) {
                $request->session()->put('profile_chef_data', true);
            } else {
                $request->session()->put('profile_chef_data', false);
            }

            if(!session('profile_core_data')) {
                return redirect(route('profile_core_data'));
            } else if(empty(session('profile')->avatar)) {
                return redirect(route('upload_avatar'));
            } else if(!session('profile_chef_data')) {
                return redirect(route('profile_chef_data'));
            } else if(empty(session('profile')->interview)) {
                return redirect(route('interview'));
            } else {
                return redirect(route('view_user_chronic', ['slug' => $profile->slug]));
            }
        }
    }

    public function postLogin(Request $request) {
        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);

        $credentials = $this->getCredentials($request);

        $user = User::where('email', $credentials['email'])->first();
        if ($user) {
            if ($user->active == 1) {
                if (Auth::attempt($credentials, 1)) {
                    $profile = $user->profile;

                    if (!empty($user->name) && !empty($profile->gender) && !empty($profile->country) && !empty($profile->city) && !empty($profile->culinary_profession)) {
                        $request->session()->put('profile_core_data', true);
                    } else {
                        $request->session()->put('profile_core_data', false);
                    }

                    if (!empty($profile->biography) && !empty($profile->work_experience) && !empty($profile->cooking_style)) {
                        $request->session()->put('profile_chef_data', true);
                    } else {
                        $request->session()->put('profile_chef_data', false);
                    }

                    if(!session('profile_core_data')) {
                        return redirect(route('profile_core_data'));
                    } else if(empty(session('profile')->avatar)) {
                        return redirect(route('upload_avatar'));
                    } else if(!session('profile_chef_data')) {
                        return redirect(route('profile_chef_data'));
                    } else if(empty(session('profile')->interview)) {
                        return redirect(route('interview'));
                    } else {
                        return redirect(route('view_user_chronic', ['slug' => $profile->slug]));
                    }
                }
            } else {
                return redirect($this->loginPath)
                                ->withInput($request->only('email', 'remember'))
                                ->withErrors([
                                    'email' => trans('pages/login.messages.not_activated'),
                ]);
            }
        }

        return redirect($this->loginPath())
                        ->withInput($request->only('email', 'remember'))
                        ->withErrors([
                            'email' => $this->getFailedLoginMessage(),
        ]);
    }

}
