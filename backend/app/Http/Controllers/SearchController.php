<?php

namespace Foodstarz\Http\Controllers;

use Foodstarz\Models\User;
use Foodstarz\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Config;
use Vinkla\Vimeo\VimeoManager;

class SearchController extends Controller {

    private $viewData = array();
    private $theme;
    private $searchResources = [
        [
            "model" => "Foodstarz\Models\User",
            "name" => "Foodstarz",
            "searchField" => "name",
            "type" => "foodstar"
        ],
        [
            "model" => "Foodstarz\Models\Recipe",
            "name" => "Recipes",
            "searchField" => "title",
            "type" => "recipe"
        ],
        [
            "model" => "Foodstarz\Models\ImageGalleries",
            "name" => "Images",
            "searchField" => "caption",
            "type" => "image"
        ],
        [
            "model" => "Foodstarz\Models\Videos",
            "name" => "Videos",
            "searchField" => "title",
            "type" => "video"
        ],
    ];

    public function __construct() {
        $this->viewData['inner_page'] = true;
        $this->theme = Config::get('custom.theme');
    }

    public function search(Request $request, VimeoManager $vimeo) { // под вопросом стоит ли что то менять
        $query = (string) $request->get("q");
        foreach ($this->searchResources AS $key => $resource) {
            if($resource['name'] == 'Foodstarz') {
                $results = $resource['model']::where($resource['searchField'], "LIKE", '%' . $query . '%')->where('role', '!=', '3')->get();
            } else {
                $results = $resource['model']::where($resource['searchField'], "LIKE", '%' . $query . '%')->get();
            }
            $resource['results'] = $results;
            $resource['count'] = $results->count();

            if($resource['name'] == 'Videos') {
                $resource['videos_pictures'] = [];

                foreach($results as $video) {
                    $picture = $vimeo->request($video->video.'/pictures', [], 'GET');
                    $resource['videos_pictures'][$video->id] = $picture['body']['data'][0]['sizes'][3]['link'];
                }
            }

            $this->searchResources[$key] = $resource;
        }

        $this->viewData['searchResources'] = $this->searchResources;

        return view($this->theme . '.pages.search', $this->viewData);
    }

}
