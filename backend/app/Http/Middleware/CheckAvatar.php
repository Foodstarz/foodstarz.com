<?php

namespace Foodstarz\Http\Middleware;

use Closure;
use Session;

class CheckAvatar
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $profile = $request->session()->get('profile');

        if(!empty($profile->avatar) || !empty(Session::get('old_user'))) {
            $request->session()->flash('avatar', true);
        } else {
            if($request->isMethod('post')) {
                abort(403, 'Unauthorized action.');
            } else {
                $request->session()->flash('avatar', false);
            }
        }

        return $next($request);
    }
}
