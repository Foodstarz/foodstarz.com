<?php

namespace Foodstarz\Http\Middleware;

use Closure;
use Session;

class ChefData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $profile = $request->session()->get('profile');

        if((empty($profile->biography) || empty($profile->work_experience) || empty($profile->cooking_style)) && empty(Session::get('old_user'))) {
            if($request->isMethod('post')) {
                abort(403, 'Unauthorized action.');
            } else {
                $request->session()->flash('chefData', false);
            }
        } else {
            $request->session()->flash('chefData', true);
        }

        return $next($request);
    }
}
