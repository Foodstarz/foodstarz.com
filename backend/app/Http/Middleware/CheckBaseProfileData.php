<?php

namespace Foodstarz\Http\Middleware;

use Closure;
use Session;

class CheckBaseProfileData
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->session()->get('user');
        $profile = $request->session()->get('profile');

        if((!empty($user->name) && !empty($profile->gender) && !empty($profile->country) && !empty($profile->city) && !empty($profile->culinary_profession)) || !empty(Session::get('old_user'))) {
            $request->session()->flash('base_profile_data', true);
        } else {
            if($request->isMethod('post')) {
                abort(403, 'Unauthorized action.');
            } else {
                $request->session()->flash('base_profile_data', false);
            }
        }

        return $next($request);
    }
}
