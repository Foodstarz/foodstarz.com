<?php

namespace Foodstarz\Http\Middleware;

use Closure;
use Foodstarz\Helpers\UserHelpers;
use Auth;
use Session;

class ChefProfile
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!UserHelpers::isChefProfileDone(Auth::user()) && empty(Session::get('old_user'))) {
            return redirect()->route('homepage');
        }

        return $next($request);
    }
}
