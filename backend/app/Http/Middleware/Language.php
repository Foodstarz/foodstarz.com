<?php

namespace Foodstarz\Http\Middleware;

use Closure;
use Auth;
use Config;
use App;
use GeoIP;

class Language
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $available_languages = Config::get('custom.languages');
        if(Auth::check()) {
            $userLang = Auth::user()->profile->lang;

            if (empty($userLang)) {
                $user_ip = $request->ip;
                $geoip = GeoIP::getLocation($user_ip);

                if (array_key_exists(strtolower($geoip['isoCode']), $available_languages)) {
                    App::setLocale(strtolower($geoip['isoCode']));
                } else {
                    App::setLocale('en');
                }
            } else {
                App::setLocale($userLang);
            }
        } else {
            $user_ip = $request->ip;
            $geoip = GeoIP::getLocation($user_ip);
            if (array_key_exists(strtolower($geoip['isoCode']), $available_languages)) {
                App::setLocale(strtolower($geoip['isoCode']));
            } else {
                App::setLocale('en');
            }
        }

        return $next($request);
    }
}
