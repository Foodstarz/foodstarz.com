<?php

Route::get('/ads', function() {
    return view('ads');
});

Route::get('/', ['as' => 'homepage', 'uses' => 'HomepageController@index']);
Route::get('/login_back', ['as' => 'login_back', 'uses' => 'HomepageController@loginBack', 'middleware' => 'auth']);


//User System Routes
Route::get('user/register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
Route::post('user/register', ['as' => 'register_process', 'uses' => 'Auth\AuthController@postRegister']);
Route::get('user/activate/{key}', ['as' => 'user_activate', 'uses' => 'Auth\AuthController@userActivate']);
Route::get('user/login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
Route::post('user/login', ['as' => 'login_process', 'uses' => 'Auth\AuthController@postLogin']);
Route::get('user/logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);

// Password reset link request routes...
Route::get('user/forgotten-password', ['as' => 'forgotten_password', 'uses' => 'Auth\PasswordController@getEmail']);
Route::post('user/forgotten-password', ['as' => 'forgotten_password_process', 'uses' => 'Auth\PasswordController@postEmail']);

// Password reset routes...
Route::get('user/forgotten-password/reset/{token}', ['as' => 'password_reset', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('user/forgotten-password/reset', ['as' => 'password_reset_process', 'uses' => 'Auth\PasswordController@postReset']);

//Profile Routes
Route::get('user/profile/core-data', ['as' => 'profile_core_data', 'uses' => 'UserProfileController@coreDataForm', 'middleware' => 'auth']);
Route::post('user/profile/core-data', ['as' => 'profile_core_data_process', 'uses' => 'UserProfileController@coreDataFormProcess', 'middleware' => 'auth']);
Route::get('user/profile/upload-avatar', ['as' => 'upload_avatar', 'uses' => 'UserProfileController@uploadAvatar', 'middleware' => ['auth', 'baseProfileData']]);
Route::post('user/profile/upload-avatar', ['as' => 'upload_avatar_process', 'uses' => 'UserProfileController@uploadAvatarProcess', 'middleware' => ['auth', 'baseProfileData']]);
Route::get('user/profile/chef-data', ['as' => 'profile_chef_data', 'uses' => 'UserProfileController@chefDataForm', 'middleware' => ['auth', 'baseProfileData', 'avatar']]);
Route::post('user/profile/chef-data', ['as' => 'profile_chef_data_process', 'uses' => 'UserProfileController@chefDataFormProcess', 'middleware' => ['auth', 'baseProfileData', 'avatar']]);
Route::get('user/profile/interview', ['as' => 'interview', 'uses' => 'UserProfileController@interview', 'middleware' => ['auth', 'baseProfileData', 'avatar', 'chefData']]);
Route::post('user/profile/interview', ['as' => 'interview_process', 'uses' => 'UserProfileController@interviewProcess', 'middleware' => ['auth', 'baseProfileData', 'avatar', 'chefData']]);

Route::get('user/profile/settings', ['as' => 'profile_settings', 'uses' => 'UserProfileController@settings', 'middleware' => 'auth']);
Route::get('user/profile/settings/change-password', ['as' => 'change_password', 'uses' => 'UserProfileController@changePassword', 'middleware' => 'auth']);
Route::post('user/profile/settings/change-password', ['as' => 'change_password_process', 'uses' => 'UserProfileController@changePasswordProcess', 'middleware' => 'auth']);
Route::get('user/profile/settings/change-background', ['as' => 'change_background', 'uses' => 'UserProfileController@changeBackground', 'middleware' => 'auth']);
Route::post('user/profile/settings/change-background', ['as' => 'change_background_process', 'uses' => 'UserProfileController@changeBackgroundProcess', 'middleware' => 'auth']);
Route::get('user/profile/settings/close-account', ['as' => 'close_account', 'uses' => 'UserProfileController@closeAccount', 'middleware' => 'auth']);
Route::post('user/profile/settings/close-account', ['as' => 'close_account_process', 'uses' => 'UserProfileController@closeAccountProcess', 'middleware' => 'auth']);
Route::get('user/profile/settings/change-email', ['as' => 'change_email', 'uses' => 'UserProfileController@changeEmail', 'middleware' => 'auth']);
Route::post('user/profile/settings/change-email', ['as' => 'change_email_process', 'uses' => 'UserProfileController@changeEmailProcess', 'middleware' => 'auth']);
Route::get('user/profile/settings/language', ['as' => 'change_language', 'uses' => 'UserProfileController@changeLanguage', 'middleware' => 'auth']);
Route::post('user/profile/settings/language', ['as' => 'change_language_process', 'uses' => 'UserProfileController@changeLanguageProcess', 'middleware' => 'auth']);
Route::get('user/profile/settings/unsubscribe', ['as' => 'newsletter_subscription', 'uses' => 'UserProfileController@newsletter_subscription', 'middleware' => 'auth']);
Route::post('user/profile/settings/unsubscribe', ['as' => 'newsletter_subscription_process', 'uses' => 'UserProfileController@newsletter_subscriptionProcess', 'middleware' => 'auth']);

Route::get('user/{slug}', ['as' => 'view_user_profile', 'uses' => 'UserProfileController@viewUserProfile']);
Route::get('user/{slug}/chronic', ['as' => 'view_user_chronic', 'uses' => 'UserProfileController@viewUserChronic']);
Route::get('user/{slug}/interview', ['as' => 'view_user_profile_interview', 'uses' => 'UserProfileController@viewUserProfileInterview']);
Route::get('user/{slug}/recipes', ['as' => 'view_user_profile_recipes', 'uses' => 'UserProfileController@recipes']);
Route::get('user/{slug}/gallery', ['as' => 'view_user_gallery', 'uses' => 'GalleryController@userGallery']);
Route::get('user/{slug}/videos', ['as' => 'view_user_videos', 'uses' => 'VideosController@userVideos']);

//Recipes
Route::get('recipes', ['as' => 'recipes', 'uses' => 'RecipesController@recipes']);
Route::get('recipe/categories', ['as' => 'recipe_categories', 'uses' => 'RecipesController@listCategories']);
Route::get('recipe/category/{id}', ['as' => 'category_recipes', 'uses' => 'RecipesController@categoryRecipes']);
Route::get('recipe/add', ['as' => 'recipe_add', 'uses' => 'RecipesController@addRecipe', 'middleware' => ['auth']]);
Route::post('recipe/add', ['as' => 'recipe_add_process', 'uses' => 'RecipesController@addRecipeProcess', 'middleware' => ['auth']]);
Route::get('recipe/{slug}', ['as' => 'recipe_view', 'uses' => 'RecipesController@viewRecipe']);
Route::get('get_recipes/', ['as' => 'get_recipes', 'uses' => 'RecipesController@getRecipes']);
Route::get('recipe/delete/{id}', ['as' => 'recipe_delete', 'uses' => 'RecipesController@delete']);
Route::get('recipe/edit/{id}', ['as' => 'recipe_edit', 'uses' => 'RecipesController@editRecipe', 'middleware' => ['auth']]);
Route::post('recipe/edit/{id}', ['as' => 'recipe_edit_process', 'uses' => 'RecipesController@editRecipeProcess', 'middleware' => ['auth']]);
Route::get('recipe/delete-component-picture/{id}/{component}', ['as' => 'recipe_component_delete_picture', 'uses' => 'RecipesController@deleteRecipeComponentPicture', 'middleware' => ['auth']]);

//Gallery
Route::get('gallery', ['as' => 'gallery', 'uses' => 'GalleryController@gallery']);
Route::get('gallery/add/image', ['as' => 'gallery_add_image', 'uses' => 'GalleryController@addImage', 'middleware' => ['auth']]);
Route::post('gallery/add/image', ['as' => 'gallery_add_image_process', 'uses' => 'GalleryController@addImageProcess', 'middleware' => ['auth']]);
Route::get('gallery/edit/image/{id}', ['as' => 'gallery_edit_image', 'uses' => 'GalleryController@editImage', 'middleware' => ['auth']]);
Route::post('gallery/edit/image/{id}', ['as' => 'gallery_edit_image_process', 'uses' => 'GalleryController@editImageProcess', 'middleware' => ['auth']]);
Route::post('gallery/delete/image/{id}', ['as' => 'gallery_delete_image_process', 'uses' => 'GalleryController@deleteImageProcess', 'middleware' => ['auth']]);
Route::get('gallery/view/{id}', ['as' => 'gallery_view_image', 'uses' => 'GalleryController@viewImage']);
Route::post('gallery/hide/image/{id}', ['as' => 'gallery_hide_image_process', 'uses' => 'GalleryController@hideImageProcess', 'middleware' => ['auth']]);

//Videos
Route::get('videos', ['as' => 'videos', 'uses' => 'VideosController@videos']);
Route::get('videos/videos-null', ['as' => 'videos_null', 'uses' => 'VideosController@videosNull']);
Route::get('videos/add', ['as' => 'add_video', 'uses' => 'VideosController@addVideo', 'middleware' => ['auth']]);
Route::post('videos/add', ['as' => 'add_video_process', 'uses' => 'VideosController@addVideoProcess', 'middleware' => ['auth']]);
Route::get('videos/get_ticket', ['as' => 'get_vimeo_ticket', 'uses' => 'VideosController@getVimeoTicket', 'middleware' => ['auth']]);
Route::post('videos/completed', ['as' => 'completed_vimeo_upload', 'uses' => 'VideosController@completedVimeoUpload', 'middleware' => ['auth']]);
Route::get('videos/edit/{id}', ['as' => 'video_edit', 'uses' => 'VideosController@editVideo', 'middleware' => ['auth']]);
Route::post('videos/edit/{id}', ['as' => 'video_edit_process', 'uses' => 'VideosController@editVideoProcess', 'middleware' => ['auth']]);
Route::get('videos/view/{id}', ['as' => 'video_view', 'uses' => 'VideosController@viewVideo']);
Route::post('videos/delete/{id}', ['as' => 'video_delete_process', 'uses' => 'VideosController@deleteVideoProcess', 'middleware' => ['auth']]);
Route::post('videos/hide/{id}', ['as' => 'video_hide_process', 'uses' => 'VideosController@hideVideoProcess', 'middleware' => ['auth']]);

//Backgrounds
Route::post('background/add/image', ['as' => 'background_add_image', 'uses' => 'BackgroundController@addImage', 'middleware' => ['auth']]);

//Foodstarz
Route::get('chefs', ['as' => 'foodstarz_listing', 'uses' => 'FoodstarzController@listing']);

//Contact us
Route::get('contact-us/', ['as' => 'contact_us', 'uses' => 'ContactController@index']);
Route::post('contact-us/send', ['as' => 'contact_process', 'uses' => 'ContactController@send']);

//The jury
Route::get('the-jury', ['as' => 'the_jury', 'uses' => 'JuryController@index']);
Route::get('jury/vote/{id}/{v}', ['as' => 'jury_vote', 'uses' => 'JuryController@vote']);

//Search
Route::get('search/', ['as' => 'search', 'uses' => 'SearchController@search']);

//News
Route::get('news', ['as' => 'news', 'uses' => 'NewsController@news']);
Route::get('news/{slug}', ['as' => 'view_news', 'uses' => 'NewsController@viewNews']);

//Facebook
Route::get('facebook/callback', ['as' => 'facebook_login_callback', 'uses' => 'FacebookController@callback']);

//Admin Panel
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'isAdmin']], function() {
    Route::get('/', ['as' => 'admin_homepage', 'uses' => 'Admin\HomepageController@index']);
    Route::post('homepage_header/process', ['as' => 'homepage_header_process', 'uses' => 'Admin\HomepageController@homepageHeaderProcess']);
        Route::get('/clear_cache', ['as' => 'clear_cache', 'uses' => 'Admin\HomepageController@clear_cache', 'middleware' => 'auth']);
    //Manage content
    Route::post('manage/{action}/{type}/{id}', ['as' => 'manage_content', 'uses' => 'Admin\ManageController@index']);

    //Overview
    Route::get('overview/', ['as' => 'overview_main', 'uses' => 'Admin\OverviewController@index']);
    Route::get('overview/users', ['as' => 'overview_users', 'uses' => 'Admin\OverviewController@users']);
    Route::get('overview/countries', ['as' => 'overview_countries', 'uses' => 'Admin\OverviewController@countries']);
    Route::get('overview/compare', ['as' => 'overview_compare', 'uses' => 'Admin\OverviewController@compare']);
    Route::get('overview/compare_process', ['as' => 'compare_process', 'uses' => 'Admin\OverviewController@compare_process']);

    //Features
    Route::get('features/', ['as' => 'features_main', 'uses' => 'Admin\FeaturesController@index']);
    Route::post('features/switch_data', ['as' => 'features_switch_data', 'uses' => 'Admin\FeaturesController@switchData']);

    //Recipes
    Route::get('recipes/listing/', ['as' => 'recipes_listing', 'uses' => 'Admin\RecipesController@listing']);
    Route::get('recipes/search', ['as' => 'recipes_search', 'uses' => 'Admin\RecipesController@search']);
    Route::get('recipes/review/{id}', ['as' => 'recipe_review', 'uses' => 'Admin\RecipesController@review']);
    Route::get('recipes/approve/{id}/{status}', ['as' => 'recipe_approve', 'uses' => 'Admin\RecipesController@approve']);
    Route::post('recipes/watermark/{id}', ['as' => 'recipe_watermark', 'uses' => 'Admin\RecipesController@watermark']);
    Route::get('recipes/featured', ['as' => 'featured_recipes', 'uses' => 'Admin\RecipesController@featured']);
    Route::post('recipes/featured/add/{id}', ['as' => 'add_featured_recipe', 'uses' => 'Admin\RecipesController@addFeatured']);
    Route::post('recipes/featured/delete/{id}', ['as' => 'delete_featured_recipe', 'uses' => 'Admin\RecipesController@deleteFeatured']);

    //Common texts
    Route::get('common-texts/listing/', ['as' => 'c_pages_listing', 'uses' => 'Admin\CommonTextController@listing']);
    Route::get('common-texts/edit/{id}', ['as' => 'c_pages_edit', 'uses' => 'Admin\CommonTextController@edit']);
    Route::get('common-texts/delete/{id}', ['as' => 'c_pages_delete', 'uses' => 'Admin\CommonTextController@delete']);
    Route::post('common-texts/edit/process/{id}', ['as' => 'c_pages_edit_p', 'uses' => 'Admin\CommonTextController@editProcess']);
    Route::get('common-texts/add', ['as' => 'c_pages_add', 'uses' => 'Admin\CommonTextController@add']);
    Route::post('common-texts/add/process', ['as' => 'c_pages_add_p', 'uses' => 'Admin\CommonTextController@addProcess']);

    //Mass Email
    //Route::get('mass-email', ['as' => 'mass_email', 'uses' => 'Admin\EmailController@index']);
    //Route::post('mass-email', ['as' => 'mass_email_process', 'uses' => 'Admin\EmailController@process']);
    
    //Route::get('fill-featured', ['as' => 'fill_featured', 'uses' => 'Admin\RecipesController@fillFeatured']);
    //Route::get('fix-bios', ['as' => 'fix-bios', 'uses' => 'Admin\UsersController@fixBios']);
    //Route::get('fix-interviews', ['as' => 'fix-interviews', 'uses' => 'Admin\UsersController@fixInterviews']);
    //Route::get('add-new-recipes', ['as' => 'add-new-recipes', 'uses' => 'Admin\UsersController@addNewRecipes']);
    
    //News
    Route::get('news/listing', ['as' => 'news_listing', 'uses' => 'Admin\NewsController@listing']);
    Route::get('news/add', ['as' => 'add_news', 'uses' => 'Admin\NewsController@add']);
    Route::post('news/add/process', ['as' => 'add_news_process', 'uses' => 'Admin\NewsController@addProcess']);
    Route::get('news/edit/{id}', ['as' => 'edit_news', 'uses' => 'Admin\NewsController@edit']);
    Route::post('news/edit/{id}', ['as' => 'edit_news_process', 'uses' => 'Admin\NewsController@editProcess']);
    Route::post('news/delete/{id}', ['as' => 'delete_news_process', 'uses' => 'Admin\NewsController@deleteProcess']);
    
    //Menus
    Route::get('menus/{menu_id}', ['as' => 'menus_listing', 'uses' => 'Admin\MenusController@listing']);
    Route::get('menus/{menu_id}/add', ['as' => 'menus_add', 'uses' => 'Admin\MenusController@add']);
    Route::post('menus/{menu_id}/add/process', ['as' => 'menus_add_process', 'uses' => 'Admin\MenusController@addProcess']);
    Route::get('menus/{menu_id}/edit/{id}', ['as' => 'menus_edit', 'uses' => 'Admin\MenusController@edit']);
    Route::post('menus/{menu_id}/edit/{id}/process', ['as' => 'menus_edit_process', 'uses' => 'Admin\MenusController@editProcess']);
    Route::post('menus/{menu_id}/{id}', ['as' => 'menus_delete_process', 'uses' => 'Admin\MenusController@deleteProcess']);

    //Users
    Route::get('users/listing/', ['as' => 'users_listing', 'uses' => 'Admin\UsersController@listing']);
    Route::get('users/search', ['as' => 'users_search', 'uses' => 'Admin\UsersController@search']);
    Route::get('users/login-as/{id}', ['as' => 'users_login_as', 'uses' => 'Admin\UsersController@loginAs']);
    Route::get('users/delete/{id}', ['as' => 'users_delete', 'uses' => 'Admin\UsersController@delete']);
    Route::get('users/inactivate/{id}', ['as' => 'users_inactivate', 'uses' => 'Admin\UsersController@inactivate']);
    Route::get('users/edit/{id}', ['as' => 'users_edit', 'uses' => 'Admin\UsersController@edit']);

    //User Groups
    Route::get('users/groups', ['as' => 'users_groups', 'uses' => 'Admin\UsersController@groups']);
    Route::get('users/groups/{id}', ['as' => 'users_groups_edit', 'uses' => 'Admin\UsersController@groups_edit']);
    Route::post('users/groups/{id}', ['as' => 'users_groups_edit_process', 'uses' => 'Admin\UsersController@groups_edit_process']);
    Route::get('users/groups/add', ['as' => 'users_groups_add', 'uses' => 'Admin\UsersController@groups_create']);
    Route::post('users/groups/add', ['as' => 'users_groups_add_process', 'uses' => 'Admin\UsersController@groups_create_process']);
    Route::get('users/groups/delete/{id}', ['as' => 'users_groups_delete', 'uses' => 'Admin\UsersController@groups_delete']);


    //Instagram
    Route::get('users/instagram/', ['as' => 'users_instagram', 'uses' => 'Admin\InstagramController@index']);

    //Partners
    Route::get('partners/listing/', ['as' => 'partners_listing', 'uses' => 'Admin\PartnersController@listing']);
    Route::get('partners/add/', ['as' => 'partners_add', 'uses' => 'Admin\PartnersController@add']);
    Route::post('partners/add/process/', ['as' => 'partners_add_process', 'uses' => 'Admin\PartnersController@addProcess']);
    Route::get('partners/delete/{id}', ['as' => 'partners_delete', 'uses' => 'Admin\PartnersController@delete']);
    Route::get('partners/edit/{id}', ['as' => 'partners_edit', 'uses' => 'Admin\PartnersController@edit']);
    Route::post('partners/edit/process/{id}', ['as' => 'partners_edit_process', 'uses' => 'Admin\PartnersController@editProcess']);

    //Interview Questions
    Route::get('interview/questions/listing', ['as' => 'interview_questions_listing', 'uses' => 'Admin\InterviewController@listing']);
    Route::get('interview/questions/add', ['as' => 'interview_questions_add', 'uses' => 'Admin\InterviewController@add']);
    Route::post('interview/questions/add/process', ['as' => 'interview_questions_add_process', 'uses' => 'Admin\InterviewController@addProcess']);
    Route::get('interview/questions/edit/{id}', ['as' => 'interview_questions_edit', 'uses' => 'Admin\InterviewController@edit']);
    Route::post('interview/questions/edit/{id}/process', ['as' => 'interview_questions_edit_process', 'uses' => 'Admin\InterviewController@editProcess']);
    Route::post('interview/questions/delete/{id}', ['as' => 'interview_questions_delete_process', 'uses' => 'Admin\InterviewController@delete']);

    //Recipe Categories
    Route::get('recipes/categories/listing', ['as' => 'recipes_categories_listing', 'uses' => 'Admin\RecipesCategoriesController@listing']);
    Route::get('recipes/categories/add', ['as' => 'recipes_categories_add', 'uses' => 'Admin\RecipesCategoriesController@add']);
    Route::post('recipes/categories/add/process', ['as' => 'recipes_categories_add_process', 'uses' => 'Admin\RecipesCategoriesController@addProcess']);
    Route::get('recipes/categories/edit/{id}', ['as' => 'recipes_categories_edit', 'uses' => 'Admin\RecipesCategoriesController@edit']);
    Route::post('recipes/categories/edit/{id}/process', ['as' => 'recipes_categories_edit_process', 'uses' => 'Admin\RecipesCategoriesController@editProcess']);
    Route::post('recipes/categories/delete/{id}', ['as' => 'recipes_categories_delete_process', 'uses' => 'Admin\RecipesCategoriesController@delete']);

    //Newsletter
    Route::get('newsletter/list', ['as' => 'newsletter_export', 'uses' => 'Admin\NewsletterController@export']);
    Route::get('newsletter/change-list', ['as' => 'newsletter_change', 'uses' => 'Admin\NewsletterController@change']);
    Route::post('newsletter/change-list/process', ['as' => 'newsletter_change_process', 'uses' => 'Admin\NewsletterController@changeProcess']);

    //Bookmarks
    Route::get('bookmark/{type}/{id}', ['as' => 'bookmark_resource', 'uses' => 'Admin\BookmarkController@bookmark']);
    Route::get('bookmark/remove/{type}/{id}', ['as' => 'bookmark_remove_resource', 'uses' => 'Admin\BookmarkController@remove']);
    Route::get('bookmarks/{type}', ['as' => 'view_bookmarks', 'uses' => 'Admin\BookmarkController@view_bookmarks']);
    Route::get('bookmarks/share/{id}', ['as' => 'share_bookmark', 'uses' => 'Admin\BookmarkController@share']);
});

//Route::get('add_data', ['as' => 'add_data', 'uses' => 'SearchController@insertData']);
//Route::get('fix_data', ['as' => 'fix_data', 'uses' => 'SearchController@fixDateCat']);
//Common text pages
Route::get('/{slug}', ['as' => 'c_page', 'uses' => 'CommonTextController@view']);