<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class JuryVote extends Model {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'jury_votes';
    
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['recipe_id', 'jury_id', 'vote'];

    public function recipe() {
        return $this->belongsTo('Foodstarz\Models\RecipePrivate');
    }

    public function jury() {
        return $this->belongsTo('Foodstarz\Models\Jury');
    }

}
