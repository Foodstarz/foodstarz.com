<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class FeaturedRecipes extends Model
{
    protected $table = 'featured_recipes';
    public $timestamps = false;
    
    public function recipe() {
        return $this->belongsTo('Foodstarz\Models\Recipe');
    }

    public function bookmark() {
        return $this->hasOne('Foodstarz\Models\Bookmark', 'resource_id', 'recipe_id');
    }

    public static function getRecipe($id, $user_id = null) {

        $recipe = FeaturedRecipes::where('recipe_id', $id);

        if($user_id) $recipe->where('user_id', (int)$user_id);

        return $recipe->first();

    }
    
    public static function getLastRecipe($limit) {

        return FeaturedRecipes::orderBy('id', 'DESC')->limit($limit)->get();
            
    }

    public static function getAllRecipes($order = 'id', $sort = 'DESC') {

        return FeaturedRecipes::orderBy($order, $sort)->get();

    }

    public static function getRecipes($page_size = 9) {

        return FeaturedRecipes::orderBy('id', 'DESC')->paginate($page_size);

    }
}
