<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class Newsletter extends Model
{
    protected $table = 'newsletter';
    public $timestamps = false;

    protected $fillable = ['email', 'subscribe'];

    public static function getNewsLetterByEmail($email) {

        return Newsletter::where('email', $email)->first();

    }
}
