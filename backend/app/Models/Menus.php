<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{
    protected $table = 'menus';
    public $timestamps = false;

    public static function getMenu($menu, $order = 'order', $sort = 'ASC') {

        return Menus::where('menu', $menu)->orderBy($order, $sort)->get();

    }

    public static function getMenuById($id) {

        return Menus::where('id', $id)->first();

    }
}
