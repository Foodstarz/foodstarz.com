<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class Interview extends Model
{

    protected $table = 'interview';
    public $timestamps = false;
    
    public static function getInterviews($order = 'id', $sort = 'ASC', $page_size = 20) {

        return Interview::orderBy($order, $sort)->paginate($page_size);
        
    }

    public static function getInterview($id) {

        return Interview::where('id', $id)->first();

    }

}
