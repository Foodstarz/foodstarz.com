<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class Features extends Model {

    protected $table = 'features';

    protected $fillable = ['feature', 'user_group', 'status'];

    function users_group() {
        return $this->hasOne('Foodstarz\Models\UserGroup', 'id', 'user_group');
    }

    public static function getFeatures() {

        return Features::with('users_group')->get();

    }

    public static function getPermition($feature, $user_group) {
        $permition = Features::where('feature', $feature)->where('user_group', $user_group)->where('status', 1)->first();
        if($permition) return true;
        else return false;
    }

}
