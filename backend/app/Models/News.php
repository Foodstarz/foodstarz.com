<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $table = 'news';

    protected $fillable = ['slug', 'title', 'content'];

    public static function getNews($page_size = 20) {

        return News::orderBy('id', 'DESC')->paginate($page_size);

    }

    public static function getNewsById($id) {

        return News::where('id', $id)->first();

    }

    public static function getNewsBySLug($slug) {

        return News::where("slug", (string) $slug)->first();

    }
    
    public static function countNewsBySlug($news_slug) {
        
        return News::where('slug', $news_slug)->count();
        
    }
    
}
