<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class UserToGroup extends Model
{

    protected $table = 'user_to_group';

    public function groups() {
        return $this->hasOne('Foodstarz\Models\UserGroup', 'id', 'group_id');
    }

    public static function getGroupsByUser($user_id) {
        return UserToGroup::with('groups')->where('user_id', $user_id)->get();
    }

}
