<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{

    protected $table = 'user_groups';

    function users() {
        return $this->hasMany('Foodstarz\Models\UserToGroup', 'group_id', 'id');
    }

    public static function getGroups() {
        return UserGroup::all();
    }

    public static function getGroup($id) {
        return UserGroup::with('users')->where('id', $id)->first();
    }

    public static function updateGroup($id, $data) {
        return UserGroup::where('id', $id)->update($data);
    }

}
