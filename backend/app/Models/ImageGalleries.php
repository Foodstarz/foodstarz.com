<?php

namespace Foodstarz\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ImageGalleries extends Model
{
    protected $table = 'image_galleries';

    public function user() {
        return $this->belongsTo('Foodstarz\Models\User');
    }

    public static function getLastImages($limit) {

        return ImageGalleries::where('hidden', 0)
            ->whereNotNull('caption')
            ->join('users', 'image_galleries.user_id', '=', 'users.id')
            ->leftJoin('bookmarks', function ($join) {
                $join->on('bookmarks.resource_id', '=', 'image_galleries.id')
                    ->where('bookmarks.resource_type', '=', 2);
            })->select('image_galleries.*', 'users.role', 'bookmarks.id as bookmark')
            ->where('hidden', 0)
            ->where('caption', '!=', '')
            ->where('users.role', '!=', 3)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();

    }

    public static function getGallery($page_size = 9) {

        $gallery = ImageGalleries::whereNotNull('caption')
            ->join('users', 'image_galleries.user_id', '=', 'users.id')
            ->leftJoin('bookmarks', function ($join) {
                $join->on('bookmarks.resource_id', '=', 'image_galleries.id')
                    ->where('bookmarks.resource_type', '=', 2);
            })->select('image_galleries.*', 'users.role', 'bookmarks.id as bookmark')
            ->where('hidden', 0)
            ->where('caption', '!=', '')
            ->where('users.role', '!=', 3)
            ->orderBy('created_at', 'desc');

        return $gallery->paginate($page_size);

    }

    public static function getGalleriesByUser($user_id, $page_size = 9) {

        return ImageGalleries::where('user_id', $user_id)
            ->whereNotNull('caption')
            ->where('caption', '!=', '')
            ->orderBy('created_at', 'DESC')
            ->paginate($page_size);

    }
    
    public static function getGalleryById($id) {
        
        return ImageGalleries::where('id', $id)->first();
        
    }
    
    public static function getGalleriesWithCaption($user_id) {

        return ImageGalleries::where('user_id', $user_id)
            ->where('hidden', 0)
            ->whereNotNull('caption')
            ->get();

    }

    public static function getUserIds($date = '')
    {
        if($date == '') $date = Carbon::now();
        return ImageGalleries::select('user_id')->groupBy('user_id')->whereDate('created_at', '<', $date)->get();
    }

    public static function getUserIdsPeriod($date1 = '', $date2 = '')
    {
        if($date1 == '') $date1 = Carbon::now();
        if($date2 != '') $date2 = Carbon::now();

        return ImageGalleries::select('user_id')->groupBy('user_id')->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->get();

    }

    public static function getGalleryNotNullCaption($id) {

        return ImageGalleries::where('id', $id)->whereNotNull('caption')->first();

    }
}
