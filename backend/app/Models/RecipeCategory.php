<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class RecipeCategory extends Model
{

    protected $table = 'recipe_categories';

    public $timestamps = false;

    public static function getCategories($order = null, $sort = 'ASC', $page_size = null) {

        if($order == null) return RecipeCategory::all();
        else {
            $categories = RecipeCategory::orderBy($order, $sort);
            if($page_size == null) return $categories->get();
            else return $categories->paginate($page_size);
        }

    }

    public static function getCategory($id) {

         return RecipeCategory::where('id', '=', $id)->first();

    }
}
