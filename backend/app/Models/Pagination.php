<?php

namespace Foodstarz\Models;

use Config;
use Illuminate\Database\Eloquent\Model;

class Pagination extends Model
{

    public static function render($data) {

        echo view(Config::get('custom.theme') . '.pagination', ['data' => $data])->render();

    }

}
