<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class Partner extends Model {

    protected $table = 'partners';
    
    public $timestamps = false;

    protected $fillable = ['name', 'url', 'image'];

    public static function getHomepageParnters($type, $limit) {

        return Partner::where('type', $type)->limit($limit)->get();

    }
    
    public static function getPartners($page_size = 20) {
    
        return Partner::paginate($page_size);
        
    }

    public static function getPartner($id) {

        return Partner::where('id', $id)->first();

    }

    public static function updatePartner($id, $update) {

        return Partner::where('id', $id)->update($update);

    }

    public static function delPartner($id) {

        return Partner::find($id)->delete();

    }

}
