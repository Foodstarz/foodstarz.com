<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class CommonText extends Model {

    protected $table = 'common_texts';

    public $timestamps = false;

    protected $fillable = ['slug', 'title', "page"];

    public static function getTextBySlug($slug) {

        return CommonText::where("slug", "=", (string) $slug)->first();

    }
    
    public static function getTextById($id) {

        return CommonText::where('id', '=', $id)->first();
        
    }

    public static function updateTextById($id, $update) {

        return CommonText::where("id", "=", $id)->update($update);

    }

    public static function delTextById($id) {

        return CommonText::find($id)->delete();

    }

}
