<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_roles';
    
    /**
     * Users relationship.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users() {
        return $this->hasMany('Foodstarz\Models\User');
    }

    public static function getGroups() {
        return UserRole::all();
    }

}
