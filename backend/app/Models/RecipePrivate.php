<?php

namespace Foodstarz\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class RecipePrivate extends Model
{

    protected $table = 'recipes_private';

    protected $fillable = [
        'slug',
        'user_id',
        'title',
        'category',
        'portions',
        'preparation_time',
        'difficulty',
        'components',
        'main_image_original',
        'main_image_watermarked',
        //'watermark',
        'status'
    ];

    public function user() {
        return $this->belongsTo('Foodstarz\Models\User');
    }
    
    public static function getRecipeBySlug($slug) {
        
        return RecipePrivate::where('slug', $slug)->first();
        
    }

    public static function getRecipes($page_size = 20, $status = null, $order = null, $sort = 'ASC') {

        $recipes = RecipePrivate::select('*');

        if($status) $recipes->where('status', $status);
        if($order) $recipes->orderBy($order, $sort);

        if($page_size == 0) return $recipes->get();
        else return $recipes->paginate($page_size);


    }

    public static function getRecipe($id, $user_id = null) {

        $recipe = RecipePrivate::where('id', $id);

        if($user_id) $recipe->where('user_id', (int)$user_id);

        return $recipe->first();

    }

    public static function getRecipesByUser($user_id, $status = null) {

        $recipes = RecipePrivate::where('user_id', $user_id);

        if($status) $recipes->where('status', (int)$status);

        return $recipes->get();

    }
    
    public static function updateRecipe($id, $data) {
        
        return RecipePrivate::where('id', $id)->update($data);
        
    }
    
    public static function searchRecipes($search_title, $search_author, $page_size = 20) {

        $recipes = RecipePrivate::leftJoin('users', 'recipes_private.user_id', '=', 'users.id')
            ->select('recipes_private.id', 'recipes_private.slug', 'recipes_private.title', 'recipes_private.user_id', 'recipes_private.status');

            if(!empty($search_title)) $recipes->where('title', 'LIKE', '%'.$search_title.'%');
            if(!empty($search_author)) $recipes->where('users.name', 'LIKE', '%'.$search_author.'%');

        return $recipes->paginate($page_size);

    }

    public static function getUserIds($date = '')
    {
        if($date == '') $date = Carbon::now();
        return RecipePrivate::select('user_id')->groupBy('user_id')->whereDate('created_at', '<', $date)->get();
    }

    public static function getUserIdsPeriod($date1 = '', $date2 = '')
    {
        if($date1 == '') $date1 = Carbon::now();
        if($date2 != '') $date2 = Carbon::now();

        return RecipePrivate::select('user_id')->groupBy('user_id')->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->get();

    }

}
