<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;
use Foodstarz\Helpers\CommonHelpers;

class Background extends Model
{
    protected $fillable = [
        'user_id',
        'image'
    ];

    /*
     * param UploadedFile $image
     */
    public static function saveImage($image){

        if (!file_exists(storage_path( CommonHelpers::pathBackground() ))) {
            mkdir(storage_path( CommonHelpers::pathBackground() ), 0777, true);
        }

        $image_name = uniqid(time()) . '.' . $image->getClientOriginalExtension();
        $image->move(storage_path( CommonHelpers::pathBackground() ), $image_name);
        chmod(storage_path( CommonHelpers::pathBackground() . $image_name), 0777);

        return CommonHelpers::convertImageToJPG($image_name);
    }
}
