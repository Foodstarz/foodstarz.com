<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class UserActivation extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user_activations';

    public $timestamps = false;

    public function user() {
        return $this->belongsTo('Foodstarz\Models\User');
    }
}
