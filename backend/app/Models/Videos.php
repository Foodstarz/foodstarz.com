<?php

namespace Foodstarz\Models;

use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Videos extends Model
{
    protected $table = 'videos';

    public function user() {
        return $this->belongsTo('Foodstarz\Models\User');
    }

    public static function getVideo($id, $not_null = null) {

        $video = Videos::where('id', $id);

        if($not_null) $video->whereNotNull($not_null);

        return $video->first();

    }

    public static function getLastVideos($limit) {

        return Videos::where('hidden', 0)
            ->whereNotNull('title')
            ->join('users', 'videos.user_id', '=', 'users.id')
            ->leftJoin('bookmarks', function ($join) {
                $join->on('bookmarks.resource_id', '=', 'videos.id')
                    ->where('bookmarks.resource_type', '=', 3);
            })->select('videos.*', 'users.role', 'bookmarks.id as bookmark')
            ->where('hidden', 0)
            ->where('title', '!=', '')
            ->where('users.role', '!=', 3)
            ->limit($limit)
            ->orderBy('created_at', 'desc')
            ->get();

    }

    public static function getVideosByUser($user_id) {

        return Videos::where('user_id', $user_id)
            ->where('hidden', 0)
            ->whereNotNull('title')
            ->where('title', '!=', '')
            ->get();

    }

    public static function getAllVideosByUser($user_id, $order = 'created_at', $sort = 'DESC', $page_size = 9) {

        return Videos::where('user_id', $user_id)
            ->whereNotNull('title')
            ->where('title', '!=', '')
            ->orderBy($order, $sort)
            ->paginate($page_size);

    }

    public static function getVideos($page_size = 9) {

        $videos = Videos::whereNotNull('title')
            ->join('users', 'videos.user_id', '=', 'users.id')
            ->leftJoin('bookmarks', function ($join) {
                $join->on('bookmarks.resource_id', '=', 'videos.id')
                    ->where('bookmarks.resource_type', '=', 3);
            })->select('videos.*', 'users.role', 'bookmarks.id as bookmark')
                ->where('hidden', 0)
                ->where('title', '!=', '')
                ->where('users.role', '!=', 3)
                ->orderBy('updated_at', 'desc');

        return $videos->paginate($page_size);

    }

    public static function getUserIds($date = '')
    {
        if($date == '') $date = Carbon::now();

        return Videos::select('user_id')->groupBy('user_id')->whereDate('created_at', '<', $date)->get();
    }

    public static function getUserIdsPeriod($date1 = '', $date2 = '')
    {
        if($date1 == '') $date1 = Carbon::now();
        if($date2 != '') $date2 = Carbon::now();

        return Videos::select('user_id')->groupBy('user_id')->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->get();

    }
}
