<?php

namespace Foodstarz\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{

    protected $table = 'recipes';

    protected $fillable = [
        'slug',
        'user_id',
        'title',
        'category',
        'portions',
        'preparation_time',
        'difficulty',
        'components',
        'main_image_original',
        'main_image_watermarked',
        //'watermark',
        'status'
    ];

    public function user() {
        return $this->belongsTo('Foodstarz\Models\User');
    }

    public static function countRecipeBySlug($slug) {

        return Recipe::where('slug', $slug)->count();

    }

    public static function countRecipeByUser($user_id) {

        return Recipe::where("user_id", $user_id)->count();

    }

    public static function getRecipe($id, $user_id = null) {

        $recipe = Recipe::where('id', $id);

        if($user_id) $recipe->where('user_id', (int)$user_id);

        return $recipe->first();

    }

    public static function getRecipes() {

        $recipe = ImageGalleries::whereNotNull('title')
            ->join('users', 'recipes.user_id', '=', 'users.id')
            ->leftJoin('bookmarks', function ($join) {
                $join->on('bookmarks.resource_id', '=', 'recipes.id')
                    ->where('bookmarks.resource_type', '=', 1);
            })->select('recipes.*', 'users.role', 'bookmarks.id as bookmark')
            ->where('status', 2)
            ->where('title', '!=', '')
            ->where('users.role', '!=', 3)
            ->orderBy('recipes.created_at', 'ASC');

        return $recipe->get();

    }

    public static function getRecipeBySlug($slug) {

        return Recipe::where('slug', $slug)->first();

    }

    public static function getRecipesByCat($id, $page_size = 9) {

        return Recipe::where('category', 'LIKE', '%' . $id . '%')->where('status', 2)->orderBy('updated_at', 'DESC')->paginate($page_size);

    }

    public static function getRecipesByNotIn($ids, $order = 'created_at', $sort = 'DESC', $page_size = 20) {

        return Recipe::whereNotIn('id', $ids)->where('status', 2)->orderBy($order, $sort)->paginate($page_size);

    }

    public static function getRecipesByUser($user_id, $status = null) {

        $recipes = Recipe::where('user_id', $user_id);

        if($status) $recipes->where('status', (int)$status);

        return $recipes->get();

    }

    public static function updateRecipe($id, $data) {

        return Recipe::where('id', $id)->update($data);

    }

    public static function getUserIds($date = '')
    {
        if($date == '') $date = Carbon::now();
        return Recipe::select('user_id')->groupBy('user_id')->whereDate('created_at', '<', $date)->get();
    }

    public static function getUserIdsPeriod($date1 = '', $date2 = '')
    {
        if($date1 == '') $date1 = Carbon::now();
        if($date2 != '') $date2 = Carbon::now();

        return Recipe::select('user_id')->groupBy('user_id')->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->get();

    }
}
