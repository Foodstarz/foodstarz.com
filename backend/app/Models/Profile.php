<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Profile extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    protected $fillable = [
        'user_id',
        'slug',
        'avatar',
        'culinary_profession',
        'gender',
        'city',
        'country',
        'biography',
        'work_experience',
        'awards',
        'organisations',
        'role_models',
        'cooking_style',
        'kitchen_skills',
        'business_skills',
        'personal_skills',
        'search',
        'offer',
        'website',
        'instagram',
        'facebook',
        'twitter',
        'pinterest',
        'tumblr',
        'lang'
    ];

    public function user() {
        return $this->belongsTo('Foodstarz\Models\User');
    }

    public static function getProfileBySlug($slug) {

        return Profile::where('slug', $slug)->first();

    }

    public static function getUsersGroupCountry($date = '') {

        if($date == '') $date = Carbon::now();

        return Profile::select('country')->whereDate('created_at', '<=', $date)->get();

    }

    public static function getUsersGroupCountryPeriod($date1 = '', $date2 = '') {

        if($date1 == '') $date1 = Carbon::now();
        if($date2 == '') $date2 = Carbon::now();

        return Profile::select('country')->whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->get();

    }

    public static function countByDate($date, $char = '>', $date2, $char2 = '<') {

        return Profile::whereDate('created_at', $char, $date)->whereDate('created_at', $char2, $date2)->count();

    }

    public static function countByPeriod($date1, $date2) {

        return Profile::whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2)->count();

    }

    public static function countProfilesWithData($date = '', $columns = []) {

        if($date == '') $date = Carbon::now();

        $profiles = Profile::whereDate('created_at', '<', $date);

        foreach($columns as $column) {
            $profiles->whereNotNull($column);
        }
        
        return $profiles->count();

    }

    public static function countProfilesWithPeriod($date1 = '', $date2 = '', $columns = []) {

        if($date1 == '') $date1 = Carbon::now();
        if($date2 == '') $date2 = Carbon::now();

        $profiles = Profile::whereDate('created_at', '>=', $date1)->whereDate('created_at', '<=', $date2);

        foreach($columns as $column) {
            $profiles->whereNotNull($column);
        }

        return $profiles->count();

    }

}
