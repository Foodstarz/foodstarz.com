<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class Bookmark extends Model
{
    protected $table = 'bookmarks';

    public static function getBookmark($type, $resource_id) {

        return Bookmark::where('resource_type', $type)->where('resource_id', $resource_id)->first();

    }

    public static function getBookmarksByType($type, $page_size = 15) {

        return Bookmark::where('resource_type', $type)->paginate($page_size);

    }

    public static function getBookmarkById($id) {

        return Bookmark::where('id', $id)->first();

    }
}
