<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class InfoWidget extends Model
{
    protected $table = 'info_widget';
    public $timestamps = false;

    public static function getWidgetByItem($item) {

        return InfoWidget::where('item', $item)->first();

    }

}
