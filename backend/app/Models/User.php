<?php

namespace Foodstarz\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Foodstarz\Helpers\UserHelpers;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable,
        CanResetPassword;

    protected $table = 'users';

    protected $fillable = ['name', 'email', 'password', 'role', 'active', 'register_ip', 'last_ip', 'role_changed'];

    protected $hidden = ['password', 'role', 'register_ip', 'last_ip', 'remember_token', 'role_changed'];

    public function background(){
        return $this->hasOne('Foodstarz\Models\Background');
    }

    public function profile() {
        return $this->hasOne('Foodstarz\Models\Profile');
    }

    public function Videos() {
        return $this->hasMany('Foodstarz\Models\Videos');
    }

    public function Images() {
        return $this->hasMany('Foodstarz\Models\ImageGalleries');
    }

    public function Recipes() {
        return $this->hasMany('Foodstarz\Models\Recipe');
    }

    public function role() {
        return $this->belongsTo('Foodstarz\Models\UserRole');
    }

    public function hasRecipes() {
        return (Recipe::countRecipeByUser($this->id) > 0) ? true : false;
    }
    
    public function getApprovedRecipes() {
        return Recipe::where("user_id", $this->id)
            ->where("status", 2)
            ->orderBy('created_at', 'DESC')
            ->get();
    }

    public function countRecipes($status) {
        return Recipe::where("user_id", $this->id)
            ->where("status", $status)
            ->count();
    }

    public function setNameAttribute($value) {
        $this->attributes['name'] = UserHelpers::usernameFix($value);
    }
    
    public static function getUsers($roles = [1,2,4], $order = 'name', $sort = 'asc',$page_size = 9) {

        return User::whereIn('role', $roles)->orderBy($order, $sort)->paginate($page_size);
        
    }

    public static function getList($list = []) {

        return User::all($list);

    }

    public static function getHomepageUsers($role, $limit) {

        return  User::where("role", "=", $role)
            ->where("active", "=", 1)
            ->limit($limit)
            ->orderBy('role_changed', 'desc')
            ->get();

    }
    
    public static function getProfileBySlug($slug) {
        
        return User::where('slug', $slug)->first();
        
    }
    
    public static function getUser($id) {

        return User::where('id', $id)->first();
        
    }

    public static function getUserByEmail($email) {

        return User::where('email', $email)->first();

    }

    public static function getUserByRole($char = '!=', $role) {

        return User::where("role", $char, $role)->get();

    }

    public static function updateUser($id, $update) {

        return User::where("id", $id)->update($update);

    }

    public static function delUser($id) {

        return User::find($id)->delete();

    }

    public static function getListing($order = null, $sort = null, $page_size = 20) {

        $users = User::leftJoin('user_roles', 'users.role', '=', 'user_roles.id')
            ->leftJoin('profiles', 'users.id', '=', 'profiles.user_id')
            ->select("users.id", "users.name", "users.email", 'users.active', "user_roles.title AS title", "users.created_at as created_at")
            ->with('videos')
            ->with('images');

        if($order) $users->orderBy($order, $sort);
        else $users->orderBy('users.name', 'asc');

        return $users->paginate($page_size);

    }

    public static function getStatistics($page_size = 50) {

        return User::with('profile')->with('videos')->with('images')->with('recipes')->get();

    }

    public static function searchUser($search_name, $search_email, $page_size = 20) {

        $users = User::leftJoin('user_roles', 'users.role', '=', 'user_roles.id')
            ->select("*", "users.id as id", "user_roles.title AS title");

            if(!empty($search_name)) $users->where('name', 'LIKE', '%'.$search_name.'%');
            if(!empty($search_email)) $users->where('email', 'LIKE', '%'.$search_email.'%');

        return $users->paginate($page_size);

    }

    public static function getUsersByCountry($key) {

        return User::whereHas('profile', function ($query) use ($key) {
            $query->where('country', $key);
        })->with('videos')->with('recipes')->with('images')->get();
    }

}
