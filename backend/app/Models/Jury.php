<?php

namespace Foodstarz\Models;

use Illuminate\Database\Eloquent\Model;

class Jury extends Model {

    protected $table = 'jury';

    protected $fillable = ['user_id', 'text'];

    
    public function user() {
        return $this->belongsTo('Foodstarz\Models\User');
    }

    public static function getJuryByUser($user_id) {

        return Jury::where("user_id", $user_id)->first();

    }

}
