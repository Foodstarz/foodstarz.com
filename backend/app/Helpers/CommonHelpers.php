<?php

namespace Foodstarz\Helpers;
use Foodstarz\Models\Partner;
use Foodstarz\Models\Profile;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Imagine\Gd\Imagine;

use Illuminate\Http\Request;

class CommonHelpers {

    protected static $include_route_name = [
        'view_user_chronic',
        'view_user_profile',
        'view_user_profile_interview',
        'view_user_profile_recipes',
        'view_user_gallery',
        'view_user_videos',
        'profile_chef_data',
        'profile_settings',
        'profile_core_data',
        'upload_avatar'
    ];

    protected static $pathBackground = 'storage/background/';

    public static function titleCase($string, $delimiters = array(" ", "-", ".", "'", "O'", "Mc"), $exceptions = array("and", "to", "of", "on", "with", "from", "das", "dos", "I", "II", "III", "IV", "V", "VI", "s")) {

        /*
         * Exceptions in lower case are words you don't want converted
         * Exceptions all in upper case are any words you don't want converted to title case
         *   but should be converted to upper case, e.g.:
         *   king henry viii or king henry Viii should be King Henry VIII
         */
        $string = mb_convert_case($string, MB_CASE_TITLE, "UTF-8");
        foreach ($delimiters as $dlnr => $delimiter) {
            $words = explode($delimiter, $string);
            $newwords = array();
            foreach ($words as $wordnr => $word) {
                if (in_array(mb_strtoupper($word, "UTF-8"), $exceptions)) {
                    // check exceptions list for any words that should be in upper case
                    $word = mb_strtoupper($word, "UTF-8");
                } elseif (in_array(mb_strtolower($word, "UTF-8"), $exceptions)) {
                    // check exceptions list for any words that should be in upper case
                    $word = mb_strtolower($word, "UTF-8");
                } elseif (!in_array($word, $exceptions)) {
                    // convert to uppercase (non-utf8 only)
                    $word = ucfirst($word);
                }
                array_push($newwords, $word);
            }
            $string = join($delimiter, $newwords);
        }//foreach
        return strtr($string, array('.' => ' ', '\'S' => '\'s'));
    }

    public static function getSponsoringPartners() {
        return Partner::where('type', '1')->limit(3)->get();
    }

    public static function pathBackground(){
        return 'app/background/';
    }

    public static function convertImageToJPG($fileImage){
        $new_ext = 'jpg';

        //delete old ext
        $file_name_arr = explode('.', $fileImage);
        $ext = array_pop($file_name_arr);
        $file_name = implode(",", $file_name_arr);

        //save new image
        $file_jpg = $file_name . "." . $new_ext;
        $imagine = new Imagine();
        $imagine->open( storage_path( CommonHelpers::pathBackground() . $fileImage) )
                ->save( storage_path( CommonHelpers::pathBackground() . $file_jpg) , array('jpeg_quality' => 80));

        if($ext !== $new_ext){
            unlink( storage_path( CommonHelpers::pathBackground() . $fileImage ));
        }

        return $file_jpg;
    }

    public static function showCameraUpload($route_name){

        if (Auth::check()) {

            if (in_array($route_name, self::$include_route_name)) {

                $arr = \Route::current()->parameters();

                if (!empty($arr)) {

                    $slug = array_shift($arr);

                    $chef_name = Profile::getProfileBySlug($slug)->slug;

                    if ($chef_name == Auth::user()->profile->slug) {
                        return true;
                    }
                }
                else{
                    return true;
                }
            }
        }

        return false;
    }

    public static function setBackground($route_name){

        $background = false;

        if ( in_array($route_name, self::$include_route_name) ){

            $arr = \Route::current()->parameters();


            if ( !empty($arr) ) {

                $slug = array_shift($arr);

                $background = Profile::getProfileBySlug($slug)->user->background;
            }
            else{
                $background = Auth::user()->background;
            }

            //if $background == null
            $background = $background
                                    ? self::$pathBackground . $background->image
                                    : false;//'assets/images/bg-auth-' . rand(1, 11).'.jpg';
        }

        return $background;
    }

}