<?php

namespace Foodstarz\Helpers;
use Foodstarz\Models\InfoWidget;
use Foodstarz\Models\Recipe;
use Foodstarz\Models\ImageGalleries;
use Foodstarz\Models\Videos;

class UserHelpers {

    /**
     * Checks if the user's profile is done.
     *
     * @return bool
     */
    public static function isChefProfileDone($user) {
        if(empty($user->name) || empty($user->profile->gender) || empty($user->profile->culinary_profession) || empty($user->profile->city) || empty($user->profile->country)
                || empty($user->profile->avatar) || empty($user->profile->biography) || empty($user->profile->work_experience) || empty($user->profile->interview)) {
            return false;
        } else {
            return true;
        }

        /*
        if(!session('profile_core_data') || empty(session('profile')->avatar) || !session('profile_chef_data') || empty(session('profile')->interview)) {
            return false;
        } else {
            return true;
        }
         *
         */
    }

    public static function userActivity($user) {
        $date = new \DateTime();
        $date->modify('-14 days');
        $recipes = Recipe::where('user_id', $user->id)->where('created_at', '>', $date)->count();
        $images = ImageGalleries::where('user_id', $user->id)->where('created_at', '>', $date)->count();
        $videos = Videos::where('user_id', $user->id)->where('created_at', '>', $date)->count();

        return ($recipes == 0 && $images == 0 && $videos == 0) ? false : true;
    }

    public static function InfoWidget($widget) {
        $widget = InfoWidget::where('item', $widget)->first();
        if($widget) {
            $ret = json_decode($widget->content, true);
        } else {
            $ret = 'N/A';
        }

        return $ret;
    }

    public static function usernameFix($string) {
        $word_splitters = array(' ', '-', "O'", "L'", "D'", 'St.', 'Mc');
        $lowercase_exceptions = array('the', 'van', 'den', 'von', 'und', 'der', 'de', 'da', 'of', 'and', "l'", "d'");
        $uppercase_exceptions = array('III', 'IV', 'VI', 'VII', 'VIII', 'IX');

        $string = strtolower($string);
        foreach ($word_splitters as $delimiter)
        {
            $words = explode($delimiter, $string);
            $newwords = array();
            foreach ($words as $word)
            {
                if (in_array(strtoupper($word), $uppercase_exceptions))
                    $word = strtoupper($word);
                else
                if (!in_array($word, $lowercase_exceptions))
                    $word = ucfirst($word);

                $newwords[] = $word;
            }

            if (in_array(strtolower($delimiter), $lowercase_exceptions))
                $delimiter = strtolower($delimiter);

            $string = join($delimiter, $newwords);
        }
        return $string;
    }

}