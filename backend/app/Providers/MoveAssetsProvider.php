<?php

namespace Foodstarz\Providers;

use Illuminate\Support\ServiceProvider;

class MoveAssetsProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            base_path('resources/assets') => public_path('assets'),
        ], 'public');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
