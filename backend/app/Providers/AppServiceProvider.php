<?php

namespace Foodstarz\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function boot()
    {

    }

    public function register()
    {

    }
}
