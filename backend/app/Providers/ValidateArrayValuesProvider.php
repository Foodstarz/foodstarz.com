<?php

namespace Foodstarz\Providers;

use Validator;
use Illuminate\Support\ServiceProvider;

class ValidateArrayValuesProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('arrayValues', function($attribute, $array, $parameters) {
            foreach($array as $value) {
                if(!in_array($value, explode('/', $parameters[0]))) {
                    return false;
                }
            }
            return true;
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
