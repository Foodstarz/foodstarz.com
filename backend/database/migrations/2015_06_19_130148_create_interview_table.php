<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInterviewTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interview', function(Blueprint $table) {
            $table->increments('id');
            $table->string('question');
            $table->enum('answer_type', ['input', 'textarea']);
            $table->boolean('required');
        });

        $questions = [
            [
                'question' => 'Who are you and where do you come from?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'When did you start working in a kitchen?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'Where does your inspiration come from?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'How would you describe your style of cooking?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'What is your favorite ingredient?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'What is your favorite thing to cook?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'What is your favorite cuisine?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'What is your advice to people who want to become a chef?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'What is your ultimate food dream?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'Where are you working now?',
                'answer_type' => 'input',
                'required' => true
            ],
            [
                'question' => 'Where do you see yourself in 1 year, 3 years and 5 years?',
                'answer_type' => 'textarea',
                'required' => true
            ]
        ];

        DB::table('interview')->insert($questions);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('interview');
    }
}
