<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoWidgetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_widget', function(Blueprint $table) {
            $table->increments('id');
            $table->string('item');
            $table->text('content');
        });
        
        $widgets = [
            [
                'item' => 'homepage_heading',
                'content' => 'Welcome!'
            ],
            [
                'item' => 'homepage_subheading',
                'content' => 'On foodstarz.com, you will find professional and amateur chefs from all around the world who share their most interesting recipes, in hopes to enhance your cooking interest and abilities in the kitchen.<br><br>Start your discovery now!'
            ]
        ];
        
        DB::table('info_widget')->insert($widgets);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('info_widget');
    }
}
