<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToProfileTable extends Migration
{

    public function up()
    {
        Schema::table('profiles', function($table) {
            $table->string('background', 255)->nullable();
        });
    }

    public function down()
    {
        //
    }
}
