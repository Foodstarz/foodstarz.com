<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('menu');
            $table->string('title');
            $table->string('url');
            $table->integer('order');
        });
        
        $menu_items = [
            [
                'menu' => 1,
                'title' => 'Home',
                'url' => '/',
                'order' => 1
            ],
            [
                'menu' => 1,
                'title' => 'Recipes',
                'url' => '/recipes',
                'order' => 2
            ],
            [
                'menu' => 1,
                'title' => 'Foodstarz',
                'url' => '/chefs',
                'order' => 3
            ],
            [
                'menu' => 1,
                'title' => 'Categories',
                'url' => '/recipe/categories',
                'order' => 4
            ],
            [
                'menu' => 1,
                'title' => 'The Jury',
                'url' => '/the-jury',
                'order' => 5
            ],
            [
                'menu' => 1,
                'title' => 'About Us',
                'url' => '/about-us',
                'order' => 6
            ],
            [
                'menu' => 2,
                'title' => 'Contact/Feedback',
                'url' => '/contact-us',
                'order' => 1
            ],
            [
                'menu' => 2,
                'title' => 'Imprint',
                'url' => '/imprint',
                'order' => 2
            ],
            [
                'menu' => 2,
                'title' => 'Terms of Use',
                'url' => '/tos',
                'order' => 3
            ],
            [
                'menu' => 2,
                'title' => 'Privacy Policy',
                'url' => '/privacy',
                'order' => 4
            ]
        ];
        
        DB::table('menus')->insert($menu_items);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('menus');
    }
}
