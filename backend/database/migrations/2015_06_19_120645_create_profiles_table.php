<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function(Blueprint $table) {
            $table->integer('user_id')->unsigned()->unique();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->enum('culinary_profession', ['professional', 'amateur', 'blogger'])->nullable();
            $table->enum('gender', ['male', 'female'])->nullable();
            $table->string('city', 64)->nullable();
            $table->string('country', 64)->nullable();
            $table->string('avatar', 64)->nullable();
            $table->text('biography')->nullable();
            $table->json('work_experience')->nullable();
            $table->json('awards')->nullable();
            $table->json('organisations')->nullable();
            $table->json('role_models')->nullable();
            $table->string('cooking_style')->nullable();
            $table->json('kitchen_skills')->nullable();
            $table->json('business_skills')->nullable();
            $table->json('personal_skills')->nullable();
            $table->text('search')->nullable();
            $table->text('offer')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('instagram')->nullable();
            $table->string('pinterest')->nullable();
            $table->string('tumblr')->nullable();
            $table->json('interview')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profiles');
    }
}
