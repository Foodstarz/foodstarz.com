<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRecipeCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recipe_categories', function(Blueprint $table) {
            $table->increments('id');
            $table->string('title', 32);
            $table->string('image');
        });

        DB::table('recipe_categories')->insert(['title' => 'Appetizers', 'image' => 'appetizer.png']);
        DB::table('recipe_categories')->insert(['title' => 'Salads & Soups', 'image' => 'soup.png']);
        DB::table('recipe_categories')->insert(['title' => 'Breads & Rolls', 'image' => 'bread.png']);
        DB::table('recipe_categories')->insert(['title' => 'Side Dishes', 'image' => 'side.png']);
        DB::table('recipe_categories')->insert(['title' => 'Main Dishes', 'image' => 'main.png']);
        DB::table('recipe_categories')->insert(['title' => 'Desserts', 'image' => 'icecream.png']);
        DB::table('recipe_categories')->insert(['title' => 'Cakes & Baking', 'image' => 'dessert.png']);
        DB::table('recipe_categories')->insert(['title' => 'Herbs & Spices', 'image' => 'spice.png']);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('recipe_categories');
    }
}
