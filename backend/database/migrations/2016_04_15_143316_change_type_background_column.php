<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeTypeBackgroundColumn extends Migration
{

    public function up()
    {
        Schema::table('profiles', function ($table) {
            $table->dropColumn('background');
        });
        Schema::table('profiles', function ($table) {
            $table->text('background');
        });
    }

    public function down()
    {

    }
}
