<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeaturesTable extends Migration
{

    public function up()
    {
        Schema::create('features', function(Blueprint $table) {
            $table->increments('id');
            $table->string('feature', 255);
            $table->integer('user_group');
            $table->integer('status');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('features');
    }
}
