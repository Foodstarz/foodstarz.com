<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToVideosTable extends Migration
{
    public function up()
    {
        Schema::table('videos', function($table) {
            $table->string('image', 255)->nullable();
        });
    }

    public function down()
    {
        //
    }
}
