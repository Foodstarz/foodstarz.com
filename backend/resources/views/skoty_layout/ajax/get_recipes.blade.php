@foreach ($recipes as $recipe)
    <article class="post hentry">
        <div class="foodstar_header author">
            <table>
                <tr>
                    <td width="70px">
                        <a href="{{ route('view_user_profile', ['slug' => $recipe->user->profile->slug]) }}">
                            <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$recipe->user->profile->avatar) }}" alt="">
                        </a>
                    </td>
                    <td style="vertical-align: middle;">
                        <a href="{{ route('view_user_profile', ['slug' => $recipe->user->profile->slug]) }}">{{ $recipe->user->name }}</a>
                    </td>
                </tr>
            </table>
        </div>

        <figure class="post-thumbnail">
            <a href="{{ route('recipe_view', ['slug' => $recipe->slug]) }}"
               title="{{ $recipe->title }}"><img width="357" height="357"
                                                 src="@if(empty($recipe->main_image_watermarked)) {{ asset('storage/recipes/original/'.$recipe->main_image_original) }} @else {{ asset('storage/recipes/watermark/'.$recipe->main_image_watermarked) }} @endif"
                                                 class="attachment-skoty-blog-post"
                                                 alt="{{ $recipe->title }}"/>
            </a>
            <h2 class="post-title">
                <a href="{{ route('recipe_view', ['slug' => $recipe->slug]) }}">{{ mb_strimwidth($recipe->title, 0, 56, '...') }}</a>
            </h2>
        </figure>

        <div class="post-content">
            <div class="additional-info">
                <img src="{{ asset('assets/images/yield.png') }}" alt="{{ trans('pages/homepage.serves') }}"> @if(Lang::locale() == 'de'){{ $recipe->portions }} {{ trans('pages/homepage.serves') }}@else{{ trans('pages/homepage.serves') }} {{ $recipe->portions }}@endif
            </div>

            <div class="additional-info" style="float:right;">
                <img src="{{ asset('assets/images/'.strtolower($recipe->difficulty).'.png') }}" alt="{{ trans('pages/add_recipe.form.difficulty.options.'.$recipe->difficulty) }}"> {{ trans('pages/add_recipe.form.difficulty.options.'.$recipe->difficulty) }}
            </div>

            <div class="clearfix"></div>
        </div>
    </article>
@endforeach

@if(count($recipes) < 1)
<div class="text-center">
    <h3>{{ trans('pages/recipe.no_recipes') }}</h3>
</div>
@endif