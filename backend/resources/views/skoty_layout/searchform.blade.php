<form role="search" method="get" class="search-form" action="{{ route('search') }}">
    <label>
        <span class="screen-reader-text">Search for:</span>
        <input type="text" class="search-field" placeholder="Type and hit enter" value="" name="q" title="Search for:" />
    </label>
    <input type="submit" class="search-submit" value="Search" />
</form>