@extends('skoty_layout.layout')

@section('title', trans('pages/news_listing.page_title'))

@section('seo-metas')
<meta name="description" content="Keep up-to-date with the news regarding Foodstarz.com!"/>
@endsection

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/rrssb.css') }}" />
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/js/rrssb.min.js') }}"></script>
@endsection

@section('seo-metas')
<meta name="description" content="{{trans('pages/seo.homepage.description')}}"/>
@endsection

@section('og-tags')
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{ trans('pages/news_listing.page_title') }}" />
<meta property="og:description" content="{{ trans('pages/seo.homepage.description') }}" />
<meta property="og:url" content="http://foodstarz.com" />
<meta property="og:site_name" content="Foodstarz" />
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
<article class="homepage center-box">
    <h1 class="post-title">{{ trans('pages/news_listing.content_title') }}</h1>

    <div class="post-content c_page">
        <table width="100%" class="news">
            <tr>
                <td><strong>{{ trans('pages/news_listing.title') }}</strong></td>
                <td><strong>{{ trans('pages/news_listing.date') }}</strong></td>
            </tr>
            @foreach($news as $new)
            <tr>
                <td><a href="{{ route('view_news', $new->slug) }}">{{ $new->title }}</a></td>
                <td>{{ date('d M, Y', strtotime($new->created_at)) }}</td>
            </tr>
            @endforeach
        </table>
    </div>
    
    <div class="content text_center">
        {!! $news->render() !!}
    </div>
    
</article>
@endsection