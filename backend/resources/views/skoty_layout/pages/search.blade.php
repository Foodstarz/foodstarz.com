@extends('skoty_layout.layout')

@section('title', trans('pages/search.page_title'))

@section('seo-metas')
<meta name="description" content="Search for high-quality content on our website Foodstarz.com!"/>
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('body_classes')
show-avatar
@endsection

@section('content')
<article class="search homepage center-box">
    @foreach ($searchResources as $sources)
    <h1 class="post-title">{{ trans('pages/search.search_in') }}: {{ $sources['name'] }} ( {{ $sources['count'] }} )</h1>
    <div class="content posts">
        @if($sources['type'] == "foodstar")
            @foreach ($sources['results'] as $result)
                <article class="post hentry">
                    <figure class="post-thumbnail">
                        <a href="{{ route('view_user_chronic', ['slug' => $result->profile->slug]) }}"
                           title="{{ $result->name }}"><img width="357" height="357"
                                                              src="{{ asset('storage/avatars/'.$result->profile->avatar) }}"
                                                              class="attachment-skoty-blog-post"
                                                              alt="{{ $result->name }}"/>
                        </a>
                        <h2 class="post-title">
                            <a href="{{ route('view_user_chronic', ['slug' => $result->profile->slug]) }}">{{ mb_strimwidth($result->name, 0, 56, '...') }}</a>
                        </h2>
                    </figure>

                </article>
            @endforeach
        @elseif ($sources['type'] == "recipe")
            @foreach ($sources['results'] as $result)
                <article class="post hentry">
                    <div class="foodstar_header author">
                        <table>
                            <tr>
                                <td width="70px">
                                    <a href="{{ route('view_user_chronic', ['slug' => $result->user->profile->slug]) }}">
                                        <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$result->user->profile->avatar) }}" alt="">
                                    </a>
                                </td>
                                <td style="vertical-align: middle;">
                                    <a href="{{ route('view_user_chronic', ['slug' => $result->user->profile->slug]) }}">{{ $result->user->name }}</a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <figure class="post-thumbnail">
                        <a href="{{ route('recipe_view', ['slug' => $result->slug]) }}"
                           title="{{ $result->title }}"><img width="357" height="357"
                                                                src="@if(empty($result->main_image_watermarked)) {{ asset('storage/recipes/original/'.$result->main_image_original) }} @else {{ asset('storage/recipes/watermark/'.$result->main_image_watermarked) }} @endif"
                                                                class="attachment-skoty-blog-post"
                                                                alt="{{ $result->title }}"/>
                        </a>
                        <h2 class="post-title">
                            <a href="{{ route('recipe_view', ['slug' => $result->slug]) }}">{{ mb_strimwidth($result->title, 0, 56, '...') }}</a>
                        </h2>
                    </figure>

                    <div class="post-content">
                        <div class="additional-info">
                            <img src="{{ asset('assets/images/yield.png') }}" alt="{{ trans('pages/homepage.serves') }}"> <span>@if(Lang::locale() == 'de'){{ $result->portions }} {{ trans('pages/homepage.serves') }}@else{{ trans('pages/homepage.serves') }} {{ $result->portions }}@endif</span>
                        </div>

                        <div class="additional-info" style="float:right;">
                            <img src="{{ asset('assets/images/'.strtolower($result->difficulty).'.png') }}" alt="{{ trans('pages/add_recipe.form.difficulty.options.'.$result->difficulty) }}"> <span>{{ trans('pages/add_recipe.form.difficulty.options.'.$result->difficulty) }}</span>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </article>
            @endforeach

        @elseif ($sources['type'] == "image")
            @foreach ($sources['results'] as $image)
                <article class="post hentry">
                    <div class="foodstar_header author">
                        <table>
                            <tr>
                                <td width="70px">
                                    <a href="{{ route('view_user_chronic', ['slug' => $image->user->profile->slug]) }}">
                                        <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$image->user->profile->avatar) }}" alt="">
                                    </a>
                                </td>
                                <td style="vertical-align: middle;">
                                    <a href="{{ route('view_user_chronic', ['slug' => $image->user->profile->slug]) }}">{{ $image->user->name }}</a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <figure class="post-thumbnail">
                        <a href="{{ route('gallery_view_image', $image->id) }}"
                           title="{{ $image->caption }}"><img width="357"
                                                              src="{{ asset('storage/gallery/thumbnails/'.$image->image) }}"
                                                              class="attachment-skoty-blog-post"
                                                              alt="{{ $image->caption }}"/>
                        </a>
                        <h2 class="post-title">
                            <a href="{{ route('gallery_view_image', $image->id) }}">{{ mb_strimwidth($image->caption, 0, 56, '...') }}</a>
                        </h2>
                    </figure>

                </article>
            @endforeach
        @elseif ($sources['type'] == "video")
            @foreach ($sources['results'] as $video)
                <article class="post hentry">
                    <div class="foodstar_header author">
                        <table>
                            <tr>
                                <td width="70px">
                                    <a href="{{ route('view_user_chronic', ['slug' => $video->user->profile->slug]) }}">
                                        <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$video->user->profile->avatar) }}" alt="">
                                    </a>
                                </td>
                                <td style="vertical-align: middle;">
                                    <a href="{{ route('view_user_chronic', ['slug' => $video->user->profile->slug]) }}">{{ $video->user->name }}</a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <figure class="post-thumbnail">
                        <a href="{{ route('video_view', $video->id) }}"
                           title="{{ $video->title }}"><img width="357"
                                                              src="{{ $sources['videos_pictures'][$video->id] }}"
                                                              class="attachment-skoty-blog-post"
                                                              alt="{{ $video->title }}"/>
                        </a>
                        <h2 class="post-title">
                            <a href="{{ route('video_view', $video->id) }}">{{ mb_strimwidth($video->title, 0, 56, '...') }}</a>
                        </h2>
                    </figure>

                </article>
            @endforeach
        @endif
    </div>
    @if(count($sources['results']) < 1)
                <div class="text-center">
                    <h3>{{ trans('pages/search.no_results') }}</h3>
                </div>
    @endif
    @endforeach

</article>
@endsection