@extends('skoty_layout.layout')

@section('title', trans('pages/password.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        <h1 class="post-title">{{ trans('pages/password.content_title') }}</h1>

        <div class="post-content">

            @if(session('status'))
                <div class="alert-box success">{{ session('status') }}</div>
            @endif

            @if(session('error'))
                <div class="alert-box error">{{ session('error') }}</div>
            @endif

            <form method="POST" action="{{ route('forgotten_password_process') }}">
                {!! csrf_field() !!}

                <p>
                    <b>{{ trans('pages/password.form.email.label') }}</b><br>
                    <input type="email" name="email" placeholder="{{ trans('pages/password.form.email.placeholder') }}" value="{{ old('email') }}">
                    @if(!empty($errors->get('email')))
                        @foreach($errors->get('email') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    {!! Recaptcha::render() !!}
                    @if(!empty($errors->get('g-recaptcha-response')))
                        @foreach($errors->get('g-recaptcha-response') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <button type="submit">{{ trans('pages/password.form.submit.label') }}</button>
                </p>
            </form>

        </div>
    </article>
@endsection