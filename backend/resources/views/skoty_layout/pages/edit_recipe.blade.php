@extends('skoty_layout.layout')

@section('title', trans('pages/edit_recipe.page_title'))

@section('header_scripts')
<script src="{{ asset('assets/js/jquery.cropit.js') }}"></script>
<script type="text/javascript">
show_avatar = true;
</script>
@endsection

@section('footer_scripts')
<script src="{{ asset('assets/js/custom/add_recipe.js') }}"></script>
@endsection

@section('body_classes')
show-avatar page
@endsection

@section('content')
<article class="content-article center-box">
    <h1 class="post-title">{{ trans('pages/edit_recipe.content_title') }}</h1>

    <div class="post-content">

        <div class="alert-box success" style="display: none;">{!! session('success') !!}</div>
        <form method="POST" action="{{ route('recipe_edit_process', ['id' => $recipe['id']]) }}" id="add_recipe_form" enctype="multipart/form-data">
            {!! csrf_field() !!}

            <fieldset>
                <legend>{{ trans('pages/add_recipe.form.fieldset.main_data') }}</legend>
                <p>
                    <b>{{ trans('pages/add_recipe.form.title.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br/>
                    <input type="text" name="title" value="{{ trim($recipe['title']) }}"/>
                </p>
                <div class="title_errors errDiv"></div>

                <p>
                    <b>{{ trans('pages/add_recipe.form.category.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br/>
                    @foreach($recipe_categories as $category)
                        <input type="checkbox" name="category[]" value="{{ $category->id }}" @if(in_array($category->id, (array)$recipe['category'])) checked @endif/>&nbsp;
                        @if(Auth::check())
                            @if(empty(Auth::user()->profile->lang))
                                @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                    {{ json_decode($category->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                                @else
                                    {{ json_decode($category->title, true)['en'] }}
                                @endif
                            @else
                                {{ json_decode($category->title, true)[Auth::user()->profile->lang] }}
                            @endif
                        @else
                            @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                {{ $category->title[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                            @else
                                {{ json_decode($category->title, true)['en'] }}
                            @endif
                        @endif
                        &nbsp;
                    @endforeach
                </p>
                <div class="category_errors errDiv"></div>

                <p>
                    <b>{{ trans('pages/add_recipe.form.portions.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br/>
                    <input type="text" name="portions" value="{{ trim($recipe['portions']) }}"/>
                </p>
                <div class="portions_errors errDiv"></div>

                <p>
                    <b>{{ trans('pages/add_recipe.form.preparation_time.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br/>
                    <input type="text" name="preparation_time" value="{{ trim($recipe['preparation_time']) }}"/>
                </p>
                <div class="preparation_time_errors errDiv"></div>

                <p>
                    <b>{{ trans('pages/add_recipe.form.difficulty.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br/>
                    <select name="difficulty" @if(!empty($errors->get('difficulty'))) class="input-error" @endif>
                        <option value="easy" @if($recipe['difficulty'] == 'easy') selected @endif>{{ trans('pages/add_recipe.form.difficulty.options.easy') }}</option>
                        <option value="medium" @if($recipe['difficulty'] == 'medium') selected @endif>{{ trans('pages/add_recipe.form.difficulty.options.medium') }}</option>
                        <option value="hard" @if($recipe['difficulty'] == 'hard') selected @endif>{{ trans('pages/add_recipe.form.difficulty.options.hard') }}</option>
                    </select>
                </p>
                <div class="difficulty_errors errDiv"></div>

                <div class="images">
                    <b>{{ trans('pages/add_recipe.form.attachments.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span> ({{ trans('pages/add_recipe.accepted_file_formats') }})<br/>
                    <i>{{ trans('pages/add_recipe.form.attachments.desc') }}</i><br/>
                    <img src="{{ asset('storage/recipes/original/'.$recipe['main_image_original']) }}" width="200px" height="200px"><br>
                    <b class="warning_text">{{ trans('pages/edit_recipe.main_image') }}</b><br/>

                    <div class="image-editor">
                        <input type="file" class="cropit-image-input">
                        <div class="cropit-image-preview recipe-image-preview"></div>
                        <div class="image-size-label">
                            <b>{{ trans('pages/upload_avatar.resize_image') }}</b>
                        </div>
                        <input type="range" class="cropit-image-zoom-input">
                        <input type="hidden" name="image" class="hidden-image-data" />
                    </div>

                    <div class="image_errors errDiv"></div>
                </div>
            </fieldset>

            <div class="components">

                <div class="componentsList">
                    @if(empty($recipe['components']) || isset($recipe['components']['html']))
                    <div class="component">
                        <fieldset>
                            <legend><span class="componentNumber first">1</span><span class="componentNumberText">{{ trans_choice('pages/add_recipe.form.components.plural', 1) }}</span> {{ trans('pages/add_recipe.form.components.component') }}</legend>

                            {{ trans('pages/add_recipe.form.components.name') }} <span class="required">{{ trans('general.required_fields') }}</span><br/>
                            <input type="text" name="components[1][name]"/><br/>
                            <div class="component_name_1_errors errDiv"></div>

                            {{ trans('pages/add_recipe.form.components.component_image') }} <span class="required">{{ trans('general.required_fields') }}</span><br>({{ trans('pages/add_recipe.accepted_file_formats') }})<br/>
                            <i>{{ trans('pages/add_recipe.form.attachments.desc') }}</i><br>
                            <div class="image-editor">
                                <input type="file" class="cropit-image-input">
                                <div class="cropit-image-preview recipe-image-preview"></div>
                                <div class="image-size-label">
                                    <b>{{ trans('pages/upload_avatar.resize_image') }}</b>
                                </div>
                                <input type="range" class="cropit-image-zoom-input">
                                <input type="hidden" name="components[1][image]" class="hidden-image-data" />
                            </div>
                            <div class="component_image_1_errors errDiv"></div>

                            {{ trans('pages/add_recipe.form.components.ingredients') }} <span class="required">{{ trans('general.required_fields') }}</span><br/>
                            <div class="ingredientsList">
                                <div class="ingredientRow">
                                    <input type="text" name="components[1][ingredients][name][]" placeholder="{{ trans('pages/add_recipe.form.components.ingredient') }}" class="name"/>
                                    <input type="text" name="components[1][ingredients][quantity][]" placeholder="{{ trans('pages/add_recipe.form.components.quantity') }}" class="quantity"/>
                                    <select name="components[1][ingredients][unit][]" class="unit">
                                        <option value=" ">{{ trans('pages/add_recipe.form.components.select_unit') }}</option>
                                        @foreach($ingredient_units as $unit)
                                            <option value="{{ $unit }}">{{ trans('pages/add_recipe.form.components.units.'.$unit) }}</option>
                                        @endforeach
                                    </select>
                                    <button class="deleteIngredient"><i class="fa fa-trash-o fa-lg"></i></button>
                                </div>
                            </div>
                            <div class="component_ingredients_1_errors errDiv"></div>
                            <button class="addIngredient">{{ trans('pages/add_recipe.form.components.add_ingredient') }}</button>

                            {{ trans('pages/add_recipe.form.components.instructions') }} <span class="required">{{ trans('general.required_fields') }}</span><br/>
                            <div class="instructionsList">
                                <div class="instructionRow">
                                    <input type="text" name="components[1][instructions][]" placeholder="{{ trans('pages/add_recipe.form.components.enter_instruction') }}" class="instruction" />
                                    <button class="deleteInstruction"><i class="fa fa-trash-o fa-lg"></i></button>
                                </div>
                            </div>
                            <div class="component_instructions_1_errors errDiv"></div>
                            <button class="addInstruction">{{ trans('pages/add_recipe.form.components.add_step') }}</button>
                            <button class="deleteComponent">{{ trans('pages/add_recipe.form.components.remove_component') }}</button>
                        </fieldset>
                    </div>
                    @else
                        @foreach($recipe['components'] as $key => $component)
                        <div class="component">
                            <fieldset>
                                <legend><span class="componentNumber first">{{ $key }}</span><span class="componentNumberText">{{ trans_choice('pages/add_recipe.form.components.plural', $key) }}</span> {{ trans('pages/add_recipe.form.components.component') }}</legend>
                                {{ trans('pages/add_recipe.form.components.name') }} <span class="required">{{ trans('general.required_fields') }}</span><br/>
                                <input type="text" name="components[{{ $key }}][name]" @if(!empty($errors->get('component_name_'.$key))) class="input-error" @endif value="{{ trim($component['name']) }}"/><br/>
                                <div class="component_name_{{ $key }}_errors errDiv"></div>

                                {{ trans('pages/add_recipe.form.components.component_image') }} <span class="required">{{ trans('general.required_fields') }}</span><br>({{ trans('pages/add_recipe.accepted_file_formats') }})<br/>
                                <i>{{ trans('pages/add_recipe.form.attachments.desc') }}</i><br>
                                @if(!empty(trim($component['image'])))
                                    <div class="compImg">
                                        <img src="{{ asset('storage/recipes/original/'.$component['image']) }}" width="200px" height="200px" /><br>
                                        <a class="component_picture_delete" href="{{ route('recipe_component_delete_picture', ['id' => $recipe['id'], 'component' => $key ]) }}">Delete this picture...</a><br>
                                    </div>
                                @endif
                                <b class="warning_text">{{ trans('pages/edit_recipe.component_image') }}</b><br/>
                                <div class="image-editor">
                                    <input type="file" class="cropit-image-input">
                                    <div class="cropit-image-preview recipe-image-preview"></div>
                                    <div class="image-size-label">
                                        <b>{{ trans('pages/upload_avatar.resize_image') }}</b>
                                    </div>
                                    <input type="range" class="cropit-image-zoom-input">
                                    <input type="hidden" name="components[{{ $key }}][image]" class="hidden-image-data" />
                                </div>
                                <div class="component_image_{{ $key }}_errors errDiv"></div>

                                {{ trans('pages/add_recipe.form.components.ingredients') }} <span class="required">{{ trans('general.required_fields') }}</span><br/>
                                <div class="ingredientsList">
                                    @if(!empty($component['ingredients']))
                                        @for($i=0;$i<sizeof($component['ingredients']['name']);$i++)
                                            <div class="ingredientRow">
                                                <input type="text" name="components[{{ $key }}][ingredients][name][]" value="{{ trim($component['ingredients']['name'][$i]) }}" placeholder="{{ trans('pages/add_recipe.form.components.ingredient') }}" class="name"/>
                                                <input type="text" name="components[{{ $key }}][ingredients][quantity][]" value="{{ trim($component['ingredients']['quantity'][$i]) }}" placeholder="{{ trans('pages/add_recipe.form.components.quantity') }}" class="quantity"/>
                                                <select name="components[{{ $key }}][ingredients][unit][]" class="unit">
                                                    <option value=" ">{{ trans('pages/add_recipe.form.components.select_unit') }}</option>
                                                    @foreach($ingredient_units as $unit)
                                                        <option value="{{ $unit }}" @if(isset($component['ingredients']['unit'][$i]) && $unit == $component['ingredients']['unit'][$i]) selected @endif>{{ trans('pages/add_recipe.form.components.units.'.$unit) }}</option>
                                                    @endforeach
                                                </select>
                                                <button class="deleteIngredient"><i class="fa fa-trash-o fa-lg"></i></button>
                                            </div>
                                        @endfor
                                    @endif
                                </div>
                                <div class="component_ingredients_{{ $key }}_errors errDiv"></div>
                                <button class="addIngredient">{{ trans('pages/add_recipe.form.components.add_ingredient') }}</button>

                                {{ trans('pages/add_recipe.form.components.instructions') }} <span class="required">{{ trans('general.required_fields') }}</span><br/>
                                <div class="instructionsList">
                                    @for($i=0;$i<sizeof($component['instructions']);$i++)
                                        <div class="instructionRow">
                                            <input type="text" name="components[{{ $key }}][instructions][]" value="{{ trim($component['instructions'][$i]) }}" placeholder="{{ trans('pages/add_recipe.form.components.enter_instruction') }}" class="instruction" />
                                            <button class="deleteInstruction"><i class="fa fa-trash-o fa-lg"></i></button>
                                        </div>
                                    @endfor
                                    <div class="component_instructions_{{ $key }}_errors errDiv"></div>
                                </div>
                                <button class="addInstruction">{{ trans('pages/add_recipe.form.components.add_step') }}</button>
                                <button class="deleteComponent">{{ trans('pages/add_recipe.form.components.remove_component') }}</button>
                            </fieldset>
                        </div>
                        @endforeach
                    @endif
                </div>

                <button class="addComponent">{{ trans('pages/add_recipe.form.components.add_component') }}</button>

                <div class="components_errors errDiv"></div>
            </div>

            <fieldset>
                <legend>{{ trans('pages/add_recipe.form.fieldset.other_data') }}</legend>

                <p>
                    <b>{{ trans('pages/add_recipe.form.status.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span> <i>({{ trans('pages/add_recipe.form.status.desc') }})</i><br/>
                    {{ trans('pages/add_recipe.form.status.text') }}<br/>
                    <input type="radio" name="status" value="0" @if($recipe['status'] == '0') checked @endif/> {{ trans('pages/add_recipe.form.status.options.not_ready') }}<br/>
                    <input type="radio" name="status" value="1" @if($recipe['status'] == '1') checked @endif/> {{ trans('pages/add_recipe.form.status.options.ready') }}
                </p>
                <div class="status_errors errDiv"></div>
            </fieldset>

            <p>
                <button type="submit">{{ trans('pages/add_recipe.form.submit.label') }}</button>
            </p>
            
            <div class="request-loading hidden text-center">
                <p>{{ trans('pages/add_recipe.form.loading') }}</p>
                <img src="{{ asset('assets/images/ajax-loader.gif') }}" />
            </div>
        </form>

        <div class="hidden">
            <div class="ingredientRow sample">
                <input type="text" name="components[{num}][ingredients][name][]" placeholder="{{ trans('pages/add_recipe.form.components.ingredient') }}" class="name"/>
                <input type="text" name="components[{num}][ingredients][quantity][]" placeholder="{{ trans('pages/add_recipe.form.components.quantity') }}" class="quantity"/>
                <select name="components[{num}][ingredients][unit][]" class="unit">
                    <option value=" ">{{ trans('pages/add_recipe.form.components.select_unit') }}</option>
                    @foreach($ingredient_units as $unit)
                    <option value="{{ $unit }}">{{ trans('pages/add_recipe.form.components.units.'.$unit) }}</option>
                    @endforeach
                </select>
                <button class="deleteIngredient"><i class="fa fa-trash-o fa-lg"></i></button>
            </div>
            <div class="instructionRow sample">
                <input type="text" name="components[{num}][instructions][]" placeholder="{{ trans('pages/add_recipe.form.components.enter_instruction') }}" class="instruction" />
                <button class="deleteInstruction"><i class="fa fa-trash-o fa-lg"></i></button>
            </div>

            <div class="component sample">
                <fieldset>
                    <legend><span class="componentNumber first">{num}</span><span class="componentNumberText"></span> {{ trans('pages/add_recipe.form.components.component') }}</legend>
                    
                    {{ trans('pages/add_recipe.form.components.name') }} <span class="required">{{ trans('general.required_fields') }}</span><br/>
                    <input type="text" name="components[{num}][name]"/><br/>
                    <div class="component_name_{num}_errors errDiv"></div>

                    {{ trans('pages/add_recipe.form.components.component_image') }} <span class="required">{{ trans('general.required_fields') }}</span><br>({{ trans('pages/add_recipe.accepted_file_formats') }})<br/>
                    <div class="image-editor">
                        <input type="file" class="cropit-image-input">
                        <div class="cropit-image-preview recipe-image-preview"></div>
                        <div class="image-size-label">
                            <b>{{ trans('pages/upload_avatar.resize_image') }}</b>
                        </div>
                        <input type="range" class="cropit-image-zoom-input">
                        <input type="hidden" name="components[{num}][image]" class="hidden-image-data" />
                    </div>
                    <div class="component_image_{num}_errors errDiv"></div>

                    {{ trans('pages/add_recipe.form.components.ingredients') }} <span class="required">{{ trans('general.required_fields') }}</span><br/>
                    <div class="ingredientsList">
                        <div class="ingredientRow">
                            <input type="text" name="components[{num}][ingredients][name][]" placeholder="{{ trans('pages/add_recipe.form.components.ingredient') }}" class="name"/>
                            <input type="text" name="components[{num}][ingredients][quantity][]" placeholder="{{ trans('pages/add_recipe.form.components.quantity') }}" class="quantity"/>
                            <select name="components[{num}][ingredients][unit][]" class="unit">
                                <option value=" ">{{ trans('pages/add_recipe.form.components.select_unit') }}</option>
                                @foreach($ingredient_units as $unit)
                                    <option value="{{ $unit }}">{{ trans('pages/add_recipe.form.components.units.'.$unit) }}</option>
                                @endforeach
                            </select>
                            <button class="deleteIngredient"><i class="fa fa-trash-o fa-lg"></i></button>
                        </div>
                    </div>
                    <div class="component_ingredients_{num}_errors errDiv"></div>
                    <button class="addIngredient">{{ trans('pages/add_recipe.form.components.add_ingredient') }}</button>

                    {{ trans('pages/add_recipe.form.components.instructions') }} <span class="required">{{ trans('general.required_fields') }}</span><br/>
                    <div class="instructionsList">
                        <div class="instructionRow">
                            <input type="text" name="components[{num}][instructions][]" placeholder="{{ trans('pages/add_recipe.form.components.enter_instruction') }}" class="instruction" />
                            <button class="deleteInstruction"><i class="fa fa-trash-o fa-lg"></i></button>
                        </div>
                    </div>
                    <div class="component_instructions_{num}_errors errDiv"></div>
                    <button class="addInstruction">{{ trans('pages/add_recipe.form.components.add_step') }}</button>
                    <button class="deleteComponent">{{ trans('pages/add_recipe.form.components.remove_component') }}</button>
                </fieldset>
            </div>
        </div>
    </div>
</article>
@endsection