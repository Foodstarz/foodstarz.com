@extends('skoty_layout.layout')

@section('title', $c_text->title . " | Foodstarz.com")

@section('seo-metas')
<meta name="description" content="{{trans('pages/seo.homepage.description')}}"/>
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('og-tags')
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$c_text->title}}" />
<meta property="og:description" content="{{trans('pages/seo.homepage.description')}}" />
<meta property="og:url" content="http://foodstarz.com" />
<meta property="og:site_name" content="Foodstarz" />
@endsection

@section('body_classes')
show-avatar page
@endsection

@section('content')
<article class="homepage center-box">
    <h1 class="post-title">{{ $c_text->title }}</h1>

    <div class="post-content c_page">
        {!! $c_text->text !!}
    </div>
</article>
@endsection