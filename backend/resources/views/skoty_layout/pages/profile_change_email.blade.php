@extends('skoty_layout.layout')

@section('title', trans('pages/profile_change_email.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        <h1 class="post-title">{{ trans('pages/profile_change_email.content_title') }}</h1>        
        
        <div class="post-content">

            <div class="alert-box warning">{{ trans('pages/profile_change_email.warning') }}</div>

            <form method="POST" action="{{ route('change_email_process') }}">
                {!! csrf_field() !!}

                <p>
                    <b>{{ trans('pages/profile_change_email.form.email.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="email" name="email" @if(!empty($errors->get('email'))) class="input-error" @endif value="{{ $email }}" placeholder="{{ trans('pages/profile_change_email.form.email.placeholder') }}">
                    @if(!empty($errors->get('email')))
                        @foreach($errors->get('email') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>
                
                <p>
                    <button type="submit">{{ trans('pages/profile_change_email.form.submit.label') }}</button>
                </p>
                
            </form>

        </div>
    </article>
@endsection