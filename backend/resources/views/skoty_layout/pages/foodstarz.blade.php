@extends('skoty_layout.layout')

@section('title', trans('pages/foodstarz.page_title'))

@section('seo-metas')
<meta name="description" content="Meet the best chefs in the industry! Only high-quality chefs and their recipes are approved here."/>
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('content')

    <h1 class="post-title">{{ trans('pages/foodstarz.content_title') }}</h1>

    <div class="gourmet-ads">
        <div id="ga_9466753">
            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466753');
                });
            </script>
        </div>
    </div>

    <div class="gourmet-ads-mobile">
        <div id="ga_9466758">
            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466758');
                });
            </script>
        </div>
    </div>


<article class="homepage center-box">

    <div class="content posts">
        @foreach ($foodstarz as $foodstar)
            <article class="post hentry">
                <figure class="post-thumbnail">
                    <a href="{{ route('view_user_chronic', ['slug' => $foodstar->profile->slug]) }}"
                       title="{{ $foodstar->name }}"><img width="357" height="357"
                                                          src="{{ asset('storage/avatars/'.$foodstar->profile->avatar) }}"
                                                          class="attachment-skoty-blog-post"
                                                          alt="{{ $foodstar->name }}"/>
                    </a>
                    <h2 class="post-title">
                        <a href="{{ route('view_user_chronic', ['slug' => $foodstar->profile->slug]) }}">{{ mb_strimwidth($foodstar->name, 0, 56, '...') }}</a>
                    </h2>
                </figure>

            </article>
        @endforeach
    </div>
    <div class="content text_center">
        <?php Foodstarz\Models\Pagination::render($foodstarz); ?>
    </div>
</article>
@endsection
