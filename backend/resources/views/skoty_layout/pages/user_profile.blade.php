@extends('skoty_layout.layout')

@section('title', $user->name . " - " . trans('pages/user_profile.page_title') . " | Foodstarz.com")
@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/rrssb.css') }}" />
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('seo-metas')
    @if(sizeof($bio) > 0)
        <meta name="description" content="{{ $bio }}"/>
    @else
        <meta name="description" content="View the profile of {{$user->name }}"/>
    @endif
@endsection

@section('og-tags')
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{$user->name }}" />
    <meta property="og:description" content="@if(sizeof($bio) > 0) {{ $bio }} @endif" />
    <meta property="og:url" content="{{ route('view_user_profile', ['slug' => $profile->slug]) }}" />
    <meta property="og:site_name" content="Foodstarz" />
    <meta property="fb:admins" content="582794454" />
    <meta property="og:image" content="{{ asset('storage/avatars/'.$user->profile->avatar) }}" />
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/js/rrssb.min.js') }}"></script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    @if($user->role == 4 || $user->role == 1 || (Auth::check() && ($user->id == Auth::user()->id || Auth::user()->role == 1)))
    <article class="profile-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        @if(Auth::check() && Auth::user()->id == $user->id)
            @if(!\Foodstarz\Helpers\UserHelpers::userActivity($user))
                <div class="alert-box notice">{!! nl2br(trans('pages/user_profile.activity_interstitial', ['name' => $user->name]))  !!}</div>
            @endif
        @endif

        <div class="biography wider-content">
            @if(sizeof($profile->biography) > 0)
            @foreach($profile->biography as $paragraph)
            <p>{{ $paragraph }}</p>
            @endforeach
            @endif
        </div>
        <div class="clearfix"></div>
    </article>

    <div class="clearfix"></div>

    @if(sizeof($profile->work_experience) > 0)
        <div class="work-experience">
            <div class="work-outer">
                <h3>{{ trans('pages/user_profile.work_experience.title') }}</h3>

                <table cellpadding="0" cellspacing="0" border="0">
                    @foreach($profile->work_experience as $experience)
                    <tr>
                        <td width="40%" class="text-bold">@if(isset($experience['period']['from']) && !empty(trim($experience['period']['from']))){{ date('M Y', strtotime($experience['period']['from'])) }} -@endif
                            @if(isset($experience['period']['now']) && $experience['period']['now'])
                            {{ trans('pages/user_profile.work_experience.still_working') }}
                            @else
                            @if(isset($experience['period']['to']) && !empty(trim($experience['period']['to']))){{ date('M Y', strtotime($experience['period']['to'])) }}@endif
                            @endif
                        </td>
                        <td width="60%" class="text-bold">@if(isset($experience['position'])){{ $experience['position'] }}@endif</td>
                    </tr>
                    <tr>
                        <td>@if(isset($experience['employer'])){{ $experience['employer'] }}@endif</td>
                        <td>@if(isset($experience['job_description'])){{ $experience['job_description'] }}@endif</td>
                    </tr>
                    <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
                    @endforeach
                </table>
            </div>
            <div data-ratio="1.5" data-media-opacity="1" data-media-mime="image/jpeg" data-media-type="image" data-media-url="{{ asset('assets/images/work_experience_bg.jpg') }}" class="work-media"></div>
        </div>
    @endif

    <div class="lists">
        <div class="centerLists">
            <div class="category">
                <i class="fa fa-trophy icon"></i>
                <h3>{{ trans('pages/user_profile.awards.title') }}</h3>
                <p>
                    @if(!empty($profile->awards))
                    @foreach($profile->awards as $award)
                    {{ $award }}<br>
                    @endforeach
                    @else
                    {{ trans('pages/user_profile.awards.no_awards') }}
                    @endif
                </p>
            </div>

            <div class="category">
                <i class="fa fa-shield icon"></i>
                <h3>{{ trans('pages/user_profile.organisations.title') }}</h3>
                <p>
                    @if(!empty($profile->organisations))
                    @foreach($profile->organisations as $organisation)
                    {{ $organisation }}<br>
                    @endforeach
                    @else
                    {{ trans('pages/user_profile.organisations.no_organisations') }}
                    @endif
                </p>
            </div>

            <div class="category">
                <i class="fa fa-star icon"></i>
                <h3>{{ trans('pages/user_profile.role_models.title') }}</h3>
                <p>
                    @if(!empty($profile->role_models))
                    @foreach($profile->role_models as $role_model)
                    {{ $role_model }}<br>
                    @endforeach
                    @else
                    {{ trans('pages/user_profile.role_models.no_role_models') }}
                    @endif
                </p>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    @if(!empty($profile->cooking_style))
    <div class="cooking-style">
        <h3>{{ trans('pages/user_profile.cooking_style.title') }}</h3>
        <p>{{ $profile->cooking_style }}</p>
    </div>
    @endif

    @if(!empty($profile->kitchen_skills) || !empty($profile->business_skills) || !empty($profile->personal_skills))
    <div class="skills">
        <div class="outer">
            @if(!empty($profile->kitchen_skills))
            <h3>{{ trans('pages/user_profile.skills.kitchen_skills') }}</h3>
            <div class="cards">
                @for($i=0;$i<sizeof($profile->kitchen_skills);$i++)
                    @if($i==0)
                        <div class="side">
                            <ul>
                    @endif

                    @if($i==3)
                            </ul>
                        </div>
                        <div class="side">
                            <ul>
                    @endif
                                <li>{{ trans('pages/profile_chef_data.form.kitchen_skills.checkboxes.'.$profile->kitchen_skills[$i]) }}</li>
                    @if($i==sizeof($profile->kitchen_skills)-1)
                            </ul>
                        </div>
                    @endif
                @endfor
            </div>
            <div class="clearfix"></div>
            @endif

            @if(!empty($profile->business_skills))
            <h3>{{ trans('pages/user_profile.skills.business_skills') }}</h3>
            <div class="cards">
                @for($i=0;$i<sizeof($profile->business_skills);$i++)
                    @if($i==0)
                        <div class="side">
                            <ul>
                    @endif

                    @if($i==3)
                            </ul>
                        </div>
                        <div class="side">
                            <ul>
                    @endif
                                <li>{{ trans('pages/profile_chef_data.form.business_skills.checkboxes.'.$profile->business_skills[$i]) }}</li>
                    @if($i==sizeof($profile->business_skills)-1)
                            </ul>
                        </div>
                    @endif
                @endfor
            </div>
            <div class="clearfix"></div>
            @endif

            @if(!empty($profile->personal_skills))
            <h3>{{ trans('pages/user_profile.skills.personal_skills') }}</h3>
            <div class="cards">
                @for($i=0;$i<sizeof($profile->personal_skills);$i++)
                    @if($i==0)
                        <div class="side">
                            <ul>
                    @endif

                    @if($i==3)
                            </ul>
                        </div>
                        <div class="side">
                            <ul>
                    @endif
                                <li>{{ trans('pages/profile_chef_data.form.personal_skills.checkboxes.'.$profile->personal_skills[$i]) }}</li>
                    @if($i==sizeof($profile->personal_skills)-1)
                            </ul>
                        </div>
                    @endif
                @endfor
            </div>
            <div class="clearfix"></div>
            @endif
        </div>
        <div data-ratio="1.5" data-media-opacity="1" data-media-mime="image/jpeg" data-media-type="image" data-media-url="{{ asset('assets/images/skills_bg.jpg') }}" class="skills-media"></div>
    </div>
    @endif

    @if(!empty($profile->search) || !empty($profile->offer))
    <div class="searchoffer">
        <div class="limit-content">
            @if(!empty($profile->search))
            <div class="side">
                <h3>{{ trans('pages/user_profile.search.title') }}</h3>
                <p>{{ $profile->search }}</p>
            </div>
            @endif

            @if(!empty($profile->offer))
            <div class="side">
                <h3>{{ trans('pages/user_profile.offer.title') }}</h3>
                <p>{{ $profile->offer }}</p>
            </div>
            @endif
        </div>

        <div class="clearfix"></div>
    </div>
    @endif

    @if(!empty($profile->website) || !empty($profile->instagram) || !empty($profile->facebook) || !empty($profile->twitter) || !empty($profile->pinterest) || !empty($profile->tumblr))
    <div class="social">
        <h3>{{ trans('pages/user_profile.social.title') }}</h3>
        <div class="links">
            @if(!empty($profile->website))
            <a href="{{ $profile->website }}" target="_blank"><i class="fa fa-home"></i></a>
            @endif

            @if(!empty($profile->instagram))
            <a href="{{ $profile->instagram }}" target="_blank"><i class="fa fa-instagram"></i></a>
            @endif


            @if(!empty($profile->facebook))
            <a href="{{ $profile->facebook }}" target="_blank"><i class="fa fa-facebook"></i></a>
            @endif

            @if(!empty($profile->twitter))
            <a href="{{ $profile->twitter }}" target="_blank"><i class="fa fa-twitter"></i></a>
            @endif

            @if(!empty($profile->pinterest))
            <a href="{{ $profile->pinterest }}" target="_blank"><i class="fa fa-pinterest"></i></a>
            @endif

            @if(!empty($profile->tumblr))
            <a href="{{ $profile->tumblr }}" target="_blank"><i class="fa fa-tumblr"></i></a>
            @endif
        </div>
    </div>
    @endif

    <hr>

    <div class="profile_social">
        @include('skoty_layout.includes.social_buttons')
    </div>

    @else
        <article class="profile-article center-box">
            <h1 class="new-post-title">{{ $user->name }}</h1>

            @include('skoty_layout.pages.user_profile_menu')

            <div class="text-center">
                <h3>{{ trans('pages/user_profile.not_approved_heading') }}</h3>
                <p>{{ trans('pages/user_profile.not_approved_description') }}</p>
            </div>
        </article>
    @endif
    <div class="clearfix"></div>
@endsection