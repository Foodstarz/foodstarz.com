@extends('skoty_layout.layout')

@section('title', trans('pages/profile_change_language.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        <h1 class="post-title">{{ trans('pages/profile_change_language.content_title') }}</h1>

        <div class="post-content">
            @if(session('success'))
                <div class="alert-box success">{!! session('success') !!}</div>
            @endif

            <form method="POST" action="{{ route('change_language_process') }}">
                {!! csrf_field() !!}

                <p>
                    <b>{{ trans('pages/profile_change_language.form.language.label') }}</b><br>
                    <select name="language">
                        <option value="">{{ trans('pages/profile_change_language.auto_detect') }}</option>
                        @foreach(Config::get('custom.languages') as $code => $lang)
                            <option value="{{ $code }}" @if(Auth::user()->profile->lang == $code) selected @endif>{{ $lang }}</option>
                        @endforeach
                    </select>
                    @if(!empty($errors->get('language')))
                        @foreach($errors->get('language') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <button type="submit">{{ trans('pages/profile_change_language.form.submit.label') }}</button>
                </p>

            </form>

        </div>
    </article>
@endsection