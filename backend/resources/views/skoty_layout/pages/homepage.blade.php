﻿@extends('skoty_layout.layout')

@section('title', trans('pages/homepage.page_title'))

@section('seo-metas')
<meta name="description" content="{{ trans('pages/seo.homepage.description') }}"/>
@endsection

@if($agent->isPhone() || $agent->isTablet())
    @section('header_scripts')
        <script type="text/javascript">
            show_avatar = true;
        </script>
    @endsection

    @section('body_classes')
        show-avatar
    @endsection
@endif

@section('content')
    <h1 class="post-title">Foodstarz Blog</h1>
    <div class="gourmet-ads">

        <div id="ga_9466751">

            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466751');
                });
            </script>

        </div>

    </div>
    <div class="gourmet-ads-mobile">
        <div id="ga_9466758">
            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466758');
                });
            </script>
        </div>
    </div>
<article class="homepage center-box">

    <div class="content posts" style="position: relative; height: 1007.03px;">
        @foreach($feed_items as $item)
            <article class="post hentry" style="position: absolute; left: 0px; top: 0px;">
                <div class="foodstar_header">
                    <a href="{{ $item->link }}" target="_blank">{{ $item->title }}</a>
                </div>
                <p>
                    {!! $item->description !!}
                </p>
            </article>
        @endforeach
    </div>
    <div class="more_recipes">
        <a href="http://blog.foodstarz.com/" target="_blank">more posts</a>
    </div>

    <hr class="hr-gray" />
    <h1 class="post-title">{{ trans('pages/homepage.latest_images') }}</h1>
    <div class="content posts">
        @foreach ($images as $image)
            <article class="post hentry">
                <div class="foodstar_header author">
                    <table>
                        <tr>
                            <td width="70px">
                                <a href="{{ route('view_user_chronic', ['slug' => $image->user->profile->slug]) }}">
                                    <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$image->user->profile->avatar) }}" alt="">
                                </a>
                            </td>
                            <td style="vertical-align: middle;">
                                <a href="{{ route('view_user_chronic', ['slug' => $image->user->profile->slug]) }}">{{ $image->user->name }}</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <figure class="post-thumbnail">
                    @if(Auth::check() && Auth::user()->role == 1)
                        <div class="admin-actions">
                            @if(isset($image->bookmark))
                                <a class="action-unbookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unbookmark', 'type' => 'image', 'id' => $image->id]) }}"></a>
                            @else
                                <a class="action-bookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'bookmark', 'type' => 'image', 'id' => $image->id]) }}"></a>
                            @endif
                            @if($image->hidden == 0)
                                <a class="action-hide" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'hide', 'type' => 'image', 'id' => $image->id]) }}"></a>
                            @else
                                <a class="action-unhide" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unhide', 'type' => 'image', 'id' => $image->id]) }}"></a>
                            @endif
                            <a class="action-edit" token="{{csrf_token()}}" not-ajax="true" href="{{ route('gallery_edit_image', ['id' => $image->id]) }}"></a>
                            <a class="action-delete" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'delete', 'type' => 'image', 'id' => $image->id]) }}"></a>
                        </div>
                    @endif
                    <a href="{{ route('gallery_view_image', $image->id) }}"
                       title="{{ $image->caption }}"><img width="357"
                                                          src="{{ asset('storage/gallery/thumbnails/'.$image->image) }}"
                                                          class="attachment-skoty-blog-post"
                                                          alt="{{ $image->caption }}"/>
                    </a>
                    <h2 class="post-title">
                        <a href="{{ route('gallery_view_image', $image->id) }}">{{ mb_strimwidth($image->caption, 0, 56, '...') }}</a>
                    </h2>
                </figure>

            </article>
        @endforeach
    </div>
    <div class="more_recipes">
        <a href="{{ route('gallery') }}">{{ trans('pages/homepage.more_images') }}</a>
    </div>

    <hr class="hr-gray" />
    <h1 class="post-title">{{ trans('pages/homepage.latest_videos') }}</h1>

    <div class="content posts">
        @foreach ($videos as $video)
            <article class="post hentry">
                <div class="foodstar_header author">
                    <table>
                        <tr>
                            <td width="70px">
                                <a href="{{ route('view_user_chronic', ['slug' => $video->user->profile->slug]) }}">
                                    <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$video->user->profile->avatar) }}" alt="">
                                </a>
                            </td>
                            <td style="vertical-align: middle;">
                                <a href="{{ route('view_user_chronic', ['slug' => $video->user->profile->slug]) }}">{{ $video->user->name }}</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <figure class="post-thumbnail">
                    @if(Auth::check() && Auth::user()->role == 1)
                        <div class="admin-actions">
                            @if(isset($video->bookmark))
                                <a class="action-unbookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unbookmark', 'type' => 'video', 'id' => $video->id]) }}"></a>
                            @else
                                <a class="action-bookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'bookmark', 'type' => 'video', 'id' => $video->id]) }}"></a>
                            @endif
                            @if($video->hidden == 0)
                                <a class="action-hide" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'hide', 'type' => 'video', 'id' => $video->id]) }}"></a>
                            @else
                                <a class="action-unhide" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unhide', 'type' => 'video', 'id' => $video->id]) }}"></a>
                            @endif
                            <a class="action-edit" token="{{csrf_token()}}" not-ajax="true" href="{{ route('video_edit', ['id' => $video->id]) }}"></a>
                            <a class="action-delete" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'delete', 'type' => 'video', 'id' => $video->id]) }}"></a>
                        </div>
                    @endif
                            <!-- <a href="{{ route('video_view', $video->id) }}"
                     title="{{ $video->title }}"><img width="357"
                                                        src="{{ $video->image != '' ? $video->image : asset('assets/images/vimeo.jpg')}}"
                                                        class="attachment-skoty-blog-post"
                                                        alt="{{ $video->title }}"/>
                  </a> -->
                        <a href="{{ route('video_view', $video->id) }}"
                           title="{{ $video->title }}"><img width="357"
                                                            src="{{ $videos_pictures[$video->id]}}"
                                                            class="attachment-skoty-blog-post"
                                                            alt="{{ $video->title }}"/>
                        </a>
                    <h2 class="post-title">
                        <a href="{{ route('video_view', $video->id) }}">{{ mb_strimwidth($video->title, 0, 56, '...') }}</a>
                    </h2>
                </figure>

            </article>
        @endforeach
    </div>
    <div class="more_recipes">
        <a href="{{ route('videos') }}">{{ trans('pages/homepage.more_videos') }}</a>
    </div>

    <hr class="hr-gray" />
    <h1 class="post-title">{{ trans('pages/homepage.gold_partners') }}</h1>

    <div class="content g_partners">
        @foreach ($gold_partners as $partner)
            <div class="one-third-column">
                <a href="{{$partner->url}}" target="_blank">
                    <img src="{{ asset('storage/partners/'.$partner->image) }}" title="{{$partner->name}}" width="{{ $partner->width }}" height="{{ $partner->height }}"/>
                </a>
            </div>
        @endforeach
    </div>

    <hr class="hr-gray" />
    <h1 class="post-title">{{ trans('pages/homepage.latest_foodstarz') }}</h1>

    <div class="content posts">
        @foreach ($foodstarz as $foodstar)
            <article class="post hentry">
                <figure class="post-thumbnail">
                    <a href="{{ route('view_user_chronic', ['slug' => $foodstar->profile->slug]) }}"
                       title="{{ $foodstar->name }}"><img width="357" height="357"
                                                            src="{{ asset('storage/avatars/'.$foodstar->profile->avatar) }}"
                                                            class="attachment-skoty-blog-post"
                                                            alt="{{ $foodstar->name }}"/>
                    </a>
                    <h2 class="post-title">
                        <a href="{{ route('view_user_chronic', ['slug' => $foodstar->profile->slug]) }}">{{ mb_strimwidth($foodstar->name, 0, 56, '...') }}</a>
                    </h2>
                </figure>

            </article>
        @endforeach
    </div>

    <div class="more_recipes">
        <a href="{{ route('foodstarz_listing') }}">{{ trans('pages/homepage.more_foodstarz') }}</a>
    </div>

    <hr class="hr-gray" />

    <h1 class="post-title">{{ trans('pages/homepage.latest_recipes') }}</h1>
    <div class="content posts">
        @foreach ($recipes as $f)
            <article class="post hentry">
                <div class="foodstar_header author">
                    <table>
                        <tr>
                            <td width="70px">
                                <a href="{{ route('view_user_chronic', ['slug' => $f->recipe->user->profile->slug]) }}">
                                    <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$f->recipe->user->profile->avatar) }}" alt="">
                                </a>
                            </td>
                            <td style="vertical-align: middle;">
                                <a href="{{ route('view_user_chronic', ['slug' => $f->recipe->user->profile->slug]) }}">{{ $f->recipe->user->name }}</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <figure class="post-thumbnail">
                    @if(Auth::check() && Auth::user()->role == 1)
                        <div class="admin-actions">
                            @if($f->bookmark)
                                <a class="action-unbookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unbookmark', 'type' => 'recipe', 'id' => $f->recipe->id]) }}"></a>
                            @else
                                <a class="action-bookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'bookmark', 'type' => 'recipe', 'id' => $f->recipe->id]) }}"></a>
                            @endif
                            <a class="action-edit" token="{{csrf_token()}}" not-ajax="true" href="{{ route('recipe_edit', ['id' => $f->recipe->id]) }}"></a>
                            <a class="action-delete" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'delete', 'type' => 'recipe', 'id' => $f->recipe->id]) }}"></a>
                        </div>
                    @endif
                    <a href="{{ route('recipe_view', ['slug' => $f->recipe->slug]) }}"
                       title="{{ $f->recipe->title }}"><img width="357" height="357"
                                                            src="@if(empty($f->recipe->main_image_watermarked)) {{ asset('storage/recipes/original/'.$f->recipe->main_image_original) }} @else {{ asset('storage/recipes/watermark/'.$f->recipe->main_image_watermarked) }} @endif"
                                                            class="attachment-skoty-blog-post"
                                                            alt="{{ $f->recipe->title }}"/>
                    </a>
                    <h2 class="post-title">
                        <a href="{{ route('recipe_view', ['slug' => $f->recipe->slug]) }}">{{ mb_strimwidth($f->recipe->title, 0, 56, '...') }}</a>
                    </h2>
                </figure>


                {{--
                <div class="post-date">
                    <span class="fa fa-clock-o"></span> {{$f->recipe->created_at->format('d M, Y')}}
                </div>
                --}}


                <div class="post-content">
                    <div class="additional-info">
                        <img src="{{ asset('assets/images/yield.png') }}" alt="{{ trans('pages/homepage.serves') }}"> <span>@if(Lang::locale() == 'de'){{ $f->recipe->portions }} {{ trans('pages/homepage.serves') }}@else{{ trans('pages/homepage.serves') }} {{ $f->recipe->portions }}@endif</span>
                    </div>

                    <div class="additional-info" style="float:right;">
                        <img src="{{ asset('assets/images/'.strtolower($f->recipe->difficulty).'.png') }}" alt="{{ trans('pages/add_recipe.form.difficulty.options.'.$f->recipe->difficulty) }}"> <span>{{ trans('pages/add_recipe.form.difficulty.options.'.$f->recipe->difficulty) }}</span>
                    </div>

                    <div class="clearfix"></div>

                    {{--
                    <p>
                        <a href="{{ route('view_user_chronic', ['slug' => $f->recipe->user->profile->slug]) }}" class="more-link">{{ trans('pages/homepage.read_more') }}</a>
                    </p>
                    --}}
                </div>
            </article>
        @endforeach
    </div>
    <div class="more_recipes">
        <a href="{{ route('recipes') }}">{{ trans('pages/homepage.more_recipes') }}</a>
    </div>

    @if(count($charity_partners) != 0)
    <hr class="hr-gray" />
    <h1 class="post-title">{{ trans('pages/homepage.charity_partners') }}</h1>

    <div class="content g_partners">
        @foreach ($charity_partners as $partner)
            <div class="one-third-column">
                <a href="{{$partner->url}}" target="_blank">
                    <img src="{{ asset('storage/partners/'.$partner->image) }}" title="{{$partner->name}}" width="{{ $partner->width }}" height="{{ $partner->height }}"/>
                </a>
            </div>
        @endforeach
    </div>
    @endif
</article>
@endsection
