@extends('skoty_layout.layout')

@section('title', trans('pages/profile_interview.page_title'))

@section('header_scripts')
    <script src="{{ asset('assets/js/jquery.cropit.js') }}"></script>

    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')
        
        @include('skoty_layout.pages.profile_menu')

        <h1 class="post-title">{{ trans('pages/profile_interview.content_title') }}</h1>
        
        <div class="post-content">
            @if(session('base_profile_data') && session('avatar') && session('chefData'))

                @if(session('success'))
                    <div class="alert-box success">{!! session('success') !!}</div>
                @endif

                <form method="POST" action="{{ route('interview_process') }}">
                    {!! csrf_field() !!}

                    @foreach($questions as $question)
                        <p>
                            @if(empty(Auth::user()->profile->lang))
                                @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                    <b>{{ json_decode($question->question, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}</b> @if($question->required == 1)<span class="required">{{ trans('general.required_fields') }}</span>@endif<br/>
                                @else
                                    <b>{{ json_decode($question->question, true)['en'] }}</b> @if($question->required == 1)<span class="required">{{ trans('general.required_fields') }}</span>@endif<br/>
                                @endif
                            @else
                                <b>{{ json_decode($question->question, true)[Auth::user()->profile->lang] }}</b> @if($question->required == 1)<span class="required">{{ trans('general.required_fields') }}</span>@endif<br/>
                            @endif

                            @if($question->answer_type == 'input')
                                @if(empty($interview))
                                    <input type="text" name="interview[{{ $question->id }}]"/>
                                @else
                                    <input type="text" name="interview[{{ $question->id }}]" value="@if(isset($interview[$question->id]) && !empty($interview[$question->id])){{ $interview[$question->id] }}@endif"/>
                                @endif
                            @else
                                @if(empty($interview))
                                    <textarea name="interview[{{ $question->id }}]" rows="10"></textarea>
                                @else
                                    <textarea name="interview[{{ $question->id }}]" rows="10">@if(isset($interview[$question->id]) && !empty($interview[$question->id])){{ $interview[$question->id] }}@endif</textarea>
                                @endif
                            @endif
                        </p>
                    @endforeach

                    <p>
                        @if(!empty($errors->get('interview')))
                            @foreach($errors->get('interview') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <button type="submit">{{ trans('pages/profile_interview.form.submit.label') }}</button>
                    </p>
                </form>
            @else
                @if(!session('base_profile_data'))
                    <h4>{{ trans('pages/profile_interview.base_profile_data_error') }}</h4>
                @endif

                @if(!session('avatar'))
                    <h4>{{ trans('pages/profile_interview.upload_avatar_error') }}</h4>
                @endif

                @if(!session('chefData'))
                    <h4>{{ trans('pages/profile_interview.chef_data_error') }}</h4>
                @endif
            @endif
        </div>
    </article>
@endsection