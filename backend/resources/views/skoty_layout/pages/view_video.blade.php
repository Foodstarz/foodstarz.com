@extends('skoty_layout.layout')

@section('title', trans('pages/view_video.page_title'))

@section('og-tags')
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $video->title }}" />
    <meta property="og:description" content="{{ $video->description }}" />
    <meta property="og:url" content="{{ route('video_view', $video->id) }}" />
    <meta property="og:site_name" content="Foodstarz" />
    <meta property="fb:admins" content="582794454" />
    <meta property="og:image" content="{{ $social_image }}" />
@endsection

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/rrssb.css') }}" />
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/js/rrssb.min.js') }}"></script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box" style="margin-top: 85px;">
        <h1 class="post-title">{{ $video->title }}</h1>

        @if(Auth::check() && ($video->user_id == Auth::user()->id || Auth::user()->role == 1))
            <center><a href="{{route("video_edit", $video->id)}}" class="edit_recipe_button">{{ trans('pages/view_video.edit') }}</a></center>
            <form method="post" action="{{ route('video_delete_process', $video->id) }}">
                {{ csrf_field() }}
                <center><a href="#" id="deleteVideoBtn" class="edit_recipe_button">{{ trans('pages/view_video.delete') }}</a></center>
            </form>
            <form method="post" action="{{ route('video_hide_process', $video->id) }}">
                {{ csrf_field() }}
                <center><a href="#" id="hideVideoBtn" class="edit_recipe_button">@if($video->hidden == 0){{ trans('pages/view_video.hide') }}@else{{ trans('pages/view_video.unhide') }}@endif</a></center>
            </form>
            <script>
                $(function() {
                    $('#deleteVideoBtn').click(function(e) {
                        e.preventDefault();
                        var ask = confirm('{{ trans('pages/view_video.confirm_deletion') }}');
                        if(ask) {
                            $(this).closest('form').submit();
                        }
                    });

                    $('#hideVideoBtn').click(function(e) {
                        e.preventDefault();
                        var ask = confirm('{{ trans('pages/view_video.confirm_hide') }}');
                        if(ask) {
                            $(this).closest('form').submit();
                        }
                    });
                });
            </script>
        @endif

        @if(Auth::check() && Auth::user()->role == 1)
            @if($bookmarked)
                <a href="{{ route('bookmark_remove_resource', ['type' => 3, 'id' => $video->id]) }}" class="edit_recipe_button">{{ trans('bookmarks.remove') }}</a>
            @else
                <a href="{{ route('bookmark_resource', ['type' => 3, 'id' => $video->id]) }}" class="edit_recipe_button">{{ trans('bookmarks.add') }}</a>
            @endif
        @endif

        <div class="post-content">
            <iframe src="https://player.vimeo.com/video/{{ explode('/', $video->video)[2] }}" width="100%" height="300px" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
        </div>

        @if(Auth::check() && Auth::user()->role == 1)
            <hr style="margin-bottom: 5px;">
            <textarea style="width: 100%;" onClick="this.setSelectionRange(0, this.value.length)">Foodstar {{ $video->user->name }} (@) shared a new video via Foodstarz PLUS /// {{ $video->title }}

# # # # # #foodstarz

If you also want to get featured on Foodstarz, just join us, create your own chef profile for free, and start sharing recipes, images and videos.

Foodstarz - Your International Premium Chef Network</textarea>
            @include('skoty_layout.includes.social_buttons')
        @endif
    </article>
@endsection