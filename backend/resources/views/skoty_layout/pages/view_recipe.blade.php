@extends('skoty_layout.layout')

@section('title', $recipe->title . ' | Foodstarz.com')

@section('seo-metas')
<meta name="description" content="Check out this awesome recipe!"/>
@endsection

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/rrssb.css') }}" />
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/js/rrssb.min.js') }}"></script>
@endsection

@section('seo-metas')
    <meta name="description" content="{{ $recipe->title }}"/>
@endsection

@section('og-tags')
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="@if(empty($recipe->main_image_watermarked)){{ asset('storage/recipes/original/'.$recipe->main_image_original) }}@else{{ asset('storage/recipes/watermark/'.$recipe->main_image_watermarked) }}@endif" />
    <meta property="og:title" content="{{ $recipe->title }}" />
    <meta property="og:description" content="Check out this awesome recipe!" />
    <meta property="og:url" content="{{ route('recipe_view', $recipe->slug) }}" />
    <meta property="og:site_name" content="Foodstarz" />
    <meta property="fb:admins" content="582794454" />
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="recipe-article center-box">
        <h1 class="post-title">{{ $recipe->title }}</h1>

        @if(Auth::check() && Auth::user()->id == $recipe->user_id)
        <center>
            <a href="{{route("recipe_edit", $recipe->id)}}" class="edit_recipe_button">{{ trans('pages/edit_recipe.content_title') }}</a>

            @if(Auth::user()->role == 1)
                @if($bookmarked)
                    <a href="{{ route('bookmark_remove_resource', ['type' => 1, 'id' => $recipe->id]) }}" class="edit_recipe_button">{{ trans('bookmarks.remove') }}</a>
                @else
                    <a href="{{ route('bookmark_resource', ['type' => 1, 'id' => $recipe->id]) }}" class="edit_recipe_button">{{ trans('bookmarks.add') }}</a>
                @endif
            @endif
        </center>
        @endif
        
        <figure class="post-thumbnail">
            <img width="800" height="800" alt="{{ $recipe->title }}" src="@if(empty($recipe->main_image_watermarked)) {{ asset('storage/recipes/original/'.$recipe->main_image_original) }} @else {{ asset('storage/recipes/watermark/'.$recipe->main_image_watermarked) }} @endif">
        </figure>

        <div class="post-content-wrapper">
            <div class="post-content">
                <ul style="list-style-type: none; margin-top: 28px;">
                    <li><img src="{{ asset('assets/images/serves.png') }}" class="ficon">{{ trans('pages/homepage.serves') }} {{ $recipe->portions }}</li>
                    <li><img src="{{ asset('assets/images/time.png') }}" class="ficon">{{ $recipe->preparation_time }}</li>
                    <li><img src="{{ asset('assets/images/difficulty.png') }}" class="ficon">{{ trans('pages/view_recipe.difficulty.'.$recipe->difficulty) }}</li>
                </ul>

                @if(isset($recipe->components['html']))
                    {!! $recipe->components['html'] !!}
                @else
                    <hr>
                    <h3>{{ trans('pages/add_recipe.form.components.ingredients') }}:</h3>
                    
                    @foreach($components as $component)
                    <div>
                        <strong>{{ $component['name'] }}</strong>
                        <ul>
                            @for($n=0;$n<sizeof($component['ingredients']['name']);$n++)
                                <li>@if(isset($component['ingredients']['quantity'][$n]) && !empty(trim($component['ingredients']['quantity'][$n]))){{ $component['ingredients']['quantity'][$n] }}@endif @if(isset($component['ingredients']['unit'][$n]) && !empty(trim($component['ingredients']['unit'][$n]))){{ $component['ingredients']['unit'][$n] }}(s)@endif {{ $component['ingredients']['name'][$n] }}</li>
                            @endfor
                        </ul>
                    </div>
                    @endforeach

                    <h3>{{ trans('pages/view_recipe.method') }}:</h3>
                    @foreach($components as $component)
                    <div>
                        <strong>{{ $component['name'] }}</strong>

                        @if(!empty(trim($component['image'])))
                        <br><br>
                        <figure class="post-thumbnail">
                            <img width="800" height="800" alt="{{ $component['name'] }}" src="{{ asset('storage/recipes/'.(!empty($component["image_watermarked"]) ? 'watermark/'.$component["image_watermarked"] : 'original/'.$component["image"])) }}">
                        </figure>
                        @endif

                        <ol>
                            @foreach($component['instructions'] as $instruction)
                            <li>{{ $instruction }}</li>
                            @endforeach
                        </ol>
                    </div>
                    @endforeach
                    <hr>
                @endif
            </div>

        </div>

        @if(Auth::check() && Auth::user()->role == 1)
            <textarea style="width: 100%;" onClick="this.setSelectionRange(0, this.value.length)">Foodstar {{ $recipe->user->name }} (@) shared a new recipe via Foodstarz PLUS /// {{ $recipe->title }}

# # # # # #foodstarz

If you also want to get featured on Foodstarz, just join us, create your own chef profile for free, and start sharing recipes, images and videos.

Foodstarz - Your International Premium Chef Network</textarea>
        @endif

        @include('skoty_layout.includes.social_buttons')

        <div class="comments">
            <div id="disqus_thread"></div>
            <script type="text/javascript">
                /* * * CONFIGURATION VARIABLES * * */
                var disqus_shortname = 'foodstarz';

                /* * * DON'T EDIT BELOW THIS LINE * * */
                (function () {
                    var dsq = document.createElement('script');
                    dsq.type = 'text/javascript';
                    dsq.async = true;
                    dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                    (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
                })();
            </script>
            <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
        </div>

    </article>
    <div class="clearfix"></div>
@endsection