@extends('skoty_layout.layout')

@section('title', trans('pages/videos.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('content')
    <h1 class="post-title">{{ trans('pages/videos.content_title') }}</h1>

    <div class="gourmet-ads">
        <div id="ga_9466753">
            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466753');
                });
            </script>
        </div>
    </div>

    <div class="gourmet-ads-mobile">
        <div id="ga_9466758">
            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466758');
                });
            </script>
        </div>
    </div>

    <article class="homepage center-box">

        <div class="content posts">
            @foreach ($videos as $video)
                <article class="post hentry">
                    <div class="foodstar_header author">
                        <table>
                            <tr>
                                <td width="70px">
                                    <a href="{{ route('view_user_profile', ['slug' => $video->user->profile->slug]) }}">
                                        <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$video->user->profile->avatar) }}" alt="">
                                    </a>
                                </td>
                                <td style="vertical-align: middle;">
                                    <a href="{{ route('view_user_profile', ['slug' => $video->user->profile->slug]) }}">{{ $video->user->name }}</a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <figure class="post-thumbnail">
                        @if(Auth::check() && Auth::user()->role == 1)
                            <div class="admin-actions">
                                @if(isset($video->bookmark))
                                    <a class="action-unbookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unbookmark', 'type' => 'video', 'id' => $video->id]) }}"></a>
                                @else
                                    <a class="action-bookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'bookmark', 'type' => 'video', 'id' => $video->id]) }}"></a>
                                @endif
                                @if($video->hidden == 0)
                                    <a class="action-hide" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'hide', 'type' => 'video', 'id' => $video->id]) }}"></a>
                                @else
                                    <a class="action-unhide" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unhide', 'type' => 'video', 'id' => $video->id]) }}"></a>
                                @endif
                                <a class="action-edit" token="{{csrf_token()}}" not-ajax="true" href="{{ route('video_edit', ['id' => $video->id]) }}"></a>
                                <a class="action-delete" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'delete', 'type' => 'video', 'id' => $video->id]) }}"></a>
                            </div>
                        @endif
                                <!-- <a href="{{ route('video_view', $video->id) }}"
                     title="{{ $video->title }}"><img width="357"
                                                        src="{{ $video->image != '' ? $video->image : asset('assets/images/vimeo.jpg')}}"
                                                        class="attachment-skoty-blog-post"
                                                        alt="{{ $video->title }}"/>
                  </a> -->
                            <a href="{{ route('video_view', $video->id) }}"
                               title="{{ $video->title }}"><img width="357"
                                                                src="{{ $videos_pictures[$video->id]}}"
                                                                class="attachment-skoty-blog-post"
                                                                alt="{{ $video->title }}"/>
                            </a>
                        <h2 class="post-title">
                            <a href="{{ route('video_view', $video->id) }}">{{ mb_strimwidth($video->title, 0, 56, '...') }}</a>
                        </h2>
                    </figure>

                </article>
            @endforeach
        </div>
        @if(count($videos) < 1)
            <div class="text-center">
                <h3>{{ trans('pages/videos.no_videos') }}</h3>
            </div>
        @endif
        <div class="content text_center">
            <?php Foodstarz\Models\Pagination::render($videos); ?>
        </div>

    </article>
@endsection