@extends('skoty_layout.layout')

@section('title', trans('pages/register.page_title'))

@section('seo-metas')
<meta name="description" content="Everybody is welcome to sign up in our website!"/>
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
    <script src="{{ asset('assets/js/jquery.pwstrength.min.js') }}"></script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        <h1 class="post-title">{{ trans('pages/register.content_title') }}</h1>

        <div class="post-content reg">

            @if(!empty($success))
                <div class="alert-box success">{{$success}}</div>
                <!--div class="alert-box success">{--{ session('success') }--}</div-->
            @elseif(!empty($error))
                <div class="alert-box success">{{$success}}</div>
                <!--div class="alert-box error">{--{ session('error') }--}</div-->
            @else
                <form method="POST" action="{{ route('register_process') }}">
                    {!! csrf_field() !!}

                    <p>
                        <b>{{ trans('pages/register.form.name.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                        <input type="text" name="name" @if(!empty($errors->get('name'))) class="input-error" @endif placeholder="{{ trans('pages/register.form.name.placeholder') }}" value="{{ Request::get('name') }}">
                        @if(!empty($errors->get('name')))
                            @foreach($errors->get('name') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <b>{{ trans('pages/register.form.email.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                        <input type="email" name="email" @if(!empty($errors->get('email'))) class="input-error" @endif placeholder="{{ trans('pages/register.form.email.placeholder') }}" value="{{ Request::get('email') }}">
                        @if(!empty($errors->get('email')))
                            @foreach($errors->get('email') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <script>
                            $(function() {
                                $('input[name=password]').pwstrength();
                            });
                        </script>
                        <b>{{ trans('pages/register.form.password.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                        <input type="password" name="password" @if(!empty($errors->get('password'))) class="input-error" @endif placeholder="{{ trans('pages/register.form.password.placeholder') }}" data-indicator="pwindicator">
                        <div id="pwindicator">
                            <div class="bar"></div>
                            <div class="label"></div>
                        </div>
                        @if(!empty($errors->get('password')))
                            @foreach($errors->get('password') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <b>{{ trans('pages/register.form.password_confirmation.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                        <input type="password" name="password_confirmation" @if(!empty($errors->get('password_confirmation'))) class="input-error" @endif placeholder="{{ trans('pages/register.form.password_confirmation.placeholder') }}">
                    </p>

                    <p>
                        <b>{{ trans('pages/register.form.culinary_profession.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                        <input type="radio" name="culinary_profession" value="amateur" @if(Request::get('culinary_profession') == 'amateur') checked @endif>
                        {{ trans('pages/register.form.culinary_profession.amateur.label') }}<br>
                        <input type="radio" name="culinary_profession" value="blogger" @if(Request::get('culinary_profession') == 'blogger') checked @endif>
                        {{ trans('pages/register.form.culinary_profession.blogger.label') }}<br>
                        <input type="radio" name="culinary_profession" value="professional" @if(Request::get('culinary_profession') == 'professional') checked @endif>
                        {{ trans('pages/register.form.culinary_profession.professional.label') }}
                        @if(!empty($errors->get('culinary_profession')))
                            @foreach($errors->get('culinary_profession') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        @if(Lang::locale() == 'de')
                            <span class="required">{{ trans('general.required_fields') }}</span> <input type="checkbox" name="agreement" value="yes" @if(Request::get('agreement') == 'yes') checked @endif> {{ trans('pages/register.form.agreement.label') }} <a href="{{ route('c_page', ['slug' => 'tos']) }}" target="_blank">{{ trans('pages/register.form.agreement.tos') }}</a> and <a href="{{ route('c_page', ['slug' => 'privacy']) }}" target="_blank">{{ trans('pages/register.form.agreement.privacy') }}</a> {{ trans('pages/register.form.agreement.second_label') }}
                        @else
                            <span class="required">{{ trans('general.required_fields') }}</span> <input type="checkbox" name="agreement" value="yes" @if(Request::get('agreement') == 'yes') checked @endif> {{ trans('pages/register.form.agreement.label') }} <a href="{{ route('c_page', ['slug' => 'tos']) }}" target="_blank">{{ trans('pages/register.form.agreement.tos') }}</a> and <a href="{{ route('c_page', ['slug' => 'privacy']) }}" target="_blank">{{ trans('pages/register.form.agreement.privacy') }}</a>
                        @endif
                        @if(!empty($errors->get('agreement')))
                            @foreach($errors->get('agreement') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        {!! Recaptcha::render() !!}
                        @if(!empty($errors->get('g-recaptcha-response')))
                            @foreach($errors->get('g-recaptcha-response') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <button type="submit">{{ trans('pages/register.form.submit.label') }}</button>
                    </p>
                </form>
            @endif
        </div>
    </article>
@endsection