@extends('skoty_layout.layout')

@section('title', trans('pages/video_add.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/js/custom/add_video.js') }}"></script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        <h1 class="post-title"> </h1>

        <div class="post-content">
            <div id="success" class="alert-box success" style="display: none;"></div>

            <form method="POST" action="{{ route('add_video_process') }}" id="addVideoForm">
                <fieldset>
                    <legend>{{ trans('pages/video_add.content_title') }}</legend>

                    {!! csrf_field() !!}

                    <p>
                        <b>{{ trans('pages/video_add.accepted_file_formats') }}</b>
                        <br>
                        <input type="file" name="video" accept="video/mp4,video/x-m4v,video/*" capture="camcorder" />
                    </p>
                    <div class="video_errors errDiv"></div>

                    <p>
                        <b>{{ trans('pages/video_add.form.title.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br/>
                        <input type="text" name="title" placeholder="{{ trans('pages/video_add.form.title.placeholder') }}" value="" />
                    </p>
                    <div class="title_errors errDiv"></div>

                    <p>
                        <b>{{ trans('pages/video_add.form.description.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br/>
                        <textarea style="width: 100%;" rows="6" name="description" placeholder="{{ trans('pages/video_add.form.description.placeholder') }}"></textarea>
                    </p>
                    <div class="description_errors errDiv"></div>

                    <p>
                        <b>{{ trans('pages/video_add.form.social.label') }}</b><br/>
                        @if(isset($login_link))
                            <a href="{{ $login_link }}" target="_blank" style="color: #003366; border-bottom: 1px dotted #003366;"><i class="fa fa-facebook"></i> {{ trans('pages/video_add.form.social.configure') }}</a>
                        @else
                            <input type="checkbox" name="facebook" value="1" checked> {{ trans('pages/video_add.form.social.facebook') }}
                        @endif
                    </p><br>

                    <p>
                        <button type="submit">{{ trans('pages/video_add.form.submit.label') }}</button>
                    </p>

                    <div class="request-loading hidden text-center">
                        <br>
                        <img src="{{ asset('assets/images/ajax-loader.gif') }}" />
                    </div>
                </fieldset>
            </form>
        </div>
    </article>
@endsection
