@extends('skoty_layout.layout')

@section('title', trans('pages/recipe_categories.page_title'))

@section('seo-metas')
<meta name="description" content="All recipes are divided into different categories for the ease-of-use of our visitors."/>
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('content')
<article class="homepage center-box">
    <h1 class="post-title">{{ trans('pages/recipe_categories.content_title') }}</h1>
    <div class="content posts">
        @foreach ($categories as $category)
            <article class="post hentry">
                <figure class="post-thumbnail">
                    <a href="{{ url("recipe/category", $category->id) }}">
                        <img width="357" height="357"
                          src="{{ asset('storage/recipe_categories/'.$category->image) }}"
                          class="attachment-skoty-blog-post"/>
                    </a>
                    <h2 class="post-title">
                        <a href="{{ url("recipe/category", $category->id) }}">
                            @if(Auth::check())
                                @if(empty(Auth::user()->profile->lang))
                                    @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                        {{ json_decode($category->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                                    @else
                                        {{ json_decode($category->title, true)['en'] }}
                                    @endif
                                @else
                                    {{ json_decode($category->title, true)[Auth::user()->profile->lang] }}
                                @endif
                            @else
                                @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                    {{ json_decode($category->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                                @else
                                    {{ json_decode($category->title, true)['en'] }}
                                @endif
                            @endif
                        </a>
                    </h2>
                </figure>

            </article>
        @endforeach
    </div>
</article>
@endsection