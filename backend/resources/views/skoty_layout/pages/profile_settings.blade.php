@extends('skoty_layout.layout')

@section('title', trans('pages/profile_settings.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
    <script src="{{ asset('assets/js/jquery.pwstrength.min.js') }}"></script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        <h1 class="post-title"> {{-- trans('pages/profile_settings.content_title') --}}</h1>
        
        <div class="post-content">

            <form action="{{ route('profile_core_data') }}" method="get">
                <button type="submit">{{ trans('pages/profile_settings.actions.edit_profile') }}</button>
            </form>
            
            <br>
            
            <form action="{{ route('change_email') }}" method="get">
                <button type="submit">{{ trans('pages/profile_settings.actions.change_email') }}</button>
            </form>

            <br>
            @if($backgrounds)
            <form action="{{ route('change_background') }}" method="get">
                <button type="submit">{{ trans('pages/profile_settings.actions.change_background') }}</button>
            </form>
            
            <br>
            @endif
            
            <form action="{{ route('change_password') }}" method="get">
                <button type="submit">{{ trans('pages/profile_settings.actions.change_password') }}</button>
            </form>
            
            <br>

            <form action="{{ route('change_language') }}" method="get">
                <button type="submit">{{ trans('pages/profile_settings.actions.change_language') }}</button>
            </form>

            <br>
            
            <form action="{{ route('close_account') }}" method="get">
                <button type="submit">{{ trans('pages/profile_settings.actions.close_account') }}</button>
            </form>

        </div>
    </article>
@endsection