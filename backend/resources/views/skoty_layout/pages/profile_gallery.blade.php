﻿@extends('skoty_layout.layout')

@section('title', trans('pages/profile_gallery.page_title'))

@section('seo-metas')
    <meta name="description" content="{{ trans('profile_gallery.page_description', ['name' => $user->name]) }}"/>
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('content')
    @if($user->role == 4 || $user->role == 1 || (Auth::check() && ($user->id == Auth::user()->id || Auth::user()->role == 1)))
        <article class="profile-article center-box">
            <h1 class="new-post-title">{{ $user->name }}</h1>
            <p class="text-center text-bold">@if(isset($profile->culinary_profession)) {{ trans(Config::get('profile.culinary_profession')[$profile->culinary_profession]) }} | @endif @if(isset($profile->city)) {{ $profile->city }} | @endif @if(isset($profile->country)) @if(isset(Config::get('custom.countryList')[$profile->country])) {{ Config::get('custom.countryList')[$profile->country] }} @else {{ $profile->country }} @endif @endif</p>

            @include('skoty_layout.pages.user_profile_menu')
        </article>
        <div class="clearfix"></div>

        <article class="homepage center-box" style="margin-top: 30px;">
            {{--<h1 class="post-title">{{ trans('pages/profile_gallery.content_title') }}</h1>--}}

            <div class="content posts">
                @foreach ($gallery as $item)
                    <article class="post hentry">
                        <div class="foodstar_header author">
                            <table>
                                <tr>
                                    <td width="70px">
                                        <a href="{{ route('view_user_chronic', ['slug' => $item->user->profile->slug]) }}">
                                            <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$item->user->profile->avatar) }}" alt="">
                                        </a>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <a href="{{ route('view_user_chronic', ['slug' => $item->user->profile->slug]) }}">{{ $item->user->name }}</a>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <figure class="post-thumbnail">
                            <a href="{{ route('gallery_view_image', $item->id) }}"
                               title="{{ $item->caption }}"><img width="357"
                                                                  src="{{ asset('storage/gallery/thumbnails/'.$item->image) }}"
                                                                  class="attachment-skoty-blog-post"
                                                                  alt="{{ $item->caption }}"/>
                            </a>
                            <h2 class="post-title">
                                <a href="{{ route('gallery_view_image', $item->id) }}">{{ mb_strimwidth($item->caption, 0, 56, '...') }}</a>
                            </h2>
                        </figure>

                    </article>
                @endforeach
            </div>
            @if(count($gallery) < 1)
                <div class="text-center">
                    <h3>{{ trans('pages/profile_gallery.no_images') }}</h3>
                </div>
            @endif
            <div class="content text_center">
                <?php Foodstarz\Models\Pagination::render($gallery); ?>
            </div>

        </article>
    @else
        <article class="profile-article center-box">
            <h1 class="new-post-title">{{ $user->name }}</h1>

            @include('skoty_layout.pages.user_profile_menu')

            <div class="text-center">
                <h3>{{ trans('pages/user_profile.not_approved_heading') }}</h3>
                <p>{{ trans('pages/user_profile.not_approved_description') }}</p>
            </div>
        </article>
    @endif
@endsection