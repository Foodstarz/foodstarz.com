@extends('skoty_layout.layout')

@section('title', trans('pages/jury.page_title'))

@section('seo-metas')
<meta name="description" content="Our Jury ensures the quality content of the best cooking social media."/>
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('body_classes')
show-avatar page
@endsection

@section('content')
<article class="homepage center-box">
    <h1 class="post-title">{{ trans('pages/jury.content_title') }}</h1>

    <div class="content posts">
        @foreach ($jury as $jury)
        <article class="post type-post">
            <div class="category_title">  
                <center><a href="{{ route('view_user_chronic', ['slug' => $jury->user->profile->slug]) }}"><img width="250px" src="{{ asset('storage/avatars/'.$jury->user->profile->avatar) }}" /></a></center>
                <h3><center><a href="{{ route('view_user_chronic', ['slug' => $jury->user->profile->slug]) }}">{{ $jury->user->name }}</a></center></h3>
                <div class="atbb-team-member-details text_center">
                    <div class="atbb-team-member-job">{{ $jury->user->profile->city }} | {{ $jury->user->profile->country }}</div>
                    <div class="atbb-team-member-description">
                        <p>{{ $jury->text }}</p>
                    </div>
                </div>
            </div>
        </article>
        @endforeach
    </div>
</article>
@endsection