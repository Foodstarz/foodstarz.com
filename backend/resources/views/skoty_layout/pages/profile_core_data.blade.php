@extends('skoty_layout.layout')

@section('title', trans('pages/profile_core_data.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')
            
        @include('skoty_layout.pages.profile_menu')

        <h1 class="post-title">{{ trans('pages/profile_core_data.content_title') }}</h1>        
        
        <div class="post-content">

            @if(session('success'))
                <div class="alert-box success">{!! session('success') !!}</div>
            @endif

            <form method="POST" action="{{ route('profile_core_data_process') }}">
                {!! csrf_field() !!}

                <p>
                    <b>{{ trans('pages/profile_core_data.form.name.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="text" name="name" @if(!empty($errors->get('name'))) class="input-error" @endif placeholder="{{ trans('pages/profile_core_data.form.name.placeholder') }}" value="{{ session('user')->name }}">
                    @if(!empty($errors->get('name')))
                        @foreach($errors->get('name') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                {{--
                <p>
                    <b>{{ trans('pages/profile_core_data.form.email.label') }}</b><br>
                    <input type="email" name="email" placeholder="{{ trans('pages/profile_core_data.form.email.placeholder') }}" value="{{ session('user')->email }}" disabled>
                </p>
                --}}

                <p>
                    <b>{{ trans('pages/profile_core_data.form.gender.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="radio" name="gender" value="male" @if(session('profile')->gender == 'male') checked @endif>
                    {{ trans('pages/profile_core_data.form.gender.choices.male') }}<br>
                    <input type="radio" name="gender" value="female" @if(session('profile')->gender == 'female') checked @endif>
                    {{ trans('pages/profile_core_data.form.gender.choices.female') }}
                    @if(!empty($errors->get('gender')))
                        @foreach($errors->get('gender') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <b>{{ trans('pages/profile_core_data.form.country.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <select name="country" @if(!empty($errors->get('country'))) class="input-error" @endif>
                        <option value="">{{ trans('pages/profile_core_data.form.country.placeholder') }}</option>
                        @foreach(Config::get('custom.countryList') as $countryCode => $countryName)
                            <option value="{{ $countryCode }}" @if(session('profile')->country == $countryCode) selected @endif>{{ trans('general.countryList.'.strtoupper($countryCode)) }}</option>
                        @endforeach
                    </select>
                    @if(!empty($errors->get('country')))
                        @foreach($errors->get('country') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <b>{{ trans('pages/profile_core_data.form.city.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="text" name="city" @if(!empty($errors->get('city'))) class="input-error" @endif placeholder="{{ trans('pages/profile_core_data.form.city.placeholder') }}" value="{{ session('profile')->city }}">
                    @if(!empty($errors->get('city')))
                        @foreach($errors->get('city') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <b>{{ trans('pages/profile_core_data.form.culinary_profession.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="radio" name="culinary_profession" value="amateur" @if(session('profile')->culinary_profession == 'amateur') checked @endif>
                    {{ trans('pages/profile_core_data.form.culinary_profession.amateur.label') }}<br>
                    <input type="radio" name="culinary_profession" value="blogger" @if(session('profile')->culinary_profession == 'blogger') checked @endif>
                    {{ trans('pages/profile_core_data.form.culinary_profession.blogger.label') }}<br>
                    <input type="radio" name="culinary_profession" value="professional" @if(session('profile')->culinary_profession == 'professional') checked @endif>
                    {{ trans('pages/profile_core_data.form.culinary_profession.professional.label') }}
                    @if(!empty($errors->get('culinary_profession')))
                        @foreach($errors->get('culinary_profession') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <button type="submit">{{ trans('pages/profile_core_data.form.submit.label') }}</button>
                </p>
            </form>

        </div>
    </article>
@endsection