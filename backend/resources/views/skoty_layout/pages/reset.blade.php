@extends('skoty_layout.layout')

@section('title', trans('pages/reset.page_title'))

@section('seo-metas')
<meta name="description" content="Did you forget your password? No problem - we will help you find it!"/>
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        <h1 class="post-title">{{ trans('pages/reset.content_title') }}</h1>

        <div class="post-content">

            <form method="POST" action="{{ route('password_reset_process') }}">
                {!! csrf_field() !!}
                <input type="hidden" name="token" value="{{ $token }}">

                <p>
                    <b>{{ trans('pages/reset.form.email.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="email" name="email" @if(!empty($errors->get('email'))) class="input-error" @endif placeholder="{{ trans('pages/reset.form.email.placeholder') }}" value="{{ old('email') }}">
                    @if(!empty($errors->get('email')))
                        @foreach($errors->get('email') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <b>{{ trans('pages/reset.form.password.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="password" name="password" @if(!empty($errors->get('password'))) class="input-error" @endif placeholder="{{ trans('pages/reset.form.password.placeholder') }}">
                    @if(!empty($errors->get('password')))
                        @foreach($errors->get('password') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <b>{{ trans('pages/reset.form.password_confirmation.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="password" name="password_confirmation" placeholder="{{ trans('pages/reset.form.password_confirmation.placeholder') }}">
                </p>

                <p>
                    <button type="submit">{{ trans('pages/reset.form.submit.label') }}</button>
                </p>
            </form>

        </div>
    </article>
@endsection