@extends('skoty_layout.layout')

@section('title', trans('pages/change_background.page_title'))

@section('header_scripts')
    <script src="{{ asset('assets/js/jquery.cropit.js') }}"></script>

    <script type="text/javascript">
        show_background = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        @include('skoty_layout.pages.profile_menu')

        <h1 class="post-title">{{ trans('pages/change_background.content_title') }}</h1>

        <div class="post-content">
            {{--@if(session('base_profile_data'))--}}
                <div id="success" class="alert-box success" style="display: none;">
                    @if(Auth::user()->role != 4)
                        {{ trans('pages/change_background.messages.congratulations') }}
                        <br>
                        {{ trans('pages/change_background.messages.next') }}
                        <br>
                        {{ trans('pages/change_background.messages.uploading') }} <a href="{{ route('recipe_add') }}">{{ trans('pages/change_background.messages.upload_recipes') }}</a>, <a href="{{ route('gallery') }}">{{ trans('pages/change_background.messages.upload_images') }}</a> {{ trans('pages/change_background.messages.or') }} <a href="{{ route('videos') }}">{{ trans('pages/change_background.messages.upload_videos') }}</a>
                        <br>
                        {{ trans('pages/change_background.messages.or') }}
                        <br>
                        {{ trans('pages/change_background.messages.completing') }} <a href="{{ route('profile_chef_data') }}">{{ trans('pages/change_background.messages.chef_profile') }}</a> {{ trans('pages/change_background.messages.or') }} <a href="{{ route('interview') }}">{{ trans('pages/change_background.messages.interview') }}</a>
                    @else
                        {{ trans('pages/change_background.messages.success') }}
                        <br>
                        {{ trans('pages/change_background.messages.proceed') }} <a href="{{ route('profile_chef_data') }}">{{ trans('pages/change_background.messages.proceed_action') }}</a>.
                    @endif
                </div>

                <form method="POST" action="{{ route('change_background_process') }}">
                    {!! csrf_field() !!}

                    <div>
                        <b>{{ trans('pages/change_background.form.background.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                        <div class="image-editor">
                            <input type="file" class="cropit-image-input">
                            <div class="cropit-image-preview"></div>
                            <div class="text-center"><b>Minimum size:</b> 750x750px</div>
                            <div class="image-size-label">
                                <b>{{ trans('pages/change_background.resize_image') }}</b>
                            </div>
                            <input type="range" class="cropit-image-zoom-input">
                            <input type="hidden" name="background" class="hidden-image-data" />
                        </div>

                        <script>
                            $(function() {
                                var imageEditor = $('.image-editor');

                                imageEditor.cropit({
                                    imageState: {
                                        src: '@if(empty(Auth::user()->profile->background)){{ asset('assets/images/no-background.jpg') }}@else{{ asset('storage/backgrounds/'.Auth::user()->profile->background) }}@endif'
                                    },
                                    export: {
                                        type: 'image/jpeg',
                                        quality: 1,
                                        originalSize: false
                                    },
                                    previewSize: {
                                        width: 200,
                                        height: 200
                                    },
                                    exportZoom: 3.75,
                                    rejectSmallImage: false,
                                    onImageError: function() {
                                        alert('{{ trans('pages/change_background.messages.image_dimensions_error') }}');
                                    }
                                });

                                var requestLoading = $('.loading');
                                $('form').submit(function() {
                                    requestLoading.show();

                                    var errorsDiv = $('#errors');
                                    errorsDiv.html('');

                                    var successDiv = $('#success');
                                    successDiv.hide();

                                    // Move cropped image data to hidden input
                                    var imageData = $('.image-editor').cropit('export');
                                    $('.hidden-image-data').val(imageData);

                                    var actionUrl = $(this).attr('action');
                                    var postData = new FormData(this);

                                    $.ajax({
                                        type: 'POST',
                                        url: actionUrl,
                                        data: postData,
                                        dataType: 'json',
                                        processData: false,
                                        contentType: false
                                    }).done(function(responseData) {
                                        requestLoading.hide();

                                        $(this).find('input[name=_token]').val(responseData._token);

                                        if(responseData.ok === false) {
                                            if(typeof(responseData.errors) !== 'undefined' && Object.keys(responseData.errors).length > 0) {
                                                var errorsList = responseData.errors;
                                                var html = '';
                                                $.each(errorsList, function(key, value) {
                                                    html += '<br><span class="error-msg">'+value+'</span>';
                                                });
                                                errorsDiv.html(html);
                                            }
                                        } else {
                                            successDiv.show();
                                            $('.avatarProgBtn').addClass('progtrckr-done');
                                        }
                                    });

                                    // Prevent the form from actually submitting
                                    return false;
                                });
                            });
                        </script>

                        <div class="request-loading hidden text-center">
                            <img src="{{ asset('assets/images/ajax-loader.gif') }}" />
                        </div>

                        <div id="errors">

                        </div>
                    </div>
                    <p>
                        <button type="submit">{{ trans('pages/change_background.form.submit.label') }}</button>
                    </p>
                </form>
            {{--@else--}}
                {{--<h4>{{ trans('pages/change_background.base_profile_data_error') }}</h4>--}}
            {{--@endif--}}
        </div>
    </article>
@endsection