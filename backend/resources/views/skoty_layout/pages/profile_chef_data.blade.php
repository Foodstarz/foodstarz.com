@extends('skoty_layout.layout')

@section('title', trans('pages/profile_chef_data.page_title'))

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/libs/jquery-ui/jquery-ui.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/libs/jquery-ui/jquery-ui.theme.min.css') }}"/>
@endsection

@section('header_scripts')
    <script src="{{ asset('assets/libs/jquery-ui/jquery-ui.min.js') }}"></script>
    <script src="{{ asset('assets/js/custom/profile_chef_data.js') }}"></script>
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box jui">
        <h1 class="post-title">{{ $user->name }}</h1>
        <p class="text-center text-bold">@if(isset($profile->culinary_profession)) {{ trans(Config::get('profile.culinary_profession')[$profile->culinary_profession]) }} | @endif @if(isset($profile->city)) {{ $profile->city }} | @endif @if(isset($profile->country)) @if(isset(Config::get('custom.countryList')[$profile->country])) {{ Config::get('custom.countryList')[$profile->country] }} @else {{ $profile->country }} @endif @endif</p>

        @include('skoty_layout.pages.user_profile_menu')

        @include('skoty_layout.pages.profile_menu')

        <h1 class="post-title">{{ trans('pages/profile_chef_data.content_title') }}</h1>

        <div class="post-content">

            @if(session('base_profile_data') && session('avatar'))

                @if(session('success'))
                    <div class="alert-box success">{!! session('success') !!}</div>
                @endif

                <form method="POST" action="{{ route('profile_chef_data_process') }}">
                    {!! csrf_field() !!}

                    <p>
                        <b>{{ trans('pages/profile_chef_data.form.biography.label') }}</b> ({{ trans('pages/profile_chef_data.form.biography.valid') }})<br>
                        <textarea name="biography" @if(!empty($errors->get('biography'))) class="input-error" @endif rows="5" placeholder="{{ trans('pages/profile_chef_data.form.biography.placeholder') }}">{{ $biography }}</textarea>
                        @if(!empty($errors->get('biography')))
                            @foreach($errors->get('biography') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <div id="work_experience">
                        <fieldset @if(!empty($errors->get('work_experience'))) class="input-error" @endif>
                            <legend><b>{{ trans('pages/profile_chef_data.form.work_experience.label') }}</b></legend>

                            <div class="items">

                                @if(empty($work_experience))
                                    <div class="post_item">
                                        <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.position.label') }}</b> <br>
                                        <input type="text" name="work_experience[position][]"/><br>

                                        <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.employer.label') }}</b> <br>
                                        <input type="text" name="work_experience[employer][]"/><br>

                                        {{--
                                        <label for="startDate"><span class="required">*</span> <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.from') }}</b></label>
                                        <input name="work_experience[period][from][]" class="monthYearPicker startDate"/><br>
                                        <label for="endDate"><span class="required">*</span> <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.to') }}</b></label>
                                        <input name="work_experience[period][to][]" class="monthYearPicker endDate" value=" "/><br>
                                        --}}

                                        <div class="datePick">
                                            <label for="startYear"><b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.from') }}</b> <br></label>
                                            <select name="startYear" id="startYear">
                                                <option value="">{{ trans('pages/profile_chef_data.form.year') }}</option>
                                                @for($i=date('Y'); $i>=1950; $i--)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                            <select name="startMonth">
                                                <option value="">{{ trans('pages/profile_chef_data.form.month') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.jan') }}">{{ trans('pages/profile_chef_data.form.months.jan') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.feb') }}">{{ trans('pages/profile_chef_data.form.months.feb') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.mar') }}">{{ trans('pages/profile_chef_data.form.months.mar') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.apr') }}">{{ trans('pages/profile_chef_data.form.months.apr') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.may') }}">{{ trans('pages/profile_chef_data.form.months.may') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.jun') }}">{{ trans('pages/profile_chef_data.form.months.jun') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.jul') }}">{{ trans('pages/profile_chef_data.form.months.jul') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.aug') }}">{{ trans('pages/profile_chef_data.form.months.aug') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.sep') }}">{{ trans('pages/profile_chef_data.form.months.sep') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.oct') }}">{{ trans('pages/profile_chef_data.form.months.oct') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.nov') }}">{{ trans('pages/profile_chef_data.form.months.nov') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.dec') }}">{{ trans('pages/profile_chef_data.form.months.dec') }}</option>
                                            </select>
                                            <input name="work_experience[period][from][]" type="hidden" class="wexp_from" />
                                            <input type="checkbox" name="work_experience[period][now][]" value="true" class="still_working"/> {{ trans('pages/profile_chef_data.form.work_experience.subfields.period.still_working') }}
                                            <br>

                                            <label for="endYear"><b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.to') }}</b> <br></label>
                                            <select name="endYear" id="endYear">
                                                <option value="">{{ trans('pages/profile_chef_data.form.year') }}</option>
                                                @for($i=date('Y'); $i>=1950; $i--)
                                                    <option value="{{ $i }}">{{ $i }}</option>
                                                @endfor
                                            </select>
                                            <select name="endMonth">
                                                <option value="">{{ trans('pages/profile_chef_data.form.month') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.jan') }}">{{ trans('pages/profile_chef_data.form.months.jan') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.feb') }}">{{ trans('pages/profile_chef_data.form.months.feb') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.mar') }}">{{ trans('pages/profile_chef_data.form.months.mar') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.apr') }}">{{ trans('pages/profile_chef_data.form.months.apr') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.may') }}">{{ trans('pages/profile_chef_data.form.months.may') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.jun') }}">{{ trans('pages/profile_chef_data.form.months.jun') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.jul') }}">{{ trans('pages/profile_chef_data.form.months.jul') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.aug') }}">{{ trans('pages/profile_chef_data.form.months.aug') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.sep') }}">{{ trans('pages/profile_chef_data.form.months.sep') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.oct') }}">{{ trans('pages/profile_chef_data.form.months.oct') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.nov') }}">{{ trans('pages/profile_chef_data.form.months.nov') }}</option>
                                                <option value="{{ trans('pages/profile_chef_data.form.months.dec') }}">{{ trans('pages/profile_chef_data.form.months.dec') }}</option>
                                            </select>
                                            <input name="work_experience[period][to][]" type="hidden" class="wexp_to" value=" "/>
                                        </div>

                                        <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.job_description.label') }}</b> <br>
                                        <textarea name="work_experience[job_description][]" rows="8"></textarea>

                                        <br/>
                                        <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.work_experience.subfields.remove_button.label') }}</button>
                                    </div>
                                @else
                                    @foreach($work_experience as $experience)
                                        <div class="post_item">
                                            <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.position.label') }}</b> <br>
                                            <input type="text" name="work_experience[position][]" value="@if(isset($experience['position'])){{ $experience['position'] }}@endif"/><br>

                                            <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.employer.label') }}</b> <br>
                                            <input type="text" name="work_experience[employer][]" value="@if(isset($experience['employer'])){{ $experience['employer'] }}@endif"/><br>

                                            {{--
                                            <label for="startDate"><span class="required">*</span> <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.from') }}</b></label>
                                            <input name="work_experience[period][from][]" class="monthYearPicker startDate" value="@if(isset($experience['period']['from'])){{ $experience['period']['from'] }}@endif"/><br>
                                            @if(isset($experience['period']['now']) && $experience['period']['now'] == 'true')
                                                <label for="endDate" style="display:none;"><span class="required">*</span> <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.to') }}</b></label>
                                                <input name="work_experience[period][to][]" class="monthYearPicker endDate" value=" " style="display:none;"/><br>
                                                <input type="checkbox" name="work_experience[period][now][]" value="true" class="still_working" checked/> {{ trans('pages/profile_chef_data.form.work_experience.subfields.period.still_working') }}<br>
                                            @else
                                                <label for="endDate"><span class="required">*</span> <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.to') }}</b></label>
                                                <input name="work_experience[period][to][]" class="monthYearPicker endDate" value="@if(isset($experience['period']['to'])){{ $experience['period']['to'] }}@endif"/><br>
                                                <input type="checkbox" name="work_experience[period][now][]" value="true" class="still_working"/> {{ trans('pages/profile_chef_data.form.work_experience.subfields.period.still_working') }}<br>
                                            @endif
                                            --}}

                                            <div class="datePick">
                                                <?php
                                                if(isset($experience['period']['from'])) {
                                                    $periodFrom = explode(' ', $experience['period']['from']);
                                                } else {
                                                    $periodFrom = ['',''];
                                                }

                                                if(isset($experience['period']['to'])) {
                                                    $periodTo = explode(' ', $experience['period']['to']);
                                                } else {
                                                    $periodTo = ['',''];
                                                }

                                                ?>
                                                @if(isset($experience['period']['now']) && $experience['period']['now'] == 'true')
                                                    <div class="pull-left">
                                                        <label for="startYear"><b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.from') }}</b> <br></label>
                                                        <select name="startYear" id="startYear">
                                                            <option value="">{{ trans('pages/profile_chef_data.form.year') }}</option>
                                                            @for($i=date('Y'); $i>=1950; $i--)
                                                                <option value="{{ $i }}" @if(isset($periodFrom[1]) && $periodFrom[1] == $i) selected @endif>{{ $i }}</option>
                                                            @endfor
                                                        </select>
                                                        <select name="startMonth">
                                                            <option value="">{{ trans('pages/profile_chef_data.form.month') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jan') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.jan'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.jan') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.feb') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.feb'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.feb') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.mar') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.mar'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.mar') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.apr') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.apr'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.apr') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.may') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.may'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.may') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jun') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.jun'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.jun') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jul') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.jul'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.jul') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.aug') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.aug'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.aug') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.sep') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.sep'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.sep') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.oct') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.oct'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.oct') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.nov') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.nov'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.nov') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.dec') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.dec'))  selected  @endif>{{ trans('pages/profile_chef_data.form.months.dec') }}</option>
                                                        </select>
                                                        <input name="work_experience[period][from][]" type="hidden" class="wexp_from" @if(isset($experience['period']['from']))value="{{$experience['period']['from']}}"@endif />
                                                    </div>

                                                    <div class="pull-left">
                                                        <label for="endYear" class="hidden"><b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.to') }}</b> <br></label>
                                                        <select name="endYear" id="endYear" class="hidden">
                                                            <option value="">{{ trans('pages/profile_chef_data.form.year') }}</option>
                                                            @for($i=date('Y'); $i>=1950; $i--)
                                                                <option value="{{ $i }}">{{ $i }}</option>
                                                            @endfor
                                                        </select>
                                                        <select name="endMonth" class="hidden">
                                                            <option value="">{{ trans('pages/profile_chef_data.form.month') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jan') }}">{{ trans('pages/profile_chef_data.form.months.jan') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.feb') }}">{{ trans('pages/profile_chef_data.form.months.feb') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.mar') }}">{{ trans('pages/profile_chef_data.form.months.mar') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.apr') }}">{{ trans('pages/profile_chef_data.form.months.apr') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.may') }}">{{ trans('pages/profile_chef_data.form.months.may') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jun') }}">{{ trans('pages/profile_chef_data.form.months.jun') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jul') }}">{{ trans('pages/profile_chef_data.form.months.jul') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.aug') }}">{{ trans('pages/profile_chef_data.form.months.aug') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.sep') }}">{{ trans('pages/profile_chef_data.form.months.sep') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.oct') }}">{{ trans('pages/profile_chef_data.form.months.oct') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.nov') }}">{{ trans('pages/profile_chef_data.form.months.nov') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.dec') }}">{{ trans('pages/profile_chef_data.form.months.dec') }}</option>
                                                        </select><br>
                                                        <input name="work_experience[period][to][]" type="hidden" class="wexp_to" value=" "/>
                                                        <input type="checkbox" name="work_experience[period][now][]" value="true" class="still_working" checked/> {{ trans('pages/profile_chef_data.form.work_experience.subfields.period.still_working') }}
                                                    </div>
                                                    <div class="clearfix"></div>
                                                @else
                                                    <div class="pull-left">
                                                        <label for="startYear"><b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.from') }}</b> <br></label>
                                                        <select name="startYear" id="startYear">
                                                            <option value="">{{ trans('pages/profile_chef_data.form.year') }}</option>
                                                            @for($i=date('Y'); $i>=1950; $i--)
                                                                <option value="{{ $i }}" @if(isset($periodFrom[1]) && $periodFrom[1] == $i) selected @endif>{{ $i }}</option>
                                                            @endfor
                                                        </select>
                                                        <select name="startMonth">
                                                            <option value="">{{ trans('pages/profile_chef_data.form.month') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jan') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.jan')) selected @endif>{{ trans('pages/profile_chef_data.form.months.jan') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.feb') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.feb')) selected @endif>{{ trans('pages/profile_chef_data.form.months.feb') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.mar') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.mar')) selected @endif>{{ trans('pages/profile_chef_data.form.months.mar') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.apr') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.apr')) selected @endif>{{ trans('pages/profile_chef_data.form.months.apr') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.may') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.may')) selected @endif>{{ trans('pages/profile_chef_data.form.months.may') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jun') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.jun')) selected @endif>{{ trans('pages/profile_chef_data.form.months.jun') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jul') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.jul')) selected @endif>{{ trans('pages/profile_chef_data.form.months.jul') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.aug') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.aug')) selected @endif>{{ trans('pages/profile_chef_data.form.months.aug') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.sep') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.sep')) selected @endif>{{ trans('pages/profile_chef_data.form.months.sep') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.oct') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.oct')) selected @endif>{{ trans('pages/profile_chef_data.form.months.oct') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.nov') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.nov')) selected @endif>{{ trans('pages/profile_chef_data.form.months.nov') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.dec') }}" @if(isset($periodFrom[0]) && $periodFrom[0] == trans('pages/profile_chef_data.form.months.dec')) selected @endif>{{ trans('pages/profile_chef_data.form.months.dec') }}</option>
                                                        </select>

                                                        <input name="work_experience[period][from][]" type="hidden" class="wexp_from" @if(isset($experience['period']['from']))value="{{$experience['period']['from']}}"@endif />
                                                    </div>

                                                    <div class="pull-left">
                                                        <label for="endYear"><b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.to') }}</b> <br></label>
                                                        <select name="endYear" id="endYear">
                                                            <option value="">{{ trans('pages/profile_chef_data.form.year') }}</option>
                                                            @for($i=date('Y'); $i>=1950; $i--)
                                                                <option value="{{ $i }}" @if(isset($periodTo[1]) && $periodTo[1] == $i) selected @endif>{{ $i }}</option>
                                                            @endfor
                                                        </select>
                                                        <select name="endMonth">
                                                            <option value="">{{ trans('pages/profile_chef_data.form.month') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jan') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.jan')) selected @endif>{{ trans('pages/profile_chef_data.form.months.jan') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.feb') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.feb')) selected @endif>{{ trans('pages/profile_chef_data.form.months.feb') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.mar') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.mar')) selected @endif>{{ trans('pages/profile_chef_data.form.months.mar') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.apr') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.apr')) selected @endif>{{ trans('pages/profile_chef_data.form.months.apr') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.may') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.may')) selected @endif>{{ trans('pages/profile_chef_data.form.months.may') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jun') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.jun')) selected @endif>{{ trans('pages/profile_chef_data.form.months.jun') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.jul') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.jul')) selected @endif>{{ trans('pages/profile_chef_data.form.months.jul') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.aug') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.aug')) selected @endif>{{ trans('pages/profile_chef_data.form.months.aug') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.sep') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.sep')) selected @endif>{{ trans('pages/profile_chef_data.form.months.sep') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.oct') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.oct')) selected @endif>{{ trans('pages/profile_chef_data.form.months.oct') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.nov') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.nov')) selected @endif>{{ trans('pages/profile_chef_data.form.months.nov') }}</option>
                                                            <option value="{{ trans('pages/profile_chef_data.form.months.dec') }}" @if(isset($periodTo[0]) && $periodTo[0] == trans('pages/profile_chef_data.form.months.dec')) selected @endif>{{ trans('pages/profile_chef_data.form.months.dec') }}</option>
                                                        </select><br>
                                                        <input name="work_experience[period][to][]" type="hidden" class="wexp_to" @if(isset($experience['period']['to']))value="{{$experience['period']['to']}}"@endif />
                                                        <input type="checkbox" name="work_experience[period][now][]" value="true" class="still_working"/> {{ trans('pages/profile_chef_data.form.work_experience.subfields.period.still_working') }}
                                                    </div>
                                                    <div class="clearfix"></div>
                                                @endif
                                            </div>

                                            <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.job_description.label') }}</b> <br>
                                            <textarea name="work_experience[job_description][]" rows="8">@if(isset($experience['job_description'])){{ $experience['job_description'] }}@endif</textarea>

                                            <br/>
                                            <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.work_experience.subfields.remove_button.label') }}</button>
                                        </div>
                                    @endforeach
                                @endif
                            </div>

                            <br>

                            <button type="submit" id="add_experience">{{ trans('pages/profile_chef_data.form.work_experience.subfields.add_button.label') }}</button>
                        </fieldset>

                        @if(!empty($errors->get('work_experience')))
                            @foreach($errors->get('work_experience') as $error)
                                <span class="error-msg">{{ $error }}</span>
                                <br>
                            @endforeach
                        @endif
                    </div>

                    <div id="awards">
                        <fieldset @if(!empty($errors->get('awards'))) class="input-error" @endif>
                            <legend><b>{{ trans('pages/profile_chef_data.form.awards.label') }}</b> </legend>

                            <div class="items">
                                @if(empty($awards))
                                    <div class="post_item">
                                        <input type="text" name="awards[]" placeholder="{{ trans('pages/profile_chef_data.form.awards.placeholder') }}"/>
                                        <br/>
                                        <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.awards.remove_button') }}</button>
                                    </div>
                                @else
                                    @foreach($awards as $award)
                                        <div class="post_item">
                                            <input type="text" name="awards[]" placeholder="{{ trans('pages/profile_chef_data.form.awards.placeholder') }}" value="{{ $award }}"/>
                                            <br/>
                                            <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.awards.remove_button') }}</button>
                                        </div>
                                    @endforeach
                                @endif

                            </div>

                            <br>

                            <button type="submit" id="add_award">{{ trans('pages/profile_chef_data.form.awards.add_button') }}</button>
                        </fieldset>

                        @if(!empty($errors->get('awards')))
                            @foreach($errors->get('awards') as $error)
                                <span class="error-msg">{{ $error }}</span>
                                <br>
                            @endforeach
                        @endif
                    </div>

                    <div id="organisations">
                        <fieldset @if(!empty($errors->get('organisations'))) class="input-error" @endif>
                            <legend><b>{{ trans('pages/profile_chef_data.form.organisations.label') }}</b> </legend>

                            <div class="items">
                                @if(empty($organisations))
                                    <div class="post_item">
                                        <input type="text" name="organisations[]"/>
                                        <br/>
                                        <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.organisations.remove_button') }}</button>
                                    </div>
                                @else
                                    @foreach($organisations as $organisation)
                                        <div class="post_item">
                                            <input type="text" name="organisations[]" value="{{ $organisation }}"/>
                                            <br/>
                                            <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.organisations.remove_button') }}</button>
                                        </div>
                                    @endforeach
                                @endif
                            </div>

                            <br>

                            <button type="submit" id="add_organisation">{{ trans('pages/profile_chef_data.form.organisations.add_button') }}</button>
                        </fieldset>

                        @if(!empty($errors->get('organisations')))
                            @foreach($errors->get('organisations') as $error)
                                <span class="error-msg">{{ $error }}</span>
                                <br>
                            @endforeach
                        @endif
                    </div>

                    <div id="role_models">
                        <fieldset @if(!empty($errors->get('role_models'))) class="input-error" @endif>
                            <legend><b>{{ trans('pages/profile_chef_data.form.role_models.label') }}</b> </legend>

                            <div class="items">
                                @if(empty($role_models))
                                    <div class="post_item">
                                        <input type="text" name="role_models[]"/>
                                        <br/>
                                        <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.role_models.remove_button') }}</button>
                                    </div>
                                @else
                                    @foreach($role_models as $role_model)
                                        <div class="post_item">
                                            <input type="text" name="role_models[]" value="{{ $role_model }}"/>
                                            <br/>
                                            <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.role_models.remove_button') }}</button>
                                        </div>
                                    @endforeach
                                @endif
                            </div>

                            <br>

                            <button type="submit" id="add_role_model">{{ trans('pages/profile_chef_data.form.role_models.add_button') }}</button>
                        </fieldset>

                        @if(!empty($errors->get('role_models')))
                            @foreach($errors->get('role_models') as $error)
                                <span class="error-msg">{{ $error }}</span>
                                <br>
                            @endforeach
                        @endif
                    </div>

                    <p>
                        <b>{{ trans('pages/profile_chef_data.form.cooking_style.label') }}</b> <br>
                        <input type="text" name="cooking_style" @if(!empty($errors->get('cooking_style'))) class="input-error" @endif value="{{ $cooking_style }}">
                        @if(!empty($errors->get('cooking_style')))
                            @foreach($errors->get('cooking_style') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <b>{{ trans('pages/profile_chef_data.form.kitchen_skills.label') }}</b> ({{ trans('pages/profile_chef_data.form.kitchen_skills.valid') }})<br>
                        @foreach($kitchen_skills as $skill)
                            <input type="checkbox" name="kitchen_skills[]" value="{{ $skill }}" @if(in_array($skill, (array)$kitchen_skills_post)) checked @endif /> {{ trans('pages/profile_chef_data.form.kitchen_skills.checkboxes.'.$skill) }}<br>
                        @endforeach

                        @if(!empty($errors->get('kitchen_skills')))
                            @foreach($errors->get('kitchen_skills') as $error)
                                <span class="error-msg">{{ $error }}</span>
                                <br>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <b>{{ trans('pages/profile_chef_data.form.business_skills.label') }}</b> ({{ trans('pages/profile_chef_data.form.business_skills.valid') }})<br>
                        @foreach($business_skills as $skill)
                            <input type="checkbox" name="business_skills[]" value="{{ $skill }}" @if(in_array($skill, (array)$business_skills_post)) checked @endif /> {{ trans('pages/profile_chef_data.form.business_skills.checkboxes.'.$skill) }}<br>
                        @endforeach

                        @if(!empty($errors->get('business_skills')))
                            @foreach($errors->get('business_skills') as $error)
                                <span class="error-msg">{{ $error }}</span>
                                <br>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <b>{{ trans('pages/profile_chef_data.form.personal_skills.label') }}</b> ({{ trans('pages/profile_chef_data.form.personal_skills.valid') }})<br>
                        @foreach($personal_skills as $skill)
                            <input type="checkbox" name="personal_skills[]" value="{{ $skill }}" @if(in_array($skill, (array)$personal_skills_post)) checked @endif /> {{ trans('pages/profile_chef_data.form.personal_skills.checkboxes.'.$skill) }}<br>
                        @endforeach

                        @if(!empty($errors->get('personal_skills')))
                            @foreach($errors->get('personal_skills') as $error)
                                <span class="error-msg">{{ $error }}</span>
                                <br>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <b>{{ trans('pages/profile_chef_data.form.search.label') }}</b> <br>
                        <textarea name="search" @if(!empty($errors->get('search'))) class="input-error" @endif rows="10">{{ $search }}</textarea>
                        @if(!empty($errors->get('search')))
                            @foreach($errors->get('search') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <p>
                        <b>{{ trans('pages/profile_chef_data.form.offer.label') }}</b> <br>
                        <textarea name="offer" @if(!empty($errors->get('offer'))) class="input-error" @endif rows="10">{{ $offer }}</textarea>
                        @if(!empty($errors->get('offer')))
                            @foreach($errors->get('offer') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>

                    <div id="social_networks">
                        <fieldset>
                            <legend><b>{{ trans('pages/profile_chef_data.form.social_media_links.label') }}</b> </legend>

                            <b>{{ trans('pages/profile_chef_data.form.social_media_links.website.label') }}</b><br>
                            <input type="text" name="website" placeholder="e.g. http://www.foodstarz.com" @if(!empty($errors->get('website'))) class="input-error" @endif value="{{ $website }}">
                            @if(!empty($errors->get('website')))
                                @foreach($errors->get('website') as $error)
                                    <br>
                                    <span class="error-msg">{{ $error }}</span>
                                @endforeach
                            @endif

                            <br>

                            <b>{{ trans('pages/profile_chef_data.form.social_media_links.instagram.label') }}</b><br>
                            <input type="text" name="instagram" placeholder="e.g. https://www.instagram.com/username/" @if(!empty($errors->get('instagram'))) class="input-error" @endif value="{{ $instagram }}">
                            @if(!empty($errors->get('instagram')))
                                @foreach($errors->get('instagram') as $error)
                                    <br>
                                    <span class="error-msg">{{ $error }}</span>
                                @endforeach
                            @endif

                            <br>

                            <b>{{ trans('pages/profile_chef_data.form.social_media_links.facebook.label') }}</b><br>
                            <input type="text" name="facebook" placeholder="e.g. https://www.facebook.com/username/" @if(!empty($errors->get('facebook'))) class="input-error" @endif value="{{ $facebook }}">
                            @if(!empty($errors->get('facebook')))
                                @foreach($errors->get('facebook') as $error)
                                    <br>
                                    <span class="error-msg">{{ $error }}</span>
                                @endforeach
                            @endif

                            <br>

                            <b>{{ trans('pages/profile_chef_data.form.social_media_links.twitter.label') }}</b><br>
                            <input type="text" name="twitter" placeholder="e.g. https://www.twitter.com/username/" @if(!empty($errors->get('twitter'))) class="input-error" @endif value="{{ $twitter }}">
                            @if(!empty($errors->get('twitter')))
                                @foreach($errors->get('twitter') as $error)
                                    <br>
                                    <span class="error-msg">{{ $error }}</span>
                                @endforeach
                            @endif

                            <br>

                            <b>{{ trans('pages/profile_chef_data.form.social_media_links.pinterest.label') }}</b><br>
                            <input type="text" name="pinterest" placeholder="e.g. https://www.pinterest.com/username/" @if(!empty($errors->get('pinterest'))) class="input-error" @endif value="{{ $pinterest }}">
                            @if(!empty($errors->get('pinterest')))
                                @foreach($errors->get('pinterest') as $error)
                                    <br>
                                    <span class="error-msg">{{ $error }}</span>
                                @endforeach
                            @endif

                            <br>

                            <b>{{ trans('pages/profile_chef_data.form.social_media_links.tumblr.label') }}</b><br>
                            <input type="text" name="tumblr" placeholder="e.g. http://username.tumblr.com" @if(!empty($errors->get('tumblr'))) class="input-error" @endif value="{{ $tumblr }}">
                            @if(!empty($errors->get('tumblr')))
                                @foreach($errors->get('tumblr') as $error)
                                    <br>
                                    <span class="error-msg">{{ $error }}</span>
                                @endforeach
                            @endif
                        </fieldset>
                    </div>
                    <br>
                    <p>
                        <button type="submit">{{ trans('pages/profile_chef_data.form.submit.label') }}</button>
                    </p>
                </form>

                <div class="work_experience hidden_example">
                    <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.position.label') }}</b> <br>
                    <input type="text" name="work_experience[position][]"/><br>

                    <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.employer.label') }}</b> <br>
                    <input type="text" name="work_experience[employer][]"/><br>

                    {{--
                    <label for="startDate"><span class="required">*</span> <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.from') }}</b></label>
                    <input name="work_experience[period][from][]" class="monthYearPicker startDate"/><br>
                    <label for="endDate"><span class="required">*</span> <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.to') }}</b></label>
                    <input name="work_experience[period][to][]" class="monthYearPicker endDate"/><br>
                    <input type="checkbox" name="work_experience[period][now][]" value="true" class="still_working"/> {{ trans('pages/profile_chef_data.form.work_experience.subfields.period.still_working') }}<br>
                    --}}

                    <div class="datePick">
                        <div class="pull-left">
                            <label for="startYear"><b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.from') }}</b> <br></label>
                            <select name="startYear" id="startYear">
                                <option value="">{{ trans('pages/profile_chef_data.form.year') }}</option>
                                @for($i=date('Y'); $i>=date('Y')-50; $i--)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                            <select name="startMonth">
                                <option value="">{{ trans('pages/profile_chef_data.form.month') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.jan') }}">{{ trans('pages/profile_chef_data.form.months.jan') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.feb') }}">{{ trans('pages/profile_chef_data.form.months.feb') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.mar') }}">{{ trans('pages/profile_chef_data.form.months.mar') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.apr') }}">{{ trans('pages/profile_chef_data.form.months.apr') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.may') }}">{{ trans('pages/profile_chef_data.form.months.may') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.jun') }}">{{ trans('pages/profile_chef_data.form.months.jun') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.jul') }}">{{ trans('pages/profile_chef_data.form.months.jul') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.aug') }}">{{ trans('pages/profile_chef_data.form.months.aug') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.sep') }}">{{ trans('pages/profile_chef_data.form.months.sep') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.oct') }}">{{ trans('pages/profile_chef_data.form.months.oct') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.nov') }}">{{ trans('pages/profile_chef_data.form.months.nov') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.dec') }}">{{ trans('pages/profile_chef_data.form.months.dec') }}</option>
                            </select>
                            <input name="work_experience[period][from][]" type="hidden" class="wexp_from" />
                        </div>

                        <div class="pull-left">
                            <label for="endYear"><b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.period.to') }}</b> <br></label>
                            <select name="endYear" id="endYear">
                                <option value="">{{ trans('pages/profile_chef_data.form.year') }}</option>
                                @for($i=date('Y'); $i>=date('Y')-50; $i--)
                                    <option value="{{ $i }}">{{ $i }}</option>
                                @endfor
                            </select>
                            <select name="endMonth">
                                <option value="">{{ trans('pages/profile_chef_data.form.month') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.jan') }}">{{ trans('pages/profile_chef_data.form.months.jan') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.feb') }}">{{ trans('pages/profile_chef_data.form.months.feb') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.mar') }}">{{ trans('pages/profile_chef_data.form.months.mar') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.apr') }}">{{ trans('pages/profile_chef_data.form.months.apr') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.may') }}">{{ trans('pages/profile_chef_data.form.months.may') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.jun') }}">{{ trans('pages/profile_chef_data.form.months.jun') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.jul') }}">{{ trans('pages/profile_chef_data.form.months.jul') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.aug') }}">{{ trans('pages/profile_chef_data.form.months.aug') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.sep') }}">{{ trans('pages/profile_chef_data.form.months.sep') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.oct') }}">{{ trans('pages/profile_chef_data.form.months.oct') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.nov') }}">{{ trans('pages/profile_chef_data.form.months.nov') }}</option>
                                <option value="{{ trans('pages/profile_chef_data.form.months.dec') }}">{{ trans('pages/profile_chef_data.form.months.dec') }}</option>
                            </select><br>
                            <input name="work_experience[period][to][]" type="hidden" class="wexp_to" value=" "/>
                            <input type="checkbox" name="work_experience[period][now][]" value="true" class="still_working"/> {{ trans('pages/profile_chef_data.form.work_experience.subfields.period.still_working') }}
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <b>{{ trans('pages/profile_chef_data.form.work_experience.subfields.job_description.label') }}</b><br>
                    <textarea name="work_experience[job_description][]" rows="8"></textarea>

                    <br/>
                    <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.work_experience.subfields.remove_button.label') }}</button>
                </div>

                <div class="awards hidden_example">
                    <input type="text" name="awards[]" placeholder="{{ trans('pages/profile_chef_data.form.awards.placeholder') }}"/>
                    <br/>
                    <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.awards.remove_button') }}</button>
                </div>

                <div class="organisations hidden_example">
                    <input type="text" name="organisations[]"/>
                    <br/>
                    <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.organisations.remove_button') }}</button>
                </div>

                <div class="role_models hidden_example">
                    <input type="text" name="role_models[]"/>
                    <br/>
                    <button class="remove_post_item">{{ trans('pages/profile_chef_data.form.role_models.remove_button') }}</button>
                </div>

            @else
                @if(!session('base_profile_data'))
                    <h4>{{ trans('pages/profile_chef_data.base_profile_data_error') }}</h4>
                @endif

                @if(!session('avatar'))
                    <h4>{{ trans('pages/profile_chef_data.upload_avatar_error') }}</h4>
                @endif
            @endif
        </div>
    </article>
@endsection
