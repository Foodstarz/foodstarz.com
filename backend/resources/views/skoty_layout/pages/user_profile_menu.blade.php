<div id="centeredmenu">
    @if(Auth::check() && session('user')->id == $user->id)
        @if(Route::currentRouteNamed('view_user_profile'))
            <div class="editBtnBg"><a href="{{ route('profile_core_data') }}"><img src="{{ asset('assets/images/configure-5.png') }}" width="20px" height="20px" alt="{{ trans('pages/profile_settings.actions.edit_profile') }}"></a></div>
        @elseif(Route::currentRouteNamed('view_user_profile_interview'))
            <div class="editBtnBg"><a href="{{ route('interview') }}"><img src="{{ asset('assets/images/configure-5.png') }}" width="20px" height="20px" alt="{{ trans('pages/profile_settings.actions.edit_profile') }}"></a></div>
        @endif
    @endif
    <ul class="hMenu" style="z-index: 99; width: auto;">
        <li><a @if(Route::currentRouteNamed('view_user_chronic')) class="active" @endif href="{{ route('view_user_chronic', ['slug' => $profile->slug]) }}">{{ trans('pages/user_profile.menu.chronic') }}</a></li>
        <li><a @if(Route::currentRouteNamed('view_user_profile')) class="active" @endif href="{{ route('view_user_profile', ['slug' => $profile->slug]) }}">{{ trans('pages/user_profile.menu.profile') }}</a></li>
        <li><a @if(Route::currentRouteNamed('view_user_profile_interview')) class="active" @endif href="{{ route('view_user_profile_interview', ['slug' => $profile->slug]) }}">{{ trans('pages/user_profile.menu.interview') }}</a></li>
        <li><a @if(Route::currentRouteNamed('view_user_profile_recipes')) class="active" @endif href="{{ route('view_user_profile_recipes', ['slug' => $profile->slug]) }}">{{ trans('pages/recipe.recipes') }}</a></li>
        <li><a @if(Route::currentRouteNamed('view_user_gallery')) class="active" @endif href="{{ route('view_user_gallery', ['slug' => $profile->slug]) }}">{{ trans('pages/user_profile.menu.gallery') }}</a></li>
        <li><a @if(Route::currentRouteNamed('view_user_videos')) class="active" @endif href="{{ route('view_user_videos', ['slug' => $profile->slug]) }}">{{ trans('pages/user_profile.menu.videos') }}</a></li>
        @if(Auth::check() && session('user')->id == $user->id)
            {{--<li><a @if(Route::currentRouteNamed('recipe_add')) class="active" @endif href="{{ route('recipe_add') }}">{{ trans('pages/user_profile.menu.add_recipe') }}</a></li>
            <li><a @if(Route::currentRouteNamed('profile_core_data') || Route::currentRouteNamed('upload_avatar') || Route::currentRouteNamed('profile_chef_data') || Route::currentRouteNamed('interview')) class="active" @endif href="{{ route('profile_core_data') }}">{{ trans('pages/user_profile.menu.edit') }}</a></li>--}}
            <li><a @if(Route::currentRouteNamed('profile_settings') || Route::currentRouteNamed('change_password') || Route::currentRouteNamed('close_account')) class="active" @endif href="{{ route('profile_settings') }}">{{ trans('pages/user_profile.menu.settings') }}</a></li>
        @endif
    </ul>
</div>