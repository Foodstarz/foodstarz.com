@extends('skoty_layout.layout')

@section('title', trans('pages/user_profile_interview.page_title').' - '.$user->name)

@section('seo-metas')
<meta name="description" content="Check out the exclusive interview with {{ $user->name }}"/>
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('og-tags')
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ asset('storage/avatars/'.$user->profile->avatar) }}" />
    <meta property="og:title" content="{{$user->name }} - Interview" />
    <meta property="og:description" content="{{trans('pages/seo.homepage.description')}}" />
    <meta property="og:url" content="{{ route('view_user_profile_interview', $user->profile->slug) }}" />
    <meta property="og:site_name" content="Foodstarz" />
    <meta property="fb:admins" content="582794454" />
@endsection

@section('content')
    @if($user->role == 4 || $user->role == 1 || (Auth::check() && ($user->id == Auth::user()->id || Auth::user()->role == 1)))
    <article class="profile-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        <div class="interview wider-content">
            @foreach($interview_questions as $question)
                <p>
                    @if(Auth::check())
                        @if(empty(Auth::user()->profile->lang))
                            @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                <strong><em>{{ json_decode($question->question, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}</em></strong><br/>
                            @else
                                <strong><em>{{ json_decode($question->question, true)['en'] }}</em></strong><br/>
                            @endif
                        @else
                            <strong><em>{{ json_decode($question->question, true)[Auth::user()->profile->lang] }}</em></strong><br/>
                        @endif
                    @else
                        @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                            <strong><em>{{ json_decode($question->question, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}</em></strong><br/>
                        @else
                            <strong><em>{{ json_decode($question->question, true)['en'] }}</em></strong><br/>
                        @endif
                    @endif

                    @if(!empty($profile->interview[$question->id]))
                        {{ $profile->interview[$question->id] }}
                    @else
                        {{ trans('pages/user_profile_interview.no_answer') }}
                    @endif
                </p>
            @endforeach
        </div>
    </article>
    @else
        <article class="profile-article center-box">
            <h1 class="post-title">{{ $user->name }}</h1>

            @include('skoty_layout.pages.user_profile_menu')

            <div class="text-center">
                <h3>{{ trans('pages/user_profile.not_approved_heading') }}</h3>
                <p>{{ trans('pages/user_profile.not_approved_description') }}</p>
            </div>
        </article>
    @endif

    <div class="clearfix"></div>

@endsection