@extends('skoty_layout.layout')

@section('title', trans('pages/profile_change_password.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
    <script src="{{ asset('assets/js/jquery.pwstrength.min.js') }}"></script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        <h1 class="post-title">{{ trans('pages/profile_change_password.content_title') }}</h1>        
        
        <div class="post-content">

            @if(session('success'))
                <div class="alert-box success">{!! session('success') !!}</div>
            @endif

            <form method="POST" action="{{ route('change_password_process') }}">
                {!! csrf_field() !!}

                <p>
                    <b>{{ trans('pages/profile_change_password.form.old_password.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="password" name="old_password" @if(!empty($errors->get('old_password'))) class="input-error" @endif placeholder="{{ trans('pages/profile_change_password.form.old_password.placeholder') }}">
                    @if(!empty($errors->get('old_password')))
                        @foreach($errors->get('old_password') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>
                
                <p>
                    <script>
                        $(function() {
                            $('input[name=new_password]').pwstrength();
                        });
                    </script>
                    <b>{{ trans('pages/profile_change_password.form.new_password.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="password" name="new_password" @if(!empty($errors->get('new_password'))) class="input-error" @endif placeholder="{{ trans('pages/profile_change_password.form.new_password.placeholder') }}" data-indicator="pwindicator">
                    <div id="pwindicator">
                        <div class="bar"></div>
                        <div class="label"></div>
                    </div>
                    @if(!empty($errors->get('new_password')))
                        @foreach($errors->get('new_password') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>
                
                <p>
                    <b>{{ trans('pages/profile_change_password.form.new_password_confirm.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="password" name="new_password_confirmation" @if(!empty($errors->get('new_password_confirmation'))) class="input-error" @endif placeholder="{{ trans('pages/profile_change_password.form.new_password_confirm.placeholder') }}">
                    @if(!empty($errors->get('new_password_confirmation')))
                        @foreach($errors->get('new_password_confirmation') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>
                
                <p>
                    <button type="submit">{{ trans('pages/profile_change_password.form.submit.label') }}</button>
                </p>
                
            </form>

        </div>
    </article>
@endsection