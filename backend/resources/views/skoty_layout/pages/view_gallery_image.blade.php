@extends('skoty_layout.layout')

@section('title', trans('pages/view_gallery_image.page_title'))

@section('og-tags')
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{ $image->caption }}" />
    <meta property="og:description" content="Check this awesome dish photo uploaded by {{ $image->user->name }}" />
    <meta property="og:url" content="{{ route('gallery_view_image', $image->id) }}" />
    <meta property="og:site_name" content="Foodstarz" />
    <meta property="fb:admins" content="582794454" />
    <meta property="og:image" content="{{ asset('storage/gallery/thumbnails/'.$image->image) }}" />
@endsection

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/rrssb.css') }}" />
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/js/rrssb.min.js') }}"></script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box" style="margin-top: 85px;">
        <h1 class="post-title">{{ $image->caption }}</h1>

        @if(Auth::check() && ($image->user_id == Auth::user()->id || Auth::user()->role == 1))
            <center><a href="{{route("gallery_edit_image", $image->id)}}" class="edit_recipe_button">{{ trans('pages/view_gallery_image.edit') }}</a></center>
            <form method="post" action="{{ route('gallery_delete_image_process', $image->id) }}">
                {{ csrf_field() }}
                <center><a href="#" id="deleteImageBtn" class="edit_recipe_button">{{ trans('pages/view_gallery_image.delete') }}</a></center>
            </form>
            <form method="post" action="{{ route('gallery_hide_image_process', $image->id) }}">
                {{ csrf_field() }}
                <center><a href="#" id="hideImageBtn" class="edit_recipe_button">@if($image->hidden == 0){{ trans('pages/view_gallery_image.hide') }}@else{{ trans('pages/view_gallery_image.unhide') }}@endif</a></center>
            </form>
            <script>
                $(function() {
                    $('#deleteImageBtn').click(function(e) {
                        e.preventDefault();
                        var ask = confirm('{{ trans('pages/view_gallery_image.confirm_deletion') }}');
                        if(ask) {
                            $(this).closest('form').submit();
                        }
                    });

                    $('#hideImageBtn').click(function(e) {
                        e.preventDefault();
                        var ask = confirm('{{ trans('pages/view_gallery_image.confirm_hide') }}');
                        if(ask) {
                            $(this).closest('form').submit();
                        }
                    });
                });
            </script>
        @endif

        @if(Auth::check() && Auth::user()->role == 1)
            <center>
            @if($bookmarked)
                <a href="{{ route('bookmark_remove_resource', ['type' => 2, 'id' => $image->id]) }}" class="edit_recipe_button">{{ trans('bookmarks.remove') }}</a>
            @else
                <a href="{{ route('bookmark_resource', ['type' => 2, 'id' => $image->id]) }}" class="edit_recipe_button">{{ trans('bookmarks.add') }}</a>
            @endif
            </center>
        @endif

        <div class="post-content">
            <a href="{{ asset('storage/gallery/'.$image->image) }}"><img src="{{ asset('storage/gallery/thumbnails/'.$image->image) }}" alt="{{ $image->caption }}" width="100%"></a>
        </div>

        @if(Auth::check() && Auth::user()->role == 1)
            <hr style="margin-bottom: 5px;">
            <textarea style="width: 100%;" onClick="this.setSelectionRange(0, this.value.length)">Foodstar {{ $image->user->name }} (@) shared a new image via Foodstarz PLUS /// {{ $image->caption }}

# # # # # #foodstarz

If you also want to get featured on Foodstarz, just join us, create your own chef profile for free, and start sharing recipes, images and videos.

Foodstarz - Your International Premium Chef Network</textarea>
            @include('skoty_layout.includes.social_buttons')
        @endif
    </article>
@endsection