@extends('skoty_layout.layout')

@section('title', trans('pages/recipes.page_title'))

@section('seo-metas')
<meta name="description" content="Find the best recipes available on the internet at Foodstarz.com!"/>
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('content')
    <h1 class="post-title">{{ trans('pages/recipes.content_title') }}</h1>

    <div class="gourmet-ads">
        <div id="ga_9466753">
            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466753');
                });
            </script>
        </div>
    </div>

    <div class="gourmet-ads-mobile">
        <div id="ga_9466758">
            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466758');
                });
            </script>
        </div>
    </div>

<article class="homepage center-box">


    <div class="content posts">
        @foreach ($recipes as $f)
            <article class="post hentry">
                <div class="foodstar_header author">
                    <table>
                        <tr>
                            <td width="70px">
                                <a href="{{ route('view_user_chronic', ['slug' => $f->recipe->user->profile->slug]) }}">
                                    <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$f->recipe->user->profile->avatar) }}" alt="">
                                </a>
                            </td>
                            <td style="vertical-align: middle;">
                                <a href="{{ route('view_user_chronic', ['slug' => $f->recipe->user->profile->slug]) }}">{{ $f->recipe->user->name }}</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <figure class="post-thumbnail">
                    @if(Auth::check() && Auth::user()->role == 1)
                        <div class="admin-actions">
                            @if(isset($f->recipe->bookmark))
                                <a class="action-unbookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unbookmark', 'type' => 'recipe', 'id' => $f->recipe->id]) }}"></a>
                            @else
                                <a class="action-bookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'bookmark', 'type' => 'recipe', 'id' => $f->recipe->id]) }}"></a>
                            @endif
                            <a class="action-edit" token="{{csrf_token()}}" not-ajax="true" href="{{ route('recipe_edit', ['id' => $f->recipe->id]) }}"></a>
                            <a class="action-delete" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'delete', 'type' => 'recipe', 'id' => $f->recipe->id]) }}"></a>
                        </div>
                    @endif
                    <a href="{{ route('recipe_view', ['slug' => $f->recipe->slug]) }}"
                       title="{{ $f->recipe->title }}"><img width="357" height="357"
                                                            src="@if(empty($f->recipe->main_image_watermarked)) {{ asset('storage/recipes/original/'.$f->recipe->main_image_original) }} @else {{ asset('storage/recipes/watermark/'.$f->recipe->main_image_watermarked) }} @endif"
                                                            class="attachment-skoty-blog-post"
                                                            alt="{{ $f->recipe->title }}"/>
                    </a>
                    <h2 class="post-title">
                        <a href="{{ route('recipe_view', ['slug' => $f->recipe->slug]) }}">{{ mb_strimwidth($f->recipe->title, 0, 56, '...') }}</a>
                    </h2>
                </figure>


                {{--
                <div class="post-date">
                    <span class="fa fa-clock-o"></span> {{$f->recipe->created_at->format('d M, Y')}}
                </div>
                --}}


                <div class="post-content">
                    <div class="additional-info">
                        <img src="{{ asset('assets/images/yield.png') }}" alt="{{ trans('pages/homepage.serves') }}"> <span>@if(Lang::locale() == 'de'){{ $f->recipe->portions }} {{ trans('pages/homepage.serves') }}@else{{ trans('pages/homepage.serves') }} {{ $f->recipe->portions }}@endif</span>
                    </div>

                    <div class="additional-info" style="float:right;">
                        <img src="{{ asset('assets/images/'.strtolower($f->recipe->difficulty).'.png') }}" alt="{{ trans('pages/add_recipe.form.difficulty.options.'.$f->recipe->difficulty) }}"> <span>{{ trans('pages/add_recipe.form.difficulty.options.'.$f->recipe->difficulty) }}</span>
                    </div>

                    <div class="clearfix"></div>

                    {{--
                    <p>
                        <a href="{{ route('view_user_chronic', ['slug' => $f->recipe->user->profile->slug]) }}" class="more-link">{{ trans('pages/homepage.read_more') }}</a>
                    </p>
                    --}}
                </div>
            </article>
        @endforeach
    </div>
    @if(count($recipes) < 1)
    <div class="text-center">
        <h3>{{ trans('pages/recipe.no_recipes') }}</h3>
    </div>
    @endif
    <div class="content text_center">
        <?php Foodstarz\Models\Pagination::render($recipes); ?>
    </div>

</article>
@endsection