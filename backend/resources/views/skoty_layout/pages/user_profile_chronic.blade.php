@extends('skoty_layout.layout')

@section('title', trans('pages/user_profile_chronic.page_title'))

@section('seo-metas')
    <meta name="description" content="{{ trans('user_profile_chronic.page_description', ['name' => $user->name]) }}"/>
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('content')
    @if($user->role == 4 || $user->role == 1 || (Auth::check() && ($user->id == Auth::user()->id || Auth::user()->role == 1)))
        <article class="profile-article center-box">
            <h1 class="new-post-title">{{ $user->name }}</h1>
            <p class="text-center text-bold">@if(isset($profile->culinary_profession)) {{ trans(Config::get('profile.culinary_profession')[$profile->culinary_profession]) }} | @endif @if(isset($profile->city)) {{ $profile->city }} | @endif @if(isset($profile->country)) @if(isset(Config::get('custom.countryList')[$profile->country])) {{ Config::get('custom.countryList')[$profile->country] }} @else {{ $profile->country }} @endif @endif</p>

            @include('skoty_layout.pages.user_profile_menu')
        </article>
        <div class="clearfix"></div>

        <article class="homepage center-box" style="margin-top: 30px;">
            {{--<h1 class="post-title">{{ trans('pages/user_profile_chronic.content_title') }}</h1>--}}

            @foreach ($items->items() as $item)
                <div class="chronic_post">
                    <div class="foodstar_header author">
                        <table>
                            <tr>
                                <td width="70px">
                                    <a href="{{ route('view_user_chronic', ['slug' => $profile->slug]) }}">
                                        <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$profile->avatar) }}" alt="">
                                    </a>
                                </td>
                                <td style="vertical-align: middle;">
                                    <a href="{{ route('view_user_chronic', ['slug' => $profile->slug]) }}">{{ $user->name }}</a> > <a href="{{ $item['category_url'] }}">{{ $item['type'] }}</a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <h2 class="post-title">
                        <a href="{{ $item['url'] }}">{{ mb_strimwidth($item['title'], 0, 56, '...') }}</a>
                    </h2>

                    <a href="{{ $item['url'] }}"
                           title="{{ $item['title'] }}"><img width="100%"
                                                              src="{{ $item['image'] }}"
                                                              class="attachment-skoty-blog-post"
                                                              alt="{{ $item['title'] }}"/>
                    </a>
                </div>
            @endforeach

            @if(count($items) < 1)
                <div class="text-center">
                    <h3>{{ trans('pages/user_profile_chronic.no_activity') }}</h3>
                </div>
            @endif
            <div class="content text_center">
                <?php Foodstarz\Models\Pagination::render($items); ?>
            </div>

        </article>
    @else
        <article class="profile-article center-box">
            <h1 class="new-post-title">{{ $user->name }}</h1>

            @include('skoty_layout.pages.user_profile_menu')

            <div class="text-center">
                <h3>{{ trans('pages/user_profile.not_approved_heading') }}</h3>
                <p>{{ trans('pages/user_profile.not_approved_description') }}</p>
            </div>
        </article>
    @endif
@endsection