@extends('skoty_layout.layout')

@section('title')
    {{ trans('pages/category_recipes.page_title').' - ' }}
    @if(Auth::check())
        @if(empty(Auth::user()->profile->lang))
            @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                {{ json_decode($category->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
            @else
                {{ json_decode($category->title, true)['en'] }}
            @endif
        @else
            {{ json_decode($category->title, true)[Auth::user()->profile->lang] }}
        @endif
    @else
        @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
            {{ json_decode($category->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
        @else
            {{ json_decode($category->title, true)['en'] }}
        @endif
    @endif
@endsection

{{--@section('seo-metas')
<meta name="description" content="Check all the awesome recipes in the {{ $category->title }} category in our website!"/>
@endsection--}}

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('content')
<article class="homepage center-box">
    <h1 class="post-title">
        @if(Auth::check())
            @if(empty(Auth::user()->profile->lang))
                @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                    {{ json_decode($category->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                @else
                    {{ json_decode($category->title, true)['en'] }}
                @endif
            @else
                {{ json_decode($category->title, true)[Auth::user()->profile->lang] }}
            @endif
        @else
            @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                {{ json_decode($category->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
            @else
                {{ json_decode($category->title, true)['en'] }}
            @endif
        @endif
    </h1>

    <div class="content posts">
        @foreach ($recipes as $recipe)
            <article class="post hentry">
                <div class="foodstar_header author">
                    <table>
                        <tr>
                            <td width="70px">
                                <a href="{{ route('view_user_chronic', ['slug' => $recipe->user->profile->slug]) }}">
                                    <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$recipe->user->profile->avatar) }}" alt="">
                                </a>
                            </td>
                            <td style="vertical-align: middle;">
                                <a href="{{ route('view_user_chronic', ['slug' => $recipe->user->profile->slug]) }}">{{ $recipe->user->name }}</a>
                            </td>
                        </tr>
                    </table>
                </div>

                <figure class="post-thumbnail">
                    <a href="{{ route('recipe_view', ['slug' => $recipe->slug]) }}"
                       title="{{ $recipe->title }}"><img width="357" height="357"
                                                         src="@if(empty($recipe->main_image_watermarked)) {{ asset('storage/recipes/original/'.$recipe->main_image_original) }} @else {{ asset('storage/recipes/watermark/'.$recipe->main_image_watermarked) }} @endif"
                                                         class="attachment-skoty-blog-post"
                                                         alt="{{ $recipe->title }}"/>
                    </a>
                    <h2 class="post-title">
                        <a href="{{ route('recipe_view', ['slug' => $recipe->slug]) }}">{{ mb_strimwidth($recipe->title, 0, 56, '...') }}</a>
                    </h2>
                </figure>

                <div class="post-content">
                    <div class="additional-info">
                        <img src="{{ asset('assets/images/yield.png') }}" alt="{{ trans('pages/homepage.serves') }}"> <span>@if(Lang::locale() == 'de'){{ $recipe->portions }} {{ trans('pages/homepage.serves') }}@else{{ trans('pages/homepage.serves') }} {{ $recipe->portions }}@endif</span>
                    </div>

                    <div class="additional-info" style="float:right;">
                        <img src="{{ asset('assets/images/'.strtolower($recipe->difficulty).'.png') }}" alt="{{ trans('pages/add_recipe.form.difficulty.options.'.$recipe->difficulty) }}"> <span>{{ trans('pages/add_recipe.form.difficulty.options.'.$recipe->difficulty) }}</span>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </article>
        @endforeach
    </div>
    @if(count($recipes) < 1)
    <div class="text-center">
        <h3>{{ trans('pages/recipe.no_recipes') }}</h3>
    </div>
    @endif
    <div class="content text_center">
        <?php Foodstarz\Models\Pagination::render($recipes); ?>
    </div>

</article>
@endsection