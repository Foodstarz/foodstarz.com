@extends('skoty_layout.layout')

@section('title', trans('pages/user_profile_recipes.page_title').' - '.$user->name)

@section('seo-metas')
<meta name="description" content="Check the recipes of {{ $user->name }} - one of our Foodstarz!"/>
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('og-tags')
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:image" content="{{ asset('storage/avatars/'.$user->profile->avatar) }}" />
    <meta property="og:title" content="{{$user->name }} - Recipes" />
    <meta property="og:description" content="{{trans('pages/seo.homepage.description')}}" />
    <meta property="og:url" content="{{ route('view_user_profile_interview', $user->profile->slug) }}" />
    <meta property="og:site_name" content="Foodstarz" />
    <meta property="fb:admins" content="582794454" />
@endsection

@section('content')
    @if($user->role == 4 || $user->role == 1 || (Auth::check() && ($user->id == Auth::user()->id || Auth::user()->role == 1)))

    <article class="profile-article center-box">
        <h1 class="new-post-title">{{ $user->name }}</h1>
        <p class="text-center text-bold">@if(isset($profile->culinary_profession)) {{ trans(Config::get('profile.culinary_profession')[$profile->culinary_profession]) }} | @endif @if(isset($profile->city)) {{ $profile->city }} | @endif @if(isset($profile->country)) @if(isset(Config::get('custom.countryList')[$profile->country])) {{ Config::get('custom.countryList')[$profile->country] }} @else {{ $profile->country }} @endif @endif</p>

        @include('skoty_layout.pages.user_profile_menu')
    </article>
    <div class="clearfix"></div>
    
        @if($user->hasRecipes())
            <article class='homepage center-box' style='margin-top: 30px;'>
                @if(Auth::check() && Auth::user()->id == $user->id)
                    <div class="filter_recipes">
                        <a href="#" data-key="approved" class="active">{{ trans('pages/recipe.approved') }}</a>
                        <a href="#" data-key="in-progress">{{ trans('pages/recipe.in-progress') }}</a>
                        <a href="#" data-key="waiting">{{ trans('pages/recipe.waiting') }}</a>
                    </div>
                    <div class="recipes_holder content posts">
                        @if(sizeof($user->getApprovedRecipes()) == 0)
                            <div class="text-center">
                                <h3>{{ trans('pages/recipe.no_recipes') }}</h3>
                            </div>
                            <script>
                                $(function() {
                                    setTimeout(function() {
                                        $('.recipes_holder').css({'height':'auto'});
                                    }, 1000);
                                });
                            </script>
                        @else
                            @foreach ($user->getApprovedRecipes() as $recipe)
                                <article class="post hentry">
                                    <div class="foodstar_header author">
                                        <table>
                                            <tr>
                                                <td width="70px">
                                                    <a href="{{ route('view_user_profile', ['slug' => $recipe->user->profile->slug]) }}">
                                                        <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$recipe->user->profile->avatar) }}" alt="">
                                                    </a>
                                                </td>
                                                <td style="vertical-align: middle;">
                                                    <a href="{{ route('view_user_profile', ['slug' => $recipe->user->profile->slug]) }}">{{ $recipe->user->name }}</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <figure class="post-thumbnail">
                                        <a href="{{ route('recipe_view', ['slug' => $recipe->slug]) }}"
                                           title="{{ $recipe->title }}"><img width="357" height="357"
                                                                             src="@if(empty($recipe->main_image_watermarked)) {{ asset('storage/recipes/original/'.$recipe->main_image_original) }} @else {{ asset('storage/recipes/watermark/'.$recipe->main_image_watermarked) }} @endif"
                                                                             class="attachment-skoty-blog-post"
                                                                             alt="{{ $recipe->title }}"/>
                                        </a>
                                        <h2 class="post-title">
                                            <a href="{{ route('recipe_view', ['slug' => $recipe->slug]) }}">{{ mb_strimwidth($recipe->title, 0, 56, '...') }}</a>
                                        </h2>
                                    </figure>

                                    <div class="post-content">
                                        <div class="additional-info">
                                            <img src="{{ asset('assets/images/yield.png') }}" alt="{{ trans('pages/homepage.serves') }}"> <span>@if(Lang::locale() == 'de'){{ $recipe->portions }} {{ trans('pages/homepage.serves') }}@else{{ trans('pages/homepage.serves') }} {{ $recipe->portions }}@endif</span>
                                        </div>

                                        <div class="additional-info" style="float:right;">
                                            <img src="{{ asset('assets/images/'.strtolower($recipe->difficulty).'.png') }}" alt="{{ trans('pages/add_recipe.form.difficulty.options.'.$recipe->difficulty) }}"> <span>{{ trans('pages/add_recipe.form.difficulty.options.'.$recipe->difficulty) }}</span>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </article>
                            @endforeach
                        @endif
                    </div>
                    <script>
                        $(function () {
                           $(".filter_recipes a").on("click", function (e) {
                                var that = $(this);
                                $.ajax({
                                   url: "{{ route("get_recipes") }}",
                                   data: {key: that.attr("data-key")},
                                   success: function(data) {
                                       if(data.status == false) {
                                           alert(data.message);
                                       } else {
                                           $('.recipes_holder').html(data.result);
                                           $('.recipes_holder').css({'height':'auto'});
                                           $(".filter_recipes a.active").removeClass('active');
                                           that.addClass("active");
                                       }
                                   }
                                });
                                e.preventDefault();
                           });
                        });
                    </script>
                @else
                    @if(count($user->getApprovedRecipes()) == 0)
                        <div class="text-center">
                            <h3>{{ trans('pages/recipe.no_recipes') }}</h3>
                        </div>
                    @else
                        <div class="recipes_holder content posts">
                        @foreach ($user->getApprovedRecipes() as $recipe)
                                <article class="post hentry">
                                    <div class="foodstar_header author">
                                        <table>
                                            <tr>
                                                <td width="70px">
                                                    <a href="{{ route('view_user_profile', ['slug' => $recipe->user->profile->slug]) }}">
                                                        <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$recipe->user->profile->avatar) }}" alt="">
                                                    </a>
                                                </td>
                                                <td style="vertical-align: middle;">
                                                    <a href="{{ route('view_user_profile', ['slug' => $recipe->user->profile->slug]) }}">{{ $recipe->user->name }}</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <figure class="post-thumbnail">
                                        <a href="{{ route('recipe_view', ['slug' => $recipe->slug]) }}"
                                           title="{{ $recipe->title }}"><img width="357" height="357"
                                                                             src="@if(empty($recipe->main_image_watermarked)) {{ asset('storage/recipes/original/'.$recipe->main_image_original) }} @else {{ asset('storage/recipes/watermark/'.$recipe->main_image_watermarked) }} @endif"
                                                                             class="attachment-skoty-blog-post"
                                                                             alt="{{ $recipe->title }}"/>
                                        </a>
                                        <h2 class="post-title">
                                            <a href="{{ route('recipe_view', ['slug' => $recipe->slug]) }}">{{ mb_strimwidth($recipe->title, 0, 56, '...') }}</a>
                                        </h2>
                                    </figure>

                                    <div class="post-content">
                                        <div class="additional-info">
                                            <img src="{{ asset('assets/images/yield.png') }}" alt="{{ trans('pages/homepage.serves') }}"> <span>@if(Lang::locale() == 'de'){{ $recipe->portions }} {{ trans('pages/homepage.serves') }}@else{{ trans('pages/homepage.serves') }} {{ $recipe->portions }}@endif</span>
                                        </div>

                                        <div class="additional-info" style="float:right;">
                                            <img src="{{ asset('assets/images/'.strtolower($recipe->difficulty).'.png') }}" alt="{{ trans('pages/add_recipe.form.difficulty.options.'.$recipe->difficulty) }}"> <span>{{ trans('pages/add_recipe.form.difficulty.options.'.$recipe->difficulty) }}</span>
                                        </div>

                                        <div class="clearfix"></div>
                                    </div>
                                </article>
                        @endforeach
                    @endif
                    </div>
                @endif
                <div class="clearfix"></div>
            </article>
        @else
            <div class="text-center">
                <h3>{{ trans('pages/recipe.no_recipes') }}</h3>
            </div>
        @endif
        
    @else
        <article class="profile-article center-box">
            <h1 class="new-post-title">{{ $user->name }}</h1>

            @include('skoty_layout.pages.user_profile_menu')

            <div class="text-center">
                <h3>{{ trans('pages/user_profile.not_approved_heading') }}</h3>
                <p>{{ trans('pages/user_profile.not_approved_description') }}</p>
            </div>
        </article>
    @endif

    <div class="clearfix"></div>

@endsection