@extends('skoty_layout.layout')

@section('title', trans('pages/profile_videos.page_title'))

@section('seo-metas')
    <meta name="description" content="{{ trans('profile_videos.page_description', ['name' => $user->name]) }}"/>
@endsection

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('content')
    @if($user->role == 4 || $user->role == 1 || (Auth::check() && ($user->id == Auth::user()->id || Auth::user()->role == 1)))
        <article class="profile-article center-box">
            <h1 class="new-post-title">{{ $user->name }}</h1>
            <p class="text-center text-bold">@if(isset($profile->culinary_profession)) {{ trans(Config::get('profile.culinary_profession')[$profile->culinary_profession]) }} | @endif @if(isset($profile->city)) {{ $profile->city }} | @endif @if(isset($profile->country)) @if(isset(Config::get('custom.countryList')[$profile->country])) {{ Config::get('custom.countryList')[$profile->country] }} @else {{ $profile->country }} @endif @endif</p>

            @include('skoty_layout.pages.user_profile_menu')
        </article>
        <div class="clearfix"></div>

        <article class="homepage center-box" style="margin-top: 30px;">
            {{--<h1 class="post-title">{{ trans('pages/profile_videos.content_title') }}</h1>--}}

            <div class="content posts">
                @foreach ($videos as $video)
                    <article class="post hentry">
                        <div class="foodstar_header author">
                            <table>
                                <tr>
                                    <td width="70px">
                                        <a href="{{ route('view_user_chronic', ['slug' => $video->user->profile->slug]) }}">
                                            <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$video->user->profile->avatar) }}" alt="">
                                        </a>
                                    </td>
                                    <td style="vertical-align: middle;">
                                        <a href="{{ route('view_user_chronic', ['slug' => $video->user->profile->slug]) }}">{{ $video->user->name }}</a>
                                    </td>
                                </tr>
                            </table>
                        </div>

                        <figure class="post-thumbnail">
                            <a href="{{ route('video_view', $video->id) }}"
                               title="{{ $video->title }}"><img width="357"
                                                                  src="{{ $videos_pictures[$video->id] }}"
                                                                  class="attachment-skoty-blog-post"
                                                                  alt="{{ $video->title }}"/>
                            </a>
                            <h2 class="post-title">
                                <a href="{{ route('video_view', $video->id) }}">{{ mb_strimwidth($video->title, 0, 56, '...') }}</a>
                            </h2>
                        </figure>

                    </article>
                @endforeach
            </div>
            @if(count($videos) < 1)
                <div class="text-center">
                    <h3>{{ trans('pages/profile_videos.no_videos') }}</h3>
                </div>
            @endif
            <div class="content text_center">
                <?php Foodstarz\Models\Pagination::render($videos); ?>
            </div>

        </article>
    @else
        <article class="profile-article center-box">
            <h1 class="new-post-title">{{ $user->name }}</h1>

            @include('skoty_layout.pages.user_profile_menu')

            <div class="text-center">
                <h3>{{ trans('pages/user_profile.not_approved_heading') }}</h3>
                <p>{{ trans('pages/user_profile.not_approved_description') }}</p>
            </div>
        </article>
    @endif
@endsection