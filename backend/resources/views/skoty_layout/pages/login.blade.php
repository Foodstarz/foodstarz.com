@extends('skoty_layout.layout')

@section('title', trans('pages/login.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        <h1 class="post-title">{{ trans('pages/login.content_title') }}</h1>

        <div class="post-content">
            @if(session('success'))
                <div class="alert-box success">{{ session('success') }}</div>
            @endif

            <form method="POST" action="{{ route('login_process') }}">
                {!! csrf_field() !!}

                <p>
                    <b>{{ trans('pages/login.form.email.label') }}</b><br>
                    <input type="email" name="email" placeholder="{{ trans('pages/login.form.email.placeholder') }}" value="@if(session('email')) {{ session('email') }} @else {{ old('email') }} @endif">
                    @if(!empty($errors->get('email')))
                        @foreach($errors->get('email') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <b>{{ trans('pages/login.form.password.label') }}</b><br>
                    <input type="password" name="password" placeholder="{{ trans('pages/login.form.password.placeholder') }}">
                    @if(!empty($errors->get('password')))
                        @foreach($errors->get('password') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <input type="checkbox" name="remember" @if(old('remember')) checked @endif> {{ trans('pages/login.form.remember.label') }}
                    @if(!empty($errors->get('remember')))
                        @foreach($errors->get('remember') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>

                <p>
                    <button type="submit">{{ trans('pages/login.form.submit.label') }}</button>
                </p>
            </form>

            <p><a href="{{ route('forgotten_password') }}">{{ trans('pages/login.forgotten_password') }}</a></p>
        </div>
    </article>
@endsection