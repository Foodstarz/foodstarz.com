@extends('skoty_layout.layout')

@section('title', trans('pages/profile_close_account.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        <h1 class="post-title">{{ trans('pages/profile_close_account.content_title') }}</h1>        
        
        <div class="post-content">

            <div class="alert-box warning"><span>{{ trans('general.notification_boxes.warning') }}: </span>{{ trans('pages/profile_close_account.warning') }}</div>

            <form method="POST" action="{{ route('close_account_process') }}">
                {!! csrf_field() !!}

                <p>
                    <b>{{ trans('pages/profile_close_account.form.agreement.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="radio" name="agreement" value="yes"> {{ trans('pages/profile_close_account.form.agreement.yes') }}<br>
                    <input type="radio" name="agreement" value="no"> {{ trans('pages/profile_close_account.form.agreement.no') }}
                    @if(!empty($errors->get('agreement')))
                        @foreach($errors->get('agreement') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>
                
                <p>
                    <b>{{ trans('pages/profile_close_account.form.password.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                    <input type="password" name="password" @if(!empty($errors->get('password'))) class="input-error" @endif placeholder="{{ trans('pages/profile_close_account.form.password.placeholder') }}">
                    @if(!empty($errors->get('password')))
                        @foreach($errors->get('password') as $error)
                            <br>
                            <span class="error-msg">{{ $error }}</span>
                        @endforeach
                    @endif
                </p>
                
                <p>
                    <button type="submit">{{ trans('pages/profile_close_account.form.submit.label') }}</button>
                </p>
                
            </form>

        </div>
    </article>
@endsection