@extends('skoty_layout.layout')

@section('title', trans('pages/upload_avatar.page_title'))

@section('header_scripts')
    <script src="{{ asset('assets/js/jquery.cropit.js') }}"></script>

    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')
        
        @include('skoty_layout.pages.profile_menu')

        <h1 class="post-title">{{ trans('pages/upload_avatar.content_title') }}</h1>
        
        <div class="post-content">
            @if(session('base_profile_data'))

                <div id="success" class="alert-box success" style="display: none;">

                        {{ trans('pages/upload_avatar.messages.congratulations') }}
                        <br>
                        {{ trans('pages/upload_avatar.messages.next') }}
                        <br>
                        {{ trans('pages/upload_avatar.messages.uploading') }} <a href="{{ route('recipe_add') }}">{{ trans('pages/upload_avatar.messages.upload_recipes') }}</a>, <a href="{{ route('gallery') }}">{{ trans('pages/upload_avatar.messages.upload_images') }}</a> {{ trans('pages/upload_avatar.messages.or') }} <a href="{{ route('videos') }}">{{ trans('pages/upload_avatar.messages.upload_videos') }}</a>
                        <br>
                        {{ trans('pages/upload_avatar.messages.or') }}
                        <br>
                        {{ trans('pages/upload_avatar.messages.completing') }} <a href="{{ route('profile_chef_data') }}">{{ trans('pages/upload_avatar.messages.chef_profile') }}</a> {{ trans('pages/upload_avatar.messages.or') }} <a href="{{ route('interview') }}">{{ trans('pages/upload_avatar.messages.interview') }}</a>

                </div>

                <form method="POST" action="{{ route('upload_avatar_process') }}">
                    {!! csrf_field() !!}

                    <div>
                        <b>{{ trans('pages/upload_avatar.form.avatar.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br>
                        <div class="image-editor">
                            <input type="file" class="cropit-image-input">
                            <div class="cropit-image-preview"></div>
                            <div class="text-center"><b>Minimum size:</b> 750x750px</div>
                            <div class="image-size-label">
                                <b>{{ trans('pages/upload_avatar.resize_image') }}</b>
                            </div>
                            <input type="range" class="cropit-image-zoom-input">
                            <input type="hidden" name="avatar" class="hidden-image-data" />
                        </div>

                        <script>
                            $(function() {
                                var imageEditor = $('.image-editor');

                                imageEditor.cropit({
                                    imageState: {
                                        src: '{{ asset('storage/avatars/'.Auth::user()->profile->avatar) }}'
                                    },
                                    export: {
                                        type: 'image/jpeg',
                                        quality: 1,
                                        originalSize: false
                                    },
                                    previewSize: {
                                        width: 200,
                                        height: 200
                                    },
                                    exportZoom: 3.75,
                                    rejectSmallImage: false
                                });

                                var requestLoading = $('.loading');
                                $('form').submit(function() {
                                    requestLoading.show();

                                    var errorsDiv = $('#errors');
                                    errorsDiv.html('');

                                    var successDiv = $('#success');
                                    successDiv.hide();

                                    if(typeof(imageEditor.cropit('imageSrc')) === 'undefined') {
                                        errorsDiv.html('<span class="error-msg">{{ trans('pages/upload_avatar.messages.valid_image_error') }}</span>');
                                        requestLoading.hide();
                                        return false;
                                    }

                                    if(imageEditor.cropit('imageSize').width < 750 || $('.image-editor').cropit('imageSize').height < 750)
                                    {
                                        errorsDiv.html('<span class="error-msg">{{ trans('pages/upload_avatar.messages.image_dimensions_error') }}</span>');
                                        requestLoading.hide();
                                        return false;
                                    }

                                    // Move cropped image data to hidden input
                                    var imageData = $('.image-editor').cropit('export');
                                    $('.hidden-image-data').val(imageData);

                                    var actionUrl = $(this).attr('action');
                                    var postData = new FormData(this);

                                    $.ajax({
                                        type: 'POST',
                                        url: actionUrl,
                                        data: postData,
                                        dataType: 'json',
                                        processData: false,
                                        contentType: false
                                    }).done(function(responseData) {
                                        requestLoading.hide();

                                        $(this).find('input[name=_token]').val(responseData._token);

                                        if(responseData.ok === false) {
                                            if(typeof(responseData.errors) !== 'undefined' && Object.keys(responseData.errors).length > 0) {
                                                var errorsList = responseData.errors;
                                                var html = '';
                                                $.each(errorsList, function(key, value) {
                                                    html += '<br><span class="error-msg">'+value+'</span>';
                                                });
                                                errorsDiv.html(html);
                                            }
                                        } else {

                                            successDiv.show();
                                            $('form').hide();
                                            $('.plusMenu').show();
                                            $('.avatarProgBtn').addClass('progtrckr-done');
                                            $('li.avatarProgBtn').attr('class','progtrckr-done');
                                        }
                                    });

                                    // Prevent the form from actually submitting
                                    return false;
                                });
                            });
                        </script>

                        <div class="request-loading hidden text-center">
                            <img src="{{ asset('assets/images/ajax-loader.gif') }}" />
                        </div>

                        <div id="errors">

                        </div>
                    </div>

                    <p>
                        <button type="submit">{{ trans('pages/upload_avatar.form.submit.label') }}</button>
                    </p>
                </form>

            @else
                <h4>{{ trans('pages/upload_avatar.base_profile_data_error') }}</h4>
            @endif
        </div>
    </article>
@endsection