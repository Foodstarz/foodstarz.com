@extends('skoty_layout.layout')

@section('title', trans('pages/edit_gallery_image.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/js/custom/add_gallery_image.js') }}"></script>
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
    <article class="content-article center-box">
        @include('skoty_layout.includes.user_top_data')
        @include('skoty_layout.pages.user_profile_menu')

        <h1 class="post-title"> </h1>

        <div class="post-content">
            <div id="success" class="alert-box success" style="display: none;"></div>

            <form method="POST" action="{{ route('gallery_edit_image_process', $imageData->id) }}" id="gallery_image_form" enctype="multipart/form-data">
                <fieldset>
                    <legend>{{ trans('pages/edit_gallery_image.content_title') }}</legend>

                    {!! csrf_field() !!}

                    <p>
                        <b>{{ trans('pages/edit_gallery_image.form.caption.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br/>
                        <input type="text" name="caption" placeholder="{{ trans('pages/edit_gallery_image.form.caption.placeholder') }}" value="{{ $imageData->caption }}" />
                    </p>
                    <div class="caption_errors errDiv"></div>

                    <p>
                        <b>{{ trans('pages/edit_gallery_image.form.image.label') }}</b> <span class="required">{{ trans('general.required_fields') }}</span><br/>
                        {{ trans('pages/edit_gallery_image.accepted_file_formats') }}
                        <br>
                        <center><img id="previewImg" src="{{ asset('storage/gallery/'.$imageData->image) }}" alt="{{ $imageData->caption }}" width="300px"></center><br>
                        {{ trans('pages/edit_gallery_image.form.image.replace') }}<br>
                        <input type="file" name="image" accept="image/*" capture="camera" />
                    </p>
                    <div class="image_errors errDiv"></div>

                    <p>
                        <b>{{ trans('pages/edit_gallery_image.form.social.label') }}</b><br/>
                        @if(isset($login_link))
                            <a href="{{ $login_link }}" target="_blank" style="color: #003366; border-bottom: 1px dotted #003366;"><i class="fa fa-facebook"></i> {{ trans('pages/edit_gallery_image.form.social.configure') }}</a>
                        @else
                            <input type="checkbox" name="facebook" value="1" checked> {{ trans('pages/edit_gallery_image.form.social.facebook') }}
                        @endif
                    </p><br>

                    <p>
                        <button type="submit">{{ trans('pages/edit_gallery_image.form.submit.label') }}</button>
                    </p>

                    <div class="request-loading hidden text-center">
                        <br>
                        <img src="{{ asset('assets/images/ajax-loader.gif') }}" />
                    </div>
                </fieldset>
            </form>
        </div>
    </article>
@endsection
