﻿@extends('skoty_layout.layout')

@section('title', trans('pages/gallery.page_title'))

@section('header_scripts')
    <script type="text/javascript">
        show_avatar = true;
    </script>
@endsection

@section('body_classes')
    show-avatar
@endsection

@section('content')
    <h1 class="post-title">{{ trans('pages/gallery.content_title') }}</h1>

    <div class="gourmet-ads">
        <div id="ga_9466753">
            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466753');
                });
            </script>
        </div>
    </div>

    <div class="gourmet-ads-mobile">
        <div id="ga_9466758">
            <script type="text/javascript">
                apntag.anq.push(function() {
                    apntag.showTag('ga_9466758');
                });
            </script>
        </div>
    </div>

    <article class="homepage center-box">

        <div class="content posts">
            @foreach ($images as $image)
                <article class="post hentry">
                    <div class="foodstar_header author">
                        <table>
                            <tr>
                                <td width="70px">
                                    <a href="{{ route('view_user_chronic', ['slug' => $image->user->profile->slug]) }}">
                                        <img style="border-radius: 50%;" width="70" align="middle" src="{{ asset('storage/avatars/'.$image->user->profile->avatar) }}" alt="">
                                    </a>
                                </td>
                                <td style="vertical-align: middle;">
                                    <a href="{{ route('view_user_chronic', ['slug' => $image->user->profile->slug]) }}">{{ $image->user->name }}</a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <figure class="post-thumbnail">
                        @if(Auth::check() && Auth::user()->role == 1)
                        <div class="admin-actions">
                            @if(isset($image->bookmark))
                                <a class="action-unbookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unbookmark', 'type' => 'image', 'id' => $image->id]) }}"></a>
                            @else
                                <a class="action-bookmark" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'bookmark', 'type' => 'image', 'id' => $image->id]) }}"></a>
                            @endif
                            @if($image->hidden == 0)
                                <a class="action-hide" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'hide', 'type' => 'image', 'id' => $image->id]) }}"></a>
                            @else
                                <a class="action-unhide" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'unhide', 'type' => 'image', 'id' => $image->id]) }}"></a>
                            @endif
                            <a class="action-edit" token="{{csrf_token()}}" not-ajax="true" href="{{ route('gallery_edit_image', ['id' => $image->id]) }}"></a>
                            <a class="action-delete" token="{{csrf_token()}}" href="{{ route('manage_content', ['action' => 'delete', 'type' => 'image', 'id' => $image->id]) }}"></a>
                        </div>
                        @endif
                        <a href="{{ route('gallery_view_image', $image->id) }}"
                           title="{{ $image->caption }}"><img width="357"
                                                              src="{{ asset('storage/gallery/thumbnails/'.$image->image) }}"
                                                              class="attachment-skoty-blog-post"
                                                              alt="{{ $image->caption }}"/>
                        </a>
                        <h2 class="post-title">
                            <a href="{{ route('gallery_view_image', $image->id) }}">{{ mb_strimwidth($image->caption, 0, 56, '...') }}</a>
                        </h2>
                    </figure>

                </article>
            @endforeach
        </div>
        @if(count($images) < 1)
            <div class="text-center">
                <h3>{{ trans('pages/gallery.no_images') }}</h3>
            </div>
        @endif
        <div class="content text_center">
            <?php Foodstarz\Models\Pagination::render($images); ?>
        </div>

    </article>
@endsection