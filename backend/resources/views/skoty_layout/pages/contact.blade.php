@extends('skoty_layout.layout')

@section('title', trans('pages/contacts.page_title') . " | Foodstarz.com")

@section('seo-metas')
<meta name="description" content="Your feedback is really important to us. We are open to any suggestions so don't waste a second - email us!"/>
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('og-tags')
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{trans('pages/contacts.page_title')}}" />
<meta property="og:description" content="{{trans('pages/seo.homepage.description')}}" />
<meta property="og:url" content="http://foodstarz.com" />
<meta property="og:site_name" content="Foodstarz" />
@endsection

@section('body_classes')
show-avatar page
@endsection

@section('content')
<article class="homepage center-box">
    <h1 class="post-title">{{ trans('pages/contacts.content_title') }}</h1>

    <div class="post-content" style="width: 100%;">
        <div role="form">
            @if(session('success'))
                <div class="alert-box success">{!! session('success') !!}</div>
            @else
                <form method="post" action="{{ route('contact_process') }}">
                    {!! csrf_field() !!}
                    <p>{{ trans('pages/contacts.your_name') }}<br>
                        <input type="text" value="{{ old('name') }}" name="name">
                        @if(!empty($errors->get('name')))
                            @foreach($errors->get('name') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>
                    <p>
                        {{ trans('pages/contacts.your_email') }}<br>
                        <input type="email" value="{{ old('email') }}" name="email">
                        @if(!empty($errors->get('email')))
                            @foreach($errors->get('email') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>
                    <p>
                        {{ trans('pages/contacts.subject') }}<br>
                        <input type="text" value="{{ old('subject') }}" name="subject">
                        @if(!empty($errors->get('subject')))
                            @foreach($errors->get('subject') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>
                    <p>
                        {{ trans('pages/contacts.your_message') }}<br>
                        <textarea rows="10" cols="40" name="message">{{ old('message') }}</textarea>
                        @if(!empty($errors->get('message')))
                            @foreach($errors->get('message') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>
                    <p>
                        {!! Recaptcha::render() !!}
                        @if(!empty($errors->get('g-recaptcha-response')))
                            @foreach($errors->get('g-recaptcha-response') as $error)
                                <br>
                                <span class="error-msg">{{ $error }}</span>
                            @endforeach
                        @endif
                    </p>
                    <input type="submit" value="{{ trans('pages/contacts.send') }}">
                </form>
            @endif
        </div>
    </div>


</article>
@endsection