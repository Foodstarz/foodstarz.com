@extends('skoty_layout.layout')

@section('title', $news->title . " | Foodstarz.com")

@section('seo-metas')
<meta name="description" content="Read the latest news of our website!"/>
@endsection

@section('header_styles')
    <link rel="stylesheet" href="{{ asset('assets/css/normalize.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/css/rrssb.css') }}" />
@endsection

@section('header_scripts')
<script type="text/javascript">
    show_avatar = true;
</script>
@endsection

@section('footer_scripts')
    <script src="{{ asset('assets/js/rrssb.min.js') }}"></script>
@endsection

@section('seo-metas')
<meta name="description" content="{{trans('pages/seo.homepage.description')}}"/>
@endsection

@section('og-tags')
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="website" />
<meta property="og:title" content="{{$news->title}}" />
<meta property="og:description" content="{{trans('pages/seo.homepage.description')}}" />
<meta property="og:url" content="http://foodstarz.com" />
<meta property="og:site_name" content="Foodstarz" />
@endsection

@section('body_classes')
    show-avatar page
@endsection

@section('content')
<article class="homepage center-box">
    <h1 class="post-title">{{ $news->title }}</h1>
    <div class="post-details">
        <center>
            <div class="post-date">
                <a href="#"><span class="fa fa-clock-o"></span> {{ date('d M, Y', strtotime($news->created_at)) }}</a>
            </div>
        </center>
    </div>

    <div class="post-content c_page">
        {!! $news->content !!}
    </div>
    
    <hr>
    
    @include('skoty_layout.includes.social_buttons')

    <div class="comments">
        <div id="disqus_thread"></div>
        <script type="text/javascript">
            /* * * CONFIGURATION VARIABLES * * */
            var disqus_shortname = 'foodstarz';

            /* * * DON'T EDIT BELOW THIS LINE * * */
            (function () {
                var dsq = document.createElement('script');
                dsq.type = 'text/javascript';
                dsq.async = true;
                dsq.src = '//' + disqus_shortname + '.disqus.com/embed.js';
                (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(dsq);
            })();
        </script>
        <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript" rel="nofollow">comments powered by Disqus.</a></noscript>
    </div>
</article>
@endsection