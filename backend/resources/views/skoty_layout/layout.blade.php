<!DOCTYPE html>
<html>
    <head>
        <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0" name="viewport">
        <meta charset="utf-8">
        @yield('seo-metas')
        <meta name="keywords" content="chef,food,network,kitchen,cook,recipes,presentation,cooking,culinary,international,worldwide,plating,profile,community,ingredients,restaurateur,cooking">
        
        @yield('og-tags')
        <link href="{{ asset('assets/images/apple-touch-icon-57x57.png') }}" rel="apple-touch-icon-precomposed">
        <link href="{{ asset('assets/images/apple-touch-icon-76x76.png') }}" sizes="76x76" rel="apple-touch-icon-precomposed">
        <link href="{{ asset('assets/images/apple-touch-icon-120x120.png') }}" sizes="120x120" rel="apple-touch-icon-precomposed">
        <link href="{{ asset('assets/images/apple-touch-icon-152x152.png') }}" sizes="152x152" rel="apple-touch-icon-precomposed">

        <title>@yield('title')</title>

        <link media="all" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C300%2C400%2C600&amp;subset=latin%2Clatin-ext&amp;ver=4.1.5" id="open-sans-css" rel="stylesheet">
        @yield('header_styles')
        <link media="all" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        <link media="all" type="text/css" href="{{ asset('assets/css/main-style.css') }}" rel="stylesheet">
        <link media="all" type="text/css" href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" />
        <link media="all" type="text/css" href="{{ asset('assets/css/loading.css') }}" rel="stylesheet" />
        <script type="text/javascript" src="http://cdn.grmtas.com/pub/ga_pub_5620.js"></script>
        <script src="{{ asset('assets/js/jquery-1.11.3.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/jquery-migrate-1.2.1.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/dropit.js') }}"></script>

        <script type="text/javascript">
            var show_avatar = false;
        </script>
        @yield('header_scripts')
    </head>

    <body class="content-header-height has-header-content horizontal-navigation @yield('body_classes')">
        <div class="loading" style="display:none;"></div>

        @if(session("old_user"))
        <div class="old_user">
            {{ trans('general.old_user') }}
            <a href="{{route("login_back")}}">{{ trans('general.old_user_back') }}</a>
        </div>
        @endif
        <div class="main-content">
            <div class="header-search-form">
                <div class="header-search-form-wrapper">
                    @include('skoty_layout.searchform')
                </div>
            </div>

            <div id="navigation">
                <div class="navigation-container">

                    <div class="navigation-wrapper">
                        <nav>
                            <ul class="navigation" id="menu-topmenu-1">
                                @foreach(Foodstarz\Models\Menus::where('menu', 1)->orderBy('order', 'ASC')->get() as $item)
                                    <li @if('/'.Route::getCurrentRoute()->getPath() == $item->url || Route::getCurrentRoute()->getPath() == $item->url)class="current-menu-item"@endif><a href="{{ $item->url }}">
                                            @if(Auth::check())
                                                @if(empty(Auth::user()->profile->lang))
                                                    @if(Auth::user()->role == 1)
                                                        @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                                            {{ json_decode($item->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                                                        @else
                                                            {{ json_decode($item->title, true)['en'] }}
                                                        @endif
                                                    @else
                                                        {{ json_decode($item->title, true)['en'] }}
                                                    @endif
                                                @else
                                                    {{ json_decode($item->title, true)[Auth::user()->profile->lang] }}
                                                @endif
                                            @else
                                                @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                                    {{ json_decode($item->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                                                @else
                                                    {{ json_decode($item->title, true)['en'] }}
                                                @endif
                                            @endif
                                    </a></li>
                                @endforeach
                                
                                @if(Auth::check() && Auth::user()->role == 1)
                                <li><a href="{{ route('admin_homepage') }}">Admin Panel</a></li>
                                @endif
                                
                                {{--
                                <li @if(Route::currentRouteNamed('homepage'))class="current-menu-item"@endif><a href="{{ route('homepage') }}">{{ trans('general.menu.homepage') }}</a></li>
                                <li @if(Route::currentRouteNamed('recipes'))class="current-menu-item"@endif><a href="{{ route('recipes') }}">{{ trans('general.menu.recipes') }}</a></li>
                                <li @if(Route::currentRouteNamed('foodstarz_listing'))class="current-menu-item"@endif><a href="{{ route("foodstarz_listing") }}">{{ trans('general.menu.chefs') }}</a></li>
                                <li @if(Route::currentRouteNamed('recipe_categories'))class="current-menu-item"@endif><a href="{{ route("recipe_categories")}}">{{ trans('general.menu.categories') }}</a></li>
                                <li @if(Route::currentRouteNamed('the_jury'))class="current-menu-item"@endif><a href="{{ route("the_jury") }}">{{ trans('general.menu.jury') }}</a></li>
                                <li @if(Route::currentRouteNamed('c_page'))class="current-menu-item"@endif><a href="{{ route("c_page", "about-us") }}">{{ trans('general.menu.about') }}</a></li>
                                --}}
                            </ul>
                        </nav>

                        <div class="search-opener icons-hover">
                            <span class="fa fa-search"></span>
                            <span class="fa fa-terminal"></span>
                            <span class="fa fa-times"></span>
                        </div>
                        <!--<div class="vertical-nav-separator"></div>-->
                        <div class="navigation-opener icons-hover">
                            <span class="fa fa-bars"></span>
                            <span class="fa fa-compass"></span>

                        </div>
                    </div>

                </div>
            </div>
            <!-- end #navigation -->

            <!-- Header Start -->
            <header id="header">

                @if( \Foodstarz\Helpers\CommonHelpers::showCameraUpload( Route::getCurrentRoute()->getName()) )

                    <div class="upload-background">
                        <span class="fa fa-camera fa-6" aria-hidden="true"></span>
                        <span class="text hidden">Update Cover Picture</span>

                        <form
                                method="POST"
                                action="{{ route('background_add_image') }}"
                                id="form-upload-background"
                                enctype="multipart/form-data"
                                style="display: none;"
                                data-ajax="false"
                        >
                            {!! csrf_field() !!}
                            <input type="file" name="image-background" accept="images/*" />
                        </form>
                    </div>
                @endif

                <!-- Header Wrapper Start -->
                <div class="header-wrapper" {!! (isset($inner_page) && !$inner_page) ? '' : 'style="height: 336px"' !!}>
                    <!-- Header Container Start -->
                    <div class="header-container">
                        <div class="logo-container">

                            <a href="{{ route('homepage') }}">
                                <img alt="Foodstarz" src="{{ asset('assets/images/logo.png') }}" class="logo">
                            </a>
                        </div>
                    </div>
                    <!-- Header Container End -->

                    @if (isset($inner_page) && !$inner_page)
                    <!-- Header Content Wrapper Start -->
                    <div class="header-content-wrapper">
                        @if(Auth::check())
                            @if(empty(Auth::user()->profile->lang))
                                @if(Auth::user()->role == 1)
                                    @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                        <h1>{{ Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_heading')[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}</h1>
                                        <div class="sub-heading">{!! Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_subheading_auth')[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] !!}</div>
                                    @else
                                        <h1>{{ Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_heading')['en'] }}</h1>
                                        <div class="sub-heading">{!! Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_subheading_auth')['en'] !!}</div>
                                    @endif
                                @else
                                    <h1>{{ Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_heading')['en'] }}</h1>
                                    <div class="sub-heading">{!! Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_subheading_auth')['en'] !!}</div>
                                @endif
                            @else
                                <h1>{{ Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_heading')[Auth::user()->profile->lang] }}</h1>
                                <div class="sub-heading">{!! Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_subheading_auth')[Auth::user()->profile->lang] !!}</div>
                            @endif
                        @else
                            @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                <h1>{{ Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_heading')[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}</h1>
                                <div class="sub-heading">{!! Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_subheading')[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] !!}</div>
                            @else
                                <h1>{{ Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_heading')['en'] }}</h1>
                                <div class="sub-heading">{!! Foodstarz\Helpers\UserHelpers::InfoWidget('homepage_subheading')['en'] !!}</div>
                            @endif
                        @endif
                    </div>
                    <!-- Header Content Wrapper End -->
                    @endif

                </div>
                <!-- Header Wrapper End -->
7 - 125
5 - x
                <!-- Header Avatar Wrapper Start -->
                @if(Route::currentRouteNamed('recipe_view') && !empty($recipe_author->profile->avatar))
                <a href="{{ route('view_user_chronic', ['slug' => $recipe_author->profile->slug]) }}">
                @endif
                @if(Route::currentRouteNamed('view_user_profile') || Route::currentRouteNamed('view_user_gallery') || Route::currentRouteNamed('gallery_view_image') || Route::currentRouteNamed('video_view') || Route::currentRouteNamed('view_user_profile_interview') || Route::currentRouteNamed('view_user_profile_recipes') || Route::currentRouteNamed('view_user_chronic'))
                <a href="{{ route('view_user_chronic', ['slug' => $profile->slug]) }}">
                @endif
                <figure class="header-avatar-wrapper">
                    <?php
                    $avatarPages = [
                        'view_user_profile',
                        'view_user_profile_interview',
                        'view_user_profile_recipes',
                        'recipe_add',
                        'profile_core_data',
                        'upload_avatar',
                        'profile_chef_data',
                        'interview',
                        'profile_settings',
                        'change_password',
                        'close_account',
                        'change_email',
                        'view_user_gallery',
                        'gallery_view_image',
                        'view_user_videos',
                        'video_view',
                        'view_user_chronic'
                    ];
                    ?>
                    @if(in_array(Route::currentRouteName(), $avatarPages) && !empty($profile->avatar))
                    <img width="250" alt="" src="{{ asset('storage/avatars/'.$profile->avatar) }}">
                    @elseif(Route::currentRouteNamed('recipe_view') && !empty($recipe_author->profile->avatar))
                    <img width="250" height="250" alt="" src="{{ asset('storage/avatars/'.$recipe->user->profile->avatar) }}">
                    @else
                    <img width="250" alt="" src="{{ asset('assets/images/foodstarz_logo-235x235.jpg') }}">
                    @endif
                </figure>
                @if(Route::currentRouteNamed('view_user_profile') || Route::currentRouteNamed('view_user_gallery') || Route::currentRouteNamed('gallery_view_image') || Route::currentRouteNamed('view_user_profile_interview') || Route::currentRouteNamed('view_user_profile_recipes'))
                </a>
                @endif
                @if(Route::currentRouteNamed('recipe_view') && !empty($recipe_author->profile->avatar))
                </a>
                @endif

                <!-- Header Avatar Wrapper End -->

                <!-- Header Media Holder Start -->
                <div class="header-media-holder">
                    @if ( \Foodstarz\Helpers\CommonHelpers::setBackground( Route::getCurrentRoute()->getName()) )
                        <?php
                            $rand_bg = asset( \Foodstarz\Helpers\CommonHelpers::setBackground( Route::getCurrentRoute()->getName()) );
                        ?>
                    @else
                        <?php
                            if(in_array(Route::currentRouteName(), $avatarPages) && $profile->background) {

                                $bgs = json_decode($profile->background);

                                if(count($bgs) > 1) $rand_bg = asset('storage/backgrounds/' . $bgs[rand(0, count($bgs-1))]);
                                else $rand_bg = asset('storage/backgrounds/' . $bgs[0]);

                            } else $rand_bg = asset('assets/images/bg_'.rand(1, 9).'.jpg');

                            $rand_bg_ratio = round(getimagesize($rand_bg)[0]/getimagesize($rand_bg)[1],2);
                        ?>
                    @endif


                    <div data-ratio="" data-media-opacity="0.21" data-media-mime="image/jpeg" data-media-type="image" data-media-url="{{$rand_bg}}" class="header-media"></div>
                    <span class="header-media-preloader preloader"></span>
                </div>
                <!-- Header Media Holder End -->

                <!-- Scroll Down Indicator Start -->
                @if (isset($inner_page) && !$inner_page)
                <div class="scroll-down-indicator icons-hover">
                    <span class="fa fa-angle-double-down"></span>
                    <span class="fa fa-angle-double-up"></span>
                </div>
                @endif
                <!-- Scroll Down Indicator End -->

            </header>
            <!-- Header End -->

            <div class="right_menu">
                @if(Auth::check())
                <div class="menu_button">
                    <a href="{{ route('view_user_chronic', ['slug' => session('profile')->slug]) }}">
                        <img src="{{ asset('assets/images/login_icon.png') }}" alt=""/>
                        <b>{{ trans('pages/homepage.right_menu.account') }} </b>
                    </a>
                </div>
                <div class="menu_button">
                    <a href="{{ route('logout') }}">
                        <img src="{{ asset('assets/images/logout_icon.png') }}" alt=""/>
                        <b>{{ trans('pages/homepage.right_menu.logout') }}</b>
                    </a>
                </div>
                @else
                <div class="menu_button">
                    <a href="{{ route('login') }}">
                        <img src="{{ asset('assets/images/login_icon.png') }}" alt=""/>
                        <b>{{ trans('pages/homepage.right_menu.login') }}</b>
                    </a>
                </div>
                <div class="menu_button">
                    <a href="{{ route('register') }}">
                        <img src="{{ asset('assets/images/joinus_icon.png') }}" alt=""/>
                        <b>{{ trans('pages/homepage.right_menu.register') }}</b>
                    </a>
                </div>
                @endif
            </div>

            <div class="content-preloader-wrapper">
                <span class="content-preloader preloader"></span>
            </div>

            <section class="content-wrapper">
                @yield('content')

                @if(!Route::currentRouteNamed('homepage'))
                    <article class="homepage center-box">
                        <hr class="hr-gray" />
                        <h1 class="post-title">{{ trans('pages/homepage.gold_partners') }}</h1>

                        <div class="content g_partners">
                            @foreach (Foodstarz\Helpers\CommonHelpers::getSponsoringPartners() as $partner)
                                <div class="one-third-column">
                                    <a href="{{$partner->url}}" target="_blank">
                                        <img src="{{ asset('storage/partners/'.$partner->image) }}" title="{{$partner->name}}" width="{{ $partner->width }}" height="{{ $partner->height }}"/>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </article>
                @endif

            </section>

            @if(Auth::check() /*&& (Auth::user()->role == 1 || in_array(Auth::user()->id, [50, 197, 80, 71, 285, 168, 122, 15, 217, 674, 158, 100]))*/)
                @if(!Route::currentRouteNamed('gallery_add_image') && !Route::currentRouteNamed('gallery_edit_image'))
                    <form method="POST" action="{{ route('gallery_add_image_process') }}" id="gallery_image_form" enctype="multipart/form-data" style="display: none;" data-ajax="false">
                        {!! csrf_field() !!}
                        <input type="file" name="image" accept="images/*" />
                    </form>

                    <!--form method="POST" action="" id="add_video_form" enctype="multipart/form-data" style="display: none;" data-ajax="false">
                        <input type="file" name="video" accept="video/mp4,video/x-m4v,video/*" capture="camcorder" />
                    </form-->

                    <ul class="plusMenu" @if(empty(session('profile')->avatar) || empty(session('profile_core_data')))style="display:none;"@endif>
                        <li>
                            <a class="plusBtn" href="#"><img src="{{ asset('assets/images/plus.png') }}"></a>
                            <ul>
                                <li><a href="{{ route('recipe_add') }}"><img src="{{ asset('assets/images/add_recipe_128.png') }}" width="18px"> {{ trans('pages/user_profile.menu.add_recipe') }}</a></li>
                                <li><a class="addImgBtn" href="{{ route('gallery_add_image') }}"><img src="{{ asset('assets/images/add_image_128.png') }}" width="18px"> {{ trans('pages/user_profile.menu.add_image') }}</a></li>
                                <li><a class="addVideoBtn" href="{{ route('add_video') }}"><img src="{{ asset('assets/images/add_video.png') }}" width="18px"> {{ trans('pages/user_profile.menu.add_video') }}</a></li>
                            </ul>
                        </li>
                    </ul>

                    <script>
                        $(function() {

                            var imageUploadForm = $('#form-upload-background');
                            var imageUploadField = $('input[name="image-background"]');
                            var loading = $('.loading');

                            $('.upload-background > .fa-camera, .upload-background > .text').click(function(e) {
                                e.preventDefault();
                                $('.upload-background > .text').show();
                                $('.upload-background').addClass('active');

                                imageUploadField.trigger('click');
                            });

                            imageUploadField.on('change', function() {
                                imageUploadField.submit();
                            });

                            imageUploadForm.submit(function(e) {
                                e.preventDefault();
                                loading.show();

                                var actionUrl = $(this).attr('action');
                                var postData = new FormData(this);

                                $.ajax({
                                    type: 'POST',
                                    url: actionUrl,
                                    data: postData,
                                    dataType: 'json',
                                    processData: false,
                                    contentType: false
                                }).done(function (responseData) {
                                    $(this).find('input[name=_token]').val(responseData._token);
                                    loading.hide();

                                    if (responseData.ok) {
                                        window.location.reload();
                                    }
                                });
                            });

                            var videoForm = $('#add_video_form');
                            var videoField = $('input[name="video"]');
                            var loading = $('.loading');
                            var complete_uri;

                            /*$('.addVideoBtn').click(function(e) {
                                e.preventDefault();
                                loading.show();

                                $.ajax({
                                    type: 'GET',
                                    url: '<?php //echo route('get_vimeo_ticket'); ?>',
                                    dataType: 'json'
                                }).done(function (responseData) {
                                    loading.hide();
                                    complete_uri = responseData.vimeo_response.body.complete_uri;
                                    //console.log(complete_uri);
                                    $('#add_video_form').attr('action', responseData.vimeo_response.body.upload_link_secure);
                                });

                                videoField.trigger('click');
                            });*/

                            var videoFile;
                            /*videoField.on('change', function() {
                                videoFile = videoField[0].files[0];
                                videoForm.submit();
                            });*/

                            function verifyUpload(actionUrl) {
                                loading.show();

                                $.ajax({
                                    type: 'PUT',
                                    url: actionUrl,
                                    processData: false,
                                    headers: {
                                        "Content-Length": "0",
                                        "Content-Range": "bytes */*"
                                    }
                                }).complete(function (response) {
                                    //console.log(response.getResponseHeader('Range'));
                                    //console.log("File size: " + videoFile.size);
                                    loading.hide();
                                    completeUpload();
                                });
                            }

                            function completeUpload() {
                                loading.show();

                                $.ajax({
                                    type: 'POST',
                                    url: '<?php echo route('completed_vimeo_upload'); ?>',
                                    data: JSON.stringify({'complete_uri': complete_uri, '_token': '{{ csrf_token() }}'}),
                                    dataType: 'json',
                                    contentType: 'application/json',
                                }).done(function (responseData) {
                                    loading.hide();

                                    if (responseData.ok) {
                                        window.location = "" + responseData.redirect_edit;
                                    }
                                });
                            }

                            videoForm.submit(function(e) {
                                e.preventDefault();
                                loading.show();

                                var actionUrl = $(this).attr('action');

                                $.ajax({
                                    type: 'PUT',
                                    url: actionUrl,
                                    data: videoFile,
                                    processData: false,
                                    headers: {
                                        "Content-Length": videoFile.size,
                                        "Content-Type": videoFile.type
                                    }
                                }).done(function (responseData) {
                                    loading.hide();
                                    verifyUpload(actionUrl);
                                });
                            });

                            $('.plusMenu').dropit();
                        });
                    </script>
                @endif
            @endif

            <footer id="footer">

                <div class="footer-widgets-wrapper">
                    <div class="footer-widgets">
                        <ul class="widgets {{--one-half-one-fourths --}}">
                            {{--
                            <li class="widget widget_text" id="text-3">
                                <div class="widget-wrapper">
                                    <span class="widget-title">Follow us on Facebook</span>
                                    <div class="textwidget">
                                        <div data-share="true" data-show-faces="true" data-action="like" data-layout="box_count" data-href="https://www.facebook.com/foodstarz" class="fb-like fb_iframe_widget" fb-xfbml-state="rendered" fb-iframe-plugin-query="action=like&amp;app_id=814279045307343&amp;container_width=490&amp;href=https%3A%2F%2Fwww.facebook.com%2Ffoodstarz&amp;layout=box_count&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=true"><span style="vertical-align: bottom; width: 47px; height: 86px;"><iframe width="1000px" height="1000px" frameborder="0" name="f3f3ea0bb0348d" allowtransparency="true" allowfullscreen="true" scrolling="no" title="fb:like Facebook Social Plugin" style="border: medium none; visibility: visible; width: 47px; height: 86px;" src="http://www.facebook.com/v2.2/plugins/like.php?action=like&amp;app_id=814279045307343&amp;channel=http%3A%2F%2Fstatic.ak.facebook.com%2Fconnect%2Fxd_arbiter%2F1ldYU13brY_.js%3Fversion%3D41%23cb%3Dfacc630c13053%26domain%3Dfoodstarz.com%26origin%3Dhttp%253A%252F%252Ffoodstarz.com%252Ff1a923058c47896%26relation%3Dparent.parent&amp;container_width=490&amp;href=https%3A%2F%2Fwww.facebook.com%2Ffoodstarz&amp;layout=box_count&amp;locale=en_US&amp;sdk=joey&amp;share=true&amp;show_faces=true" class=""></iframe></span></div>
                                    </div>
                                </div>
                            </li>
                            --}}
                            <li class="widget widget_nav_menu" id="nav_menu-2">
                                <div class="widget-wrapper">
                                    {{-- <span class="widget-title">Information</span> --}}
                                    <div class="menu-sitelinks-container">
                                        <ul class="menu footer-links" id="menu-sitelinks">
                                            @foreach(Foodstarz\Models\Menus::where('menu', 2)->orderBy('order', 'ASC')->get() as $item)
                                                <li @if('/'.Route::getCurrentRoute()->getPath() == $item->url || Route::getCurrentRoute()->getPath() == $item->url)class="current-menu-item"@endif><a href="{{ $item->url }}">
                                                        @if(Auth::check())
                                                            @if(empty(Auth::user()->profile->lang))
                                                                @if(Auth::user()->role == 1)
                                                                    @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                                                        {{ json_decode($item->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                                                                    @else
                                                                        {{ json_decode($item->title, true)['en'] }}
                                                                    @endif
                                                                @else
                                                                    {{ json_decode($item->title, true)['en'] }}
                                                                @endif
                                                            @else
                                                                {{ json_decode($item->title, true)[Auth::user()->profile->lang] }}
                                                            @endif
                                                        @else
                                                            @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                                                {{ json_decode($item->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                                                            @else
                                                                {{ json_decode($item->title, true)['en'] }}
                                                            @endif
                                                        @endif
                                                    </a></li>
                                            @endforeach

                                            {{--
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{route("contact_us")}}">Contact/Feedback</a></li>
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ route("c_page", "imprint") }}">Imprint</a></li>
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ route("c_page", "tos") }}">Terms of Use</a></li>
                                            <li class="menu-item menu-item-type-post_type menu-item-object-page"><a href="{{ route("c_page", "privacy") }}">Privacy Policy</a></li>
                                            --}}
                                        </ul>

                                        <ul class="footer-icons">
                                            <li><a href="https://www.instagram.com/foodstarz_official/"><img src="{{ asset('assets/images/social_icon_insta.png') }}"></a></li>
                                            <li><a href="https://www.facebook.com/foodstarz/"><img src="{{ asset('assets/images/social_icon_facebook.png') }}"></a></li>
                                            <li><a href="https://www.pinterest.com/foodstarz/"><img src="{{ asset('assets/images/social_icon_pinterest.png') }}"></a></li>
                                            <li><a href="http://foodstarz.tumblr.com/"><img src="{{ asset('assets/images/social_icon_tumblr.png') }}"></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="footer-content-container">
                    <div class="footer-content-wrapper">
                        <a class="to-top fa fa-angle-up" href="#top"></a>
                        <div class="footer-content">Go Up?</div>
                    </div>
                    <div class="footer-copyright-wrapper">
                        <div class="footer-copyright">&copy; Copyright 2014-{!! date('Y') !!} by Foodstarz. All rights reserved. </div>
                    </div>
                </div>
            </footer>
        </div>
        <div class="main-sidebar">
            <div class="main-sidebar-wrapper">
                <nav>
                    <ul class="sidebar-navigation" id="menu-topmenu-2">
                        @foreach(Foodstarz\Models\Menus::where('menu', 1)->orderBy('order', 'ASC')->get() as $item)
                            <li @if('/'.Route::getCurrentRoute()->getPath() == $item->url || Route::getCurrentRoute()->getPath() == $item->url)class="current-menu-item"@endif><a href="{{ $item->url }}">
                                    @if(Auth::check())
                                        @if(empty(Auth::user()->profile->lang))
                                            @if(Auth::user()->role == 1)
                                                @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                                    {{ json_decode($item->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                                                @else
                                                    {{ json_decode($item->title, true)['en'] }}
                                                @endif
                                            @else
                                                {{ json_decode($item->title, true)['en'] }}
                                            @endif
                                        @else
                                            {{ json_decode($item->title, true)[Auth::user()->profile->lang] }}
                                        @endif
                                    @else
                                        @if(array_key_exists(strtolower(GeoIP::getLocation(Request::ip())['isoCode']), Config::get('custom.languages')))
                                            {{ json_decode($item->title, true)[strtolower(GeoIP::getLocation(Request::ip())['isoCode'])] }}
                                        @else
                                            {{ json_decode($item->title, true)['en'] }}
                                        @endif
                                    @endif
                                </a></li>
                        @endforeach
                        
                        @if(Auth::check() && Auth::user()->role == 1)
                        <li><a href="{{ route('admin_homepage') }}">Admin Panel</a></li>
                        @endif
                        
                        {{--
                        <li @if(Route::currentRouteNamed('homepage'))class="current-menu-item"@endif><a href="{{ route('homepage') }}">{{ trans('general.menu.homepage') }}</a></li>
                        <li @if(Route::currentRouteNamed('recipes'))class="current-menu-item"@endif><a href="{{ route('recipes') }}">{{ trans('general.menu.recipes') }}</a></li>
                        <li @if(Route::currentRouteNamed('foodstarz_listing'))class="current-menu-item"@endif><a href="{{ route("foodstarz_listing") }}">{{ trans('general.menu.chefs') }}</a></li>
                        <li @if(Route::currentRouteNamed('recipe_categories'))class="current-menu-item"@endif><a href="{{ route("recipe_categories")}}">{{ trans('general.menu.categories') }}</a></li>
                        <li @if(Route::currentRouteNamed('the_jury'))class="current-menu-item"@endif><a href="{{ route("the_jury") }}">{{ trans('general.menu.jury') }}</a></li>
                        <li @if(Route::currentRouteNamed('c_page'))class="current-menu-item"@endif><a href="{{ route("c_page", "about-us") }}">{{ trans('general.menu.about') }}</a></li>
                        --}}
                    </ul>
                </nav>
                <ul class="widgets">
                    <li class="widget widget_search" id="search-2">
                        <div class="widget-wrapper">
                            <form action="{{ route('search') }}" class="search-form" method="get" role="search">
                                <label>
                                    <span class="screen-reader-text">Search for:</span>
                                    <input type="text" title="Search for:" name="q" value="" placeholder="Type and hit enter" class="search-field">
                                </label>
                                <input type="submit" value="Search" class="search-submit">
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
            <span class="main-sidebar-scroller"></span>
        </div>

        <div class="main-sidebar-opener icons-hover">
            <span class="fa fa-bars"></span>
            <span class="fa fa-compass"></span>
            <span class="fa fa-times"></span>
        </div>



        @yield('footer_styles')

        <script type="text/javascript" src="{{ asset('assets/js/hammer.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.hammer.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/custom/swipe_effect.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/helpers.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.isotope.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.flexslider.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/jquery.mediabox.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/script.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/custom/common.js') }}"></script>

        <!-- Begin Cookie Consent plugin by Silktide - http://silktide.com/cookieconsent -->
        <script type="text/javascript">
            window.cookieconsent_options = {"message":"This website uses cookies. By continuing to browse on the website you are agreeing to our use of cookies.","dismiss":"Okay!","learnMore":"Find out more here","link":"http://foodstarz.com/privacy","theme":"dark-bottom"};
        </script>

        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
        <!-- End Cookie Consent plugin -->

        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-53007428-1', 'auto');
        ga('send', 'pageview');
        @if(Auth::check())
        ga('set', '&uid', {{Auth::user()->id}});
        @endif
        </script>
        
        @yield('footer_scripts')

    </body>
</html>