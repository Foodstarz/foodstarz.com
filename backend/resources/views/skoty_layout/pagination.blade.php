<?php $total = (int)ceil($data->total() / $data->perPage()); ?>
@if($total > 1)
<ul class="pagination">
    @if($data->currentPage() > 1)
        <li><a href="{!! $data->url(1) !!}">{{ trans('pagination.first') }}</a></li>
        <li><a href="{!! $data->previousPageUrl() !!}">{{ trans('pagination.prev') }}</a></li>
    @endif
    <?php for($i=$data->currentPage()-5;$i<$data->currentPage();$i++) { ?>
    @if($i>0) <li><a href="{!! $data->url($i) !!}">{!! $i !!}</a></li> @endif
    <?php } ?>
    <li class="active"><span>{!! $data->currentPage() !!}</span></li>
    <?php for($i=$data->currentPage()+1;$i<=$data->currentPage()+5;$i++) { ?>
    @if($data->currentPage() <= $total && $i <= $total) <li><a href="{!! $data->url($i) !!}">{!! $i !!}</a></li> @endif
    <?php } ?>
    @if($data->currentPage() < $total)
        <li><a href="{!! $data->nextPageUrl() !!}">{{ trans('pagination.next') }}</a></li>
        <li><a href="{!! $data->url($total) !!}">{{ trans('pagination.last') }}</a></li>
    @endif
</ul>
@endif