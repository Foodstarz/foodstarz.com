@extends('admin_layout.layout')

@section('title', trans('admin/pages/partners.partners'))

@section('content_title', trans('admin/pages/partners.partners'))
@section('content_description', trans('admin/pages/partners.partners'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li class="active">{{ trans('admin/pages/partners.partners') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            {!! session('success') !!}
        </div>
        @endif
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/partners.partners') }}</h3>
                 <div class="pull-right"><a href="{{ route("partners_add")}}" class="btn btn-primary">{{ trans('admin/pages/partners.new_partner') }}</a></div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Actions</th>
                        </tr>
                        @foreach($partners AS $partner)
                        <tr>
                            <td>{{ $partner->name }}</td>
                            <td>
                                <a class="btn btn-warning" href="{{route("partners_edit", $partner->id)}}"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger" href="{{route("partners_delete", $partner->id)}}"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $partners->render() !!}
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection