@extends('admin_layout.layout')

@section('title', trans('admin/pages/edit_menu_item.page_title'))

@section('content_title', trans('admin/pages/edit_menu_item.content_title'))
@section('content_description', trans('admin/pages/edit_menu_item.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li><a href="{{ route('news_listing') }}">{{ trans('admin/layout.menu.news.header') }}</a></li>
<li><a href="#">{{ trans('admin/layout.menu.news.items.add_news') }}</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/edit_menu_item.messages.success_title') }}</h4>
                {{ session('success') }}
            </div>
        @endif
        <!-- general form elements -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/edit_menu_item.content_title') }}</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ route('menus_edit_process', ['menu_id' => $menu_id, 'id' => $item['id']]) }}">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="title">{{ trans('admin/pages/edit_menu_item.form.title.label') }}</label><br>
                        @foreach(Config::get('custom.languages') as $code => $lang)
                            <b>{{$lang}}</b><br>
                            <input type="text" placeholder="{{ trans('admin/pages/edit_menu_item.form.title.placeholder') }}" id="title" name="title[{{$code}}]" class="form-control" value="{{ $item['title'][$code] }}">
                        @endforeach
                        @if(!empty($errors->get('title')))
                            @foreach($errors->get('title') as $error)
                                <p class="text-red">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                    
                    <div class="form-group">
                        <label for="url">{{ trans('admin/pages/edit_menu_item.form.url.label') }}</label>
                        <input type="text" placeholder="{{ trans('admin/pages/edit_menu_item.form.url.placeholder') }}" id="title" name="url" class="form-control" value="{{ $item['url'] }}">
                        @if(!empty($errors->get('url')))
                            @foreach($errors->get('url') as $error)
                                <p class="text-red">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                    
                    <div class="form-group">
                        <label for="order">{{ trans('admin/pages/edit_menu_item.form.order.label') }}</label>
                        <input type="number" placeholder="{{ trans('admin/pages/edit_menu_item.form.order.placeholder') }}" id="order" name="order" class="form-control" value="{{ $item['order'] }}">
                        @if(!empty($errors->get('order')))
                            @foreach($errors->get('order') as $error)
                                <p class="text-red">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">{{ trans('admin/pages/edit_menu_item.form.submit.label') }}</button>
                </div>
            </form>
        </div><!-- /.box -->
    </div>
</div>
@endsection