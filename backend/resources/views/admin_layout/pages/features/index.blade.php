@extends('admin_layout.layout')

@section('title', trans('admin/pages/features.title'))

@section('content_title', trans('admin/pages/features.title'))
@section('content_description', trans('admin/layout.listing'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li class="active">{{ trans('admin/pages/features.title') }}</li>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-hover table-striped">
                    <tr>
                        <th>{!! trans('admin/pages/features.columns.name') !!}</th>
                        <th>{!! trans('admin/pages/features.columns.group') !!}</th>
                        <th>{!! trans('admin/pages/features.columns.status') !!}</th>
                    </tr>
                    @foreach($features as $feature)
                        <tr>
                            <td>{!! $feature->feature !!}</td>
                            <td>
                                <span class="label label-primary">{!! $feature->users_group->name !!}</span>
                                <select name="user_group" attr-id="{!! $feature->id !!}" style="display: none;">
                                    @foreach($user_groups as $group)
                                        <option value="{!! $group->id !!}" {!! $group->id == $feature->id ? 'selected' : '' !!}>{!! $group->name !!}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td><input type="checkbox" name="status" value="1" {!! $feature->status == 1 ? 'checked' : '' !!} attr-id="{!! $feature->id !!}"/></td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/libs/switcher/switcher.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/libs/toast/jquery.toast.min.css') }}">
@stop
@section('javascript')
    <script src="{{ asset('assets/libs/switcher/switcher.min.js') }}"></script>
    <script src="{{ asset('assets/libs/toast/jquery.toast.min.js') }}"></script>
    <script>
        $(function () {
            var switcherEl = $('input').switcher();
        });
        $(document).ready(function() {
            toastr.options = {
                closeButton: true,
                progressBar: false,
                showMethod: 'slideDown',
                timeOut: 4000
            };
            $('.switcher').on('click', function() {
                var id = $(this).children('input').attr('attr-id');
                if($(this).hasClass('is-active')) var status = 1;
                else var status = 0;
                $.ajax({
                    url : '{!! route('features_switch_data') !!}',
                    method : 'post',
                    dataType : 'json',
                    data : 'id='+id+'&data=status&status='+status+'&_token={!! csrf_token() !!}',
                    success: function(response) {
                        if(response.status == '1') toastr.success(response.message, response.title);
                        else toastr.danger(response.message, response.title);
                    }
                });
            });
            $('.label-primary').on('click', function() {
                $('select').hide();
                $(this).next('select').show();
                $(this).hide();
            });
            $('select').on('change', function() {
                var select = $(this);
                var id = select.attr('attr-id'),
                        group = select.val();
                $.ajax({
                    url : '{!! route('features_switch_data') !!}',
                    method : 'post',
                    dataType : 'json',
                    data : 'id='+id+'&data=user_group&user_group='+group+'&_token={!! csrf_token() !!}',
                    success: function(response) {
                        if(response.status == '1') {
                            toastr.success(response.message, response.title);
                            select.children('option[value="'+response.data+'"]').attr('selected', 'selected');
                            select.prev().text(select.children('option[value="'+response.data+'"]').text());
                        } else toastr.danger(response.message, response.title);
                        $('select').hide();
                        select.prev().show();

                    }
                });
            });
        });

    </script>
@stop