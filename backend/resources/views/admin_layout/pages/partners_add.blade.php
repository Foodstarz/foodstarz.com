@extends('admin_layout.layout')

@section('title', trans('admin/pages/partners.partners'))

@section('content_title', trans('admin/pages/partners.partners'))
@section('content_description', trans('admin/pages/partners.partners'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li class="active">{{ trans('admin/pages/partners.partners') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/partners.partners') }}</h3>
            </div>
            <form role="form" action="{{ route('partners_add_process')}}" method="POST" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="form-group">
                        <label>Type</label>
                        <select name="type" class="form-control">
                            <option value="1">Gold Partners</option>
                            <option value="2">Friends and Charity Partners</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>URL</label>
                        <input type="text" name="url" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" name="image" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Width</label>
                        <input type="text" name="width" class="form-control"/>
                    </div>
                     <div class="form-group">
                        <label>Height</label>
                        <input type="text" name="height" class="form-control"/>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection