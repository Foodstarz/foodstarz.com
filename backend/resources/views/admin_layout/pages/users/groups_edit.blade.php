@extends('admin_layout.layout')

@section('title', trans('admin/pages/users_groups.page_title'))

@section('content_title', trans('admin/pages/users_groups.content_title'))
@section('content_description', trans('admin/pages/users_groups.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li><a href="{{ route("users_groups") }}"><i class="fa fa-users"></i> {{ trans('admin/pages/users_groups.page_title') }}</a></li>
<li class="active">{!! $group->name !!}</li>


@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            {!! session('success') !!}
        </div>
        @endif
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/users_groups.page_list_title') }}</h3>
                <div class="pull-right"><a class="btn btn-danger" href="{{ route('users_groups_delete', ['id' => $group->id]) }}">Delete group</a></div>
            </div><!-- /.box-header -->
            <div class="box-body">
                <form action="{!! route('users_groups_edit_process', ['id' => $group->id]) !!}" method="post" class="row">
                    {{ csrf_field() }}
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{ trans('admin/pages/users_groups.label.name') }}</label>
                            <input class="form-control" type="text" name="name" value="{!! $group->name !!}">
                        </div>
                        <div class="form-group">
                            <label>{{ trans('admin/pages/users_groups.label.status') }}</label>
                            <select class="form-control" name="status">
                                <option value="1" {!! $group->status == 1 ? 'selected' : '' !!}>{{ trans('admin/pages/users_groups.statuses.active') }}</option>
                                <option value="0" {!! $group->status == 0 ? 'selected' : '' !!}>{{ trans('admin/pages/users_groups.statuses.inactive') }}</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" name="submit" value="{{ trans('admin/pages/users_groups.edit') }}">
                        </div>
                    </div>
                </form>
                <div class="table-responsive">
                    <table class="table table-striped">
                        <thead>
                        <tr>
                            <td>#</td>
                            <td>Name</td>
                            <td>Role</td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@stop