@extends('admin_layout.layout')

@section('title', trans('admin/pages/users_groups.page_title'))

@section('content_title', trans('admin/pages/users_groups.content_title'))
@section('content_description', trans('admin/pages/users_groups.page_description'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li class="active">{{ trans('admin/pages/users_groups.page_title') }}</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i> Success!</h4>
                    {!! session('success') !!}
                </div>
            @endif
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('admin/pages/users_groups.page_list_title') }}</h3>
                    <div class="pull-right"><a class="btn btn-primary" href="{{ route('users_groups_add') }}">Create group</a></div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>Name</td>
                                    <td>Users count</td>
                                    <td>Status</td>
                                    <td></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($groups as $group)
                                <tr>
                                    <td>{!! $group->id !!}</td>
                                    <td>{!! $group->name !!}</td>
                                    <td>{!! count($group->users) !!}</td>
                                    <td>{!! $group->status !!}</td>
                                    <td class="text-right"><a class="btn btn-warning" href="{!! route('users_groups_edit', ['id' => $group->id]) !!}"><i class="fa fa-cog"></i></a></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@stop