@extends('admin_layout.layout')

@section('title', trans('admin/pages/overview.title'))

@section('content_title', trans('admin/pages/overview.title'))
@section('content_description', trans('admin/layout.listing'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li class="active">{{ trans('admin/pages/overview.title') }}</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover countries-tables">
                    <thead>
                        <tr>
                            <th>Country</th>
                            <th class="text-center">Code</th>
                            <th class="text-center">Recipes</th>
                            <th class="text-center">Videos</th>
                            <th class="text-center">Images</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($countries AS $country)
                        <tr>
                            <td scope="row">{{ $country['name'] }}</td>
                            <td class="text-center">{{ $country['code'] }}</td>
                            <td class="bg-warning text-center">{{ $country['recipes'] }}</td>
                            <td class="bg-success text-center">{{ $country['videos'] }}</td>
                            <td class="bg-info text-center">{{ $country['images'] }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

    </div>
@stop

@section('javascript')
    <script src="{{ asset('assets/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.countries-tables').DataTable();
        });
    </script>
@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/adminlte/plugins/datatables/jquery.dataTables.min.css') }}">
@stop