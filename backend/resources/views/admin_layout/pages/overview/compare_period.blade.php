<div class="col-md-6">
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th colspan="2">Users</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Users registrations</td><td>{!! $first_users !!}</td>
            </tr>
            <tr>
                <td>Avatars</td><td>{!! $first_avatars !!}</td>
            </tr>
            <tr>
                <td>Core Data</td><td>{!! $first_core_data !!}</td>
            </tr>
            <tr>
                <td>Any Content</td><td>{!! $first_any_content !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover countries">
            <thead>
            <tr>
                <th>Countries</th>
            <tr>
            </thead>
            <tbody>
            @foreach($first_users_countries as $country)
                <tr>
                    <td>
                        <div class="country-block">
                            <span style="width: {!! $country['percent'] !!}%;"></span>
                            <span>{!! $country['name'] !!}</span>
                            <span>{!! $country['count'] !!} ({!! $country['percent'] !!}%)</span>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-6">
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th colspan="2">Users</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Users registrations</td><td>{!! $second_users !!}</td>
            </tr>
            <tr>
                <td>Avatars</td><td>{!! $second_avatars !!}</td>
            </tr>
            <tr>
                <td>Core Data</td><td>{!! $second_core_data !!}</td>
            </tr>
            <tr>
                <td>Any Content</td><td>{!! $second_any_content !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover countries">
            <thead>
            <tr>
                <th>Countries</th>
            <tr>
            </thead>
            <tbody>
            @foreach($second_users_countries as $country)
                <tr>
                    <td>
                        <div class="country-block">
                            <span style="width: {!! $country['percent'] !!}%;"></span>
                            <span>{!! $country['name'] !!}</span>
                            <span>{!! $country['count'] !!} ({!! $country['percent'] !!}%)</span>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>