@extends('admin_layout.layout')

@section('title', trans('admin/pages/overview.title'))

@section('content_title', trans('admin/pages/overview.title'))
@section('content_description', trans('admin/layout.listing'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li class="active">{{ trans('admin/pages/overview.title') }}</li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <table class="table table-striped table-hover users-tables">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th class="text-center">Recipes</th>
                            <th class="text-center">Videos</th>
                            <th class="text-center">Images</th>
                            {{--<th></th>--}}
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($users AS $user)
                        <tr>
                            <td scope="row">{{ $user->name }}</td>
                            <td class="bg-warning text-center">{{ count($user->recipes) }}</td>
                            <td class="bg-success text-center">{{ count($user->videos) }}</td>
                            <td class="bg-info text-center">{{ count($user->images) }}</td>
                            {{--<td>--}}
                                {{--<a class="btn btn-success" href="{{route("users_login_as", $user->id)}}"><i class="fa fa-external-link"></i></a>--}}
                                {{--@if(!empty($user->profile->avatar))--}}
                                    {{--<a class="btn btn-default" href="{{asset("storage/avatars/".$user->profile->avatar)}}" download="{{asset("storage/avatars/".$user->profile->avatar)}}"><i class="fa fa-download"></i></a>--}}
                                {{--@endif--}}
                                    {{--<a class="btn btn-warning" href="{{ route('users_edit', $user->id) }}"><i class="fa fa-pencil-square-o"></i></a>--}}

                            {{--</td>--}}
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        {{--<div class="box-footer">--}}
                {{--{!! $users->render() !!}--}}
        {{--</div>--}}
    </div>
@stop

@section('javascript')
    <script src="{{ asset('assets/adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('.users-tables').DataTable();
        });
    </script>
@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/adminlte/plugins/datatables/jquery.dataTables.min.css') }}">
@stop