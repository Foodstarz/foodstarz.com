@extends('admin_layout.layout')

@section('title', trans('admin/pages/overview.title'))

@section('content_title', trans('admin/pages/overview.title'))
@section('content_description', trans('admin/layout.listing'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li class="active">{{ trans('admin/pages/overview.title') }}</li>
@endsection

@section('content')
    <form class="compare" action="{{ route('compare_process') }}">
        <div class="row">
                <div class="col-md-12">
                    <h3><i class="fa fa-long-arrow-right"></i> From start to date</h3>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" value="{{ $first_date }}" name="first_date" placeholder="first date">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <input type="text" class="form-control" value="{{ $second_date }}" name="second_date" placeholder="second date">
                    </div>
                </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Compare</button>
                </div>
            </div>
        </div>
    </form>
    <hr style="margin:0; border-top: 1px solid #3c8dbc;">
    <form class="period" action="{{ route('compare_process') }}">
        <div class="row">
            <div class="col-md-12">
                <h3><i class="fa fa-arrows-h"></i> By periods</h3>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" value="{{ $first_period_1 }}" name="first_period_1" placeholder="first date period 1">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" value="{{ $second_period_1 }}" name="second_period_1" placeholder="second date period 1">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" value="{{ $first_period_2 }}" name="first_period_2" placeholder="first date period 2">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" value="{{ $second_period_2 }}" name="second_period_2" placeholder="second date period 2">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <button class="btn btn-primary" type="submit">Compare</button>
                </div>
            </div>
        </div>
    </form>

    <div class="row result">

    </div>
@stop

@section('javascript')
    <script src="{{ asset('assets/adminlte/plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $('input[type="text"]').datepicker({dateFormat: "yy-mm-dd", showWeek: true});
            $('.compare, .period').on('submit', function() {
                var button = $('.btn-primary'),
                    text = button.text();
                var form = $(this),
                        data = {},
                        url = location.pathname + '?';
                form.find('[name]').each(function() {
                    var name = $(this).attr('name'),
                        value = $(this).val();
                    if(value != '') data[name] = value;
                    if(value != '') url += name + '='  + data[name] + '&';
                });
                $.ajax({
                    url: form.attr('action'),
                    method: form.attr('method'),
                    data: data,
                    beforeSend: function() {
                        button.attr('disabled', 'disabled');
                        button.append(' <i class="fa fa-spinner fa-spin"></i>');
                    },
                    success: function(response) {
                        $('.result').html(response);
                        button.html('Compare');
                        button.removeAttr('disabled');
                        console.log(location);
                        history.pushState(2, '<?php echo trans('admin/pages/overview.title'); ?>', url);
                    }
                });
                return false;
            });
        });
    </script>
@stop
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/adminlte/plugins/jQueryUI/jquery-ui.min.css') }}">
@stop