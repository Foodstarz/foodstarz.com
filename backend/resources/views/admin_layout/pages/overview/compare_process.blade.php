<div class="col-md-6">
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th colspan="2">Users registrations</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Day</td><td>{!! $first_day !!}</td>
            </tr>
            <tr>
                <td>Week</td><td>{!! $first_week !!}</td>
            </tr>
            <tr>
                <td>Month</td><td>{!! $first_month !!}</td>
            </tr>
            <tr>
                <td>Total</td><td>{!! $first_total !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th colspan="2">Users activities</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Core-data</td><td>{!! $first_core_data !!}</td>
            </tr>
            <tr>
                <td>Avatars</td><td>{!! $first_avatars !!}</td>
            </tr>
            <tr>
                <td>Any Content (videos, recipes, images)</td><td>{!! $first_any_content !!}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover countries">
            <thead>
            <tr>
                <th>Countries</th>
            <tr>
            </thead>
            <tbody>
            @foreach($first_users_countries as $country)
                <tr>
                    <td>
                        <div class="country-block">
                            <span style="width: {!! $country['percent'] !!}%;"></span>
                            <span>{!! $country['name'] !!}</span>
                            <span>{!! $country['count'] !!} ({!! $country['percent'] !!}%)</span>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="col-md-6">
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th colspan="2">Users registrations</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Day</td><td>{!! $second_day !!} @if($first_day<$second_day)<small class="text-success">+{{$second_day-$first_day}}</small> @else <small class="text-danger">{{$second_day-$first_day}}</small> @endif</td>
            </tr>
            <tr>
                <td>Week</td><td>{!! $second_week !!} @if($first_week<$second_week)<small class="text-success">+{{$second_week-$first_week}}</small> @else <small class="text-danger">{{$second_week-$first_week}}</small> @endif</td>
            </tr>
            <tr>
                <td>Month</td><td>{!! $second_month !!} @if($first_month<$second_month)<small class="text-success">+{{$second_month-$first_month}}</small> @else <small class="text-danger">{{$second_month-$first_month}}</small> @endif</td>
            </tr>
            <tr>
                <td>Total</td><td>{!! $second_total !!} @if($first_total<$second_total)<small class="text-success">+{{$second_total-$first_total}}</small> @else <small class="text-danger">{{$second_total-$first_total}}</small> @endif</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table table-striped table-hover">
            <thead>
            <tr>
                <th colspan="2">Users activities</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Core-data</td><td>{!! $second_core_data !!} @if($first_core_data<$second_core_data)<small class="text-success">+{{$second_core_data-$first_core_data}}</small> @else <small class="text-danger">{{$second_core_data-$first_core_data}}</small> @endif</td>
            </tr>
            <tr>
                <td>Avatars</td><td>{!! $second_avatars !!} @if($first_avatars<$second_avatars)<small class="text-success">+{{$second_avatars-$first_avatars}}</small> @else <small class="text-danger">{{$second_avatars-$first_avatars}}</small> @endif</td>
            </tr>
            <tr>
                <td>Any Content (videos, recipes, images)</td><td>{!! $second_any_content !!} @if($first_any_content<$second_any_content)<small class="text-success">+{{$second_any_content-$first_any_content}}</small> @else <small class="text-danger">{{$second_any_content-$first_any_content}}</small> @endif</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="table-responsive">
        <table class="table table-bordered table-hover countries">
            <thead>
            <tr>
                <th>Countries</th>
            <tr>
            </thead>
            <tbody>
            @foreach($second_users_countries as $key => $value)
                <tr>
                    <td>
                        <div class="country-block">
                            <span style="width: {!! $value['percent'] !!}%;"></span>
                            <span>{!! $value['name'] !!}</span>
                            <span>@if(isset($first_users_countries[$key]['count']) && $value['count']!=$first_users_countries[$key]['count'])<i class="text-success">+{{$value['count']-$first_users_countries[$key]['count']}}</i> @endif {!! $value['count'] !!} ({!! $value['percent'] !!}%)</span>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>