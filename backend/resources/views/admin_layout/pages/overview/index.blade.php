@extends('admin_layout.layout')

@section('title', trans('admin/pages/overview.title'))

@section('content_title', trans('admin/pages/overview.title'))
@section('content_description', trans('admin/layout.listing'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li class="active">{{ trans('admin/pages/overview.title') }}</li>
@endsection

@section('content')

    <div class="row">
        <div class="col-md-4 text-align">
            <h3>Month / Total New Users</h3>
            <canvas id="Chart" width="200" height="200" style="margin: 0 auto;"></canvas>

        </div>
        <div class="col-md-4 text-align">
            <h3>Week / Month New Users</h3>
            <canvas id="Chart2" width="200" height="200" style="margin: 0 auto;"></canvas>

        </div>
        <div class="col-md-4 text-align">
            <h3>Day / Week New Users</h3>
            <canvas id="Chart3" width="200" height="200" style="margin: 0 auto;"></canvas>
        </div>
        <div class="col-md-6 text-center">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th colspan="2">Users registrations</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Day</td><td>{!! $day !!}</td>
                    </tr>
                    <tr>
                        <td>Week</td><td>{!! $week !!}</td>
                    </tr>
                    <tr>
                        <td>Month</td><td>{!! $month !!}</td>
                    </tr>
                    <tr>
                        <td>Total</td><td>{!! $total !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6 text-center">
            <div class="table-responsive">
                <table class="table table-striped table-hover">
                    <thead>
                    <tr>
                        <th colspan="2">Users activities</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Core-data</td><td>{!! $core_data !!}</td>
                    </tr>
                    <tr>
                        <td>Avatars</td><td>{!! $avatars !!}</td>
                    </tr>
                    <tr>
                        <td>Any Content (videos, recipes, images)</td><td>{!! $any_content !!}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered table-hover countries">
                    <thead>
                    <tr>
                        <th>Countries</th>
                    <tr>
                    </thead>
                    <tbody>
                    @foreach($users_countries as $country)
                        <tr>
                            <td>
                                <div class="country-block">
                                    <span style="width: {!! $country['percent'] !!}%;"></span>
                                    <span>{!! $country['name'] !!}</span>
                                    <span>{!! $country['count'] !!} ({!! $country['percent'] !!}%)</span>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="col-md-6">
            <div class="table-responsive">
                <table class="table table-bordered table-hover countries mostvisited">
                    <thead>
                    <tr>
                        <th colspan="2">Most visited pages</th>
                    <tr>
                    </thead>
                    <tbody>
                        @foreach($most_viewed_pages as $page)
                            <tr>
                                <td><a href="{!! config('app.url') !!}{!! $page['url'] !!}">{!! config('app.url') !!}{!! $page['url'] !!}</a></td>
                                <td>{!! $page['pageViews'] !!}</td>
                            <tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

@stop

@section('javascript')
    <script src="{{ asset('assets/adminlte/plugins/chartjs/Chart.js') }}"></script>
    <script>
        var chart = document.getElementById("Chart").getContext("2d");
        var chart2 = document.getElementById("Chart2").getContext("2d");
        var chart3 = document.getElementById("Chart3").getContext("2d");
        var data = [
            {
                value: {!! $total !!},
                color:"#F7464A",
                highlight: "#FF5A5E",
                label: "Total"
            },
            {
                value: {!! $month !!},
                color: "#46BFBD",
                highlight: "#5AD3D1",
                label: "Month"
            }
        ];
        var data2 = [
            {
                value: {!! $week !!},
                color: "#FDB45C",
                highlight: "#FFC870",
                label: "Week"
            },
            {
                value: {!! $month !!},
                color: "#222d32",
                highlight: "#2B383E",
                label: "Month"
            }
        ];
        var data3 = [
            {
                value: {!! $day !!},
                color:"#F7464A",
                highlight: "#FF5A5E",
                label: "Day"
            },
            {
                value: {!! $week !!},
                color: "#46BFBD",
                highlight: "#5AD3D1",
                label: "Week"
            }
        ];
        var options = {
            segmentShowStroke : true,
            segmentStrokeColor : "#fff",
            segmentStrokeWidth : 2,
            percentageInnerCutout : 50,
            animationSteps : 100,
            animationEasing : "easeOutBounce",
            animateRotate : true,
            animateScale : false,
            legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
        };
        var DoughnutChart = new Chart(chart).Pie(data,options);
        var DoughnutChart2 = new Chart(chart2).Pie(data2,options);
        var DoughnutChart3= new Chart(chart3).Pie(data3,options);
    </script>
@stop