@extends('admin_layout.layout')

@section('title', trans('admin/pages/recipes.recipes'))

@section('content_title', trans('admin/pages/recipes.recipes'))
@section('content_description', trans('admin/layout.listing'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li class="active">{{ trans('admin/pages/recipes.recipes') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/recipes.recipes') }}</h3>
                <div class="pull-right"><a class="btn btn-primary" href="{{ route('recipes_search') }}">Search</a></div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            @if(isset($sort) && $sort == 'title' && $sort_type == 'ASC')
                                <th><a href="{{ route('recipes_listing') }}?sort=title&sort_type=DESC">Title</a></th>
                            @else
                                <th><a href="{{ route('recipes_listing') }}?sort=title&sort_type=ASC">Title</a></th>
                            @endif
                            
                            <th>Author</th>
                            
                            @if(isset($sort) && $sort == 'status' && $sort_type == 'ASC')
                                <th><a href="{{ route('recipes_listing') }}?sort=status&sort_type=DESC">Status</a></th>
                            @else
                                <th><a href="{{ route('recipes_listing') }}?sort=status&sort_type=ASC">Status</a></th>
                            @endif
                            
                            <th>Actions</th>
                        </tr>
                        @foreach($recipes AS $recipe)
                        <tr>
                            <td>{{ $recipe->title }}</td>
                            <td>{{ $recipe->user->name }}</td>
                            <td>
                                @if($recipe->status == 1)
                                    <div class="label label-warning">{{ trans('admin/pages/recipes.pending_approval') }}</div>
                                @endif
                            </td>
                            <td>
                                @if(Auth::user()->role == 1)
                                <a class="btn  btn-primary" href="{{route("recipe_review", $recipe->id)}}"><i class="fa fa-search"></i></a>
                                @endif
                                @if(Auth::user()->role == 2)
                                <a target="_blank" class="btn  btn-primary" href="{{route("recipe_view", $recipe->slug)}}"><i class="fa fa-search"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $recipes->render() !!}
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection