@extends('admin_layout.layout')

@section('title', trans('admin/pages/recipes_search.page_title'))

@section('content_title', trans('admin/pages/recipes_search.content_title'))
@section('content_description', trans('admin/pages/recipes_search.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li class="active">{{ trans('admin/pages/users.users') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/recipes_search.search') }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <form role="form" method="GET" action="{{ route('recipes_search') }}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="search_title">{{ trans('admin/pages/recipes_search.search_title') }}</label>
                            <input type="text" id="search_name" name="search_title" class="form-control" value="@if(!empty($search_title)){{ $search_title }}@endif">
                        </div>
                        <div class="form-group">
                            <label for="search_author">{{ trans('admin/pages/recipes_search.search_author') }}</label>
                            <input type="text" id="search_email" name="search_author" class="form-control" value="@if(!empty($search_author)){{ $search_author }}@endif">
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button class="btn btn-primary" type="submit">{{ trans('admin/pages/recipes_search.search') }}</button>
                    </div>
                </form>
            </div>
        </div><!-- /.box -->
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/users_search.results') }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        @foreach($recipes AS $recipe)
                        <tr>
                            <td>{{ $recipe->title }}</td>
                            <td>{{ $recipe->user->name }}</td>
                            <td>
                                @if($recipe->status == 1)
                                    <div class="label label-warning">{{ trans('admin/pages/recipes.pending_approval') }}</div>
                                @elseif($recipe->status == 0)
                                    <div class="label label-warning">Still Working</div>
                                @else
                                    <div class="label label-success">Approved</div>
                                @endif
                            </td>
                            <td>
                                @if(Auth::user()->role == 1)
                                <a class="btn  btn-primary" href="{{route("recipe_review", $recipe->id)}}"><i class="fa fa-search"></i></a>
                                @endif
                                @if(Auth::user()->role == 2)
                                <a target="_blank" class="btn  btn-primary" href="{{route("recipe_view", $recipe->slug)}}"><i class="fa fa-search"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $recipes->render() !!}
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection