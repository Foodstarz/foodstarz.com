@extends('admin_layout.layout')

@section('title', trans('admin/pages/c_pages.page_title'))

@section('content_title', trans('admin/pages/c_pages.content_title'))
@section('content_description', trans('admin/pages/c_pages.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li class="active">{{ trans('admin/pages/c_pages.c_pages') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            {!! session('success') !!}
        </div>
        @endif
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/c_pages.c_pages') }}</h3>
                 <div class="pull-right"><a href="{{ route("c_pages_add")}}" class="btn btn-primary">{{ trans('admin/pages/c_pages.c_pages_new') }}</a></div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>Title</th>
                            <th>Actions</th>
                        </tr>
                        @foreach($texts AS $text)
                        <tr>
                            <td>{{ $text->title }}</td>
                            <td>
                                <a class="btn btn-warning" href="{{route("c_pages_edit", $text->id)}}"><i class="fa fa-edit"></i></a>
                                <a class="btn btn-danger" href="{{route("c_pages_delete", $text->id)}}"><i class="fa fa-times"></i></a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $texts->render() !!}
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection