@extends('admin_layout.layout')

@section('title', trans('admin/pages/users.users'))

@section('content_title', trans('admin/pages/users.users'))
@section('content_description', trans('admin/layout.listing'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li class="active">{{ trans('admin/pages/users.users') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            {!! session('success') !!}
        </div>
        @endif
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/users.users') }}</h3>
                <div class="pull-right"><a class="btn btn-primary" href="{{ route('users_search') }}">Search</a></div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                    <tr>
                            @if(isset($sort) && $sort == 'created_at' && $sort_type == 'ASC')
                                <td><a href="{{ route('users_listing') }}?sort=created_at&sort_type=DESC">Date</a></td>
                            @else
                                <td><a href="{{ route('users_listing') }}?sort=created_at&sort_type=ASC">Date</a></td>
                            @endif

                            @if(isset($sort) && $sort == 'name' && $sort_type == 'ASC')
                                <td><a href="{{ route('users_listing') }}?sort=name&sort_type=DESC">Name</a></td>
                            @else
                                <td><a href="{{ route('users_listing') }}?sort=name&sort_type=ASC">Name</a></td>
                            @endif
                            
                            @if(isset($sort) && $sort == 'email' && $sort_type == 'ASC')
                                <td><a href="{{ route('users_listing') }}?sort=email&sort_type=DESC">Email</a></td>
                            @else
                                <td><a href="{{ route('users_listing') }}?sort=email&sort_type=ASC">Email</a></td>
                            @endif
                            
                            @if(isset($sort) && $sort == 'role' && $sort_type == 'ASC')
                                <td><a href="{{ route('users_listing') }}?sort=role&sort_type=DESC">Role</a></td>
                            @else
                                <td><a href="{{ route('users_listing') }}?sort=role&sort_type=ASC">Role</a></td>
                            @endif

                            @if(isset($sort) && $sort == 'country' && $sort_type == 'ASC')
                                <td><a href="{{ route('users_listing') }}?sort=country&sort_type=DESC">Country</a></td>
                            @else
                                <td><a href="{{ route('users_listing') }}?sort=country&sort_type=ASC">Country</a></td>
                            @endif

                            @if(isset($sort) && $sort == 'culinary_profession' && $sort_type == 'ASC')
                                <td><a href="{{ route('users_listing') }}?sort=culinary_profession&sort_type=DESC">Culinary Profession</a></td>
                            @else
                                <td><a href="{{ route('users_listing') }}?sort=culinary_profession&sort_type=ASC">Culinary Profession</a></td>
                            @endif

                                <td>Core Data</td>
                                <td>Avatar</td>
                                <td>Chef Profile</td>
                                <td>Interview</td>
                                <td><a data-toggle="tooltip" data-placement="top" title="Approved / in Progress / Waiting">Recipes</a></td>
                                <td><a data-toggle="tooltip" data-placement="top" title="Images / Videos">Statistic</a></td>

                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users AS $user)
                        <tr>
                            <td scope="row">{{ $user->created_at }}</td>
                            <td>{{ $user->name }}</td>
                            <td>{{ $user->email }}</td>
                            <td>{{ $user->title }}</td>
                            <td>{{ $user->profile->country }}</td>
                            <td>{{ $user->profile->culinary_profession }}</td>
                            <td>
                                @if (!empty($user->name) && !empty($user->profile->gender) && !empty($user->profile->country) && !empty($user->profile->city) && !empty($user->profile->culinary_profession))
                                    <i class="fa fa-check"></i>
                                @else
                                    <i class="fa fa-times"></i>
                                @endif
                            </td>
                            <td>@if(!empty($user->profile->avatar))<i class="fa fa-check"></i>@else<i class="fa fa-times"></i>@endif</td>
                            <td>
                                @if (!empty($user->profile->biography) && !empty($user->profile->work_experience) && !empty($user->profile->cooking_style))
                                    <i class="fa fa-check"></i>
                                @else
                                    <i class="fa fa-times"></i>
                                @endif
                            </td>
                            <td>@if(!empty($user->profile->interview))<i class="fa fa-check"></i>@else<i class="fa fa-times"></i>@endif</td>
                            <td>{{ $user->countRecipes(2) }}/{{ $user->countRecipes(0) }}/{{ $user->countRecipes(1) }}</td>
                            <td>{{ count($user->images) }}/{{ count($user->videos) }}</td>
                            <td>
                                <a class="btn btn-success" href="{{route("users_login_as", $user->id)}}"><i class="fa fa-external-link"></i></a>
                                <a class="btn btn-danger" href="{{route("users_delete", $user->id)}}"><i class="fa fa-times"></i></a>
                                @if(!empty($user->profile->avatar))
                                    <a class="btn btn-default" href="{{asset("storage/avatars/".$user->profile->avatar)}}" download="{{asset("storage/avatars/".$user->profile->avatar)}}"><i class="fa fa-download"></i></a>
                                @endif
                                @if($user->active == '1')
                                    <a class="btn btn-danger" href="{{ route('users_inactivate', $user->id) }}"><i class="fa fa-bolt"></i></a>
                                @else
                                    <a class="btn btn-success" href="{{ route('users_inactivate', $user->id) }}"><i class="fa fa-check"></i></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                        </tbody>

                </table>
                </div>
            </div>
            <div class="box-footer">
                @if(isset($sort) && isset($sort_type))
                    {!! $users->appends(['sort' => $sort, 'sort_type' => $sort_type])->render() !!}
                @else
                    {!! $users->render() !!}
                @endif
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection