@extends('admin_layout.layout')

@section('title', trans('admin/pages/add_recipes_categories.page_title'))

@section('content_title', trans('admin/pages/add_recipes_categories.content_title'))
@section('content_description', trans('admin/pages/add_recipes_categories.page_description'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li><a href="{{ route('interview_questions_listing') }}">{{ trans('admin/layout.menu.news.header') }}</a></li>
    <li><a href="#">{{ trans('admin/layout.menu.recipes_categories.items.add_category') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">?</button>
                    <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/add_recipes_categories.messages.success_title') }}</h4>
                    {{ session('success') }}
                </div>
                @endif
                        <!-- general form elements -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('admin/pages/add_recipes_categories.content_title') }}</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{ route('recipes_categories_add_process') }}">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="question">{{ trans('admin/pages/add_recipes_categories.form.title.label') }}:</label><br>
                                @foreach(Config::get('custom.languages') as $code => $lang)
                                    <b>{{ $lang }}:</b><br>
                                    <input type="text" placeholder="{{ trans('admin/pages/add_recipes_categories.form.title.placeholder') }}" id="title" name="title[{{ $code }}]" class="form-control" value="{{ old('title')[$code] }}">
                                    <br>
                                @endforeach
                                @if(!empty($errors->get('title')))
                                    @foreach($errors->get('title') as $error)
                                        <p class="text-red">{{ $error }}</p>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="image">{{ trans('admin/pages/add_recipes_categories.form.image.label') }}:</label><br>
                                <input type="text" placeholder="{{ trans('admin/pages/add_recipes_categories.form.image.placeholder') }}" id="image" name="image" class="form-control" value="{{ old('image') }}">
                                @if(!empty($errors->get('image')))
                                    @foreach($errors->get('image') as $error)
                                        <p class="text-red">{{ $error }}</p>
                                    @endforeach
                                @endif
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">{{ trans('admin/pages/add_recipes_categories.form.submit.label') }}</button>
                        </div>
                    </form>
                </div><!-- /.box -->
        </div>
    </div>
@endsection