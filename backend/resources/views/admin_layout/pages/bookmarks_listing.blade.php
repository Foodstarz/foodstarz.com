@extends('admin_layout.layout')

@section('title', trans('admin/pages/bookmarks_listing.page_title'))

@section('content_title', trans('admin/pages/bookmarks_listing.content_title'))
@section('content_description', trans('admin/pages/bookmarks_listing.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ $type }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>{{ trans('admin/pages/bookmarks_listing.title') }}</th>
                            <th>{{ trans('admin/pages/bookmarks_listing.shared') }}</th>
                            <th>{{ trans('admin/pages/bookmarks_listing.date') }}</th>
                            <th>{{ trans('admin/pages/bookmarks_listing.actions') }}</th>
                        </tr>
                        @foreach($bookmarks as $bookmark)
                        <tr>
                            <td>
                                @if($bookmark['bookmark']->resource_type == '1')
                                    <a href="{{ route('recipe_view', $bookmark['data']->id) }}" target="_blank">{{ $bookmark['data']->title }}</a>
                                    <br>
                                    <img src="{{ asset('storage/recipes/original/'.$bookmark['data']->main_image_original) }}" width="100" height="100">
                                @elseif($bookmark['bookmark']->resource_type == '2')
                                    <a href="{{ route('gallery_view_image', $bookmark['data']->id) }}" target="_blank">{{ $bookmark['data']->caption }}</a>
                                    <br>
                                    <img src="{{ asset('storage/gallery/'.$bookmark['data']->image) }}" width="100" height="100">
                                @else
                                    <a href="{{ route('video_view', $bookmark['data']->id) }}" target="_blank">{{ $bookmark['data']->title }}</a>
                                @endif
                            </td>
                            <td>
                                @if($bookmark['bookmark']->shared)
                                    <span class="glyphicon glyphicon-ok"></span>
                                @else
                                    <span class="glyphicon glyphicon-remove"></span>
                                @endif
                            </td>
                            <td>
                                {{ $bookmark['bookmark']->created_at }}
                            </td>
                            <td>
                                @if($bookmark['bookmark']->shared)
                                    <a class="btn btn-danger" href="{{ route('share_bookmark', $bookmark['bookmark']->id) }}"><span class="glyphicon glyphicon-remove"></span></a>
                                @else
                                    <a class="btn btn-success" href="{{ route('share_bookmark', $bookmark['bookmark']->id) }}"><span class="glyphicon glyphicon-ok"></span></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $paginator->render() !!}
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection