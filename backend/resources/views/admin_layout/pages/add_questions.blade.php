@extends('admin_layout.layout')

@section('title', trans('admin/pages/add_questions.page_title'))

@section('content_title', trans('admin/pages/add_questions.content_title'))
@section('content_description', trans('admin/pages/add_questions.page_description'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li><a href="{{ route('interview_questions_listing') }}">{{ trans('admin/layout.menu.news.header') }}</a></li>
    <li><a href="#">{{ trans('admin/layout.menu.interview.items.add_questions') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">?</button>
                    <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/add_questions.messages.success_title') }}</h4>
                    {{ session('success') }}
                </div>
                @endif
                        <!-- general form elements -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('admin/pages/add_questions.content_title') }}</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{ route('interview_questions_add_process') }}">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="question">{{ trans('admin/pages/add_questions.form.question.label') }}:</label><br>
                                @foreach(Config::get('custom.languages') as $code => $lang)
                                    <b>{{ $lang }}:</b><br>
                                    <input type="text" placeholder="{{ trans('admin/pages/add_questions.form.question.placeholder') }}" id="question" name="question[{{ $code }}]" class="form-control" value="{{ old('question')[$code] }}">
                                    <br>
                                @endforeach
                                @if(!empty($errors->get('question')))
                                    @foreach($errors->get('question') as $error)
                                        <p class="text-red">{{ $error }}</p>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="answer_type">{{ trans('admin/pages/add_questions.form.answer_type.label') }}:</label><br>
                                <select name="answer_type" id="answer_type">
                                    <option value="input" @if(old('answer_type') == 'input') selected @endif>{{ trans('admin/pages/add_questions.form.answer_type.input') }}</option>
                                    <option value="textarea" @if(old('answer_type') == 'textarea') selected @endif>{{ trans('admin/pages/add_questions.form.answer_type.textarea') }}</option>
                                </select>
                                @if(!empty($errors->get('answer_type')))
                                    @foreach($errors->get('answer_type') as $error)
                                        <p class="text-red">{{ $error }}</p>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="required">{{ trans('admin/pages/add_questions.form.required.label') }}:</label><br>
                                <select name="required" id="required">
                                    <option value="0" @if(old('required') == '0') selected @endif>{{ trans('admin/pages/add_questions.form.required.no') }}</option>
                                    <option value="1" @if(old('required') == '1') selected @endif>{{ trans('admin/pages/add_questions.form.required.yes') }}</option>
                                </select>
                                @if(!empty($errors->get('required')))
                                    @foreach($errors->get('required') as $error)
                                        <p class="text-red">{{ $error }}</p>
                                    @endforeach
                                @endif
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">{{ trans('admin/pages/add_questions.form.submit.label') }}</button>
                        </div>
                    </form>
                </div><!-- /.box -->
        </div>
    </div>
@endsection