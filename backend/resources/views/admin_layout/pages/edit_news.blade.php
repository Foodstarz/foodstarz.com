@extends('admin_layout.layout')

@section('title', trans('admin/pages/edit_news.page_title'))

@section('content_title', trans('admin/pages/edit_news.content_title'))
@section('content_description', trans('admin/pages/edit_news.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li><a href="{{ route('news_listing') }}">{{ trans('admin/layout.menu.news.header') }}</a></li>
<li><a href="#">{{ trans('admin/pages/edit_news.content_title') }}</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/edit_news.messages.success_title') }}</h4>
                {{ session('success') }}<br>
                <a href="{{ route('view_news', $item['slug']) }}">{{ route('view_news', $item['slug']) }}</a>
            </div>
        @endif
        <!-- general form elements -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/edit_news.content_title') }}</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ route('edit_news_process', $item['id']) }}">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="title">{{ trans('admin/pages/edit_news.form.title.label') }}</label>
                        <input type="text" placeholder="{{ trans('admin/pages/edit_news.form.title.placeholder') }}" id="title" name="title" class="form-control" value="{{ $item['title'] }}">
                        @if(!empty($errors->get('title')))
                            @foreach($errors->get('title') as $error)
                                <p class="text-red">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="editor1">{{ trans('admin/pages/edit_news.form.content.label') }}</label>
                        <textarea id="editor1" name="content" rows="10" cols="80">{{ $item['content'] }}</textarea>
                        @if(!empty($errors->get('content')))
                            @foreach($errors->get('content') as $error)
                                <p class="text-red">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">{{ trans('admin/pages/edit_news.form.submit.label') }}</button>
                </div>
            </form>
        </div><!-- /.box -->
    </div>
</div>
@endsection