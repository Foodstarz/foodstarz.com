@extends('admin_layout.layout')

@section('title', trans('admin/pages/menus_listing.page_title'))

@section('content_title', trans('admin/pages/menus_listing.content_title'))
@section('content_description', trans('admin/pages/menus_listing.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li><a href="{{ route('news_listing') }}">{{ trans('admin/layout.menu.news.header') }}</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/add_news.messages.success_title') }}</h4>
                {{ session('success') }}<br>
            </div>
        @endif
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/menus_listing.content_title') }}</h3>
                <div class="pull-right"><a href="{{ route('menus_add', $menu_id)}}" class="btn btn-primary">{{ trans('admin/pages/menus_listing.add_menu_item') }}</a></div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>{{ trans('admin/pages/menus_listing.title') }}</th>
                            <th>{{ trans('admin/pages/menus_listing.url') }}</th>
                            <th>{{ trans('admin/pages/menus_listing.order') }}</th>
                            <th>{{ trans('admin/pages/menus_listing.actions') }}</th>
                        </tr>
                        @foreach($links as $link)
                        <tr>
                            <td>{{ json_decode($link->title, true)['en'] }}</td>
                            <td><a href="{{ $link->url }}" target="_blank">{{ $link->url }}</a></td>
                            <td>{{ $link->order }}</td>
                            <td>
                                <a class="btn btn-warning" href="{{ route("menus_edit", ['menu_id' => $menu_id, 'id' => $link->id]) }}"><i class="fa fa-edit"></i></a>
                                <form method="post" action="{{ route('menus_delete_process', ['menu_id' => $menu_id, 'id' => $link->id]) }}" style="display: inline;">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection