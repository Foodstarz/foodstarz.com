@extends('admin_layout.layout')

@section('title', trans('admin/pages/questions_listing.page_title'))

@section('content_title', trans('admin/pages/questions_listing.content_title'))
@section('content_description', trans('admin/pages/questions_listing.page_description'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li><a href="{{ route('recipes_categories_listing') }}">{{ trans('admin/layout.menu.interview.header') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">?</button>
                    <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/questions_listing.messages.success_title') }}</h4>
                    {{ session('success') }}<br>
                </div>
            @endif
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('admin/pages/questions_listing.content_title') }}</h3>
                    <div class="pull-right"><a href="{{ route("interview_questions_add")}}" class="btn btn-primary">{{ trans('admin/pages/questions_listing.add_question') }}</a></div>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th>{{ trans('admin/pages/questions_listing.question') }}</th>
                            <th>{{ trans('admin/pages/questions_listing.answer_type') }}</th>
                            <th>{{ trans('admin/pages/questions_listing.required') }}</th>
                            <th>{{ trans('admin/pages/questions_listing.actions') }}</th>
                        </tr>
                        @foreach($questions as $question)
                            <tr>
                                <td>{{ json_decode($question->question, true)['en'] }}</td>
                                <td>{{ $question->answer_type }}</td>
                                <td>{{ $question->required ? 'Yes' : 'No' }}</td>
                                <td>
                                    <a class="btn btn-warning" href="{{ route("interview_questions_edit", $question->id) }}"><i class="fa fa-edit"></i></a>
                                    <form method="post" action="{{ route('interview_questions_delete_process', $question->id) }}" style="display: inline;">
                                        {!! csrf_field() !!}
                                        <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="box-footer">
                    {!! $questions->render() !!}
                </div><!-- /.box-footer -->
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
@endsection