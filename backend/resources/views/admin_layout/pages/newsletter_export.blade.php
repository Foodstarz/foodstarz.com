@extends('admin_layout.layout')

@section('title', trans('admin/pages/newsletter_export.page_title'))

@section('content_title', trans('admin/pages/newsletter_export.content_title'))
@section('content_description', trans('admin/pages/newsletter_export.page_description'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li><a href="{{ route('newsletter_export') }}">{{ trans('admin/layout.menu.newsletter.newsletter_export') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">{{ trans('admin/pages/newsletter_export.content_title') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <b>E-Mails:</b> (Number: {{ count($emails) }})<br>
                    <textarea rows="20" cols="60">@foreach($emails as $email){{ $email.PHP_EOL }}@endforeach</textarea><br>
                    <b>Names:</b> (Number: {{ count($names) }})<br>
                    <textarea rows="20" cols="60">@foreach($names as $name){{ $name.PHP_EOL }}@endforeach</textarea>
                </div>
            </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
@endsection