@extends('admin_layout.layout')

@section('title', trans('admin/pages/homepage.page_title'))

@section('content_title', trans('admin/pages/homepage.content_title'))
@section('content_description', trans('admin/pages/homepage.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/homepage.messages.success_title') }}</h4>
                {{ session('success') }}
            </div>
        @endif
        <!-- general form elements -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/homepage.homepage_header') }}</h3>
            </div><!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="POST" action="{{ route('homepage_header_process') }}">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="heading">{{ trans('admin/pages/homepage.form.heading.label') }}</label><br>
                        @foreach(Config::get('custom.languages') as $code => $lang)
                            <b>{{ $lang }}:</b><br>
                            <input type="text" placeholder="{{ trans('admin/pages/add_news.form.title.placeholder') }}" id="heading" name="heading[{{ $code }}]" class="form-control" value="@if(isset($item['heading'][$code])){{$item['heading'][$code]}}@endif">
                            <br>
                        @endforeach
                        @if(!empty($errors->get('heading')))
                            @foreach($errors->get('heading') as $error)
                                <p class="text-red">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="editor1">{{ trans('admin/pages/homepage.form.subheading.label') }}</label><br>
                        @foreach(Config::get('custom.languages') as $code => $lang)
                            <b>{{ $lang }}:</b><br>
                            <textarea id="editor_{{$code}}" name="subheading[{{ $code }}]" rows="10" cols="80">@if(isset($item['subheading'][$code])){{$item['subheading'][$code]}}@endif</textarea>
                            <script>
                                $(function() {
                                    CKEDITOR.replace('editor_{{$code}}');
                                });
                            </script>
                            <br>
                        @endforeach
                        @if(!empty($errors->get('subheading_auth')))
                            @foreach($errors->get('subheading_auth') as $error)
                                <p class="text-red">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="editor1">{{ trans('admin/pages/homepage.form.subheading_auth.label') }}</label><br>
                        @foreach(Config::get('custom.languages') as $code => $lang)
                            <b>{{ $lang }}:</b><br>
                            <textarea id="editor_auth_{{$code}}" name="subheading_auth[{{ $code }}]" rows="10" cols="80">@if(isset($item['subheading_auth'][$code])){{$item['subheading_auth'][$code]}}@endif</textarea>
                            <script>
                                $(function() {
                                    CKEDITOR.replace('editor_auth_{{$code}}');
                                });
                            </script>
                            <br>
                        @endforeach
                        @if(!empty($errors->get('subheading_auth')))
                            @foreach($errors->get('subheading_auth') as $error)
                                <p class="text-red">{{ $error }}</p>
                            @endforeach
                        @endif
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">{{ trans('admin/pages/homepage.form.submit.label') }}</button>
                </div>
            </form>
        </div><!-- /.box -->
    </div>
</div>
@endsection