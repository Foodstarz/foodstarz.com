@extends('admin_layout.layout')

@section('title', trans('admin/pages/c_pages.page_title'))

@section('content_title', trans('admin/pages/c_pages.content_title'))
@section('content_description', trans('admin/pages/c_pages.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li class="active">{{ trans('admin/pages/c_pages.c_pages') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/c_pages.c_pages') }}</h3>
            </div><!-- /.box-header -->
            <form role="form" action="{{ route('c_pages_edit_p', $text->id )}}" method="POST">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputTitle">Title</label>
                        <input type="text" name="title" value="{{$text->title}}" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="exampleInputSlug">Url</label>
                        <input type="text" name="slug" value="{{$text->slug}}" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label>Text</label>
                        <textarea id="editor1" name="text" rows="10" cols="80">
                            {{ $text->text }}
                        </textarea>
                    </div>
                </div><!-- /.box-body -->

                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Submit</button>
                </div>
            </form>
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection