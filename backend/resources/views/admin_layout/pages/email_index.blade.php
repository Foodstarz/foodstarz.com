@extends('admin_layout.layout')

@section('title', trans('admin/pages/homepage.page_title'))

@section('content_title', 'Mass Email Invitation')
@section('content_description', 'Please follow the instructions below.')

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
@endsection

@section('content')
    @if(session('success'))
        <div class="alert alert-success alert-dismissable">
            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
            <h4><i class="icon fa fa-check"></i> Success!</h4>
            {{ session('success') }}
        </div>
    @else
        <p>This script will E-Mail all the users an invitation to the new website with randomly generated 6-character-long passwords.</p>
        <p>When you have tested all the features of the website and you are ready to inform all the users about it please click <b><u>only ONCE</u></b> the button below.</p>
        <p>The page will start loading and might take up to a few minutes to complete the process so please be patient.</p>
        <p>DO NOT refresh the page after the process has been initiated. When everything finishes please close the page and inform me to remove it permanently.</p>
        <p>In the end you will see a message that informs you whether the mass email invitation went smoothly.</p>
        <p>You can enter your gmail account and check the "Sent" folder - there you will see all the messages that have been sent to the users.</p>
        <hr>
        <form method="post" action="{{ route('mass_email_process') }}">
            {{ csrf_field() }}
            <button class="btn btn-block btn-primary btn-lg">Start E-Mailing Users!</button>
        </form>
    @endif
@endsection