@extends('admin_layout.layout')

@section('title', trans('admin/pages/newsletter_change.page_title'))

@section('content_title', trans('admin/pages/newsletter_change.content_title'))
@section('content_description', trans('admin/pages/newsletter_change.page_description'))

@section('breadcrumb')
    <li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
    <li><a href="{{ route('newsletter_change') }}">{{ trans('admin/layout.menu.newsletter.items.newsletter_change') }}</a></li>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session('success'))
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">?</button>
                    <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/newsletter_change.messages.success_title') }}</h4>
                    {{ session('success') }}
                </div>
                @endif
                <!-- general form elements -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">{{ trans('admin/pages/newsletter_change.content_title') }}</h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" method="POST" action="{{ route('newsletter_change_process') }}">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="title">{{ trans('admin/pages/newsletter_change.form.email.label') }}</label><br>
                                <textarea name="emails" rows="20" cols="60"></textarea>
                                @if(!empty($errors->get('emails')))
                                    @foreach($errors->get('emails') as $error)
                                        <p class="text-red">{{ $error }}</p>
                                    @endforeach
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="subscribe">{{ trans('admin/pages/newsletter_change.form.subscribe.label') }}</label>
                                <select name="subscribe">
                                    <option value="1">Subscribe</option>
                                    <option value="0">Unsubscribe</option>
                                </select>
                                @if(!empty($errors->get('subscribe')))
                                    @foreach($errors->get('subscribe') as $error)
                                        <p class="text-red">{{ $error }}</p>
                                    @endforeach
                                @endif
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">{{ trans('admin/pages/newsletter_change.form.submit.label') }}</button>
                        </div>
                    </form>
                </div><!-- /.box -->
        </div><!-- /.col -->
    </div>
@endsection