@extends('admin_layout.layout')

@section('title', trans('admin/pages/users_search.page_title'))

@section('content_title', trans('admin/pages/users_search.content_title'))
@section('content_description', trans('admin/pages/users_search.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li class="active">{{ trans('admin/pages/users.users') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/users_search.search') }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <form role="form" method="GET" action="{{ route('users_search') }}">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="search_name">{{ trans('admin/pages/users_search.search_name') }}</label>
                            <input type="text" id="search_name" name="search_name" class="form-control" value="@if(!empty($search_name)){{ $search_name }}@endif">
                        </div>
                        <div class="form-group">
                            <label for="search_email">{{ trans('admin/pages/users_search.search_email') }}</label>
                            <input type="text" id="search_email" name="search_email" class="form-control" value="@if(!empty($search_email)){{ $search_email }}@endif">
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button class="btn btn-primary" type="submit">{{ trans('admin/pages/users_search.search') }}</button>
                    </div>
                </form>
            </div>
        </div><!-- /.box -->
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/users_search.results') }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <div class="table-responsive">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                                <td>Name</td>
                                <td>Email</td>
                                <td>Role</td>
                                <td>Country</td>
                                <td>Culinary Profession</td>
                            <td>Core Data</td>
                            <td>Avatar</td>
                            <td>Chef Profile</td>
                            <td>Interview</td>
                            <td><a data-toggle="tooltip" data-placement="top" title="Approved / in Progress / Waiting">Recipes</a></td>
                            <td><a data-toggle="tooltip" data-placement="top" title="Images / Videos">Statistic</a></td>
                            <th>Actions</th>
                        </tr>
                        @foreach($users AS $user)
                            <tr>
                                <td scope="row">{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>{{ $user->title }}</td>
                                <td>{{ $user->profile->country }}</td>
                                <td>{{ $user->profile->culinary_profession }}</td>
                                <td>
                                    @if (!empty($user->name) && !empty($user->profile->gender) && !empty($user->profile->country) && !empty($user->profile->city) && !empty($user->profile->culinary_profession))
                                        <i class="fa fa-check"></i>
                                    @else
                                        <i class="fa fa-times"></i>
                                    @endif
                                </td>
                                <td>@if(!empty($user->profile->avatar))<i class="fa fa-check"></i>@else<i class="fa fa-times"></i>@endif</td>
                                <td>
                                    @if (!empty($user->profile->biography) && !empty($user->profile->work_experience) && !empty($user->profile->cooking_style))
                                        <i class="fa fa-check"></i>
                                    @else
                                        <i class="fa fa-times"></i>
                                    @endif
                                </td>
                                <td>@if(!empty($user->profile->interview))<i class="fa fa-check"></i>@else<i class="fa fa-times"></i>@endif</td>
                                <td>{{ $user->countRecipes(2) }}/{{ $user->countRecipes(0) }}/{{ $user->countRecipes(1) }}</td>
                                <td>{{ count($user->images) }}/{{ count($user->videos) }}</td>
                                <td>
                                    <a class="btn btn-success" href="{{route("users_login_as", $user->id)}}"><i class="fa fa-external-link"></i></a>
                                    <a class="btn btn-danger" href="{{route("users_delete", $user->id)}}"><i class="fa fa-times"></i></a>
                                    @if(!empty($user->profile->avatar))
                                        <a class="btn btn-default" href="{{asset("storage/avatars/".$user->profile->avatar)}}" download="{{asset("storage/avatars/".$user->profile->avatar)}}"><i class="fa fa-download"></i></a>
                                    @endif
                                    @if($user->active == '1')
                                        <a class="btn btn-danger" href="{{ route('users_inactivate', $user->id) }}"><i class="fa fa-bolt"></i></a>
                                    @else
                                        <a class="btn btn-success" href="{{ route('users_inactivate', $user->id) }}"><i class="fa fa-check"></i></a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                </div>
            </div>
            <div class="box-footer">
                {!! $users->render() !!}
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection