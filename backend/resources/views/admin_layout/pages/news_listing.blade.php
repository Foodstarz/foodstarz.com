@extends('admin_layout.layout')

@section('title', trans('admin/pages/news_listing.page_title'))

@section('content_title', trans('admin/pages/news_listing.content_title'))
@section('content_description', trans('admin/pages/news_listing.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li><a href="{{ route('news_listing') }}">{{ trans('admin/layout.menu.news.header') }}</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/add_news.messages.success_title') }}</h4>
                {{ session('success') }}<br>
            </div>
        @endif
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/news_listing.content_title') }}</h3>
                <div class="pull-right"><a href="{{ route("add_news")}}" class="btn btn-primary">{{ trans('admin/pages/news_listing.add_news') }}</a></div>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>{{ trans('admin/pages/news_listing.title') }}</th>
                            <th>{{ trans('admin/pages/news_listing.actions') }}</th>
                        </tr>
                        @foreach($news as $new)
                        <tr>
                            <td><a href="{{ route('view_news', $new->slug) }}" target="_blank">{{ $new->title }}</a></td>
                            <td>
                                <a class="btn btn-warning" href="{{ route("edit_news", $new->id) }}"><i class="fa fa-edit"></i></a>
                                <form method="post" action="{{ route('delete_news_process', $new->id) }}" style="display: inline;">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $news->render() !!}
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection