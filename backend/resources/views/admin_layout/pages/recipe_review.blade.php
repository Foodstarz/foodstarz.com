@extends('admin_layout.layout')

@section('title', trans('admin/pages/recipes.recipes'))

@section('content_title', trans('admin/pages/recipes.recipe_review'))
@section('content_description', trans('admin/layout.edit'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li class="active">{{ trans('admin/pages/recipes.recipe_review') }}</li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/recipes.jury_opinion') }} <a target="_blank" href="{{ route("recipe_view", $recipe->slug)}}" class="btn btn-primary">View recipe</a></h3>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                            <th>Member</th>
                            <th>His decision</th>
                        </tr>
                        @foreach ($votes AS $vote)
                        <tr>
                            <td>{{ $vote->jury->user->name }}</td>
                            <td>
                                @if($vote->vote == 1)
                                <div class="label label-success">Yes</div>
                                @else
                                <div class="label label-danger">No</div>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                Do you approve this recipe? <a href="{{ route("recipe_approve", ["id" => $recipe->id, "status" => 1])}}" class="btn btn-success">Yes</a> <a href="{{ route("recipe_approve", ["id" => $recipe->id, "status" => 0])}}" class="btn btn-danger">No</a>   
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/recipes.watermark_images') }}</h3>
            </div><!-- /.box-header -->
            <form action="{{ route('recipe_watermark', $recipe->id )}}" method="POST">
                {!! csrf_field() !!}
                <div class="box-body">
                    <div class="form-group">
                        <label>Main Image</label><br />
                        <center><img src="@if(empty($recipe->main_image_watermarked)) {{ asset('storage/recipes/original/'.$recipe->main_image_original) }} @else {{ asset('storage/recipes/watermark/'.$recipe->main_image_watermarked) }} @endif" width="60%"/></center>
                        <label>Watermark</label><br />
                        <select name="main_image" style="width: 100%;">
                            <option value="">--- If you do not choose anything, you will not make changes. ---</option>
                            <option value="light">Light</option>
                            <option value="dark">Dark</option>
                        </select>
                    </div>
                </div>

                @if(!isset(json_decode($recipe->components, true)['html']))
                @foreach (json_decode($recipe->components, true) AS $key => $component)
                
                    @if(empty(trim($component['image'])))
                    <?php continue; ?>
                    @endif
                    <div class="box-body">
                        <div class="form-group">
                            <label>Component: {{ $component['name'] }}</label><br />
                            <center><img src="{{ asset('storage/recipes/'.(array_key_exists("image_watermarked", $component) ? 'watermark/'.$component['image_watermarked'] : 'original/'.$component['image'])) }}" width="60%"/></center>
                            <label>Watermark</label><br />
                            <select name="image_component[{{$key}}]" style="width: 100%;">
                                <option value="">--- If you do not choose anything, you will not make changes. ---</option>
                                <option value="light">Light</option>
                                <option value="dark">Dark</option>
                            </select>
                        </div>
                    </div>
                
                @endforeach
                @endif
                <div class="box-footer">
                    <button class="btn btn-primary" type="submit">Save</button>
                </div>
            </form>
        </div>
    </div>
</div>

</div><!-- /.col -->
</div>
@endsection
