@extends('admin_layout.layout')

@section('title', trans('admin/pages/featured_recipes.page_title'))

@section('content_title', trans('admin/pages/featured_recipes.content_title'))
@section('content_description', trans('admin/pages/featured_recipes.page_description'))

@section('breadcrumb')
<li><a href="{{ route("admin_homepage") }}"><i class="fa fa-home"></i> {{ trans('admin/layout.home') }}</a></li>
<li><a href="{{ route('news_listing') }}">{{ trans('admin/layout.menu.news.header') }}</a></li>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        @if(session('success'))
            <div class="alert alert-success alert-dismissable">
                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                <h4><i class="icon fa fa-check"></i> {{ trans('admin/pages/add_news.messages.success_title') }}</h4>
                {{ session('success') }}<br>
            </div>
        @endif
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/featured_recipes.featured_recipes') }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>{{ trans('admin/pages/featured_recipes.title') }}</th>
                            <th>{{ trans('admin/pages/featured_recipes.actions') }}</th>
                        </tr>
                        @foreach($featured as $f)
                        <tr>
                            <td><a href="{{ route('recipe_view', $f->recipe->slug) }}" target="_blank">{{ $f->recipe->title }}</a></td>
                            <td>
                                <form method="post" action="{{ route('delete_featured_recipe', $f->recipe->id) }}" style="display: inline;">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn btn-danger"><i class="fa fa-times"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
        
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">{{ trans('admin/pages/featured_recipes.all_recipes') }}</h3>
            </div><!-- /.box-header -->
            <div class="box-body no-padding">
                <table class="table table-striped">
                    <tbody>
                        <tr>
                            <th>{{ trans('admin/pages/featured_recipes.title') }}</th>
                            <th>{{ trans('admin/pages/featured_recipes.actions') }}</th>
                        </tr>
                        @foreach($recipes as $recipe)
                        <tr>
                            <td><a href="{{ route('recipe_view', $recipe->slug) }}" target="_blank">{{ $recipe->title }}</a></td>
                            <td>
                                <form method="post" action="{{ route('add_featured_recipe', $recipe->id) }}" style="display: inline;">
                                    {!! csrf_field() !!}
                                    <button type="submit" class="btn btn-success"><i class="fa fa-star"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {!! $recipes->render() !!}
            </div><!-- /.box-footer -->
        </div><!-- /.box -->
    </div><!-- /.col -->
</div>
@endsection