<h2>Hello <i>{{ $name }}</i>,</h2>
<p>Welcome to the NEW Foodstarz - your worldwide interactive chef network!</p>
<p>We're happy to inform you about the release of some nice new features on our website. Don't hesitate, just check them out :)</p>

<hr/>

<h2>Login Credentials:</h2>
<p>Since you're already a Foodstarz team member, we've automatically created an account for you. In order to use it, please follow this link:<br>
    <b>Login Page:</b> <a href="{{ route('login') }}">{{ route('login') }}</a><br>
    <b>E-Mail:</b> <i>{{ $email }}</i><br>
    <b>Password:</b> <i>{{ $password }}</i>
</p>

<hr/>

<h2>Additional information:</h2>
<p>After you log into your account, you will will be asked to complete your account information by fulfilling the following steps:</p>
<p>Core Data<br>Upload Avatar<br>Chef Data<br>Interview</p>
<p>{{ trans('emails/register.additional_information_text2') }}</p>

<hr/>

<h2>Help us improving our service:</h2>
<p>The NEW Foodstarz is currently in BETA phase what means that it likely contains a number of small bugs. In order to report bugs and for any questions or suggestions please contact feedback@foodstarz.com.</p>
<p>Thanks for your support!</p>

<hr/>

<p><b>Cheers!<br>David (CEO & Founder)</b></p>