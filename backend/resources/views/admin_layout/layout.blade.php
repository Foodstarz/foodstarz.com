<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>@yield('title')</title>

        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="{{ asset('assets/adminlte/bootstrap/css/bootstrap.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/ionicons.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/adminlte/dist/css/AdminLTE.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/adminlte/dist/css/skins/skin-blue.min.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/adminlte/plugins/select2/select2.min.css') }}">
        @yield('styles')
        <link rel="stylesheet" href="{{ asset('assets/adminlte/css/styles.css') }}">
        <!--[if lt IE 9]>
            <script src="{{ asset('assets/js/html5shiv.min.js') }}"></script>
            <script src="{{ asset('assets/js/respond.min.js') }}"></script>
        <![endif]-->
        <script src="{{ asset('assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
    </head>

    <body class="skin-blue sidebar-mini">
        <div class="wrapper">

            <!-- Main Header -->
            <header class="main-header">
                <a href="{{ route('admin_homepage') }}" class="logo">
                    <span class="logo-mini">{{ trans('admin/layout.logo_name_short') }}</span>
                    <span class="logo-lg">{{ trans('admin/layout.logo_name') }}</span>
                </a>

                <!-- Header Navbar -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">{{ trans('admin/layout.toggle_navigation') }}</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            {{--
                            <!-- Tasks Menu -->
                            <li class="dropdown tasks-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="fa fa-flag-o"></i>
                                    <span class="label label-danger">9</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="header">You have 9 tasks</li>
                                    <li>
                                        <!-- Inner menu: contains the tasks -->
                                        <ul class="menu">
                                            <li><!-- Task item -->
                                                <a href="#">
                                                    <!-- Task title and progress text -->
                                                    <h3>
                                                        Design some buttons
                                                        <small class="pull-right">20%</small>
                                                    </h3>
                                                    <!-- The progress bar -->
                                                    <div class="progress xs">
                                                        <!-- Change the css width attribute to simulate progress -->
                                                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">20% Complete</span>
                                                        </div>
                                                    </div>
                                                </a>
                                            </li><!-- end task item -->
                                        </ul>
                                    </li>
                                    <li class="footer">
                                        <a href="#">View all tasks</a>
                                    </li>
                                </ul>
                            </li>
                            --}}

                            <!-- User Account Menu -->
                            <li class="dropdown user user-menu">
                                <!-- Menu Toggle Button -->
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    @if(empty(Auth::user()->profile->avatar))
                                    <img src="{{ asset('assets/images/no-avatar.jpg') }}" class="user-image" alt="{{ trans('admin/layout.no_avatar') }}">
                                    @else
                                    <img src="{{ asset('storage/avatars/'.Auth::user()->profile->avatar) }}" class="user-image" alt="{{ trans('admin/layout.user_image') }}">
                                    @endif
                                    <span class="hidden-xs">{{ Auth::user()->name }}</span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="user-header">
                                        @if(empty(Auth::user()->profile->avatar))
                                        <img src="{{ asset('assets/images/no-avatar.jpg') }}" class="img-circle" alt="{{ trans('admin/layout.no_avatar') }}">
                                        @else
                                        <img src="{{ asset('storage/avatars/'.Auth::user()->profile->avatar) }}" class="img-circle" alt="{{ trans('admin/layout.user_image') }}">
                                        @endif

                                        <p>
                                            {{ Auth::user()->name }}
                                        </p>
                                    </li>

                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a href="{{ route('view_user_profile', ['slug' => Auth::user()->profile->slug]) }}" class="btn btn-default btn-flat">{{ trans('admin/layout.profile') }}</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="{{ route('logout') }}" class="btn btn-default btn-flat">{{ trans('admin/layout.logout') }}</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">

                    <!-- Sidebar user panel (optional) -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            @if(empty(Auth::user()->profile->avatar))
                            <img src="{{ asset('assets/images/no-avatar.jpg') }}" class="img-circle" alt="{{ trans('admin/layout.no_avatar') }}">
                            @else
                            <img src="{{ asset('storage/avatars/'.Auth::user()->profile->avatar) }}" class="img-circle" alt="{{ trans('admin/layout.user_image') }}">
                            @endif
                        </div>
                        <div class="pull-left info">
                            <p>{{ Auth::user()->name }}</p>
                        </div>
                    </div>

                    <!-- Sidebar Menu -->
                    <ul class="sidebar-menu">
                        <li class="header">{{ trans('admin/layout.menu.recipes.header') }}</li>
                        <li><a href="{{ route("recipes_listing") }}"><i class="fa fa-book"></i> <span>{{ trans('admin/layout.menu.recipes.header') }}</span></a></li>
                        <li><a href="{{ route('recipes_categories_listing') }}"><i class="fa fa-folder-open-o"></i> <span>{{ trans('admin/layout.menu.recipes_categories.header') }}</span></a></li>
                        <li><a href="{{ route('featured_recipes') }}"><i class="fa fa-star"></i> <span>{{ trans('admin/layout.menu.recipes.items.featured') }}</span></a></li>

                        <li class="header">{{ trans('admin/layout.menu.overview.header') }}</li>
                        <li><a href="{{ route("overview_main") }}"><i class="fa fa-bar-chart"></i> <span>{{ trans('admin/layout.menu.overview.items.main') }}</span></a></li>
                        <li><a href="{{ route("users_listing") }}"><i class="fa fa-user"></i> <span>{{ trans('admin/layout.menu.overview.items.users') }}</span></a></li>
                        <li><a href="{{ route("users_groups") }}"><i class="fa fa-users"></i> <span>{{ trans('admin/layout.menu.overview.items.users_groups') }}</span></a></li>
                        <li><a href="{{ route("overview_countries") }}"><i class="fa fa-globe"></i> <span>{{ trans('admin/layout.menu.overview.items.countries') }}</span></a></li>
                        <li><a href="{{ route("overview_compare") }}"><i class="fa fa-compress"></i> <span>{{ trans('admin/layout.menu.overview.items.compare') }}</span></a></li>

                        <li class="header">{{ trans('admin/layout.menu.features.header') }}</li>
                        <li><a href="{{ route("features_main") }}"><i class="fa fa-cog"></i> <span>{{ trans('admin/layout.menu.features.items.main') }}</span></a></li>

                        <li class="header">{{ trans('admin/layout.menu.main.header') }}</li>
                        <li><a href="{{ route("c_pages_listing") }}"><i class="fa fa-edit"></i> <span>{{ trans('admin/layout.menu.main.items.pages') }}</span></a></li>
                        {{--<li><a href="{{ route("users_listing") }}"><i class="fa fa-users"></i> <span>{{ trans('admin/layout.menu.main.items.users') }}</span></a></li>--}}
                        <li><a href="{{ route("partners_listing") }}"><i class="fa fa-cog"></i> <span>{{ trans('admin/layout.menu.main.items.partners') }}</span></a></li>
                    
                        <li class="header">{{ trans('admin/layout.menu.news.header') }}</li>
                        <li><a href="{{ route('news_listing') }}"><i class="fa fa-folder-open-o"></i> <span>{{ trans('admin/layout.menu.news.items.listing') }}</span></a></li>
                        <li><a href="{{ route('add_news') }}"><i class="fa fa-envelope"></i> <span>{{ trans('admin/layout.menu.news.items.add_news') }}</span></a></li>
                    
                        <li class="header">{{ trans('admin/layout.menu.menus.header') }}</li>
                        <li><a href="{{ route('menus_listing', 1) }}"><i class="fa fa-angle-double-up"></i> <span>{{ trans('admin/layout.menu.menus.items.header') }}</span></a></li>
                        <li><a href="{{ route('menus_listing', 2) }}"><i class="fa  fa-angle-double-down"></i> <span>{{ trans('admin/layout.menu.menus.items.footer') }}</span></a></li>

                        <li class="header">{{ trans('admin/layout.menu.interview.header') }}</li>
                        <li><a href="{{ route('interview_questions_listing') }}"><i class="fa fa-folder-open-o"></i> <span>{{ trans('admin/layout.menu.interview.items.listing') }}</span></a></li>
                        <li><a href="{{ route('interview_questions_add') }}"><i class="fa fa-envelope"></i> <span>{{ trans('admin/layout.menu.interview.items.add_questions') }}</span></a></li>

                        <li class="header">{{ trans('admin/layout.menu.newsletter.header') }}</li>
                        <li><a href="{{ route('newsletter_export') }}"><i class="fa fa-folder-open-o"></i> <span>{{ trans('admin/layout.menu.newsletter.items.newsletter_export') }}</span></a></li>
                        <li><a href="{{ route('newsletter_change') }}"><i class="fa fa-envelope"></i> <span>{{ trans('admin/layout.menu.newsletter.items.newsletter_change') }}</span></a></li>

                        <li class="header">{{ trans('admin/layout.menu.bookmarks.header') }}</li>
                        <li><a href="{{ route('view_bookmarks', ['type' => '1']) }}"><i class="fa fa-folder-open-o"></i> <span>{{ trans('admin/layout.menu.bookmarks.items.recipes') }}</span></a></li>
                        <li><a href="{{ route('view_bookmarks', ['type' => '2']) }}"><i class="fa fa-folder-open-o"></i> <span>{{ trans('admin/layout.menu.bookmarks.items.images') }}</span></a></li>
                        <li><a href="{{ route('view_bookmarks', ['type' => '3']) }}"><i class="fa fa-folder-open-o"></i> <span>{{ trans('admin/layout.menu.bookmarks.items.videos') }}</span></a></li>

                    </ul><!-- /.sidebar-menu -->
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        @yield('content_title')
                        <small>@yield('content_description')</small>
                    </h1>
                    <ol class="breadcrumb">
                        @yield('breadcrumb')
                    </ol>
                </section>

                <!-- Main content -->
                <section class="content">
                    @yield('content')
                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            <!-- Main Footer -->
            <footer class="main-footer">
                <!-- To the right -->
                <div class="pull-right hidden-xs">
                    Anything you want
                </div>
                <!-- Default to the left -->
                <strong>{{ trans('admin/layout.copyright') }} &copy; {{ date('Y') }} <a href="{{ route('homepage') }}">{{ trans('admin/layout.foodstarz') }}</a>.</strong> {{ trans('admin/layout.rights') }}
            </footer>

            <div class="control-sidebar-bg"></div>
        </div><!-- ./wrapper -->

        <script src="{{ asset('assets/adminlte/bootstrap/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('assets/adminlte/dist/js/app.min.js') }}"></script>
        <script src="{{ asset('assets/adminlte/plugins/select2/select2.full.min.js') }}"></script>
        <script src="{{ asset('assets/js/custom/add_recipe.js') }}"></script>
        <script src="{{ asset('assets/js/jquery.cropit.js') }}"></script>
        <script src="https://cdn.ckeditor.com/4.4.3/standard/ckeditor.js"></script>
        @yield('javascript')
        <script>
            $(function () {
               $(".select2").select2(); 
                CKEDITOR.replace('editor1');
            });
        </script>
    </body>
</html>
