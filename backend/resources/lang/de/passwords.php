<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Das Passwort muss mindestens 6 Zeichen lang sein.',
    'user' => "Wir können keinen User mit dieser E-Mail Adresse finden.",
    'token' => 'Diese  Zugriffsinformation ist ungültig.',
    'sent' => 'Wir haben dir einen E-Mail Reset Link per Mail geschickt!',
    'reset' => 'Dein Passwort wurde zurück gesetzt!',

];
