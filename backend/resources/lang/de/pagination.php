<?php

return [

    'first' => 'Erste Seite',
    'prev' => 'Vorherige',
    'next'     => 'Weiter',
    'last' => 'Letzte Seite'

];