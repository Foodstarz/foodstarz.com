<?php

return [
    'subject' => 'Änderung deiner E-Mail Adresse', //The subject of the message

    'hello' => 'Hello', //Ex. Dear David; Hello David
    'info' => 'You have successfully changed your E-Mail address!',
    'activate' => 'In order to use your account, you have to activate it by following this link:',
    
    'bottom_line' => 'Cheers!',   //Best regards,
    'from' => 'David (CEO & Founder)'      //The Foodstarz Team
];