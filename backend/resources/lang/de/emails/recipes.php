<?php

return [
    "new_recipe" => "Neues Rezept",
    "intro" => "In your website was published a new recipe.",
    "title" => "Recipe title",
    'author' => 'Author',
    "brief" => "You can approve or vote for this recipe from your website!"
];
