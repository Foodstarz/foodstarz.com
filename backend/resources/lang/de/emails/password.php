<?php
return [
    'subject' => 'Passwort zurücksetzen', //The subject of the message

    'heading' => 'Forgotten Password',
    'explanation' => 'You receive this message because someone (probably you) has requested a password reset link. If you haven\'t made such request, please ignore this email.',

    'instructions_heading' => 'Instructions',
    'instructions' => 'In order to reset your password you need to visit the following link:',

    'bottom_line' => 'Best regards,',   //Best regards,
    'from' => 'The Foodstarz Team'      //The Foodstarz Team
];