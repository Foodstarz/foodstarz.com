<?php

return [
    'subject' => 'Benachrichtigung über fehlende Rezepte',
    'hello' => 'Hallo',
    'explanation' => 'du erhälst diese E-Mail, da du bisher noch kein Rezept auf Foodstarz hinterlegt hast und dadurch dein Account noch nicht aktiviert wurde.',
    'how_to_add_heading' => 'Benötigst du Hilfe?',
    'how_to_add_text1' => 'Um ein Rezept anzulegen gehe zu',
    'add_recipe_page' => 'Add Recipe Page',
    'how_to_add_text2' => 'und folge den Anweisungen.',
    'bottom_line' => 'Danke und viele Grüße!',   //Best regards,
    'from' => 'David (CEO & Founder)'      //The Foodstarz Team
];