<?php

return [
    "ticket_intro" => "You are receiving this email because someone sent an email through the contact form.",
    "name" => "Name",
    "subject" => "Subject",
    "message" => "Message",
    "email" => "Email"
];
