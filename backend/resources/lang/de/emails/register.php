<?php

return [
    'subject' => 'Herzlich willkommen bei Foodstarz!', //The subject of the message

    'hello' => 'Hello', //Ex. Dear David; Hello David
    'welcome' => 'Welcome to the NEW Foodstarz - your worldwide interactive chef network!',
    'activate' => 'In order to use your account, you have to activate it by following this link:',

    'additional_information_heading' => 'Additional Information',
    'additional_information_text' => 'After you log into your account, you will need to complete your account information by fulfilling the following steps:',
    'additional_information_steps' => 'Core Data<br>Upload Avatar<br>Chef Data<br>Interview',
    'additional_information_text2' => 'After that you\'ll be able to set up new recipes in order to show your talent to the world.',
    
    'help_heading' => 'Help us improving our service',
    'help_text' => 'The NEW Foodstarz is currently in BETA phase what means that it likely contains a number of small bugs. In order to report bugs and for any questions or suggestions please contact feedback@foodstarz.com.',
    'help_thanks' => 'Thanks for your support!',
    
    'bottom_line' => 'Cheers!',   //Best regards,
    'from' => 'David (CEO & Founder)'      //The Foodstarz Team
];