<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted'             => 'Bitte Feld akzeptieren.',
    'active_url'           => 'Es handelt sich um eine ungültige URL.',
    'after'                => 'Es handelt sich um ein Datum.',
    'alpha'                => 'Es dürfen nur Buchstaben verwendet werden.',
    'alpha_dash'           => 'Es dürfen nur Buchstaben, Zahlen und Querstriche verwendet werden.',
    'alpha_num'            => 'Es dürfen nur Buchstaben, Zahlen verwendet werden.',
    'array'                => 'Es handelt sich um ein Datenfeld.',
    'before'               => 'Es handelt sich um ein Datum.',
    'between'              => [
        'numeric' => ':attribute muss zwischen :min und :max liegen.',
        'file'    => ':attribute muss zwischen :min und :max Kilobytes liegen.',
        'string'  => ':attribute muss zwischen :min und :max Buchstaben liegen.',
        'array'   => ':attribute muss zwischen :min und :max Elemente haben.',
    ],
    'boolean'              => ':attribute muss wahr oder unwahr sein.',
    'confirmed'            => ':attribute passt nicht.',
    'date'                 => ':attribute ist kein gültiges Datum.',
    'date_format'          => ':attribute passt nicht in das Format :format.',
    'different'            => ':attribute und :other müssen verschieden sein.',
    'digits'               => ':attribute muss mindestens :digits Zahlen haben.',
    'digits_between'       => '::attribute muss zwischen :min und :max Zahlen haben.',
    'email'                => ':attribute muss eine gültige E-mail Adresse sein.',
    'filled'               => ':attribute wird benötigt.',
    'exists'               => ':attribute ist ungültig.',
    'image'                => ':attribute muss ein Bild sein.',
    'in'                   => ':attribute ist ungültig.',
    'integer'              => ':attribute muss ein Integer sein.',
    'ip'                   => ':attribute muss eine gültige IP Adresse sein.',
    'max'                  => [
        'numeric' => ':attribute darf nicht größer als :max sein.',
        'file'    => ':attribute darf nicht mehr als :max Kilobytes groß sein.',
        'string'  => ':attribute darf nicht mehr als :max Buchstaben haben.',
        'array'   => ':attribute mdarf nicht mahr als :max Elemente haben.',
    ],
    'mimes'                => ':attribute muss den Dateityp :values haben.',
    'min'                  => [
        'numeric' => ':attribute muss mindestens :min sein.',
        'file'    => ':attribute muss mindestens :min Kilobytes groß sein.',
        'string'  => ':attribute muss mindestens :min Buchstaben haben.',
        'array'   => ':attribute muss mindestens :min Elemente haben.',
    ],
    'not_in'               => ':attribute ist ungültig.',
    'numeric'              => ':attribute muss eine Nummer sein.',
    'regex'                => ':attribute Format ist ungültig.',
    'required'             => ':attribute Feld wird benötigt.',
    'required_if'          => ':attribute Feld wird benötigt wenn :other gleich :value ist.',
    'required_with'        => ':attribute Feld wird benötigt wenn :values vorhanden ist.',
    'required_with_all'    => ':attribute Feld wird benötigt wenn :values vorhanden ist.',
    'required_without'     => ':attribute Feld wird benötigt wenn :values nicht vorhanden ist.',
    'required_without_all' => ':attribute Feld wird benötigt wenn :values nicht vorhanden ist.',
    'same'                 => ':attribute and :other must match.',
    'size'                 => [
        'numeric' => ':attribute muss :size sein.',
        'file'    => ':attribute muss :size Kilobytes groß sein.',
        'string'  => ':attribute muss :size Buchstaben haben.',
        'array'   => ':attribute muss :size Elemente groß sein.',
    ],
    'string'               => ':attribute muss ein String sein.',
    'timezone'             => ':attribute muss einen gültigen Bereich haben.',
    'unique'               => ':attribute wurde bereits übernommen.',
    'url'                  => ':attribute Format ist ungültig.',

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => [],

    'array_values' => 'Das Datenfeld beinhaltet nicht erlaubte Werte.',
    'passcheck' => 'Die Passwörter sind nicht identisch.'

];
