<?php
return [
    'page_title' => 'Rezept ansehen',
    'method' => 'Methode',

    'difficulty' => [
        'easy' => 'Leicht',
        'medium' => 'Mittel',
        'hard' => 'Schwer'
    ]
];