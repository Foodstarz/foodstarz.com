<?php

return [
    'page_title' => 'Passwort ändern',
    'content_title' => 'Passwort ändern',

    'form' => [
        'old_password' => [
            'label' => 'Aktuelles Passwort:',
            'placeholder' => 'Aktuelles Passwort eingeben...'
        ],
        
        'new_password' => [
            'label' => 'Neues Passwort:',
            'placeholder' => 'Neues Passwort eingeben...'
        ],
        
        'new_password_confirm' => [
            'label' => 'Neues Passwort bestätigen:',
            'placeholder' => 'Neues Passwort noch einmal eingeben...'
        ],
        'submit' => [
            'label' => 'Passwort ändern!'
        ]
    ],
    
    'messages' => [
        'success' => 'Du hast dein Passwort erfolgreich geändert!'
    ]
];