<?php
return [
    'page_title' => 'Sprache ändern',
    'content_title' => 'Sprache ändern',

    'form' => [
        'language' => [
            'label' => 'Sprache:',
        ],

        'submit' => [
            'label' => 'Sprache ändern!'
        ]
    ],

    'messages' => [
        'success' => 'Du hast die Sprache erfolgreich geändert!'
    ],

    'auto_detect' => 'Automatische Spracherkennung'
];