<?php
return [
    'page_title' => 'Stammdaten',
    'content_title' => 'Stammdaten',

    'form' => [
        'name' => [
            'label' => 'Vor- und Nachname:',
            'placeholder' => 'Bitte gib deinen Vor- und Nachnamen ein...'
        ],
        'email' => [
            'label' => 'E-Mail Adresse:',
            'placeholder' => 'Bitte gib deine E-Mail Adresse ein...'
        ],
        'gender' => [
            'label' => 'Geschlecht:',
            'choices' => [
                'male' => 'männlich',
                'female' => 'weiblich'
            ]
        ],
        'country' => [
            'label' => 'Land:',
            'placeholder' => 'Land auswählen...'
        ],
        'city' => [
            'label' => 'Wohnort:',
            'placeholder' => 'Bitte gib deinen Wohnort und das Bundesland ein...'
        ],
        'culinary_profession' => [
            'label' => 'Kulinarischer Status:',

            'amateur' => [
                'label' => 'Amateur Koch'
            ],
            'blogger' => [
                'label' => 'Food Blogger'
            ],
            'professional' => [
                'label' => 'Professioneller Koch'
            ]
        ],
        
        'submit' => [
            'label' => 'Stammdaten updaten!'
        ]
    ],

    'messages' => [
        'success' => 'Du hast deine Stammdaten erfolgreich abgeschlossen.',
        'proceed' => 'Nun musst du dein',
        'proceed_to' => 'Profilbild hochladen'
    ]
];