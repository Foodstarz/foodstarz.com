<?php

return [
    'page_title' => 'Einstellungen',
    'content_title' => 'Einstellungen',

    'actions' => [
        'edit_profile' => 'Profil bearbeiten',
        'change_email' => 'E-Mail Adresse ändern',
        'change_password' => 'Passwort ändern',
        'change_background' => 'Hintergrund Ändern',
        'change_language' => 'Sprache ändern',
        'close_account' => 'Mitgliedschaft beenden'
    ]
];