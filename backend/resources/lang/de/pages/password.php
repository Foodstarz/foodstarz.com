<?php
return [
    'page_title' => 'Passwort vergessen',
    'content_title' => 'Passwort vergessen',

    'form' => [
        'email' => [
            'label' => 'E-Mail:',
            'placeholder' => 'Bitte gib deine E-Mail Adresse ein...'
        ],
        'submit' => [
            'label' => 'Passwort zurücksetzen!'
        ]
    ],

    'msg' => [
        'already_sent' => 'Der Reset Link wurde dir bereits zugesendet.',
        'success' => 'Der Reset Link wurde dir erfolgreich zugesendet.'
    ]
];