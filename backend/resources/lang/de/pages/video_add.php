<?php
return [
    'page_title' => 'Add Video',
    'content_title' => 'Add Video',

    'form' => [

      'video' => [
        'label' => 'Video',
        'current' => 'Current Video',
        'replace' => 'Please select the new video file.'
      ],

        'title' => [
            'label' => 'Title',
            'placeholder' => 'Please enter a title for the uploaded video.'
        ],

        'description' => [
            'label' => 'Description',
            'placeholder' => 'Please enter a description for the uploaded video.'
        ],

        'social' => [
            'label' => 'Automatic Social Share',
            'facebook' => 'Facebook',

            'configure' => 'Click here to configure...'
        ],

        'submit' => [
            'label' => 'Send'
        ],
    ],

    'messages' => [
        'success' => 'You have successfully added the video!',
    ],

  'accepted_file_formats' => 'Accepted file formats - .mp4, .x-m4v',
  'share_text' => 'Check out this awesome dish picture shot by :name!'
];