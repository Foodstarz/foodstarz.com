<?php
return [
    'page_title' => 'Bild bearbeiten',
    'content_title' => 'Bild bearbeiten',

    'form' => [
        'caption' => [
            'label' => 'Beschreibung',
            'placeholder' => 'Bitte eine Beschreibung für das Bild hinzufügen.'
        ],

        'image' => [
            'label' => 'Bild',
            'current' => 'Aktuelles Bild',
            'replace' => 'Wenn du das aktuelle Bild austauschen möchtest, wähle ein neues aus. Ansonsten lass alles so, wie es ist.'
        ],

        'social' => [
            'label' => 'Automatic Social Share',
            'facebook' => 'Facebook',

            'configure' => 'Klicke hier um deinen Account zu configurieren...'
        ],

        'submit' => [
            'label' => 'Senden'
        ],
    ],

    'messages' => [
        'success' => 'Du hast das Bild erfolgreich bearbeitet!',
        'errors' => [
            'invalid_image' => 'Beim Bilder-Upload ist ein unerwarteter Fehler aufgetreten. Bitte versuche es noch einmal.'
        ]
    ],

    'accepted_file_formats' => 'Erlaubte Dateiformate: .jpg, .png',
    'share_text' => 'Check out this awesome dish picture shot by :name!'
];