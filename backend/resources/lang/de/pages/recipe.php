<?php

return [
    "categories" => "Kategorien",
    "no_recipes" => "Keine Rezepte vorhanden!",
    "recipes" => "Rezepte",
    "approved" => "Genehmigt",
    "in-progress" => "In Arbeit",
    "waiting" => "Warten"
];
