<?php
return [
    'page_title' => 'Nutzer Profil',

    'menu' => [
        'chronic' => 'Timeline',
        'profile' => 'Profil',
        'interview' => 'Interview',
        'gallery' => 'Bilder',
        'videos' => 'Videos',
        'add_recipe' => 'Rezept hinzufügen',
        'add_image' => 'Bild hinzufügen',
        'add_video' => 'Video hinzufügen',
        'edit' => 'Bearbeiten',
        'settings' => 'Einstellungen'
    ],

    'work_experience' => [
        'title' => 'Berufserfahrung',
        'still_working' => 'Bis heute'
    ],

    'awards' => [
        'title' => 'Auszeichnung(en)',
        'no_awards' => 'Keine Auszeichnung(en).'
    ],

    'organisations' => [
        'title' => 'Organisation(en)',
        'no_organisations' => 'Keine Organisation(en).'
    ],

    'role_models' => [
        'title' => 'Vorbild(er)',
        'no_role_models' => 'Keine Vorbild(er).'
    ],

    'cooking_style' => [
        'title' => 'Kochstil'
    ],

    'skills' => [
        'kitchen_skills' => 'Küchen Skills',
        'business_skills' => 'Business Skills',
        'personal_skills' => 'Persönliche Skills'
    ],

    'search' => [
        'title' => 'Suche'
    ],

    'offer' => [
        'title' => 'Biete'
    ],

    'social' => [
        'title' => 'Bleib in Kontakt!'
    ],

    'not_ready_heading' => 'Dieses Profil is noch nicht fertig.',
    'not_ready_description' => 'Es wird verfügbar sein, sobald der Nutzer alle erforderlichen Angaben gemacht hat.',
    
    'not_approved_heading' => 'Diese Profil wurde noch nicht freigegeben.',
    'not_approved_description' => 'Es wird öffentlich verfügbar sein, sobald das erste Rezept freigegeben wurde.',

    'activity_interstitial' => 'Hi :name,

du hast dein Foodstarz Chef Profil erfolgreich angelegt.

Um mehr Sichtbarkeit und Relevanz zu bekommen, und um auf unseren Social Media Kanälen gefeaturt zu werden, nutze bitte Foodstarz PLUS, den blauen Button, den du auf jeder Seite findest, und mit dem du Rezepte anlegen und Bilder hochladen kannst.

Danke!',

];