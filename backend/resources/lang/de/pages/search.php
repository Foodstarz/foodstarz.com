<?php

return [
    'page_title' => 'Suche',
    "search_in" => "Suche in",
    "no_results" => "Keine Ergebnisse gefunden!"
];