<?php
return [
    'page_title' => 'Login',
    'content_title' => 'Login',

    'form' => [
        'email' => [
            'label' => 'E-Mail:',
            'placeholder' => 'Bitte gib deine E-Mail Adresse ein...'
        ],
        'password' => [
            'label' => 'Passwort:',
            'placeholder' => 'Bitte gib dein Passwort ein...'
        ],
        'remember' => [
            'label' => 'Remember me'
        ],
        'submit' => [
            'label' => 'Anmelden!'
        ]
    ],

    'forgotten_password' => 'Passwort vergessen?',

    'messages' => [
        'not_activated' => 'Dieser Account ist noch nicht aktiviert.',
    ]
];