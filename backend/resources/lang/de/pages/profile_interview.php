<?php

return [
    'page_title' => 'Interview',
    'content_title' => 'Interview',

    'required' => '(erforderlich)',
    'form' => [
        'submit' => [
            'label' => 'Interview speichern!'
        ]
    ],
    'base_profile_data_error' => 'Du kannst das Interview nicht ausfüllen, solange deine Stammdaten noch nicht vollständig hinterlegt sind.',
    'upload_avatar_error' => 'Du kannst das Interview nicht ausfüllen, solange du noch kein Profilbild hochgeladen hast.',
    'chef_data_error' => 'Du kannst das Interview nicht ausfüllen, solange die Daten deines Chef Profils noch nicht vollständig hinterlegt sind.',

    'errors' => [
        'required' => 'Bitte beantworte jede Frage.'
    ],

    'success_just_activated' => 'Du hast die Formulare erfolgreich ausgefüllt und ein Chef Profil ist nun aktiv! Damit dein Profil mehr Sichbarkeit und mehr Relevanz bekommt, lege bitte ein Rezept an oder lade Bilder hoch. Nutze dazu Foodstarz PLUS (den PLUS Button, den du auf jeder Seite in der rechten unteren Ecke findest). Die besten rezepte und Bilder werden über unsere Social Media Kanäle geteilt.',
    'success_normal' => 'Du hast das Interview erfolgreich aktualisiert!'

];