<?php

return [
    'messages' => [
        'success' => 'Dein Account wurde erfolgreich aktiviert.Du kannst dich jetzt einloggen.',
        'error' => 'Leider ist ein Fehler aufgetreten! Bitte versuche es noch einmal.'
    ]
];