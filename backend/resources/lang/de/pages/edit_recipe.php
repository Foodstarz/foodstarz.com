<?php

return [
    'page_title' => 'Rezept bearbeiten',
    'content_title' => 'Rezept bearbeiten',
    
    'main_image' => 'Wenn du das aktuelle Rezeptbild durch ein neues ersetzen möchtest, wähle ein neues Bild aus. Wenn du das aktuelle Bild behalten möchtest, ist keine Aktion erforderlich.',
    'component_image' => 'Wenn du das aktuelle Komponentenbild durch ein neues ersetzen möchtest, wähle ein neues Bild aus. Wenn du das aktuelle Bild behalten möchtest, ist keine Aktion erforderlich.',
    
    'messages' => [
        'success' => 'Das Rezept wurde erfolgreich aktualisiert. Die aktualisierte Version des Rezeptes wird von unserem Team überprüft und anschließend freigegeben.'
    ],
    
    'edit' => 'Bearbeiten'
    
    
];