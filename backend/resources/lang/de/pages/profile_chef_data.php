<?php
return [
    'page_title' => 'Chef Profil',
    'content_title' => 'Chef Profil',

    'form' => [
        'optional' => 'Optional',
        'biography' => [
            'label' => 'Biografie:',
            'valid' => '30 characters required',
            'placeholder' => 'Bitte erzähle uns etwas über dich.'
        ],
        'work_experience' => [
            'label' => 'Berufserfahrung:',
            'subfields' => [
                'position' => [
                    'label' => 'Position:'
                ],
                'employer' => [
                    'label' => 'Arbeitgeber:'
                ],
                'period' => [
                    'label' => 'Zeitraum:',
                    'from' => 'Von:',
                    'to' => 'Bis:',
                    'still_working' => 'Bis heute'
                ],
                'job_description' => [
                    'label' => 'Positionsbezeichnung:'
                ],
                'add_button' => [
                    'label' => 'Nächste Position hinzufügen'
                ],
                'remove_button' => [
                    'label' => 'Diese Position löschen'
                ]
            ]
        ],
        'cooking_style' => [
            'label' => 'Kochstil:',
        ],
        'awards' => [
            'label' => 'Auszeichnungen:',
            'placeholder' => 'Bitte gib den Namen der Auszeichnung ein...',
            'add_button' => 'Nächste Auszeichnung hinzufügen',
            'remove_button' => 'Diese Auszeichnung löschen'
        ],
        'organisations' => [
            'label' => 'Organisationen:',
            'add_button' => 'Nächste Organisation hinzufügen',
            'remove_button' => 'Diese Organisation löschen'
        ],
        'role_models' => [
            'label' => 'Vorbild(er):',
            'add_button' => 'Nächstes Vorbild hinzufügen',
            'remove_button' => 'Dieses Vorbild löschen'
        ],
        'kitchen_skills' => [
            'label' => 'Küchen Skills',
            'valid' => 'optional, Max. 6',
            'checkboxes' => [
                'baking' => 'Backen',
                'bread_cake_making' => 'Zubereitung von Brot und Kuchen',
                'cake_pastry_decorating' => 'Kuchen und Gebäck Dekoration',
                'catering' => 'Catering',
                'cheese_course' => 'Käse Gang',
                'chicikate_tempering_dipping' => 'Schokolade temperieren',
                'dough_making' => 'Zubereitung und Portionierung von Teigen',
                'dressings' => 'Dressings',
                'cleaning_sanitation' => 'Umsetzung von Sicherheits- und Hygienestandards',
                'equipment' => 'Instandhaltung der Betriebsausrüstung',
                'fish_cookery' => 'Umgang mit Fisch und Meeresfrüchten',
                'food_pairing' => 'Food / Beverage Pairing',
                'food_budgeting' => 'Budgetierung',
                'food_cost_optimization' => 'Kostenoptimierung',
                'food_preparation' => 'Speisenzubereitung',
                'food_safety_knowledge' => 'Kenntnisse in Lebensmittelsicherheit',
                'food_styling' => 'Food Styling',
                'gelatos_sorbets_french_ice_creams' => 'Gelato / Speiseeis / Sorbet',
                'gluten_free' => 'Gluten frei',
                'ingredient_selection' => 'Auswahl der Zutaten',
                'kitchen_management' => 'Küchen Management',
                'kitchen_tools' => 'Küchen Tools',
                'knife_skills' => 'Messer Skills',
                'kosher_preparation' => 'Koschere Zubereitung',
                'meat_cookery' => 'Umgang mit Fleisch(produkten)',
                'menu_planning' => 'Menü / Rezept Entwicklung',
                'mis_en_place' => 'Mis En place',
                'molecular_gastronomy' => 'Molekular Küche',
                'offsite_event_management' => 'Externes Event Management',
                'onsite_special_events' => 'Hauseigene Special Events',
                'ordering_purchasing' => 'Bestellung, Einkauf, Wareneingang',
                'pasta_making' => 'Pasta Zubereitung',
                'pastry' => 'Feingebäck',
                'pastry_commissary' => 'Feingebäck Beauftragter',
                'pastry_plating' => 'Feingebäck Anrichtung',
                'pastry_production' => 'Feingebäck Produktion',
                'petit_fours' => 'Petit Fours / Mignardises',
                'pizza_cooking' => 'Pizza Zubereitung',
                'portioning' => 'Portionierung',
                'presentation' => 'Presentation',
                'sauces' => 'Saucen Entwicklung',
                'sauteing' => 'Sautieren',
                'scheduling' => 'Terminplanung',
                'show_piece_work' => 'Showstücke / Meisterstücke',
                'sous_vide_technique' => 'Sous Vide Techniken',
                'tasting_seasoning_balance' => 'Abschmecken, Würzen',
                'ticket_management' => 'Ticket Management',
                'time_management' => 'Zeit Management',
                'vegan_vegetarian' => 'Veganes und vegetarsisches Kochen/Backen',
                'grilling' => 'Kochen über Holzfeuer/Grillen',
            ]
        ],
        'business_skills' => [
    'label' => 'Business Skills',
    'valid' => 'optional, Max. 6',
    'checkboxes' => [
        'accounting_bookkeeping' => 'Rechnungswesen / Buchhaltung',
        'business_acumen' => 'Unternehmerische Fähigkeiten',
        'business_management' => 'Business Management',
        'computer_skills' => 'Computerkenntnisse',
        'concepts' => 'Konzeptionierung',
        'cost_control' => 'Kostenkontrolle',
        'customer_service' => 'Kundenservice',
        'expediting' => 'Terminüberwachung',
        'finance_degree' => 'Abschluss in Wirtschaft',
        'food_industry_consulting' => 'Beratung der Lebensmittelindustrie',
        'food_pricing' => 'Budgetierung',
        'food_regulations' => 'Lebensmittelbestimmungen',
        'food_safety' => 'Lebensmittelsicherheit',
        'food_service_management' => 'Verpflegungsmanagement',
        'food_writer_blogger' => 'Food Writer/Blogger',
        'hospitality_management_degree' => 'Hotelbetriebswirt(in)',
        'international_functions_special_events' => 'Internationale Funktionen',
        'inventory_management' => 'Lagerbestand Management',
        'leadership_time_management' => 'Führungs & Zeit-Management',
        'marketing' => 'Marketing / PR',
        'menu_development' => 'Menü Zusammenstelllung / Speisekartenetwicklung',
        'merchandising' => 'Merchandising',
        'operations' => 'Koordination von Arbeitsabläufen',
        'payroll' => 'Lohnabrechnungen',
        'ordering' => 'Einkauf, Bestellung',
        'hiring' => 'Personalbeschaffung / Einstellungsentscheidung',
        'reservation_management' => 'reservierungssystem Management',
        'social_media' => 'Social Media',
        'staff_education' => 'Mitarbeiterschulungen',
        'tv_radio_guest_appearances' => 'TV/Radio Gastauftritte',
        'vendor_management' => 'Lieferantenmanagement',
        'web_maintenance' => 'Überwachung und Pflege von Webseiten',
    ]
],
        'personal_skills' => [
    'label' => 'Personal Skills',
    'valid' => 'optional, Max. 6',
    'checkboxes' => [
        'adaptability' => 'Anpassungsfähigkeit',
        'assertiveness' => 'Durchsetzungevermögen',
        'attention_to_detail' => 'Liebe zum Detail',
        'capacity_for_teamwork' => 'Teamfähigkeit',
        'commitment_to_quality' => 'Qualitätsverpflichtung',
        'communication' => 'Kommunikation',
        'conflict_resolution' => 'konfliktlösend',
        'consistency' => 'Beständigkeit',
        'creativity' => 'Kreativität',
        'customer_oriented' => 'Kundenorientiertheit',
        'diligence' => 'Sorgfalt, Fleiß',
        'discipline' => 'Disziplin',
        'empathy' => 'Empathie',
        'flexibility' => 'Flexibilität',
        'initiative' => 'Eigeninitiative',
        'motivational' => 'motivierend',
        'multitasking' => 'Multitasking-fähig',
        'openness' => 'Aufgeschlossenheit',
        'personal_resilience' => 'hohe persönliche Belastbarkeit',
        'precision' => 'Präzision',
        'problem_solving' => 'Problem-lösend',
        'professional_commitment' => 'berufliches Engagement',
        'responsibility' => 'Zuverlässigkeit',
        'self_management' => 'Selbstorganisation',
        'sense_of_humor' => 'Sinn für Humor',
        'sense_of_urgency' => 'Bewusstsein für Dringlichkeit',
        'souvereignty' => 'Souveränität',
        'speed_and_organization' => 'Geschwindigkeit und Organisation',
        'target_oriented' => 'Zielorientiertheit',
        'time_management' => 'Zeitmanagement',
    ]
],
        'search' => [
    'label' => 'Suche:'
],
        'offer' => [
    'label' => 'Biete:'
],
        'social_media_links' => [
    'label' => 'Social Media Links',
    'valid' => 'ganze Url',
    'website' => [
        'label' => 'Persönliche Website:'
    ],
    'instagram' => [
        'label' => 'Instagram:'
    ],
    'facebook' => [
        'label' => 'Facebook:'
    ],
    'twitter' => [
        'label' => 'Twitter:'
    ],
    'pinterest' => [
        'label' => 'Pinterest:'
    ],
    'tumblr' => [
        'label' => 'Tumblr:'
    ],
],
        'submit' => [
    'label' => 'Chef Profil updaten!'
],
        'months' => [
    'jan' => 'Januar',
    'feb' => 'Februar',
    'mar' => 'März',
    'apr' => 'April',
    'may' => 'Mai',
    'jun' => 'Juni',
    'jul' => 'Juli',
    'aug' => 'August',
    'sep' => 'September',
    'oct' => 'Oktober',
    'nov' => 'November',
    'dec' => 'Dezember'
],

        'year' => 'Jahr',
        'month' => 'Monat'
    ],

    'messages' => [
    'success' => 'Du hast dein Chef Profil erfolgreich abgeschlossen.',
    'proceed' => 'Nun geht es weiter mit dem',
    'proceed_to' => 'Interview',

    'errors' => [
        'work_experience' => 'Bitte fülle alle Felder aus.',
        'awards' => 'Bitte keine Felder leer lassen.',
        'organisations' => 'Organisationen darf nicht leer sein.',
        'role_models' => 'Vorbilder darf nicht leer sein.'
    ]
],

    'base_profile_data_error' => 'Du kannst dein Chef Profil erst ausfüllen, wenn du deine Stammdaten ausgefüllt hast.',
    'upload_avatar_error' => 'Du kannst dein Chef Profil erst ausfüllen, wenn du ein Profilbild hochgeladen hast.'
];