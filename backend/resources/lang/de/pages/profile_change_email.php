<?php

return [
    'page_title' => 'E-Mail Adresse ändern',
    'content_title' => 'E-Mail Adresse ändern',
    
    'warning' => 'Wenn du deine E-Mail Adresse änderst, schicken wir dir eine E-Mail mit einem Bestätigungslink zu deiner neuen E-Mail Adresse. Dein Account wird so lange deaktiviert bis du den Bestätigungslink aktivierst. Danach kannst du dich wie gewohnt auf der Website anmelden.',

    'form' => [
        'email' => [
            'label' => 'E-Mail',
            'placeholder' => 'Bitte gib deine neue E-Mail Adresse ein...'
        ],
        
        'submit' => [
            'label' => 'E-Mail Adresse ändern.'
        ]
    ]
];