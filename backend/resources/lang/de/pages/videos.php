<?php

return [
    'page_title' => 'Videos',
    'content_title' => 'Videos',
    'no_videos' => 'Es wurden keine Videos gefunden.'
];
