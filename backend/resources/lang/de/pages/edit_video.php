<?php
return [
    'page_title' => 'Video bearbeiten',
    'content_title' => 'Video bearbeiten',

    'form' => [
        'title' => [
            'label' => 'Titel',
            'placeholder' => 'Bitte füge einen Titel für das Video hinzu.'
        ],

        'description' => [
            'label' => 'Beschreibung',
            'placeholder' => 'Bitte füge eine Beschreibung für das Video hinzu.'
        ],

        'social' => [
            'label' => 'Automatic Social Share',
            'facebook' => 'Facebook',

            'configure' => 'Klicke hier um deinen Account zu configurieren...'
        ],

        'submit' => [
            'label' => 'Senden'
        ],
    ],

    'messages' => [
        'success' => 'Du hast das Video erfolgreich bearbeitet!',
    ],
];