<?php
return [
    'page_title' => 'Bilder',
    'content_title' => 'Bilder',

    'page_description' => 'Schaue dir die Bildergalerie von :name an.',
    'no_images' => 'Dieser Nutzer hat bisher noch keine Bilder hochgeladen.'
];