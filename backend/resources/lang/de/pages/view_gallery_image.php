<?php
return [
    'page_title' => 'Bilder',
    'content_title' => 'Bilder',
    'edit' => 'Bild bearbeiten',
    'hide' => 'Bild verstecken',
    'unhide' => 'Bild aufdecken',
    'delete' => 'Bild löschen',
    'confirm_deletion' => 'Bist du sicher, dass du das Bild löschen möchtest?',
    'confirm_hide' => 'Bist du sicher, dass du das Bild verstecken möchtest?'
];