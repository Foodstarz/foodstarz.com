<?php
return [
    'page_title' => 'Chronic',
    'content_title' => 'Chronic',

    'page_description' => 'Schaue dir die Chronic von :name an.',
    'no_activity' => 'Von diesem Nutzer gibt es bisher keine Aktivitäten.'
];