<?php
return [
    'page_title' => 'Videos',
    'content_title' => 'Videos',

    'page_description' => 'Sieh dir die Videos von :name an.',
    'no_videos' => 'Dieser Nutzer hat noch keine Videos hochgeladen.'
];