<?php
return [
    'page_title' => 'Video',
    'content_title' => 'Video',
    'edit' => 'Video bearbeiten',
    'hide' => 'Video verstecken',
    'unhide' => 'Video sichtbar machen',
    'delete' => 'Video löschen',
    'confirm_deletion' => 'Bist du sicher, dass du das Video löschen möchtest?',
    'confirm_hide' => 'Bist du sicher, dass du das Video verstecken möchtest?'
];