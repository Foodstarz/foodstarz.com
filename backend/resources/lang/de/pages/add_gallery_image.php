<?php
return [
    'page_title' => 'Neues Bild hinzufügen',
    'content_title' => 'Neues Bild hinzufügen',

    'form' => [
        'caption' => [
            'label' => 'Beschreibung',
            'placeholder' => 'Bitte eine Beschreibung für das Bild hinzufügen.'
        ],

        'image' => [
            'label' => 'Bild'
        ],

        'submit' => [
            'label' => 'Senden'
        ],
    ],

    'messages' => [
        'success' => 'You Du hast erfolgreich ein neues Bild hinzugefügt. In 5 Sekunden wirst du automatisch zu dem Bild weitergeleitet…',
        'errors' => [
            'invalid_image' => 'Beim Bilder-Upload ist ein unerwarteter Fehler aufgetreten. Bitte versuche es noch einmal.'
        ]
    ],

    'accepted_file_formats' => 'Erlaubte Dateiformate: .jpg, .png',
];