<?php

return [
    'page_title' => 'Mitgliedschaft beenden',
    'content_title' => 'Mitgliedschaft beenden',
    
    'warning' => 'Wenn du deine Mitgliedschaft beendest, werden all deine Daten, Rezepte, Bilder und alle anderen Inhalte unwiederruflich gelöscht. Dieser Prozess ist nicht rückgängig zu machen!!!',

    'form' => [
        'agreement' => [
            'label' => 'Möchtest du wirklich all deine Daten und Kontakte verlieren?',
            'yes' => 'JA',
            'no' => 'NEIN'
        ],
        
        'password' => [
            'label' => 'Passwort:',
            'placeholder' => 'Passwort eingeben...'
        ],
        
        'submit' => [
            'label' => 'Mitgliedschaft beenden!'
        ]
    ]
];