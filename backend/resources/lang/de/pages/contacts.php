<?php
return [
    'page_title' => "Kontakt",
    'content_title' => "Kontakt",
    
    'your_name' => "Vor- und Nachname (erforderlich)",
    "your_email" => "E-Mail Adresse (erforderlich)",
    "subject" => "Betreff",
    "your_message" => "Nachricht (erforderlich)",
    "send" => "Senden",
    "success_message" => "Deine Nachricht wurde erfolgreich gesendet."
];