<?php

return [
    'page_title' => 'Bilder',
    'content_title' => 'Bilder',
    'no_images' => 'Es wurden keine Bilder gefunden.'
];
