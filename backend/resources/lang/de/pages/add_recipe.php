<?php
return [
    'page_title' => 'Rezept hinzufügen',
    'content_title' => 'Rezept hinzufügen',

    'form' => [
        'fieldset' => [
            'main_data' => 'Hauptdaten',
            'other_data' => 'Weitere Daten'
        ],
        
        'title' => [
            'label' => 'Rezeptname'
        ],

        'category' => [
            'label' => 'Kategorie(n)'
        ],

        'portions' => [
            'label' => 'Anzahl der Portionen'
        ],

        'preparation_time' => [
            'label' => 'Zubereitungszeit'
        ],

        'difficulty' => [
            'label' => 'Schwierigkeitsgrad',
            'options' => [
                'easy' => 'Leicht',
                'medium' => 'Mittel',
                'hard' => 'Schwer'
            ]
        ],

        'components' => [
            'label' => 'Komponenten',
            'component' => 'Komponente',
            'name' => 'Name',
            'ingredients' => 'Zutaten',
            'ingredient' => 'Zutat',
            'quantity' => 'Menge',
            'select_unit' => 'Einheit',

            'plural' => '{1} .|{2} .|{3} .|[4,Inf] .',

            'units' => [
                'bunch' => 'Bund',
                'can' => 'Dose(n)',
                'clove' => 'Zehe(n)',
                'cup' => 'Tasse(n)',
                'dash' => 'Spritzer',
                'deciliter' => 'Deziliter',
                'drop' => 'Tropfen',
                'fluid_ounce' => 'Flüssigunze(n)',
                'gallon' => 'Gallone(n)',
                'gram' => 'Gramm',
                'kilo' => 'Kilo',
                'liter' => 'Liter',
                'milliliter' => 'Milliliter',
                'ounce' => 'Unze(n)',
                'package' => 'Paket(e)',
                'piece' => 'Stück(e)',
                'pinch' => 'Prise(n)',
                'pint' => 'Pint(e)',
                'pound' => 'Pfund',
                'quart' => 'Quart',
                'shot' => 'Schüsse',
                'slice' => 'Scheibe(n)',
                'sprig' => 'Zweig(e)',
                'stick' => 'Stil(e)',
                'tablespoon' => 'Esslöffel',
                'teaspoon' => 'Teelöffel',
            ],

            'add_ingredient' => 'Zutat hinzufügen',

            'instructions' => 'Anleitung',
            'enter_instruction' => 'Zubereitungsschritt eingeben',
            'add_step' => 'Weiteren Schritt hinzufügen',

            'component_image' => 'Komponentenfoto (optional)',

            'add_component' => 'Weitere Komponenete hinzufügen',
            'remove_component' => 'Komponenete löschen'
        ],

        'attachments' => [
            'label' => 'Rezeptfoto',
            'desc' => 'Bitte nur hochauflösende Fotos verwenden.'
        ],

        'status' => [
            'label' => 'Status',
            'desc' => 'Möchtest du dieses Rezept weiter bearbeiten oder ist es fertig für die Prüfung?',
            'text' => 'Um die hohe Qualität der Website zu wahren, behalten wir uns das Recht vor, Rezepte auf Vollständigkeit und Qualität zu überprüfen. Sobald die Prüfung erfolgreich durchgeführt wurde, wird dein Rezept veröffentlicht.',

            'options' => [
                'not_ready' => 'Ich möchte weiter daran arbeiten',
                'ready' => 'Das Rezept ist fertig für die Prüfung'
            ]
        ],

        'submit' => [
            'label' => 'Abschicken'
        ],
        
        'loading' => 'Wir bearbeiten deine Anfrage...'
    ],

    'messages' => [
        'errors' => [
            'components' => [
                'name' => 'Bitte einen Komponenetennamen eingeben.',
                'no_ingredients' => 'Die Komponenete muss mindestens eine Zutat haben.',
                'no_instructions' => 'Die Komponenete muss mindestens einen Anleitungsschritt beinhalten.',
                'ingredients_empty_fields' => 'Bitte fülle alle Zutatanefelder aus.',
                'instructions_empty_fields' => 'Bitte fülle alle Anleitungsschritte aus.'
            ],
            'images' => [
                'mime_type' => 'Erlaubte Dateiformate: .jpg und .png.',
                'max_size' => 'Die maximale Dateigröße beträgt 5Mb.',
                'no_picture' => 'Ein Rezeptbild wird benötigt.',
                'invalid_image' => 'Ungültiges Bild.'
            ],
            'profile_not_ready' => 'Du kannst noch keine Rezepte anlegen, da du noch nicht alle notwendigen Formulare ausgefüllt hast. Bitte folge den unten stehenden Anweisungen, um Rezepte anlegen zu können.',
        ],
        'success' => 'Vielen Dank! Du hast erfolgreich ein neues Rezept angelegt. Unser Team wird es schnellstmöglich prüfen und im Erfolgsfall freigeben. Ansonsten werden wir mit dir in Kontakt treten.'
    ],
    
    'profile_not_ready' => [
        'instructions' => 'Anleitung',
        'explanation' => 'Um ein Rezept anlegen zu können möchten wir erstmal etwas mehr über dich erfahren. Darum bitten wir dich zuerst deine Stammdaten, deine Profildaten und das Interview zu vervollständigen. Außerdem benötigen wir dein Profilbild.',
        'core_data' => 'Bitte vervollständige zuerst deine',
        'core_data_link' => 'Stammdaten',
        'upload_avatar' => 'Nach dem ausfüllen der Stammdaten, bitte ein',
        'upload_avatar_link' => 'Profilbild hochladen',
        'chef_profile' => 'Danach musst du dein',
        'chef_profile_link' => 'Chef Profil ausfüllen',
        'interview' => 'Nun musst du nur noch das',
        'interview_link' => 'Interview abschließen',
        'note' => 'Wichtig',
        'note_text' => 'Bitte achte darauf, dass du alle Pflichtfelder eines Formulars ausfüllst, damit das jeweils nächste Formular freigeschaltet werden kann.'
    ],

    'accepted_file_formats' => 'Erlaubte Dateiformate: .jpg, .png'
];