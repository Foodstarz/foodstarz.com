<?php
/**
 * Localization for the Register Page
 */

return [
    'page_title' => 'Registrierung',
    'content_title' => 'Registrierung',

    'form' => [
        'name' => [
            'label' => 'Vor- und Nachname:',
            'placeholder' => 'Bitte gib deinen Vor- und Nachnamen ein...'
        ],
        'email' => [
            'label' => 'E-Mail Adresse:',
            'placeholder' => 'Bitte gib deine E-Mail Adresse ein...'
        ],
        'password' => [
            'label' => 'Passwort:',
            'placeholder' => 'Bitte gib dein Passwort ein...'
        ],
        'password_confirmation' => [
            'label' => 'Passwort Bestätigung:',
            'placeholder' => 'Bitte wiederhole das Passwort.'
        ],
        'culinary_profession' => [
            'label' => 'Kulinarischer Status:',

            'amateur' => [
                'label' => 'Amateur Koch'
            ],
            'blogger' => [
                'label' => 'Food Blogger'
            ],
            'professional' => [
                'label' => 'Professional Koch'
            ]
        ],
        'agreement' => [
            'label' => 'Ich bestätige die',
            'second_label' => 'gelesen zu haben, und erkläre mich damit einverstanden.',
            'tos' => 'AGB',
            'privacy' => 'Datenschutzerklärung'
        ],
        'submit' => [
            'label' => 'Registrieren!'
        ]
    ],

    'messages' => [
        'success' => 'Deine Registreirung war erfolgreich. Um deinen Account zu aktivieren, folge bitte dem Bestätigungslink, den wir soeben an die von dir genannte E-Mail Adresse geschickt haben.',
        'error' => 'Leider ist ein Fehler aufgetreten! Bitte versuche es noch einmal.'
    ]
];