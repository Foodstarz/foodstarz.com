<?php
return [
    'page_title' => 'Profilbild hochladen',
    'content_title' => 'Profilbild hochladen',

    'form' => [
        'avatar' => [
            'label' => 'Profilbild:'
        ],
        'submit' => [
            'label' => 'Mein Profilbild hochladen!'
        ]
    ],

    'resize_image' => 'Größe des Bildausschnitts ändern:',

    'base_profile_data_error' => 'Du kannst dein Profilbild erst hochladen, wenn du deine Stammdaten vervollständigt hast.',

    'messages' => [
        'success' => 'Profilbild erfolgreich hochgeladen.',
        'proceed' => 'Du kannst mit dem nächsten Schritt fortfahren',
        'proceed_action' => 'Vervollständige deine Chef Profil Daten',
        'mime_type_error' => 'Bitte wähle eine Datei mit der folgenden Endung: .jpg, .jpeg oder .png',
        'size_error' => 'Die Datei darf die folgende Größe nicht überschreiten: size MB',
        'valid_image_error' => 'Bitte wähle eine gültige Bilddatei aus.',
        'image_dimensions_error' => 'Das Bild muss eine Mindestgröße von 750x750 Pixeln haben.',

        'congratulations' => 'Glückwunsch! Du hast erfolgreich deine Stammdaten hinterlegt und dein Profilbild hochgeladen.',
        'next' => 'Was möchtest du als nächstes machen?',
        'upload_recipes' => 'Rezepte',
        'upload_images' => 'Bilder',
        'or' => 'oder',
        'upload_videos' => 'videos',
        'uploading' => 'hochladen',
        'chef_profile' => 'Chef Profile',
        'interview' => 'Interview',
        'completing' => 'bearbeiten'
    ]
];