<?php

return [
    'page_title' => 'Passwort zurücksetzen',
    'content_title' => 'Passwort zurücksetzen',

    'form' => [
        'email' => [
            'label' => 'E-Mail:',
            'placeholder' => 'Bitte gib deine E-Mail Adresse ein...'
        ],
        'password' => [
            'label' => 'Passwort:',
            'placeholder' => 'Bitte gib dein aktuelles Passwort ein...'
        ],
        'password_confirmation' => [
            'label' => 'Passwort Bestätigung:',
            'placeholder' => 'Passwort wiederholen.'
        ],
        'submit' => [
            'label' => 'Passwort ändern!'
        ]
    ]
];