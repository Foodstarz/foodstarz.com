<?php
return [
    'page_title' => 'Foodstarz | Your International Premium Chef Network!',
    
    'right_menu' => [
        'login' => 'Login',
        'register' => 'Registrierung',
        'logout' => 'Logout',
        'account' => 'Mein Profil'
    ],
    'latest_recipes' => "Gefeaturte Rezepte",
    "serves" => "Portionen",
    "minutes" => "Minuten",
    "read_more" => "Mehr",
    "more_recipes" => "Mehr Rezepte",
    "gold_partners" => "Gold Partner",
    'charity_partners' => 'Freunde und Charity Partner',
    "latest_foodstarz" => "Neuste Foodstarz",
    "more_foodstarz" => "Mehr Foodstarz",
    'latest_images' => 'Gefeaturte Bilder',
    'latest_videos' => 'Featured Videos',
    'more_images' => 'mehr Bilder',
    'more_videos' => 'mehr Videos'
]; 