<?php

return [
    "new_recipe" => "New recipe in Foodstarz.com",
    "intro" => "In your website was published a new recipe.",
    "title" => "Recipe title",
    'author' => 'Author',
    "brief" => "You can approve or vote for this recipe from your website!"
];
