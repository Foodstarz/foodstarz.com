<?php

return [
    'subject' => 'No added recipes notification',
    'hello' => 'Hello',
    'explanation' => 'You are receiving this email because you haven\'t added any recipes on our website.',
    'how_to_add_heading' => 'How to add a recipe?',
    'how_to_add_text1' => 'In order to add a new recipe, please visit the',
    'add_recipe_page' => 'Add Recipe Page',
    'how_to_add_text2' => 'and follow the instructions.',
    'bottom_line' => 'Cheers!',   //Best regards,
    'from' => 'David (CEO & Founder)'      //The Foodstarz Team
];