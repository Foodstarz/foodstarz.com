<?php
return [
    'subject' => 'New Recipe Confirmation',
    'hello' => 'Hello',
    'explanation' => 'You are receiving this email in confirmation that your new recipe was successfully added to our website.',
    'recipe_details' => 'Recipe Details',
    'recipe_name' => 'Recipe Name',
    'recipe_url' => 'Recipe URL',
    'recipe_approval' => 'Recipe Review',
    'recipe_approval_text' => 'Your recipe will be reviewed by our team in a few days. If we approve the recipe, it will become publicly visible on the website. If we decide that the quality of the recipe is high enough - your recipe will be made featured which means that it will appear on the homepage of our website as well as the recipes page.',
    'bottom_line' => 'Cheers!',   //Best regards,
    'from' => 'David (CEO & Founder)'      //The Foodstarz Team
];