<?php

return [
    'page_title' => 'Videos',
    'content_title' => 'Videos',
    'no_videos' => 'There are no videos found.'
];
