<?php
return [
    'page_title' => 'Edit Gallery Image',
    'content_title' => 'Edit Gallery Image',

    'form' => [
        'caption' => [
            'label' => 'Caption',
            'placeholder' => 'Please enter a caption for the uploaded image.'
        ],

        'image' => [
            'label' => 'Image',
            'current' => 'Current Image',
            'replace' => 'If you want to change the image, please select a new one, otherwise leave the field untouched.'
        ],

        'social' => [
            'label' => 'Automatic Social Share',
            'facebook' => 'Facebook',

            'configure' => 'Click here to configure...'
        ],

        'submit' => [
            'label' => 'Send'
        ],
    ],

    'messages' => [
        'success' => 'You have successfully edited the image!',
        'errors' => [
            'invalid_image' => 'There was an error with the uploaded image.'
        ]
    ],

    'accepted_file_formats' => 'Accepted file formats - .jpg, .png',
    'share_text' => 'Check out this awesome dish picture shot by :name!'
];