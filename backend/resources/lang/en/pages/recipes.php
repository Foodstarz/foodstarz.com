<?php

return [
    'page_title' => 'Recipes',
    'content_title' => 'Recipes',
    "delete" => "Delete this recipe"
];
