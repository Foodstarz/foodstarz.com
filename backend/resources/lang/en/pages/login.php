<?php
return [
    'page_title' => 'Login',
    'content_title' => 'Login',

    'form' => [
        'email' => [
            'label' => 'E-Mail:',
            'placeholder' => 'Please enter your E-Mail address.'
        ],
        'password' => [
            'label' => 'Password:',
            'placeholder' => 'Please enter your password.'
        ],
        'remember' => [
            'label' => 'Remember me'
        ],
        'submit' => [
            'label' => 'Sign me in!'
        ]
    ],

    'forgotten_password' => 'Did you forget your password?',

    'messages' => [
        'not_activated' => 'This account is not yet activated.',
    ]
];