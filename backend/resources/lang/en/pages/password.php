<?php
return [
    'page_title' => 'Forgotten Password',
    'content_title' => 'Forgotten Password',

    'form' => [
        'email' => [
            'label' => 'E-Mail:',
            'placeholder' => 'Please enter your E-Mail address.'
        ],
        'submit' => [
            'label' => 'Send me a password reset link!'
        ]
    ],

    'msg' => [
        'already_sent' => 'A password reset link has already been sent.',
        'success' => 'A password reset link was successfully sent to your email.'
    ]
];