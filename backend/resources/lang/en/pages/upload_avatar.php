<?php
return [
    'page_title' => 'Profile Picture',
    'content_title' => 'Profile Picture',

    'form' => [
        'avatar' => [
            'label' => 'Profile Picture (Avatar):'
        ],
        'submit' => [
            'label' => 'Upload my Avatar!'
        ]
    ],

    'resize_image' => 'Resize Image:',

    'base_profile_data_error' => 'You cannot upload an avatar before you fill your Profile Core Data.',

    'messages' => [
        'success' => 'You have successfully uploaded your avatar.',
        'proceed' => 'Now you can proceed to',
        'proceed_action' => 'fill your Chef Profile Data',
        'mime_type_error' => 'Please select a .jpg, .jpeg or .png image file.',
        'size_error' => 'The image size must not exceed :size MB.',
        'valid_image_error' => 'Please select a valid image file.',
        'image_dimensions_error' => 'Please upload a picture with dimensions of at least 750x750px',

        'congratulations' => 'Congratulations! You successfully completed your Core Data and Profile Picture.',
        'next' => 'What do you want to do next?',
        'uploading' => 'Uploading',
        'upload_recipes' => 'recipes',
        'upload_images' => 'images',
        'or' => 'or',
        'upload_videos' => 'videos',
        'completing' => 'Completing your',
        'chef_profile' => 'Chef Profile',
        'interview' => 'Interview'
    ]
];