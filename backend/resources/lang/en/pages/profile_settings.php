<?php

return [
    'page_title' => 'Settings',
    'content_title' => 'Settings',

    'actions' => [
        'edit_profile' => 'Edit Profile',
        'change_email' => 'Change E-Mail',
        'change_password' => 'Change Password',
        'change_background' => 'Change Background',
        'change_language' => 'Change Language',
        'close_account' => 'Close my Account'
    ]
];