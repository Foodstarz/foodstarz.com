<?php
return [
    'page_title' => "Contact us",
    'content_title' => "Contact us",
    
    'your_name' => "Your Name (required)",
    "your_email" => "Your Email (required)",
    "subject" => "Subject",
    "your_message" => "Your message (required)",
    "send" => "Send",
    "success_message" => "Your request has been sent successfully."
];