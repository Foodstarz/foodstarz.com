<?php
return [
    'page_title' => 'Foodstarz | Your International Premium Chef Network!',
    
    'right_menu' => [
        'login' => 'Login',
        'register' => 'Join Us',
        'logout' => 'Logout',
        'account' => 'My Profile'
    ],
    'latest_recipes' => "Featured Recipes",
    "serves" => "Serves",
    "minutes" => "minutes",
    "read_more" => "Read more",
    "more_recipes" => "more recipes",
    "gold_partners" => "Sponsoring Partners",
    'charity_partners' => 'Friends and Charity Partners',
    "latest_foodstarz" => "Latest Foodstarz",
    "more_foodstarz" => "more foodstarz",
    'latest_images' => 'Featured Images',
    'latest_videos' => 'Featured Videos',
    'more_images' => 'more images',
    'more_videos' => 'more videos'
]; 