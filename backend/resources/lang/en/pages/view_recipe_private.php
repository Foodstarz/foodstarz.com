<?php

return [
    'approve_question' => 'Do you approve this recipe?',
    'yes' => 'Yes',
    'no' => 'No'
];