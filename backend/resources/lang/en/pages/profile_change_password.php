<?php

return [
    'page_title' => 'Change Password',
    'content_title' => 'Change Password',

    'form' => [
        'old_password' => [
            'label' => 'Old Password:',
            'placeholder' => 'Please enter your current password...'
        ],
        
        'new_password' => [
            'label' => 'New Password:',
            'placeholder' => 'Please enter the new desired password...'
        ],
        
        'new_password_confirm' => [
            'label' => 'New Password Confirmation:',
            'placeholder' => 'Please confirm the new password...'
        ],
        'submit' => [
            'label' => 'Change my password!'
        ]
    ],
    
    'messages' => [
        'success' => 'You have successfully changed your password!'
    ]
];