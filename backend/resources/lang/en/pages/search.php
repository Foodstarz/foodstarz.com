<?php

return [
    'page_title' => 'Search',
    "search_in" => "Search in",
    "no_results" => "No results found!"
];