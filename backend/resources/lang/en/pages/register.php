<?php
/**
 * Localization for the Register Page
 */

return [
    'page_title' => 'Register',
    'content_title' => 'Register',

    'form' => [
        'name' => [
            'label' => 'Full Name:',
            'placeholder' => 'Please enter your full name.'
        ],
        'email' => [
            'label' => 'E-Mail:',
            'placeholder' => 'Please enter your E-Mail address.'
        ],
        'password' => [
            'label' => 'Password:',
            'placeholder' => 'Please enter your password.'
        ],
        'password_confirmation' => [
            'label' => 'Password Confirmation:',
            'placeholder' => 'Please repeat the password.'
        ],
        'culinary_profession' => [
            'label' => 'Culinary Profession:',

            'amateur' => [
                'label' => 'Amateur Chef'
            ],
            'blogger' => [
                'label' => 'Food Blogger'
            ],
            'professional' => [
                'label' => 'Professional Chef'
            ]
        ],
        'agreement' => [
            'label' => 'I agree with the',
            'tos' => 'Terms & Conditions',
            'privacy' => 'Privacy Policy'
        ],
        'submit' => [
            'label' => 'Sign me up!'
        ]
    ],

    'messages' => [
        'success' => 'Your registration was successful! In order to activate your account please follow the confirmation link you will get on your E-Mail.',
        'error' => 'Something went wrong! Please try again.'
    ]
];