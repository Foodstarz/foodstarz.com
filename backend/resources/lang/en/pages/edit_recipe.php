<?php

return [
    'page_title' => 'Edit Recipe',
    'content_title' => 'Edit Recipe',
    
    'main_image' => 'If you want to replace the old main image of this recipe, please select one. If you want to keep the old image, no action is required.',
    'component_image' => 'If you want to replace the old image of this component, please select one. If you want to keep the old image, no action is required.',
    
    'messages' => [
        'success' => 'You have successfully updated your recipe. The updated version will be reviewed by our team before it appears on the website.'
    ],
    
    'edit' => 'Edit'
    
    
];