<?php

return [
    'page_title' => 'Change E-Mail',
    'content_title' => 'Change E-Mail',
    
    'warning' => 'When you change your email, your account will temporarily get deactivated until you visit your email and follow the activation link again. After that you will be able to login with your account again.',

    'form' => [
        'email' => [
            'label' => 'E-Mail',
            'placeholder' => 'Please enter a new email address...'
        ],
        
        'submit' => [
            'label' => 'Change my E-Mail'
        ]
    ]
];