<?php

return [
    'page_title' => 'Password Reset',
    'content_title' => 'Password Reset',

    'form' => [
        'email' => [
            'label' => 'E-Mail:',
            'placeholder' => 'Please enter your E-Mail address.'
        ],
        'password' => [
            'label' => 'Password:',
            'placeholder' => 'Please enter your password.'
        ],
        'password_confirmation' => [
            'label' => 'Password Confirmation:',
            'placeholder' => 'Please repeat the password.'
        ],
        'submit' => [
            'label' => 'Change my password!'
        ]
    ]
];