<?php

return [
    'page_title' => 'Close Account',
    'content_title' => 'Close Account',
    
    'warning' => 'Closing your account will permanently delete your profile, recipes and activity on the website. The process is not reversible!!!',

    'form' => [
        'agreement' => [
            'label' => 'Do you really want to lose all your connections and contents?',
            'yes' => 'Yes',
            'no' => 'No'
        ],
        
        'password' => [
            'label' => 'Password:',
            'placeholder' => 'Please enter your password...'
        ],
        
        'submit' => [
            'label' => 'Close my account!'
        ]
    ]
];