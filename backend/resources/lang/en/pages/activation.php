<?php

return [
    'messages' => [
        'success' => 'You successfully activated your account! Now you can log in.',
        'error' => 'Something went wrong! Please try again.'
    ]
];