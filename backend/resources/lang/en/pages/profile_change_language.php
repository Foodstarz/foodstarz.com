<?php
return [
    'page_title' => 'Change Language',
    'content_title' => 'Change Language',

    'form' => [
        'language' => [
            'label' => 'Language:',
        ],

        'submit' => [
            'label' => 'Change my language!'
        ]
    ],

    'messages' => [
        'success' => 'You have successfully changed your language!'
    ],

    'auto_detect' => 'Auto-Detect Language'
];