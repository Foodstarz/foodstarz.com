<?php
return [
    'page_title' => 'Chef Profile',
    'content_title' => 'Chef Profile',

    'form' => [
        'optional' => 'Optional',
        'biography' => [
            'label' => 'Biography:',
            'valid' => '30 characters required',
            'placeholder' => 'Please tell up your bio.'
        ],
        'work_experience' => [
            'label' => 'Work Experience:',
            'subfields' => [
                'position' => [
                    'label' => 'Position:'
                ],
                'employer' => [
                    'label' => 'Employer:'
                ],
                'period' => [
                    'label' => 'Period:',
                    'from' => 'From:',
                    'to' => 'To:',
                    'still_working' => 'Still working here?'
                ],
                'job_description' => [
                    'label' => 'Job Description:'
                ],
                'add_button' => [
                    'label' => 'Add Next Position'
                ],
                'remove_button' => [
                    'label' => 'Remove This Position'
                ]
            ]
        ],
        'cooking_style' => [
            'label' => 'Cooking Style:',
        ],
        'awards' => [
            'label' => 'Awards:',
            'placeholder' => 'Please enter award name...',
            'add_button' => 'Add Next Award',
            'remove_button' => 'Remove This Award'
        ],
        'organisations' => [
            'label' => 'Organisations:',
            'add_button' => 'Add Next Organisation',
            'remove_button' => 'Remove This Organisation'
        ],
        'role_models' => [
            'label' => 'Role Model(s):',
            'add_button' => 'Add Next Role Model',
            'remove_button' => 'Remove This Role Model'
        ],
        'kitchen_skills' => [
            'label' => 'Kitchen Skills',
            'valid' => 'optional, max. 6',
            'checkboxes' => [
                'baking' => 'Baking',
                'bread_cake_making' => 'Bread / Cake Making',
                'cake_pastry_decorating' => 'Cake and Pastry Decorating',
                'catering' => 'Catering',
                'cheese_course' => 'Cheese Course',
                'chicikate_tempering_dipping' => 'Chocolate Tempering & Dipping',
                'dough_making' => 'Dough Making / Portioning',
                'dressings' => 'Dressings',
                'cleaning_sanitation' => 'Enforcement Of Safety / Sanitation Standards',
                'equipment' => 'Equipment Maintenance',
                'fish_cookery' => 'Fish Cookery / Butchery',
                'food_pairing' => 'Food / Beverage Pairing',
                'food_budgeting' => 'Food Budgeting',
                'food_cost_optimization' => 'Food Cost Optimization',
                'food_preparation' => 'Food Preparation',
                'food_safety_knowledge' => 'Food Safety Knowledge',
                'food_styling' => 'Food Styling',
                'gelatos_sorbets_french_ice_creams' => 'Gelato / Ice Cream / Sorbet',
                'gluten_free' => 'Gluten Free',
                'ingredient_selection' => 'Ingredient Selection',
                'kitchen_management' => 'Kitchen Management',
                'kitchen_tools' => 'Kitchen Tools',
                'knife_skills' => 'Knife Skills',
                'kosher_preparation' => 'Kosher Preparation',
                'meat_cookery' => 'Meat Cookery / Butchery',
                'menu_planning' => 'Menu / Recipe Development',
                'mis_en_place' => 'Mis En place',
                'molecular_gastronomy' => 'Molecular Gastronomy',
                'offsite_event_management' => 'Off-site Event Management',
                'onsite_special_events' => 'On-site Special Events',
                'ordering_purchasing' => 'Ordering, Purchasing, Receiving',
                'pasta_making' => 'Pasta Making',
                'pastry' => 'Pastry',
                'pastry_commissary' => 'Pastry Commissary',
                'pastry_plating' => 'Pastry Plating',
                'pastry_production' => 'Pastry Production',
                'petit_fours' => 'Petit Fours / Mignardises',
                'pizza_cooking' => 'Pizza Cooking',
                'portioning' => 'Portioning',
                'presentation' => 'Presentation',
                'sauces' => 'Sauce Development',
                'sauteing' => 'Sautéing',
                'scheduling' => 'Scheduling',
                'show_piece_work' => 'Show Piece Work',
                'sous_vide_technique' => 'Sous Vide Technique',
                'tasting_seasoning_balance' => 'Tasting, Seasoning, Balance',
                'ticket_management' => 'Ticket Management',
                'time_management' => 'Time Management',
                'vegan_vegetarian' => 'Vegan / Vegetarian Cooking / Baking',
                'grilling' => 'Wood Fire Cooking / Grilling',
            ]
        ],
        'business_skills' => [
            'label' => 'Business Skills',
            'valid' => 'optional, max. 6',
            'checkboxes' => [
                'accounting_bookkeeping' => 'Accounting / Bookkeeping',
                'business_acumen' => 'Business Acumen',
                'business_management' => 'Business Management',
                'computer_skills' => 'Computer Skills',
                'concepts' => 'Concepts',
                'cost_control' => 'Cost Control',
                'customer_service' => 'Customer Service',
                'expediting' => 'Expediting',
                'finance_degree' => 'Finance Degree',
                'food_industry_consulting' => 'Food Industry Consulting',
                'food_pricing' => 'Food Pricing / Budgeting',
                'food_regulations' => 'Food Regulations',
                'food_safety' => 'Food Safety',
                'food_service_management' => 'Food Service Management',
                'food_writer_blogger' => 'Food Writer/Blogger',
                'hospitality_management_degree' => 'Hospitality / Management Degree',
                'international_functions_special_events' => 'International Functions',
                'inventory_management' => 'Inventory Management',
                'leadership_time_management' => 'Leadership & Time Management',
                'marketing' => 'Marketing / PR',
                'menu_development' => 'Menu Development',
                'merchandising' => 'Merchandising',
                'operations' => 'Operations',
                'payroll' => 'Payroll',
                'ordering' => 'Purchasing / Ordering',
                'hiring' => 'Recruiting / Hiring',
                'reservation_management' => 'Reservation System Management',
                'social_media' => 'Social Media',
                'staff_education' => 'Staff Education',
                'tv_radio_guest_appearances' => 'TV/Radio Guest Appearances',
                'vendor_management' => 'Vendor Management',
                'web_maintenance' => 'Web Maintenance',
            ]
        ],
        'personal_skills' => [
            'label' => 'Personal Skills',
            'valid' => 'optional, max. 6',
            'checkboxes' => [
                'adaptability' => 'Adaptability',
                'assertiveness' => 'Assertiveness',
                'attention_to_detail' => 'Attention to Detail',
                'capacity_for_teamwork' => 'Capacity for Teamwork',
                'commitment_to_quality' => 'Commitment to Quality',
                'communication' => 'Communication',
                'conflict_resolution' => 'Conflict Resolution',
                'consistency' => 'Consistency',
                'creativity' => 'Creativity',
                'customer_oriented' => 'Customer-oriented',
                'diligence' => 'Diligence',
                'discipline' => 'Discipline',
                'empathy' => 'Empathy',
                'flexibility' => 'Flexibility',
                'initiative' => 'Initiative',
                'motivational' => 'Motivational',
                'multitasking' => 'Multitasking',
                'openness' => 'Openness',
                'personal_resilience' => 'Personal Resilience',
                'precision' => 'Precision',
                'problem_solving' => 'Problem Solving',
                'professional_commitment' => 'Professional Commitment',
                'responsibility' => 'Responsibility',
                'self_management' => 'Self-Management',
                'sense_of_humor' => 'Sense of Humor',
                'sense_of_urgency' => 'Sense of Urgency',
                'souvereignty' => 'Souvereignty',
                'speed_and_organization' => 'Speed and Organization',
                'target_oriented' => 'Target Oriented',
                'time_management' => 'Time Management',
            ]
        ],
        'search' => [
            'label' => 'Search:'
        ],
        'offer' => [
            'label' => 'Offer:'
        ],
        'social_media_links' => [
            'label' => 'Social Media Links',
            'valid' => 'Full Url',
            'website' => [
                'label' => 'Personal Website:'
            ],
            'instagram' => [
                'label' => 'Instagram:'
            ],
            'facebook' => [
                'label' => 'Facebook:'
            ],
            'twitter' => [
                'label' => 'Twitter:'
            ],
            'pinterest' => [
                'label' => 'Pinterest:'
            ],
            'tumblr' => [
                'label' => 'Tumblr:'
            ],
        ],
        'submit' => [
            'label' => 'Update my Chef Profile Data!'
        ],
        'months' => [
            'jan' => 'January',
            'feb' => 'February',
            'mar' => 'March',
            'apr' => 'April',
            'may' => 'May',
            'jun' => 'June',
            'jul' => 'July',
            'aug' => 'August',
            'sep' => 'September',
            'oct' => 'October',
            'nov' => 'November',
            'dec' => 'December'
        ],

        'year' => 'Year',
        'month' => 'Month'
    ],

    'messages' => [
        'success' => 'You have successfully filled your Chef Profile Data.',
        'proceed' => 'Now you can proceed to',
        'proceed_to' => 'the Interview',

        'errors' => [
            'work_experience' => 'Please fill all the fields.',
            'awards' => 'Do not add empty fields.',
            'organisations' => 'Do not add empty organisations.',
            'role_models' => 'Do not add empty role models.'
        ]
    ],

    'base_profile_data_error' => 'You cannot fill your Chef Profile Data before you fill your Profile Core Data.',
    'upload_avatar_error' => 'You cannot fill your Chef Profile Data before you upload an Avatar.'
];