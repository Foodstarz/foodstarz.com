<?php
return [
    'page_title' => 'View Video',
    'content_title' => 'View Video',
    'edit' => 'Edit Video',
    'hide' => 'Hide Video',
    'unhide' => 'Unhide Video',
    'delete' => 'Delete Video',
    'confirm_deletion' => 'Are you sure you want to delete that video?',
    'confirm_hide' => 'Are you sure you want to hide that video?'
];