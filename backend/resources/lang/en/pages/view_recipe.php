<?php
return [
    'page_title' => 'View Recipe',
    'method' => 'Method',

    'difficulty' => [
        'easy' => 'Easy',
        'medium' => 'Medium',
        'hard' => 'Hard'
    ]

];