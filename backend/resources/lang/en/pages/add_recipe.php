<?php
return [
    'page_title' => 'Add Recipe',
    'content_title' => 'Add Recipe',

    'form' => [
        'fieldset' => [
            'main_data' => 'Main Data',
            'other_data' => 'Other Data'
        ],
        
        'title' => [
            'label' => 'Recipe Name'
        ],

        'category' => [
            'label' => 'Category/Categories'
        ],

        'portions' => [
            'label' => 'Yield/Number of Portions'
        ],

        'preparation_time' => [
            'label' => 'Preparation Time'
        ],

        'difficulty' => [
            'label' => 'Difficulty',
            'options' => [
                'easy' => 'Easy',
                'medium' => 'Medium',
                'hard' => 'Hard'
            ]
        ],

        'components' => [
            'label' => 'Components',
            'component' => 'Component',
            'name' => 'Name',
            'ingredients' => 'Ingredients',
            'ingredient' => 'Ingredient',
            'quantity' => 'Quantity',
            'select_unit' => 'Select Ingredient Unit',

            'plural' => '{1} st|{2} nd|{3} rd|[4,Inf] th',

            'units' => [
                'bunch' => 'bunch(es)',
                'can' => 'can(s)',
                'clove' => 'clove(s)',
                'cup' => 'cup(s)',
                'dash' => 'dash(es)',
                'deciliter' => 'deciliter(s)',
                'drop' => 'drop(s)',
                'fluid_ounce' => 'fluid ounce(s)',
                'gallon' => 'gallon(s)',
                'gram' => 'gram(s)',
                'kilo' => 'kilo(s)',
                'liter' => 'liter(s)',
                'milliliter' => 'milliliter(s)',
                'ounce' => 'ounce(s)',
                'package' => 'package(s)',
                'piece' => 'piece(s)',
                'pinch' => 'pinch(es)',
                'pint' => 'pint(s)',
                'pound' => 'pound(s)',
                'quart' => 'quart(s)',
                'shot' => 'shot(s)',
                'slice' => 'slice(s)',
                'sprig' => 'sprig(s)',
                'stick' => 'stick(s)',
                'tablespoon' => 'tablespoon(s)',
                'teaspoon' => 'teaspoon(s)',
            ],

            'add_ingredient' => 'Add an Ingredient',

            'instructions' => 'Instructions',
            'enter_instruction' => 'Enter Instruction',
            'add_step' => 'Add a New Step',

            'component_image' => 'Component Image (optional)',

            'add_component' => 'Add a New Component',
            'remove_component' => 'Remove Component'
        ],

        'attachments' => [
            'label' => 'Main Image',
            'desc' => 'Only use a GREAT picture! We recommend only high-resolution digital images.'
        ],

        'status' => [
            'label' => 'Status',
            'desc' => 'Would you like to further edit this recipe or are you ready to publish it?',
            'text' => 'The administrator of this website has opted to review submissions  before publishing. After you hit submit, your recipe will be published as soon as the administrator has reviewed it.',

            'options' => [
                'not_ready' => 'I am still working on it',
                'ready' => 'I am ready to publish this recipe'
            ]
        ],

        'submit' => [
            'label' => 'Send'
        ],
        
        'loading' => 'We are processing your request. Please wait...'
    ],

    'messages' => [
        'errors' => [
            'components' => [
                'name' => 'Please enter a name for this component.',
                'no_ingredients' => 'The component must have at least one ingredient specified.',
                'no_instructions' => 'The component must have at least one instruction specified.',
                'ingredients_empty_fields' => 'Please fill all the ingredients fields.',
                'instructions_empty_fields' => 'Please fill all the instructions fields.'
            ],
            'images' => [
                'mime_type' => 'Please upload only .jpg and .png images.',
                'max_size' => 'The maximum allowed image size is 5Mb.',
                'no_picture' => 'Do not leave empty images.',
                'invalid_image' => 'Invalid image.'
            ],
            'profile_not_ready' => 'You cannot add recipes yet because you haven\'t filled your profile. Please follow the instructions below in order to get access to the Add Recipe page.',
        ],
        'success' => 'You successfully added a new recipe! Our team will review it carefully and when it is approved it will be available to the public.'
    ],
    
    'profile_not_ready' => [
        'instructions' => 'Instructions',
        'explanation' => 'In order to add a recipe on our website we need to know a little bit about you as a chef. This is why you first need to fill in your profile data and then you will be automatically allowed to publish a new recipe on our website!',
        'core_data' => 'Firstly you need to fill your',
        'core_data_link' => 'Profile Core Data',
        'upload_avatar' => 'After you have filled your Core Data you need to',
        'upload_avatar_link' => 'Upload a Profile Picture',
        'chef_profile' => 'Next you have to fill your',
        'chef_profile_link' => 'Chef Profile',
        'interview' => 'Finally you need to answer a few question in our',
        'interview_link' => 'Interview',
        'note' => 'Note',
        'note_text' => 'Please follow the steps subsequently because if you don\'t fill some part of your Profile you won\'t be able to proceed forward to the next part.'
    ],

    'accepted_file_formats' => 'Accepted file formats - .jpg, .png'
];