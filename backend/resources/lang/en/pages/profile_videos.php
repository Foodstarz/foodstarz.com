<?php
return [
    'page_title' => 'Profile Videos',
    'content_title' => 'Profile Videos',

    'page_description' => 'View the personal videos of :name.',
    'no_videos' => 'This user hasn\'t uploaded any videos yet.'
];