<?php
return [
    'page_title' => 'User Profile',

    'menu' => [
        'chronic' => 'Timeline',
        'profile' => 'Chef Profile',
        'interview' => 'Interview',
        'gallery' => 'Images',
        'videos' => 'Videos',
        'add_recipe' => 'Add Recipe',
        'add_image' => 'Add Image',
        'add_video' => 'Add Video',
        'edit' => 'Edit',
        'settings' => 'Settings'
    ],

    'work_experience' => [
        'title' => 'Work Experience',
        'still_working' => 'Now'
    ],

    'awards' => [
        'title' => 'Awards',
        'no_awards' => 'No Awards.'
    ],

    'organisations' => [
        'title' => 'Organisations',
        'no_organisations' => 'No Organisations.'
    ],

    'role_models' => [
        'title' => 'Role Models',
        'no_role_models' => 'No Role Models.'
    ],

    'cooking_style' => [
        'title' => 'Cooking Style'
    ],

    'skills' => [
        'kitchen_skills' => 'Kitchen Skills',
        'business_skills' => 'Business Skills',
        'personal_skills' => 'Personal Skills'
    ],

    'search' => [
        'title' => 'Search'
    ],

    'offer' => [
        'title' => 'Offer'
    ],

    'social' => [
        'title' => 'Follow Me!'
    ],

    'not_ready_heading' => 'This profile is not ready yet.',
    'not_ready_description' => 'It will be available when its owner fills all the necessary data.',
    
    'not_approved_heading' => 'This profile is not approved yet.',
    'not_approved_description' => 'It will become publicly available when its author gets one recipe approved.',

    'activity_interstitial' => 'Hi :name,
nice to see you again! It\'s been a while since you uploaded new contents to your Foodstarz profile. In order to get more visibility and to become featured on our social media channels, please use the Foodstarz PLUS button and create recipes or upload new images or videos.
Thank you!',

];