<?php

return [
    "categories" => "Categories",
    "no_recipes" => "There are no recipes!",
    "recipes" => "Recipes",
    "approved" => "Approved",
    "in-progress" => "In progress",
    "waiting" => "Waiting"
];
