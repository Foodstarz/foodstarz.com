<?php
return [
    'page_title' => 'View Gallery Image',
    'content_title' => 'View Gallery Image',
    'edit' => 'Edit Image',
    'hide' => 'Hide Image',
    'unhide' => 'Unhide Image',
    'delete' => 'Delete Image',
    'confirm_deletion' => 'Are you sure you want to delete that image?',
    'confirm_hide' => 'Are you sure you want to hide that image?'
];