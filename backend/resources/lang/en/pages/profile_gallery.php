<?php
return [
    'page_title' => 'Profile Images',
    'content_title' => 'Profile Images',

    'page_description' => 'View the personal gallery of :name.',
    'no_images' => 'This user hasn\'t uploaded any images to his gallery yet.'
];