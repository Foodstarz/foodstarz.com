<?php
return [
    'page_title' => 'Chronic',
    'content_title' => 'Chronic',

    'page_description' => 'View the personal chronic of :name.',
    'no_activity' => 'This user doesn\'t have any activity yet.'
];