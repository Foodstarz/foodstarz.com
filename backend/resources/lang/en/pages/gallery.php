<?php

return [
    'page_title' => 'Images',
    'content_title' => 'Images',
    'no_images' => 'There are no images found.'
];
