<?php
return [
    'page_title' => 'Edit Video',
    'content_title' => 'Edit Video',

    'form' => [
        'title' => [
            'label' => 'Title',
            'placeholder' => 'Please enter a title for the uploaded video.'
        ],

        'description' => [
            'label' => 'Description',
            'placeholder' => 'Please enter a description for the uploaded video.'
        ],

        'social' => [
            'label' => 'Automatic Social Share',
            'facebook' => 'Facebook',

            'configure' => 'Click here to configure...'
        ],

        'submit' => [
            'label' => 'Send'
        ],
    ],

    'messages' => [
        'success' => 'You have successfully edited the video!',
    ],
];