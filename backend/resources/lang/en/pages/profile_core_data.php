<?php
return [
    'page_title' => 'Core Data',
    'content_title' => 'Core Data',

    'form' => [
        'name' => [
            'label' => 'Full Name:',
            'placeholder' => 'Please enter your full name.'
        ],
        'email' => [
            'label' => 'E-Mail:',
            'placeholder' => 'Please enter your E-Mail address.'
        ],
        'gender' => [
            'label' => 'Gender:',
            'choices' => [
                'male' => 'Male',
                'female' => 'Female'
            ]
        ],
        'country' => [
            'label' => 'Country:',
            'placeholder' => 'Choose a country...'
        ],
        'city' => [
            'label' => 'City:',
            'placeholder' => 'Please enter your city and state'
        ],
        'culinary_profession' => [
            'label' => 'Culinary Profession:',

            'amateur' => [
                'label' => 'Amateur Chef'
            ],
            'blogger' => [
                'label' => 'Food Blogger'
            ],
            'professional' => [
                'label' => 'Professional Chef'
            ]
        ],
        
        'submit' => [
            'label' => 'Update my Profile Core Data!'
        ]
    ],

    'messages' => [
        'success' => 'You have successfully filled your Profile Core Data.',
        'proceed' => 'Now you can proceed to',
        'proceed_to' => 'upload an Avatar'
    ]
];