<?php
return [
    'core_data' => 'Core Data',
    'avatar' => 'Profile Picture',
    'chef_data' => 'Chef Profile',
    'interview' => 'Interview'
];