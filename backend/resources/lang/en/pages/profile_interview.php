<?php

return [
    'page_title' => 'Interview',
    'content_title' => 'Interview',

    'required' => '(required)',
    'form' => [
        'submit' => [
            'label' => 'Save my Interview!'
        ]
    ],
    'base_profile_data_error' => 'You cannot fill your Interview before you fill your Profile Core Data.',
    'upload_avatar_error' => 'You cannot fill your Interview before you upload an Avatar.',
    'chef_data_error' => 'You cannot fill your Interview before you fill your Chef Profile Data.',

    'errors' => [
        'required' => 'Please write an answer for every required question.'
    ],

    'success_just_activated' => 'Thank you for completing the forms. Your chef profile is now active. In order to get more visibility, please create a recipe or upload pictures through Foodstarz PLUS. The best recipes and pictures will be shared through our social media accounts, including a reference to your profiles.',
    'success_normal' => 'You have successfully updated your Interview!'

];