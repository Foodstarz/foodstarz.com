<?php
return [
    'page_title' => 'Add Gallery Image',
    'content_title' => 'Add Gallery Image',

    'form' => [
        'caption' => [
            'label' => 'Caption',
            'placeholder' => 'Please enter a caption for the uploaded image.'
        ],

        'image' => [
            'label' => 'Image'
        ],

        'submit' => [
            'label' => 'Send'
        ],
    ],

    'messages' => [
        'success' => 'You successfully added a new image in your gallery! You will be redirected to the image in 5 seconds...',
        'errors' => [
            'invalid_image' => 'There was an error with the uploaded image.'
        ]
    ],

    'accepted_file_formats' => 'Accepted file formats - .jpg, .png',
];