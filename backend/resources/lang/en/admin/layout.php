<?php

return [
    'title' => 'Foodstarz Admin Panel',
    'logo_name' => 'Foodstarz Admin Panel',
    'logo_name_short' => 'FA',
    'toggle_navigation' => 'Toggle Navigation',
    'home' => 'Home',
    'listing' => "Results list",
    "edit" => "Edit result",
    'no_avatar' => 'No Avatar',
    'user_image' => 'User Image',
    "edit-button" => "Edit",
    
    'profile' => 'Profile',
    'logout' => 'Logout',
    
    'menu' => [
        'main' => [
            'header' => 'Main Menu',
            'items' => [
                'pages' => 'Pages',
                "users" => "Users",
                "partners" => "Partners",
            ]
        ],

        'overview' => [
            'header' => 'Overviews',
            'items' => [
                'main' => 'Main',
                "users" => "Users",
                "users_groups" => "Users Groups",
                "countries" => "Countries",
                "compare" => "Compare",
            ]
        ],

        'features' => [
            'header' => 'Features',
            'items' => [
                'main' => 'Features',
            ]
        ],
        
        'recipes' => [
            'header' => 'Recipes',
            'items' => [
                'featured' => 'Featured Recipes'
            ]
        ],
        
        'news' => [
            'header' => 'News',
            'items' => [
                'listing' => 'Listing',
                'add_news' => 'Add News'
            ]
        ],
        
        'menus' => [
            'header' => 'Menus',
            'items' => [
                'header' => 'Header',
                'footer' => 'Footer'
            ]
        ],

        'interview' => [
            'header' => 'Interview Questions',
            'items' => [
                'listing' => 'Listing',
                'add_questions' => 'Add Questions',
                'edit_questions' => 'Edit Questions'
            ]
        ],

        'recipes_categories' => [
            'header' => 'Recipes Categories',
            'items' => [
                'add_category' => 'Add Category',
                'edit_category' => 'Edit Category'
            ]
        ],

        'newsletter' => [
            'header' => 'Newsletter',
            'items' => [
                'newsletter_export' => 'Newsletter Export',
                'newsletter_change' => 'Newsletter Change'
            ]
        ],

        'bookmarks' => [
            'header' => 'Bookmarks',
            'items' => [
                'recipes' => 'Recipes',
                'images' => 'Images',
                'videos' => 'Videos'
            ]
        ]
    ],
    
    'copyright' => 'Copyright',
    'rights' => 'All rights reserved.',
    
    'foodstarz' => 'Foodstarz'
];