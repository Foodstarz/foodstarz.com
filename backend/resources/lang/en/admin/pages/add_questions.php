<?php

return [
    'page_title' => 'Interview Add Question',
    'content_title' => 'Add Question',
    'page_description' => 'Here you can add new questions to the Interview.',

    'form' => [
        'question' => [
            'label' => 'Question',
            'placeholder' => 'Please enter a question...'
        ],

        'answer_type' => [
            'label' => 'Answer Type',
            'input' => 'Input',
            'textarea' => 'Textarea'
        ],

        'required' => [
            'label' => 'Required',
            'yes' => 'Yes',
            'no' => 'No'
        ],

        'submit' => [
            'label' => 'Submit'
        ]
    ],

    'messages' => [
        'success_title' => 'Success!',
        'success' => 'You have successfully added a new question to the Interview!'
    ],
];