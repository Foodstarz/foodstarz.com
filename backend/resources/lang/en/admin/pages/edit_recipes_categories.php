<?php

return [
    'page_title' => 'Edit Recipe Category',
    'content_title' => 'Edit Recipe Category',
    'page_description' => 'Here you can edit existing recipe category.',

    'form' => [
        'title' => [
            'label' => 'Title',
            'placeholder' => 'Please enter a title...'
        ],

        'image' => [
            'label' => 'Image',
            'placeholder' => 'Please enter a image...'
        ],

        'submit' => [
            'label' => 'Submit'
        ]
    ],

    'messages' => [
        'success_title' => 'Success!',
        'success' => 'You have successfully changed a recipe category!'
    ],
];