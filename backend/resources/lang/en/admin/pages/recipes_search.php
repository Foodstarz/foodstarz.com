<?php
return [
    'page_title' => 'Recipes Search',
    'content_title' => 'Recipes Search',
    'page_description' => 'Here you can search for recipes on your website.',
    
    'search' => 'Search',
    'results' => 'Results',
    
    'search_title' => 'Recipe Title',
    'search_author' => 'Author Name'
];