<?php

return [
    'page_title' => 'Newsletter Change',
    'content_title' => 'Newsletter Change',
    'page_description' => 'Here you can change subscribed emails for the newsletter.',

    'form' => [
        'email' => [
            'label' => 'E-Mails',
            'placeholder' => 'Please enter E-Mails - one E-Mail per line...'
        ],

        'subscribe' => [
            'label' => 'Subscription',
            'subscribe' => 'Subscribe',
            'unsubscribe' => 'Unsubscribe'
        ],

        'submit' => [
            'label' => 'Submit'
        ]
    ],

    'messages' => [
        'success_title' => 'Success!',
        'success' => 'You have successfully changed the subscription of these E-Mails.'
    ]
];