<?php

return [
    'title' => 'Features settings',
    'columns' => [
        'name' => 'Name',
        'group' => 'User Group',
        'status' => 'Status',
        'actions' => 'Actions'
    ],
    'success' => [
        'title' => 'Success',
        'message' => 'Data is saved'
    ],
    'danger' => [
        'title' => 'Danger',
        'message' => 'Something went wrong'
    ]
];