<?php

return [
    'page_title' => 'Featured Recipes',
    'content_title' => 'Featured Recipes',
    'page_description' => 'Here you can add/delete featured recipes that will show on the homepage of your website.',
    
    'title' => 'Title',
    'actions' => 'Actions',
    'add_featured_recipe' => 'Add Featured Recipe',
    'delete_featured_recipe' => 'Delete Featured Recipe',
    
    'all_recipes' => 'All Recipes',
    'featured_recipes' => 'Featured Recipes',
    
    'messages' => [
        'add' => 'The recipe was successfully made Featured.',
        'delete' => 'The recipe was removed from the Featured list.'
    ]
];