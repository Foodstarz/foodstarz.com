<?php

return [
    'page_title' => 'Pages',
    'content_title' => 'Pages',
    'page_description' => 'Here you can manage the custom pages of your website.',
    'c_pages' => 'Common Page',
    "success" => "This page has been updated successfully.",
    "delete_success" => "This page has been deleted successfully!",
    "c_pages_new" => "New common page",
    "new_success" => "The new page has been added successfilly!"
];
