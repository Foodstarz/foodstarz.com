<?php
return [
    'page_title' => 'Recipes Categories Listing',
    'content_title' => 'Categories Listing',
    'page_description' => 'Here you can browse all the recipe Categories.',

    'title' => 'Title',
    'image' => 'Image',
    'actions' => 'Actions',

    'add_category' => 'Add Category',

    'messages' => [
        'success_title' => 'Success!',
        'delete' => 'You have successfully removed a recipe category.'
    ]
];