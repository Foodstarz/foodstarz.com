<?php

return [
    "users" => "Users",
    "delete_success" => "This user has been deleted successfully!",
    "edit_success" => "This user has been edited successfully!",
    'inactivate_success' => 'You have successfully inactivated the user.',
    'activate_success' => 'You have successfully activated the user.'
];
