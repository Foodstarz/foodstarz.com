<?php
return [
    'page_title' => 'User Groups',
    'page_list_title' => 'Groups list',
    'content_title' => 'User Groups',
    'page_description' => 'Here you can manage groups of users.',
    'edit' => 'Save',
    'label' => [
        'name' => 'Name',
        'status' => 'Status'
    ],
    'statuses' => [
        'active' => 'Active',
        'inactive' => 'Inactive'
    ]
];