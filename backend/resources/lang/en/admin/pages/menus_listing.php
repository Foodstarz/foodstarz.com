<?php

return [
    'page_title' => 'Menus Listing',
    'content_title' => 'Menus Listing',
    'page_description' => 'Here you can browse all the news on your website.',
    
    'add_menu_item' => 'Add Menu Item',
    'title' => 'Title',
    'url' => 'URL',
    'order' => 'Order',
    'actions' => 'Actions',
    
    'messages' => [
        'delete' => 'The item was successfully deleted.'
    ]
];