<?php
return [
	'page_title' => 'Bookmarks Listing',
    'content_title' => 'Bookmarks Listing',
    'page_description' => 'Here you can browse all the bookmarked resources.',

    'title' => 'Title',
    'type' => 'Type',
    'shared' => 'Shared',
    'date' => 'Date',
    'actions' => 'Actions',

    'share' => 'Mark as shared'

];