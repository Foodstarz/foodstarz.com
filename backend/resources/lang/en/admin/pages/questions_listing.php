<?php
return [
    'page_title' => 'Interview Questions Listing',
    'content_title' => 'Questions Listing',
    'page_description' => 'Here you can browse all the Interview Questions.',

    'question' => 'Question',
    'answer_type' => 'Answer Type',
    'required' => 'Required',
    'actions' => 'Actions',

    'add_question' => 'Add Question',

    'messages' => [
        'success_title' => 'Success!',
        'delete' => 'You have successfully removed a question from the interview.'
    ]
];