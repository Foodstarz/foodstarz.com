<?php

return [
    'page_title' => 'News Listing',
    'content_title' => 'News Listing',
    'page_description' => 'Here you can browse all the news on your website.',
    
    'add_news' => 'Add News',
    'title' => 'Title',
    'actions' => 'Actions',
    
    'messages' => [
        'delete' => 'The item was successfully deleted.'
    ]
];