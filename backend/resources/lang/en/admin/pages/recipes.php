<?php

return [

    'recipes' => 'Recipes',
    "recipe_review" => "Recipe review",
    "jury_opinion" => "What jury members think?",
    "pending_approval" => "Pending approval",
    "watermark_images" => "Watermark Images"
];
