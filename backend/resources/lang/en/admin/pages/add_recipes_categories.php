<?php

return [
    'page_title' => 'Add Recipe Categories',
    'content_title' => 'Add Recipe Categories',
    'page_description' => 'Here you can add new recipe categories to the Interview.',

    'form' => [
        'title' => [
            'label' => 'Title',
            'placeholder' => 'Please enter a title...'
        ],

        'image' => [
            'label' => 'Image',
            'placeholder' => 'Please enter a image...'
        ],

        'submit' => [
            'label' => 'Submit'
        ]
    ],

    'messages' => [
        'success_title' => 'Success!',
        'success' => 'You have successfully added a new recipe category!'
    ],
];