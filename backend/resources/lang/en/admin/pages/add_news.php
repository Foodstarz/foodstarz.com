<?php

return [
    'page_title' => 'Add News',
    'content_title' => 'Add News',
    'page_description' => 'Here you can add news on your website.',
    
    'form' => [
        'title' => [
            'label' => 'Title',
            'placeholder' => 'Please enter a title...'
        ],
        
        'content' => [
            'label' => 'Content'
        ],
        
        'submit' => [
            'label' => 'Submit'
        ]
    ],
    
    'messages' => [
        'success_title' => 'Success!',
        'success' => 'You have successfully added news to the website! You can visit it from the link below:'
    ]
];