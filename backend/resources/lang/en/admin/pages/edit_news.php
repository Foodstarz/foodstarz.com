<?php

return [
    'page_title' => 'Edit News',
    'content_title' => 'Edit News',
    'page_description' => 'Here you can edit news on your website.',
    
    'form' => [
        'title' => [
            'label' => 'Title',
            'placeholder' => 'Please enter a title...'
        ],
        
        'content' => [
            'label' => 'Content'
        ],
        
        'submit' => [
            'label' => 'Submit'
        ]
    ],
    
    'messages' => [
        'success_title' => 'Success!',
        'success' => 'You have successfully edited this entry! You can visit it from the link below:'
    ]
];