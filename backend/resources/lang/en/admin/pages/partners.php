<?php

return [
    "partners" => "Partners",
    "new_partner" => "New partner",
    "new_success" => "The new partner has been successfully!",
    "delete_success" => "This partner has been deleted successfully!",
    "edit_success" => "The partner has been edited successfully!"
];
