<?php

return [
    'page_title' => 'Newsletter Listing',
    'content_title' => 'Newsletter Listing',
    'page_description' => 'Here you can get all subscribed emails for the newsletter.',

];