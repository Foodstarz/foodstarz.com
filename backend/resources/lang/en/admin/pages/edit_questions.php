<?php

return [
    'page_title' => 'Interview Edit Question',
    'content_title' => 'Edit Question',
    'page_description' => 'Here you can edit existing questions on the Interview.',

    'form' => [
        'question' => [
            'label' => 'Question',
            'placeholder' => 'Please enter a question...'
        ],

        'answer_type' => [
            'label' => 'Answer Type',
            'input' => 'Input',
            'textarea' => 'Textarea'
        ],

        'required' => [
            'label' => 'Required',
            'yes' => 'Yes',
            'no' => 'No'
        ],

        'submit' => [
            'label' => 'Submit'
        ]
    ],

    'messages' => [
        'success_title' => 'Success!',
        'success' => 'You have successfully changed a question on the Interview!'
    ],
];