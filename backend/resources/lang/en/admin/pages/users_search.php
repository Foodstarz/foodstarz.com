<?php
return [
    'page_title' => 'User Search',
    'content_title' => 'User Search',
    'page_description' => 'Here you can search for users on your website.',
    
    'search' => 'Search',
    'results' => 'Results',
    
    'search_name' => 'Name',
    'search_email' => 'E-Mail'
];