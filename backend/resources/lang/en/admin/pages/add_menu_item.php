<?php

return [
    'page_title' => 'Add Menu Item',
    'content_title' => 'Add Menu Item',
    'page_description' => 'Here you can add a new menu item.',
    
    'form' => [
        'title' => [
            'label' => 'Title',
            'placeholder' => 'Please enter a title...'
        ],
        
        'url' => [
            'label' => 'URL',
            'placeholder' => 'Please enter an URL...'
        ],
        
        'order' => [
            'label' => 'Order',
            'placeholder' => 'Please enter an order number...'
        ],
        
        'submit' => [
            'label' => 'Submit'
        ]
    ],
    
    'messages' => [
        'success_title' => 'Success!',
        'success' => 'You have successfully added a new menu item to the website!'
    ]
];