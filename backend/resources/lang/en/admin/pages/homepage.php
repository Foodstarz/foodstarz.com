<?php

return [
    
    'page_title' => 'Dashboard',
    'content_title' => 'Dashboard',
    'page_description' => 'Welcome to your Admin Panel!',
    
    'homepage_header' => 'Homepage Header',
    
    'form' => [
        'heading' => [
            'label' => 'Heading',
            'placeholder' => 'Please enter a Heading...'
        ],
        
        'subheading' => [
            'label' => 'Sub-Heading'
        ],

        'subheading_auth' => [
            'label' => 'Sub-Heading for authorized users'
        ],
        
        'submit' => [
            'label' => 'Submit'
        ]
    ],
    
    'messages' => [
        'success_title' => 'Success!',
        'success' => 'You have successfully updated the homepage of the website!'
    ]
    
];