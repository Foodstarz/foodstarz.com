<?php

return [

    'first' => 'First Page',
    'prev' => 'Previous',
    'next'     => 'Next',
    'last' => 'Last Page'

];
