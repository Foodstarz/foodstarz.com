$(function() {
    var ingredientsRow = $('.ingredientRow.sample');
    var instructionRow = $('.instructionRow.sample');
    var addComponentBtn = $('.addComponent');
    var componentClone = $('.component.sample');
    var addImageBtn = $('.addImage');
    var imageRow = $('.imageRow.sample');
    var imagesList = $('.imagesList');
    var addRecipeForm = $('#add_recipe_form');

    function init() {
        $('.addIngredient').unbind().on('click', function (e) {
            e.preventDefault();

            var componentNum = parseInt($(this).parent('fieldset').parent('.component').find('.componentNumber').first().text());

            var new_row = ingredientsRow.clone();
            new_row.html(function (i, oldHtml) {
                return oldHtml.replace(/{num}/g, componentNum);
            });

            var ingredientsList = $(this).parent('fieldset').parent('.component').find('.ingredientsList');

            ingredientsList.append(new_row);

            ingredientsList.find('.ingredientRow.sample').removeClass('sample');

            $('.deleteIngredient').unbind().on('click', function (e) {
                e.preventDefault();
                
                if(confirm('Are you sure?') == false) {
                    return;
                }

                $(this).parent('.ingredientRow').remove();
            });

        });

        $('.deleteIngredient').unbind().on('click', function (e) {
            e.preventDefault();
            
            if(confirm('Are you sure?') == false) {
                return;
            }

            $(this).parent('.ingredientRow').remove();
        });

        $('.addInstruction').unbind().on('click', function(e) {
            e.preventDefault();

            var componentNum = parseInt($(this).parent('fieldset').parent('.component').find('.componentNumber').first().text());

            var new_row = instructionRow.clone();
            new_row.html(function(i, oldHtml) {
                return oldHtml.replace(/{num}/g, componentNum);
            });

            var instructionsList = $(this).parent('fieldset').parent('.component').find('.instructionsList');

            instructionsList.append(new_row);

            instructionsList.find('.instructionRow.sample').removeClass('sample');

            $('.deleteInstruction').unbind().on('click', function (e) {
                e.preventDefault();
                
                if(confirm('Are you sure?') == false) {
                    return;
                }

                $(this).parent('.instructionRow').remove();
            });

        });

        $('.deleteInstruction').unbind().on('click', function (e) {
            e.preventDefault();
            
            if(confirm('Are you sure?') == false) {
                return;
            }

            $(this).parent('.instructionRow').remove();
        });

        $('.deleteComponent').unbind().on('click', function(e) {
            e.preventDefault();
            
            if(confirm('Are you sure?') == false) {
                return;
            }

            var componentsList = $(this).closest('.componentsList');

            $(this).parent('fieldset').parent('.component').remove();

            var components = componentsList.find('.component');
            var i = 1;
            
            components.each(function() {
                var fieldset = $(this).find('fieldset');
                
                fieldset.find('legend').find('.componentNumber').text(i);
                var componentNumText = '';
                if(i == 1) {
                    componentNumText = 'st';
                } else if(i == 2) {
                    componentNumText = 'nd';
                } else if(i == 3) {
                    componentNumText = 'rd';
                } else {
                    componentNumText = 'th';
                }
                fieldset.find('legend').find('.componentNumberText').text(componentNumText);
                
                fieldset.find('input, select').each(function() {
                    if($(this).attr('class') != 'cropit-image-input' && $(this).attr('class') != 'cropit-image-zoom-input')  {
                        var inpName = $(this).attr('name');
                        var newName = inpName.replace( /\d+/g, i );
                        $(this).attr('name', newName);
                    }
                });
                
                fieldset.find('.errDiv').each(function() {
                    var classes = $(this).attr('class');
                    var new_class = classes.replace( /\d+/g, i );
                    $(this).attr('class', new_class);
                });
                
                i++;
            });

            init();

        });

        $('.deleteImage').unbind().on('click', function(e) {
            e.preventDefault();
            $(this).parent('.imageRow').remove();
        });

        $('.image-editor').cropit({
            export: {
                type: 'image/jpeg',
                quality: 1
            },
            previewSize: {
                width: 400,
                height: 400
            },
            exportZoom: 1.875,
            rejectSmallImage: true,
            onImageError: function() {
                alert('The image is too small. (Required at least 750x750px)');
            }
        });
    }

    init();

    addComponentBtn.on('click', function(e) {
        e.preventDefault();

        var componentsList = $(this).prev('.componentsList');
        var componentNum = componentsList.find('.component').length+1;

        var new_component = componentClone.clone();
        new_component.html(function(i, oldHtml) {
            return oldHtml.replace(/{num}/g, componentNum);
        });

        componentsList.append(new_component);

        var added_component = componentsList.find('.component.sample');

        var componentNumText = '';
        if(componentNum == 1) {
            componentNumText = 'st';
        } else if(componentNum == 2) {
            componentNumText = 'nd';
        } else if(componentNum == 3) {
            componentNumText = 'rd';
        } else {
            componentNumText = 'th';
        }

        added_component.find('.componentNumberText').text(componentNumText);

        added_component.removeClass('sample');

        init();
    });

    $('.component_picture_delete').on('click', function(e) {
        e.preventDefault();
        
        var url = $(this).attr('href');
        var compImg = $(this).closest('.compImg');
        
        $.get(url, function(data, status) {
            if(data.ok == true) {
                compImg.remove();
                alert('You have successfully deleted this component\'s image.');
            } else {
                alert('An error occurred. Please try again.');
            }
        });
        
        return false;
    });
    
    var requestLoading = $('.loading');
    
    addRecipeForm.submit(function(e) {
        e.preventDefault();
        
        requestLoading.show();
        
        $('.image-editor').each(function() {
            var imageData = $(this).cropit('export');
            $(this).find('.hidden-image-data').val(imageData);
        });

        $(':input:not(:file)').each(function() {
            if($(this).val() == '') {
                $(this).val(' ');
            }
        });
        
        $('.input-error').removeClass('input-error');
        $('.errDiv').empty();
        
        var actionUrl = $(this).attr('action');
        var postData = new FormData(this);
        
        $.ajax({
            type: 'POST',
            url: actionUrl,
            data: postData,
            dataType: 'json',
            processData: false,
            contentType: false
        }).done(function(responseData) {
            requestLoading.hide();
            
            $(this).find('input[name=_token]').val(responseData._token);

            if(responseData.ok === false) {
                if(typeof(responseData.errors) !== 'undefined' && Object.keys(responseData.errors).length > 0) {
                    var errorsList = responseData.errors;
                    var html = '';
                    
                    $(':input:not(:file)').each(function() {
                        if($(this).val() == ' ') {
                            $(this).val('');
                        }
                    });
                    
                    $.each(errorsList, function(key, value) {
                        $('[name='+key+']').addClass('input-error');
                        $('div.'+key+'_errors').append('<span class="error-msg">'+value+'</span><br>');
                    });
                    
                    alert('There are some errors with the entered data. Please check the form again and try again.');
                }
            } else {
                addRecipeForm.hide();
                $('.alert-box.success').append(''+responseData.msg);
                $('.alert-box.success').show();
            }
            
            $("html, body").animate({ scrollTop: 0 }, "slow");
        });
        
    });

});
