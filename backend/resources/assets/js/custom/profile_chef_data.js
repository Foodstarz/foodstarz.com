function monthYearPickerInit() {
    $('.monthYearPicker').removeClass('hasDatepicker');
    $('.monthYearPicker').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
        inline: true,
        minDate: new Date(1950, 1, 1),
        maxDate: new Date,
        yearRange: "-100:+0",
        beforeShow: function (input, inst) {
            /*
            var offset = $(input).offset();
            window.setTimeout(function () {
                var calWidth = inst.dpDiv.width();
                var inpWidth = $(input).width() + parseInt($(input).css('padding'));
                inst.dpDiv.css({ left: offset.left + (inpWidth - calWidth) + 'px' })
            }, 1);
            */
           
        }
    }).focus(function() {
        var thisCalendar = $(this);
        $('.ui-datepicker-calendar').detach();
        $('.ui-datepicker-close').click(function() {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            thisCalendar.datepicker('setDate', new Date(year, month, 1));
        });
    });
    
    $('.startDate, .endDate').on('click', function() {
        var offset = $(this).offset().top;
        
        var origStyleContent = $('#ui-datepicker-div').attr('style');
        $('#ui-datepicker-div').attr('style',origStyleContent+';top:'+offset+'px !important');
        
    });
}

$(function() {
    monthYearPickerInit();

    $('.still_working').on('click', function() {
        var checkbox = $(this);
        var post_item = checkbox.closest('.post_item');
        if(checkbox.is(':checked')) {
            post_item.find('label[for="endYear"]').hide();
            post_item.find('select[name=endYear]').hide();
            post_item.find('select[name=endMonth]').hide();
        } else {
            post_item.find('label[for="endYear"]').show();
            post_item.find('select[name=endYear]').show();
            post_item.find('select[name=endMonth]').show();
        }
    });

    $('select[name=startYear], select[name=startMonth]').change(function() {
        var dp = $(this).closest('.datePick');
        var sYearVal = dp.find('select[name=startYear]').val();
        var sMonthVal = dp.find('select[name=startMonth]').val();
        dp.find('.wexp_from').val(sMonthVal+' '+sYearVal);
    });

    $('select[name=endYear], select[name=endMonth]').change(function() {
        var dp = $(this).closest('.datePick');
        var sYearVal = dp.find('select[name=endYear]').val();
        var sMonthVal = dp.find('select[name=endMonth]').val();
        dp.find('.wexp_to').val(sMonthVal+' '+sYearVal);
    });

    $('#add_experience').on('click', function() {
        var newExp = $('.work_experience.hidden_example').clone();
        var itemsDiv = $('#work_experience').find('.items');
        newExp.appendTo(itemsDiv);

        var newEl = itemsDiv.find('.work_experience.hidden_example');
        newEl.removeClass('hidden_example').removeClass('work_experience').addClass('post_item');

        monthYearPickerInit();

        $('.still_working').unbind().on('click', function() {
            var checkbox = $(this);
            var post_item = checkbox.closest('.post_item');
            if(checkbox.is(':checked')) {
                post_item.find('label[for="endYear"]').hide();
                post_item.find('select[name=endYear]').hide();
                post_item.find('select[name=endMonth]').hide();
            } else {
                post_item.find('label[for="endYear"]').show();
                post_item.find('select[name=endYear]').show();
                post_item.find('select[name=endMonth]').show();
            }
        });

        $('select[name=startYear], select[name=startMonth]').unbind().change(function() {
            var dp = $(this).closest('.datePick');
            var sYearVal = dp.find('select[name=startYear]').val();
            var sMonthVal = dp.find('select[name=startMonth]').val();
            dp.find('.wexp_from').val(sMonthVal+' '+sYearVal);
        });

        $('select[name=endYear], select[name=endMonth]').unbind().change(function() {
            var dp = $(this).closest('.datePick');
            var sYearVal = dp.find('select[name=endYear]').val();
            var sMonthVal = dp.find('select[name=endMonth]').val();
            dp.find('.wexp_to').val(sMonthVal+' '+sYearVal);
        });

        return false;
    });

    $('#add_award').on('click', function() {
        var newAward = $('.awards.hidden_example').clone();
        var itemsDiv = $('#awards').find('.items');
        newAward.appendTo(itemsDiv);

        var newEl = itemsDiv.find('.awards.hidden_example');
        newEl.removeClass('hidden_example').removeClass('awards').addClass('post_item');

        return false;
    });

    $('#add_organisation').on('click', function() {
        var newOrg = $('.organisations.hidden_example').clone();
        var itemsDiv = $('#organisations').find('.items');
        newOrg.appendTo(itemsDiv);

        var newEl = itemsDiv.find('.organisations.hidden_example');
        newEl.removeClass('hidden_example').removeClass('organisations').addClass('post_item');

        return false;
    });

    $('#add_role_model').on('click', function() {
        var newRoleModel = $('.role_models.hidden_example').clone();
        var itemsDiv = $('#role_models').find('.items');
        newRoleModel.appendTo(itemsDiv);

        var newEl = itemsDiv.find('.role_models.hidden_example');
        newEl.removeClass('hidden_example').removeClass('role_models').addClass('post_item');

        return false;
    });

    $('body').on('click', '.remove_post_item', function() {
        $(this).closest('.post_item').remove();

        return false;
    });
});