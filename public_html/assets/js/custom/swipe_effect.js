$(function() {
    
    var topMenu = $('.right_menu');
    
    $('body').hammer({
  behavior: {
    userSelect: true
  }
}).on('swipedown', function() {
        topMenu.slideDown();
    });
    
    $('body').hammer({
  behavior: {
    userSelect: true
  }
}).on('swipeup', function() {
        topMenu.slideUp();
    });
    
});