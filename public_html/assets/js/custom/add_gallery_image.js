function readURL(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#previewImg').attr('src', e.target.result).show();
        }

        reader.readAsDataURL(input.files[0]);
    }
}

(function($) {
    $.fn.goTo = function() {
        $('html, body').animate({
            scrollTop: $(this).offset().top + 'px'
        }, 'fast');
        return this; // for chaining...
    }
})(jQuery);

$(function() {
    var galleryForm = $('#gallery_image_form');
    var requestLoading = $('.loading');

    galleryForm.find('fieldset').goTo();

    $('input[name="image"]').change(function(){
        readURL(this);
    });

    galleryForm.submit(function(e) {
        e.preventDefault();
        requestLoading.show();

        $('.input-error').removeClass('input-error');
        $('.errDiv').empty();

        var actionUrl = $(this).attr('action');
        var postData = new FormData(this);

        $.ajax({
            type: 'POST',
            url: actionUrl,
            data: postData,
            dataType: 'json',
            processData: false,
            contentType: false
        }).done(function(responseData) {
            requestLoading.hide();

            $(this).find('input[name=_token]').val(responseData._token);

            if(responseData.ok === false) {
                if(typeof(responseData.errors) !== 'undefined' && Object.keys(responseData.errors).length > 0) {
                    var errorsList = responseData.errors;
                    var html = '';

                    $.each(errorsList, function(key, value) {
                        $('[name='+key+']').addClass('input-error');
                        $('div.'+key+'_errors').append('<span class="error-msg">'+value+'</span><br>');
                    });
                }
            } else {
                galleryForm.hide();
                $('.alert-box.success').append(''+responseData.msg);
                $('.alert-box.success').show();
                $('section.content-wrapper').goTo();

                setTimeout(function() {
                    window.location = ""+responseData.redirect;
                }, 5000);
            }
        });
    });
});