$('.admin-actions > a').on('click', function() {
   if($(this).attr('not-ajax') == undefined) {
       var link = $(this),
           url = link.attr('href'),
           token = link.attr('token');
       $.ajax({
           url: url,
           method: 'post',
           data: '_token=' + token,
           dataType: 'json',
           success: function(response) {
               if(response.status == '1') {
                   if(response.instruct == 'reload') window.location.reload();
                   else {
                       link.attr(response.instruct, 'action-' + response.class);
                       link.attr('href', response.href);
                   }
               } else alert(response.message);
           }
       });
       return false;
   }
});