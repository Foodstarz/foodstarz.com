<?php
if( !class_exists('AttichThemes_PluginUpdater') ) {
	class AttichThemes_PluginUpdater {
		public $plugin_slug = '';

		public function __construct( $_file ) {
			$this->plugin_slug = basename(dirname( $_file ));
			$this->plugin_path = plugin_basename( $_file );

			//error_log( var_export($this->plugin_path, true) );
			add_filter( 'plugins_api', array( $this, 'plugins_api' ), 10, 3 );
			add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'transient_update_plugins' ) );
		}

		public function plugins_api( $false, $action, $args ) {
			// Check if this plugins API is about this plugin
			//error_log( var_export($args->slug.' : '.$this->plugin_slug, true) );

			if( $args->slug != $this->plugin_slug ) {
				return $false;
			}
			// POST data to send to your API
			$req_args = array(
				'action' => 'get-plugin-information',
				'plugin' => $this->plugin_slug
			);
			// Send request for detailed information
			$response = $this->plugin_api_request( $req_args );
			
			return $response;
		} // END plugins_api

		public function transient_update_plugins( $transient ) {
			// Check if the transient contains the 'checked' information
			// If no, just return its value without hacking it
			//error_log( var_export($transient, true) );

			if ( empty( $transient->checked ) ) { return $transient; }

			// The transient contains the 'checked' information
			// Now append to it information form your own API
			
			// POST data to send to your API
			$args = array(
				'action' => 'check-latest-version',
				'plugin' => $this->plugin_slug,
				'path' => $this->plugin_path
			);
			// Send request checking for an update
			$response = $this->plugin_api_request( $args );
			//error_log( var_export($transient, true) );
			// If there is a new version, modify the transient
			if( $response && version_compare( $response->new_version, $transient->checked[$this->plugin_path], '>' ) ) {
				$transient->response[$this->plugin_path] = $response;
			}
			return $transient;
		} // END transient_update_plugins

		public function plugin_api_request( $args ) {
			// Send request	
			$request = wp_remote_post( 'http://download.atticthemes.com/updates', array( 'body' => $args ) );
			//error_log( var_export($request, true) );
			if ( is_wp_error( $request ) || 200 != wp_remote_retrieve_response_code( $request ) ) {
				return false;
			}
			$response = maybe_unserialize( wp_remote_retrieve_body( $request ) );
			if ( is_object( $response ) ) {
				return $response;
			} else {
				return false;
			}
		} // END plugin_api_request

	} //END Class
}
?>