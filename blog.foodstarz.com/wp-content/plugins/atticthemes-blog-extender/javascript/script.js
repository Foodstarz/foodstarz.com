var atbe_settings = {};

(function($) {

	atbe_settings = $.extend(true, atbe_data, atbe_settings );
	//console.log(atbe_settings);

	$(document).ready(function(){
		$('body').on('click', '.atbe-likes', function(){
			var button = $(this);
			var post_id = parseInt( $(this).attr('data-post-id') );

			if( !button.hasClass('atbe-liked') ) {
				button.addClass('atbe-liking');
				add_like(post_id, function( count ) {
					setTimeout(function() {
						button.removeClass('atbe-liking');
						button.addClass('atbe-liked');
						$('.atbe-count', button).text( count );
					}, 600);
				});
			} else {
				button.addClass('atbe-unliking');
				remove_like(post_id, function( count ) {
					setTimeout(function() {
						button.removeClass('atbe-unliking');
						button.removeClass('atbe-liked');
						$('.atbe-count', button).text( count );
					}, 600);
				});
			}
		});

		applySharingButtons();
	});// end document ready

	$(window).load(function(){
		if( atbe_settings.smart_view ) {
			init_smart_views();
		}
	});

	function init_smart_views() {
		var viewed = false;
		$(window).scroll(function(){
			post_viewed();
		});

		function post_viewed() {
			var scrollTop = $(window).scrollTop();
			var docHeight = $(document).height();
			var winHeight = $(window).height();
			var scrollOffset = parseFloat(atbe_settings.smart_view_offset) ? parseFloat(atbe_settings.smart_view_offset) : 0;
			var indicatorOffset = $('.atbe-content-post-end').offset().top - 50;

			var perc = Math.ceil(scrollTop / (docHeight-winHeight)  * 100);
			//console.log(scrollOffset);

			if( (perc >= scrollOffset || (scrollTop + winHeight) >= indicatorOffset) && !viewed ) {
				update_view();
				viewed = true;
			}
		}
		post_viewed();
	}

	function update_view() {
		add_view( function( count ) {
			$('.atbe-views .atbe-count').text( count );
			//console.log('updating views text');
		});
	}












	atbe_settings.remove_like = remove_like;
	atbe_settings.add_like = add_like;

	function remove_like( post_id, callback ) {
		if( post_id === undefined && !atbe_settings.post_id ) return;
		//console.log('removing a like...');

		request({
			post_id: post_id ? post_id : false,
			action: atbe_settings.likes.remove_action,
			nonce: atbe_settings.likes.nonce,
			success: function( response ) {
				//console.log( response );
				if( response && response.message && response.message === 'success' ) {
					if( callback ) {
						callback( response.counter );
					}
				}
			}
		});
	}//remove_like

	function add_like( post_id, callback ) {
		if( post_id === undefined && !atbe_settings.post_id ) return;
		//console.log('adding like...');

		request({
			post_id: post_id ? post_id : false,
			action: atbe_settings.likes.add_action,
			nonce: atbe_settings.likes.nonce,
			success: function( response ) {
				//console.log( response );
				if( response && response.message && response.message === 'success' ) {
					if( callback ) {
						callback( response.counter );
					}
				}
			}// end success
		});
	}//add_like
	






	function add_view( callback ) {
		if(!atbe_settings.post_id || atbe_settings.is_logedin) return;
		//console.log('adding view...');

		request({
			action: atbe_settings.views.add_action,
			nonce: atbe_settings.views.nonce,
			success: function( response ) {
				//console.log( response );
				if( response && response.message && response.message === 'success' ) {
					if( callback ) {
						callback( response.counter );
					}
				}
			}
		});
	}//add_view





	function request( settings ) {
		if( !settings.action || !settings.nonce ) {
			if( settings.success && typeof(settings.success) === 'function' ) settings.success( response );
			return false;
		}

		$.ajax({
			type : 'get',
			dataType : 'json',
			url : atbe_settings.ajax_url,
			data : {
				action: settings.action,
				nonce: settings.nonce,
				post_id: settings.post_id !== undefined ? parseInt(settings.post_id) : atbe_settings.post_id
			},
			success: function( response ) {
				console.log(response);
				if( response && response.message && response.message === 'success' ) {
					if( settings.success && typeof(settings.success) === 'function' ) settings.success( response );
				} else {
					return false;
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//console.log(jqXHR, textStatus, errorThrown);
			}
		});
	}




























	function applySharingButtons() {
		//console.log('hello');
		$('.share-facebook').on('click', function() {
			var data_share = $(this).parent('.atbe-share-box').attr('data-share-url');
			var url = data_share !== undefined ? data_share : window.location.href;

			share('http://www.facebook.com/sharer/sharer.php?u='+url, 'Share on Facebook');
			//console.log('facebook');
			return false;
		});

		$('.share-google-plus').on('click', function() {
			var data_share = $(this).parent('.atbe-share-box').attr('data-share-url');
			var url = data_share !== undefined ? data_share : window.location.href;

			share('https://plus.google.com/share?url='+url, 'Share on Google Plus');
			return false;
		});

		$('.share-twitter').on('click', function() {
			var data_share = $(this).parent('.atbe-share-box').attr('data-share-url');
			var url = data_share !== undefined ? data_share : window.location.href;

			var data_title = $(this).parent('.atbe-share-box').attr('data-share-title');
			var post_title = data_title !== undefined ? (data_title + ' ') : '';
			var status = post_title + url;

			share('http://twitter.com/home?status='+status, 'Twitter');
			return false;
		});

		$('.share-pinterest').on('click', function() {
			var data_share = $(this).parent('.atbe-share-box').attr('data-share-url');
			var url = data_share !== undefined ? data_share : window.location.href;

			var data_title = $(this).parent('.atbe-share-box').attr('data-share-title');
			var post_title = data_title !== undefined ? data_title : '';

			var data_media = $(this).parent('.atbe-share-box').attr('data-share-media');
			var media = data_media !== undefined ? data_media : '';

			var data_excerpt = $(this).parent('.atbe-share-box').attr('data-share-excerpt');
			var excerpt = data_excerpt !== undefined && data_excerpt !== '' ? (' - ' + data_excerpt) : '';

			share('http://www.pinterest.com/pin/create/button/?url='+url+'&media='+media+'&description='+post_title + excerpt, 'Pin It');
			return false;
		});

		var shareWindow = null;
		var prevUrl;

		function share(url, window_title) {
			if( shareWindow === null || shareWindow.closed ) {
				shareWindow = window.open( url, window_title, 'width=640,height=300' );
			} else if( prevUrl !== url ) {
				shareWindow = window.open( url, window_title, 'width=640,height=300' );
				shareWindow.focus();
			} else {
				shareWindow.focus();
			}
			prevUrl = url;
		}
	}

})(jQuery);