<?php
/*
Plugin Name: AtticThemes: Blog Extender
Plugin URI: http://atticthemes.com
Description: Plugin that adds page view counter and like system to blog posts.
Version: 1.0.2
Author: atticthemes
Author URI: http://themeforest.net/user/atticthemes
Requires: 4.0.0
Tested: 4.2.0
Updated: 2015-04-25
Added: 2014-02-01
*/
?>
<?php
if( !class_exists('AtticThemes_BlogExtender') ) {

	class AtticThemes_BlogExtender {
		public $version = '1.0.1';
		public $plugin;
		public $theme;

		protected $dev = false;
		private $min_suffix = '.min';

		protected static $counters;
		public static $settings = array();
		public static $post_types = array();

		public static function Init() {
			new AtticThemes_BlogExtender();
		}

		public function __construct() {
			$this->theme = wp_get_theme();
			if( $this->dev ) {
				$this->min_suffix = '';
			}

			self::addPostTypeSupport( 'post' );

			$defaults = array(
				'display' => true,
				'smart-view' => false,
				'smart-view-offset' => 10
			);
			self::$settings = array_merge($defaults, self::$settings);

			/* init the counters */
			self::$counters = array(
				'views' => new AtticThemes_PostCounter( array( 'name' => 'post_views' ) ),
				'likes' => new AtticThemes_PostCounter( array( 'name' => 'post_likes', 'loggedin' => true ) ),
			);

			if( !self::$settings['smart-view'] ) {
				remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
				add_action( 'wp', array( $this, 'add_dumb_view' ) );
			}

			/* add filters */
			add_filter( 'the_content', array( $this, 'the_content_filter' ) );
			/* add actions */
			add_action( 'save_post', array( $this, 'save_post' ) );
			add_action( 'admin_init', array( $this, 'admin_init' ) );

			/* scripts and styles */
			add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_front_end_scripts_and_style' ) );

			
		}

		

		public function admin_init() {
			//error_log(class_exists('AtticThemes_BoxBuilder'));
			if( class_exists('AtticThemes_BoxBuilder') ) {
				/*AtticThemes_BoxBuilder::addResource(
					'atbe-admin-script',
					plugins_url( 'javascript/atbe-admin-script'.$this->min_suffix.'.js' , __FILE__ ),
					'script',
					$this->version
				);*/
			}

			if( get_option( 'atbe_activated', false ) ) {
				update_option( 'atbe_activated', false );
				self::add_meta_keys();
			}
		}

		public function save_post( $post_id ) {
			$post_type = get_post_type( $post_id );

			if( !in_array( $post_type, self::$post_types ) ) return;
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

			self::update_total_word_count();
		}

		public static function update_total_word_count() {
			$all_posts = new WP_Query( array('post_type' => 'post', 'posts_per_page' => -1) );

			$total_count = 0;
			while( $all_posts->have_posts() ) { $all_posts->the_post();
				$count = 0;
				//get the content of the post
				$content = get_post_field( 'post_content', $all_posts->post->ID, 'display' );
				//remove all html tags
				$content = strip_tags( $content );
				//remove all the shortcodes from content
				$content = strip_shortcodes( $content );
				//remove punctuations
				$content = preg_replace('/[0-9.(),;:!?%#$¿\'"_+=\\/-]+/', '', $content);
				//replace/remove spaces between words to count the number of words
				$content = preg_replace('/\S\s+/', '', $content, -1, $count);

				$total_count = $total_count + $count;
			}

			update_option( 'atbe_total_words', $total_count );

			wp_reset_postdata();
		}





		public static function add_meta_keys() {
			//error_log( var_dump(self::$post_types, true) );

			$all_posts = new WP_Query( array('post_type' => self::$post_types, 'posts_per_page' => -1) );
			while( $all_posts->have_posts() ) { $all_posts->the_post();
				foreach (self::$counters as $counter) {
					if( !$counter->get_count($all_posts->post->ID) ) {
						$counter->set_count( $all_posts->post->ID, 0 );
					}
				}
			}
			wp_reset_postdata();
		}





		public static function addPostTypeSupport( $post_types ) {
			if( is_array($post_types) ) {
				self::$post_types = array_merge( self::$post_types, $post_types );
			} elseif( is_string($post_types) ) {
				self::$post_types = array_merge( self::$post_types, array($post_types) );
			}
		}

		public static function GetShareIcons( $post_id = null, $icons = null ) {
			if( !$post_id || (!$icons && !empty($icons) && is_array($icons)) ) return;
			$the_post = get_post( $post_id );
			
			$output = '<div data-share-url="'.get_permalink($post_id).'" data-share-title="'.esc_attr( get_the_title($post_id) ).'" data-share-media="'.wp_get_attachment_url( get_post_thumbnail_id($post_id) ).'" data-share-excerpt="'.esc_attr($the_post->post_excerpt).'" class="atbe-share-box">';
			foreach ($icons as $icon) {
				$output .= '<div class="share-'.$icon.' fa fa-'.$icon.'"></div>';
			}
			$output .= '</div>';
			return $output;
		}

		public static function GetCount( $post_id = null, $name = null ) {
			if( !isset($post_id) || !isset($name) ) return null;

			if( isset(self::$counters[$name]) && method_exists(self::$counters[$name], 'get_count') ) {
				return self::$counters[$name]->get_count( $post_id );
			} else {
				return null;
			}
		}

		public static function IsReturning( $post_id = null, $name = null ) {
			if( !isset($post_id) || !isset($name) ) return -1;

			if( isset(self::$counters[$name]) && method_exists(self::$counters[$name], 'is_returning') ) {
				return intval( self::$counters[$name]->is_returning( $post_id ) );
			} else {
				return -1;
			}
		}

		public function enqueue_front_end_scripts_and_style() {
			global $post;

			if( ($post && isset($post->ID) && in_array(get_post_type($post->ID), self::$post_types) || is_home() ) ) {
				/* styles */
				if( self::$settings['display'] ) {
					wp_register_style(
						'atbe-main-style',
						plugins_url( 'css/style'.$this->min_suffix.'.css', __FILE__ ),
						array(),
						$this->version
					);
					wp_enqueue_style( 'atbe-main-style' );
				}

				/* scripts */
				wp_register_script(
					'atbe-main-script',
					plugins_url( 'javascript/script'.$this->min_suffix.'.js', __FILE__ ),
					array( 'jquery' ),
					$this->version,
					true
				);
				wp_enqueue_script( 'atbe-main-script' );

				wp_localize_script( 'atbe-main-script', 'atbe_data', array(
						'ajax_url' => admin_url( 'admin-ajax.php' ),
						'views' => self::$counters['views'],
						'likes' => self::$counters['likes'],
						'post_id' => isset($post) && isset($post->ID) && is_single($post->ID) ? $post->ID : null,
						'is_logedin' => is_user_logged_in() ? true : null,
						'is_single' => is_single() ? true : null,
						'smart_view_offset' => self::$settings['smart-view-offset'],
						'smart_view' => self::$settings['smart-view'] ? true : null,
					)
				);
			}//end if
		}

		public function the_content_filter( $content ) {
			global $post;

			if( !isset($post->ID) ) return $content;
			if( !in_array(get_post_type(), self::$post_types) ) return $content;

			if( self::$settings['smart-view'] ) {
				$content = $content . '<span class="atbe-content-post-end"></span>';
			}

			if( self::$settings['display'] ) {
				$views = AtticThemes_BlogExtender::GetCount( $post->ID, 'views' );
				$likes = AtticThemes_BlogExtender::GetCount( $post->ID, 'likes' );
				$liked = AtticThemes_BlogExtender::IsReturning( $post->ID, 'likes' ) ? 'atbe-liked' : '';

				$content .= '<div class="atbe-container">';
				$content .= '<div class="atbe-views">'. sprintf(__('Views <span class="atbe-count">%s</span>', 'atbe'), $views ? $views : 0) .'</div>';
				$content .= '<div class="atbe-likes '. $liked .'" data-post-id="'.$post->ID.'">'. sprintf(__('Likes <span class="atbe-count">%s</span>', 'atbe'), $likes ? $likes : 0) .'</div>';
				$content .= '</div>';
			}
			return $content;
		}

		public function add_dumb_view() {
			global $post;
			if( isset($post) && isset($post->ID) && is_single() ) {
				self::$counters['views']->add_count( $post->ID );
			}
		}

	}

	function atbe_after_setup_theme() {
		AtticThemes_BlogExtender::Init();
	}

	function atbe_activated() {
		AtticThemes_BlogExtender::update_total_word_count();

		/* init on activation and add the keys to post meta */
		update_option( 'atbe_activated', true );
	}

	register_activation_hook( __FILE__, 'atbe_activated' );

	add_action( 'after_setup_theme', 'atbe_after_setup_theme' );
}































if( !class_exists('AtticThemes_PostCounter') ) {

	class AtticThemes_PostCounter {
		public $settings;
		private $nonce_string;
		private $cookie;
		private $meta_namespace;

		function __construct( $settings ) {
			if( !isset($settings['name']) ) return;

			$defaults = array(
				'multiple' => false,
				'loggedin' => false,
			);

			$this->settings = array_merge( $defaults, $settings );
			$this->nonce_string = 'Atbe-' . $this->settings['name'] . '-Nonce-Ajax';
			$this->nonce = wp_create_nonce( $this->nonce_string );
			$this->cookie = 'wordpress_atbe_' . $this->settings['name'];
			$this->meta_namespace = 'atbe_' . $this->settings['name'];
			$this->add_action = 'atbe_add_' . $this->settings['name'];
			$this->remove_action = 'atbe_remove_' . $this->settings['name'];

			/* ajax actions */

			/* add */
			add_action( 'wp_ajax_nopriv_' . $this->add_action, array( $this, 'add_count_ajax') );
			add_action( 'wp_ajax_' . $this->add_action, array( $this, 'add_count_ajax') );
			/* remove */
			add_action( 'wp_ajax_nopriv_' . $this->remove_action, array( $this, 'remove_count_ajax') );
			add_action( 'wp_ajax_' . $this->remove_action, array( $this, 'remove_count_ajax') );
		}


		public function get_count( $post_id ) {
			return get_post_meta($post_id, $this->meta_namespace, true);
		}


		public function add_count_ajax() {
			if ( !isset($_REQUEST['nonce']) || (isset($_REQUEST['nonce']) && !wp_verify_nonce( $_REQUEST['nonce'], $this->nonce_string)) ) {
				exit('Do not even try!');
			}

			if( isset($_REQUEST['post_id']) && !empty($_REQUEST['post_id']) ) {
				$post_id = $_REQUEST['post_id'];
				$count = $this->add_count( $post_id );

				if( isset($count) ) {
					echo json_encode( array('message' => 'success', 'counter' => $count) );
				} else {
					echo json_encode( array('message' => 'error') );
				}
			}
			die;
		}

		public function remove_count_ajax() {
			if ( !isset($_REQUEST['nonce']) || (isset($_REQUEST['nonce']) && !wp_verify_nonce( $_REQUEST['nonce'], $this->nonce_string)) ) {
				exit('Do not even try!');
			}

			if( isset($_REQUEST['post_id']) && !empty($_REQUEST['post_id']) ) {
				$post_id = $_REQUEST['post_id'];
				$count = $this->remove_count( $post_id );

				if( isset($count) ) {
					echo json_encode( array('message' => 'success', 'counter' => $count) );
				} else {
					echo json_encode( array('message' => 'error') );
				}
			}
			die;
		}

		public function set_count( $post_id, $count = 0 ) {
			update_post_meta($post_id, $this->meta_namespace, $count);
		}

		public function add_count( $post_id = null ) {
			if( is_user_logged_in() && !$this->settings['loggedin'] ) return null;

			if( $this->settings['multiple'] ) {
				$this->unset_cookie( $post_id );
			}

			$count = get_post_meta($post_id, $this->meta_namespace, true);

			//error_log(var_export($count, true));

			if ( $this->is_returning(intval($post_id)) ) {
				return intval($count);
			}
			
			if( $count ) {
				$count = $count + 1;
			} else {
				$count = 1;
			}

			update_post_meta($post_id, $this->meta_namespace, $count);

			/* update the total */
			$total = get_option( $this->meta_namespace, 0 );
			update_option( $this->meta_namespace, $total + 1 );

			if( !$this->settings['multiple'] ) {
				$this->set_cookie( $post_id );
			}
			
			return intval($count);
		}

		public function remove_count( $post_id = null ) {
			if( is_user_logged_in() && !$this->settings['loggedin'] ) return null;

			if( $this->settings['multiple'] ) {
				$this->unset_cookie( $post_id );
			}

			$count = get_post_meta($post_id, $this->meta_namespace, true);
			if ( !$this->is_returning(intval($post_id)) ) {
				return intval($count);
			}
			
			if( $count ) {
				$count = $count - 1;
			} else {
				$count = 0;
			}

			update_post_meta($post_id, $this->meta_namespace, $count);

			/* update the total */
			$total = get_option( $this->meta_namespace, 0 );
			if( intval($total) > 0 ) {
				update_option( $this->meta_namespace, $total - 1 );
			}
			

			$this->unset_cookie( $post_id );

			if( !$this->settings['multiple'] ) {
				$this->set_cookie( $post_id );
			}
			
			return intval($count);
		}



		protected function set_cookie( $post_id = null ) {
			if ( !is_user_logged_in() || $this->settings['loggedin'] ) {
				$posts_list = isset($_COOKIE[$this->cookie]) ? $_COOKIE[$this->cookie] : '';
				$posts = !empty($posts_list) ? explode( ',', $posts_list ) : array();

				if( !in_array($post_id, $posts) ) {
					$posts[] = $post_id;
					setcookie( $this->cookie, implode(',',$posts), time()+3600*24*365, COOKIEPATH, COOKIE_DOMAIN, false, true );
				}
			}
		}

		protected function unset_cookie( $post_id = null ) {
			if ( !is_user_logged_in() || $this->settings['loggedin'] ) {
				$posts_list = isset($_COOKIE[$this->cookie]) ? $_COOKIE[$this->cookie] : '';
				$posts = !empty($posts_list) ? explode( ',', $posts_list ) : array();

				if( in_array($post_id, $posts) ) {
					$posts = array_diff( $posts, array($post_id) );
					setcookie( $this->cookie, implode(',',$posts), time()+3600*24*365, COOKIEPATH, COOKIE_DOMAIN, false, true );
				}
			}
		}

		







		public function is_returning( $post_id = null ) {
			$posts_list = isset($_COOKIE[$this->cookie]) ? $_COOKIE[$this->cookie] : '';
			$posts = !empty($posts_list) ? explode( ',', $posts_list ) : array();

			if( !empty($posts) && in_array($post_id, $posts) ) {
				return true;
			} else {
				return false;
			}
		}

	} //END class

}






/* add/register widgets script */
require_once( plugin_dir_path( __FILE__ ) . 'includes/widgets.php' );

/* init updater */
require_once( plugin_dir_path( __FILE__ ) . 'updater.php' );
if( class_exists('AttichThemes_PluginUpdater') ) {
	new AttichThemes_PluginUpdater( __FILE__ );
}