<?php
class ATBE_Accomplishments_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'atbe_accomplishments', // Base ID
			__('AtticThemes: Accomplishments', 'atbe'), // Name
			array( 'description' => __( 'This widget allows you to show your blog\'s accomplishments.', 'atbe' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		//delete_option('atbe_post_likes');

		$title = apply_filters( 'widget_title', $instance['title'] );
		$value = 0;

		echo $args['before_widget'];
		//
		if ( !empty($title) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}
		
		switch( $instance['accop'] ) {
			case 'post-views':
				$value = get_option( 'atbe_post_views', 0 );
			break;

			case 'post-likes':
				$value = get_option( 'atbe_post_likes', 0 );
			break;

			case 'post-count':
				$count_posts = wp_count_posts('post');
				$value = $count_posts->publish;
			break;

			case 'post-words':
				$value = get_option('atbe_total_words', 0);
			break;
		}
		?><div data-value="<?php echo $value; ?>" class="atbe-accomplishment-value"><?php echo $value; ?></div><?php
		//
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$accops = array(
			'post-views' => __('Total Post Views', 'atbe'),
			'post-likes' => __('Total Post Likes', 'atbe'),
			'post-count' => __('Total Posts', 'atbe'),
			'post-words' => __('Total Amount Words', 'atbe'),
		);
		?>
		<p>
			<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr(isset($instance[ 'title' ]) ? $instance[ 'title' ] : '' ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'accop' ); ?>"><?php _e( 'Accomplishment:' ); ?></label>
			<select class="widefat" id="<?php echo $this->get_field_id( 'accop' ); ?>" name="<?php echo $this->get_field_name( 'accop' ); ?>">
				<?php foreach ($accops as $key => $value) {
					$selected = isset($instance['accop']) && $instance['accop'] === $key ? 'selected' : '';
					?><option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo esc_attr( $value ); ?></option><?php
				} ?>
			</select>
		</p>
		<?php 
	}

	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = !empty($new_instance['title']) ? strip_tags($new_instance['title']) : '';
		$instance['accop'] = !empty($new_instance['accop']) ? $new_instance['accop'] : '';

		return $instance;
	}
}


function atbe_register_widgets() {
	register_widget( 'ATBE_Accomplishments_Widget' );
}
add_action( 'widgets_init', 'atbe_register_widgets' );
?>