			<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
			<?php $sidebar_nav = EasyFramework::EasyNav( 'header-location', 5, false, 'sidebar-navigation' ); ?>
			<?php $use_sidebar = is_active_sidebar( 'skoty-main-sidebar' ) || !empty($sidebar_nav) ? true : false; ?>

			<footer id="footer">
				<?php EasyFramework::EasyPostLinks( 'atp_project' ); ?>
				
				<?php if( EasyFramework::HasPagination() ) : ?>

					<?php if( EasyFramework::EasyOption( 'pagination-system', 'pagination' ) === 'pagination' ) : ?>
					<div class="footer-pagination-wrapper">
						<?php EasyFramework::EasyPagination(); ?>
					</div>
					<?php else: ?>
						<?php if( EasyFramework::EasyOption('load-more-label-style', 'plus') === 'plus' ) : ?>
							<div class="footer-load-more-wrapper">
								<a class="load-more" href="#load-more" title="<?php echo esc_attr(EasyFramework::EasyOption('load-more-label', __('Load More Articles', 'skoty'))); ?>"><span class="fa fa-plus"></span></a>
							</div>
						<?php elseif( EasyFramework::EasyOption('load-more-label-style', 'plus') === 'text' ): ?>
							<div class="footer-load-more-wrapper">
								<a class="load-more" href="#load-more" title="<?php echo esc_attr(EasyFramework::EasyOption('load-more-label', __('Load More Articles', 'skoty'))); ?>"><?php echo EasyFramework::EasyOption('load-more-label', __('Load More Articles', 'skoty')); ?></a>
							</div>
						<?php endif; ?>	
					<?php endif; ?>

				<?php endif; ?>



				<?php if( is_active_sidebar( 'skoty-accomplishment-sidebar' ) ) : ?>
					<div class="footer-accomplishments-wrapper">
						<div class="footer-accomplishments">
							<div class="footer-accomplishments-widgets">
								<ul class="widgets <?php echo EasyFramework::EasyOption('accomplishment-layout', 'one-fourths'); ?>">
									<?php dynamic_sidebar( 'skoty-accomplishment-sidebar' ); ?>
								</ul>
							</div>
						</div>
					</div>
				<?php endif; ?>


				<?php if( is_active_sidebar( 'skoty-footer-sidebar' ) ) : ?>
					<div class="footer-widgets-wrapper">
						<div class="footer-widgets">
							<ul class="widgets <?php echo EasyFramework::EasyOption('footer-layout', 'one-half-one-fourths'); ?>">
								<?php dynamic_sidebar( 'skoty-footer-sidebar' ); ?>
							</ul>
						</div>
					</div>
				<?php endif; ?>




				<div class="footer-content-container">
					<div class="footer-content-wrapper">
						<a href="#top" class="to-top fa fa-angle-up"></a>
						<div class="footer-content"><?php echo EasyFramework::EasyOption('footer-end-text', __('Thanks for the visit.', 'skoty')); ?></div>
					</div>
					<div class="footer-copyright-wrapper">
						<div class="footer-copyright"><?php echo EasyFramework::EasyOption( 'copyright-notice', __('&copy; Copyright 2014. All rights reserved. Designed with passion by atticthemes.', 'skoty') ); ?></div>
					</div>
				</div>
			</footer>

		</div>

		<?php if( $use_sidebar ) : ?>
			<div class="main-sidebar">
				<div class="main-sidebar-wrapper">
					<?php if( !empty($sidebar_nav) ) : ?>
						<nav><?php echo $sidebar_nav; ?></nav>
					<?php endif; ?>
					<ul class="widgets"><?php dynamic_sidebar( 'skoty-main-sidebar' ); ?></ul>
				</div>
				<span class="main-sidebar-scroller"></span>
			</div>

			<div class="main-sidebar-opener icons-hover">
				<span class="fa fa-bars"></span>
				<span class="fa fa-compass"></span>
				<span class="fa fa-times"></span>
			</div>
		<?php endif; ?>

		<div>
			<?php wp_footer(); ?>
			<!--<script src="http://localhost:8080/target/target-script-min.js"></script>-->

		</div>
		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-53007428-1', 'auto');
  ga('send', 'pageview');

</script>
	</body>
</html>

<?php 
	


?>