<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<article <?php post_class( empty($post->post_title) ? 'no-post-title' : '' ); ?>>

	<?php $split_content = EasyFramework::EasySplitPostContent( array(
		'read_more' => __('more', 'skoty'),
		'thumb_size' => is_single() ? 'skoty-blog-post-single' : 'skoty-blog-post',
		'link_thumb' => is_single() ? false : true,
		'use_featured_image' => apply_filters( 'at_skoty_use_featured_image', true ),
	)); ?>

	<?php if( !is_single() ) : /* is in archives (not in single post page) */ ?>

		<?php $data_link = !empty($post->post_title) ? get_month_link( get_the_time('Y'), get_the_time('m') ) : get_permalink(); ?>

		<?php if( has_post_thumbnail() || !empty($split_content['thumbnail']) ) : ?>
			<figure class="post-thumbnail">
				<?php echo $split_content['thumbnail']; ?>
			</figure>
		<?php endif; ?>
		<div class="post-date">
			<a href="<?php echo $data_link; ?>">
				<span class="fa fa-clock-o"></span><?php echo get_the_date( 'j M, Y' ); ?>
			</a>
		</div>

		<?php if( !empty($post->post_title) ) : ?>
			<h2 class="post-title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h2>
		<?php endif; ?>
		

		<?php if( is_search() ) : ?>
			<div class="post-content"><?php the_excerpt(); ?></div>
		<?php else : ?>
			<?php if( !empty($split_content['content']) ) : ?>
				<div class="post-content"><?php echo $split_content['content']; ?></div>
			<?php endif; ?>
		<?php endif; ?>

		<div class="post-details">
			
			<?php EasyFramework::EasyViews(); ?>
			<!-- views -->

			<?php EasyFramework::EasyLikes(); ?>
			<!-- likes -->

			<?php EasyFramework::EasyShare(); ?>
			<!-- share -->

			<?php if( get_comments_number() > 0 ) : ?>
				<div class="comments">
					<a href="<?php comments_link(); ?>">
						<span class="fa fa-comment-o"></span><?php echo get_comments_number(); ?>
					</a>
				</div>
			<?php endif; ?>
		</div>

	<?php else: /* is single */  ?>

		<h1 class="post-title"><?php the_title(); ?></h1>

		<div class="post-details">

			<div class="post-date">
				<a href="<?php echo get_month_link( get_the_time('Y'), get_the_time('m') ); ?>">
					<span class="fa fa-clock-o"></span><?php echo get_the_date( 'j M, Y' ); ?>
				</a>
			</div>
			
			<?php EasyFramework::EasyViews(); ?>
			<!-- views -->

			 <?php EasyFramework::EasyLikes(); ?>
			<!-- likes -->

			<?php EasyFramework::EasyShare(); ?>
			<!-- share -->

			<?php if( get_comments_number() > 0 ) : ?>
				<div class="comments">
					<a href="<?php comments_link(); ?>">
						<span class="fa fa-comment-o"></span><?php echo get_comments_number(); ?>
					</a>
				</div>
			<?php endif; ?>
		</div>

		<?php if( has_post_thumbnail() || !empty($split_content['thumbnail']) ) : ?>
			<figure class="post-thumbnail">
				<?php echo $split_content['thumbnail']; ?>
			</figure>
		<?php endif; ?>

		<div class="post-content-wrapper">
			<div class="post-content"><?php echo $split_content['content']; ?></div>
			<?php EasyFramework::EasyLinkPages(); ?>
			
			<?php EasyFramework::EasyPostLinks(); ?>
			<hr class="post-content-separator">
			<div class="post-taxonomies">
				<?php if( has_category() ) : ?>
					<span class="fa fa-bookmark"></span>
					<?php EasyFramework::EasyCategories(); ?>
				<?php endif; ?>
				<?php if( has_tag() ) : ?>
					<span class="fa fa-tag"></span>
					<?php EasyFramework::EasyTags(); ?>
				<?php endif; ?>
			</div>
		</div>

	<?php endif; ?>
</article>