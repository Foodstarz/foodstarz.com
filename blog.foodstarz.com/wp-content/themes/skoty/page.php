<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php get_header(); ?>

<section class="content-wrapper">
	<div class="content">
		<?php if( have_posts() ) : ?>
			<?php while( have_posts() ) : the_post(); ?>
				<article <?php post_class(); ?>>
					<?php 
						$page_settings = get_post_meta( $post->ID, '_skoty-page-header-settings', true ); 
						$header_settings = isset($page_settings['header-settings']) ? $page_settings['header-settings'] : false;
						$remove_title = isset($page_settings['remove-title']) ? true : false;
					?>
					<?php if( $header_settings !== 'page-data' && get_the_title() && !$remove_title ) : ?>
						<h1 class="post-title"><?php the_title(); ?></h1>
					<?php endif; ?>
					<div class="post-content"><?php the_content(); ?></div>
					<?php comments_template('', true); ?>
				</article>
			<?php endwhile; ?>
		<?php else: ?>
			<?php _e('Sorry, there are no posts matching your query.', 'skoty'); ?>
		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>