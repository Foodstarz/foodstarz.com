<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php get_header(); ?>

<section class="content-wrapper">

	<div class="gourmet-ads">
		<div id="ga_9466760">
			<script type="text/javascript">
				apntag.anq.push(function() {
					apntag.showTag('ga_9466760');
				});
			</script>
		</div>
	</div>

	<div class="gourmet-ads-mobile">
		<div id="ga_9466758">
			<script type="text/javascript">
				apntag.anq.push(function() {
					apntag.showTag('ga_9466758');
				});
			</script>
		</div>
	</div>

	<div class="content posts">
		<?php if( have_posts() ) : ?>
			<?php while( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
				<?php EasyFramework::EasyRelatedPosts(); ?>
				<?php comments_template('', true); ?>
			<?php endwhile; ?>
		<?php else: ?>
			<?php _e('Sorry, there are no posts matching your query.', 'skoty'); ?>
		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>