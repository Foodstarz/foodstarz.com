<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<article <?php post_class( empty($post->post_title) ? 'no-post-title' : '' ); ?>>

	<?php $split_content = EasyFramework::EasySplitPostContent( array(
		'read_more' => __('more', 'skoty'),
		'thumb_size' => is_single() ? 'skoty-blog-post-single' : 'skoty-blog-post',
	)); ?>

	<strong class="search-result-title">
		<?php if( !empty($post->post_title) ) : ?>
			<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
		<?php else: ?>
			<a href="<?php the_permalink(); ?>"><?php echo get_the_date( 'j M, Y' ); ?></a>
		<?php endif; ?>
	</strong>

	<div class="search-result-content">
		<?php the_excerpt(); ?>
	</div>

</article>