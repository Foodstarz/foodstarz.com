<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<article <?php post_class( empty($post->post_title) ? 'no-post-title' : '' ); ?>>

	<?php $split_content = EasyFramework::EasySplitPostContent( array(
		'read_more' => __('more', 'skoty'),
		'thumb_size' => is_single() ? 'skoty-blog-post-single' : 'skoty-blog-post',
	)); ?>

	<?php 
		$page_settings = get_post_meta( $post->ID, '_atp_project_metabox', true ); 
		$header_settings = isset($page_settings['header-settings']) ? $page_settings['header-settings'] : false;
	?>
	<?php if( $header_settings !== 'page-data' ) : ?>
		<h1 class="post-title"><?php the_title(); ?></h1>
		<div class="post-details">
			<div class="post-date">
				<a href="<?php echo get_month_link( get_the_time('Y'), get_the_time('m') ); ?>">
					<span class="fa fa-clock-o"></span><?php echo get_the_date( 'j M, Y' ); ?>
				</a>
			</div>
			<?php EasyFramework::EasyViews(); ?>
			<!-- views -->
			 <?php EasyFramework::EasyLikes(); ?>
			<!-- likes -->
			<?php EasyFramework::EasyShare(); ?>
			<!-- share -->
			<?php if( get_comments_number() > 0 ) : ?>
				<div class="comments">
					<a href="<?php comments_link(); ?>">
						<span class="fa fa-comment-o"></span><?php echo get_comments_number(); ?>
					</a>
				</div>
			<?php endif; ?>
		</div>
	<?php endif; ?>

	<?php if( has_post_thumbnail() && $header_settings !== 'page-data' ) : ?>
		<figure class="post-thumbnail">
			<?php the_post_thumbnail( 'skoty-project-single' ) ?>
		</figure>
	<?php endif; ?>

	<div class="post-content-wrapper">
		<div class="post-content"><?php echo $split_content['content']; ?></div>
		<?php EasyFramework::EasyLinkPages(); ?>
		
		<!-- <hr class="post-content-separator">
		<div class="post-taxonomies">
			<?php if( has_category() ) : ?>
				<span class="fa fa-bookmark"></span>
				<?php EasyFramework::EasyCategories(); ?>
			<?php endif; ?>
			<?php if( has_tag() ) : ?>
				<span class="fa fa-tag"></span>
				<?php EasyFramework::EasyTags(); ?>
			<?php endif; ?>
		</div> -->
	</div>
</article>