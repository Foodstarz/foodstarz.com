<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

		<title><?php echo EasyFramework::EasyTitle(); ?></title>

		<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<script type="text/javascript" src="http://cdn.grmtas.com/pub/ga_pub_5620.js"></script>
		<?php wp_head(); ?>
	</head>

	<?php $about_page_id = EasyFramework::EasyOption('about-page', 0); ?>
	<?php $bio = get_the_author_meta( 'description', EasyFramework::EasyOption('avatar-user-id', 1) ); ?>
	<?php $about_label = EasyFramework::EasyOption('about-button-label', false); ?>

	<?php $sidebar_nav = EasyFramework::EasyNav( 'header-location', 5, false, 'sidebar-navigation' ); ?>
	<?php $use_sidebar = is_active_sidebar( 'skoty-main-sidebar' ) || !empty($sidebar_nav) ? true : false; ?>

	<?php 
	$header_settings = '';
	$page_for_posts = get_option( 'page_for_posts', false );

	if( is_page() ) {
		$page_settings = isset($post->ID) ? get_post_meta( $post->ID, '_skoty-page-header-settings', true ) : array(); 
		$header_settings = isset($page_settings['header-settings']) ? $page_settings['header-settings'] : false;
	} elseif( is_page() || is_singular('atp_project') ) {
		$page_settings = isset($post->ID) ? get_post_meta( $post->ID, '_atp_project_metabox', true ) : array(); 
		$header_settings = isset($page_settings['header-settings']) ? $page_settings['header-settings'] : false;
	} elseif( is_home() && $page_for_posts ) {
		$page_settings = $page_for_posts ? get_post_meta( $page_for_posts, '_skoty-page-header-settings', true ) : array(); 
		$header_settings = isset($page_settings['header-settings']) ? $page_settings['header-settings'] : false;
	}

	//print_r($header_settings );
	?>

	<body <?php body_class(); ?>>

		<div class="main-content">
			<div class="header-search-form">
				<div class="header-search-form-wrapper">
					<?php get_template_part('header', 'searchform'); ?>
				</div>
			</div>

			<div id="navigation">
				<div class="navigation-container">

					<div class="navigation-wrapper">
						<?php if( has_nav_menu('header-location') && EasyFramework::EasyOption('nav-style', 'side') === 'horizontal' ) : ?>
							<nav><?php EasyFramework::EasyNav( 'header-location', 5 ); ?></nav>
						<?php endif; ?>


						<?php if( !is_front_page() && EasyFramework::EasyOption('show-home-icon') && EasyFramework::EasyOption('nav-style', 'side') !== 'horizontal' ) : ?>
							<a class="home-link icons-hover" href="<?php echo home_url(); ?>">
								<span class="fa fa-home"></span>
								<span class="fa fa-arrow-left"></span>
								<span class="fa fa-times"></span>
							</a>
						<?php endif; ?>

						<?php if( !EasyFramework::EasyOption('disable-search-icon', false) ) : ?>
							<div class="search-opener icons-hover">
								<span class="fa fa-search"></span>
								<span class="fa fa-terminal"></span>
								<span class="fa fa-times"></span>
							</div>
						<?php endif; ?>

						<?php if( $use_sidebar ) : ?>
							<!--<div class="vertical-nav-separator"></div>-->
							<div class="navigation-opener icons-hover">
								<span class="fa fa-bars"></span>
								<span class="fa fa-compass"></span>

								<?php if( has_nav_menu('header-location') && EasyFramework::EasyOption('nav-style', 'side') === 'drop-down' ) : ?>
									<nav><?php EasyFramework::EasyNav( 'header-location', 5 ); ?></nav>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>

				</div>
			</div>
			<!-- end #navigation -->

			<header id="header">
				<div class="header-wrapper">
					<?php 
						$logo_data = EasyFramework::EasyOption('logo-image');
						$logo_adjacent = EasyFramework::EasyOption('header-logo-adjacent-content', false);
					?>
					<?php if( $logo_data || $logo_adjacent ) : ?>
						<div class="header-container">

							<?php if( $logo_data ) : ?>
								<div class="logo-container">
									<a href="<?php echo home_url(); ?>">
										<img class="logo" src="<?php echo $logo_data['url']; ?>" alt="<?php echo bloginfo('name'); ?>">
									</a>
								</div>
							<?php endif; ?>

							<?php if( $logo_adjacent ) : ?>
								<div class="header-logo-adjacent-content"><?php
									echo do_shortcode( $logo_adjacent );
								?></div>
							<?php endif; ?>

						</div>
					<?php endif; ?>
					<!-- end header-container -->




					<?php if( is_home() ) : /* HOME */ ?>

						<?php if( $page_for_posts ) : /* HOME IS PAGE */ ?>
							<?php if( $header_settings === 'page-data' ) : /* PAGE-DATA */ ?>
								<?php 
									$page_for_posts_post = get_post($page_for_posts);
									EasyFramework::EasyPageDataHeading( $page_for_posts_post );
								?>
							<?php elseif( $header_settings === 'site-data' ) : /* SITE-DATA */ ?>
								<?php EasyFramework::EasySiteDataHeading(); ?>
							<?php endif; ?>
						<?php else: /* HOME IS NOT A PAGE */ ?>
							<?php EasyFramework::EasySiteDataHeading(); ?>
						<?php endif; ?>

					<?php else : /* PAGE OR PROJECT */ ?>

						<?php if( $header_settings === 'page-data' ) : /* PAGE-DATA */ ?>
							<?php EasyFramework::EasyPageDataHeading( $post ); ?>
						<?php elseif( $header_settings === 'site-data' ) : /* SITE-DATA */ ?>
							<?php EasyFramework::EasySiteDataHeading(); ?>
						<?php endif; ?>

					<?php endif; ?>

				</div>
				<!-- end header-wrapper -->




				<?php if( !EasyFramework::EasyOption('remove-avatar', false) ) : ?>
					<?php ob_start(); ?>
					<figure class="header-avatar-wrapper">
						<?php
							$avatar_data = EasyFramework::EasyOption('avatar-image');
							if( $avatar_data ) :
								$avatar_img_id = $avatar_data['ID'];
								$img_src = wp_get_attachment_image_src($avatar_img_id, 'skoty-avatar');

								?><img width="250" src="<?php echo $img_src[0]; ?>" alt=""><?php
							else :
								$stored_user_id = EasyFramework::EasyOption('avatar-user-id', null); 
								$avatar = EasyFramework::EasyAvatarImage( $stored_user_id, 235 );
								echo $avatar;
							endif;
						?>
					</figure>
					<!-- end header-avatar-wrapper -->
					<?php 
						$avatar_str = ob_get_clean();
						echo apply_filters( 'at_skoty_avatar', $avatar_str ); 
					?>
				<?php endif; ?>





				<div class="header-media-holder">
					<?php if( !is_404() ) : ?>
						<?php /* ------------PAGES----------- */ ?>
						<?php $header_media_data = EasyFramework::EasyOption( 'header-media', false ); ?>

						<?php 
							if( (is_page() || is_singular('atp_project')) && has_post_thumbnail() ) :
								$img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' );
								$header_media_data['url'] = $img[0];
								$header_media_data['type'] = 'image';
							endif;
						?>

						<?php if ( $header_media_data ) : ?>
							<div class="header-media"
								data-media-url="<?php echo $header_media_data['url']; ?>" 
								data-media-type="<?php echo $header_media_data['type']; ?>"
								data-media-mime="<?php echo isset($header_media_data['mime']) ? $header_media_data['mime'] : ''; ?>"
								data-media-opacity="<?php echo intval(EasyFramework::EasyOption('header-media-opacity', '40%'))/100; ?>"
								<?php 
									if( isset($header_media_data['fallback']) && isset($header_media_data['fallback']['url']) ) :
										?>data-media-fallback-url="<?php echo $header_media_data['fallback']['url']; ?>"<?php
									endif;
								?>
							></div>
						<?php endif; ?>
					<?php else: ?>
						<?php /* -------------404------------ */ ?>
						<?php $header_404_media_data = EasyFramework::EasyOption('error-404-header-media');
						if( $header_404_media_data ) :
							$header_media_data = $header_404_media_data;
						else:
							$header_media_data = EasyFramework::EasyOption('header-media');
						endif; ?>
						<?php if( $header_media_data ) : ?>
							<div class="header-media"
								data-media-url="<?php echo $header_media_data['url']; ?>" 
								data-media-type="<?php echo $header_media_data['type']; ?>"
								data-media-mime="<?php echo isset($header_media_data['mime']) ? $header_media_data['mime'] : ''; ?>"
								data-media-opacity="<?php echo intval(EasyFramework::EasyOption('header-media-opacity', '40%'))/100; ?>"
								<?php 
									if( isset($header_media_data['fallback']) && isset($header_media_data['fallback']['url']) ) :
										?>data-media-fallback-url="<?php echo $header_media_data['fallback']['url']; ?>"<?php
									endif;
								?>
							></div>
						<?php endif; ?>
					<?php endif; ?>


					<?php $header_pattern_data = EasyFramework::EasyOption('header-pattern'); ?>
					<?php if( $header_pattern_data ) : ?>
						<div class="header-pattern"></div>
					<?php endif; ?>
					<span class="header-media-preloader preloader"></span>
				</div>




				<?php if( is_home() || $header_settings === 'site-data' || $header_settings === 'page-data' ) : ?>
					<div class="scroll-down-indicator icons-hover">
						<span class="fa fa-angle-double-down"></span>
						<span class="fa fa-angle-double-up"></span>
					</div>
				<?php endif; ?>




				<?php if( EasyFramework::EasyYoastBreadcumbs() ) : ?>
					<div class="breadcrumbs">
						<div class="breadcrumbs-wrapper">
								<?php echo EasyFramework::EasyYoastBreadcumbs(); ?>
						</div>
					</div>
				<?php endif; ?>
			</header>
			<!-- end #header -->
 

			<section class="about">
				<?php if( $about_label ) : ?>
					<?php if( $about_page_id ) : ?>
						<div class="close-about icons-hover">
							<span class="fa fa-times"></span>
							<span class="fa fa-times"></span>
						</div>
						<div class="section-wrapper">
							<?php $post = get_post( $about_page_id ); setup_postdata( $post ); ?>
							<!-- <h2 class="about-title"><?php echo get_the_title( $about_page_id ); ?></h2> -->
							<div class="about-content">
								<?php echo apply_filters( 'the_content', get_the_content('') ); ?>
							</div>
							<?php wp_reset_postdata(); ?>
						</div>
					<?php else: ?>
						<?php if( $bio && !empty($bio) ) : ?>
							<div class="close-about icons-hover">
								<span class="fa fa-times"></span>
								<span class="fa fa-times"></span>
							</div>
							<div class="section-wrapper">
								<div class="about-content">
									<?php echo esc_html( $bio ); ?>
								</div>
							</div>
						<?php endif; ?>
					<?php endif; ?>
				<?php endif; ?>
			</section>


			

			<?php if( !is_home() && !is_singular() ) : ?>
				<div class="archives-heading">
					<?php if( is_month() ) : ?>
						<h1><?php printf( __('Archives for: %s','skoty'), get_the_time( 'F Y' ) ); ?></h1>
						<h6><?php printf(
							_n( 'There was just <span>one post</span> published in <span>%2$s</span>.', 'There were <span>%1$s posts</span> published in <span>%2$s</span>.', $wp_query->found_posts, 'skoty' ),
							number_format_i18n( $wp_query->found_posts ),
							get_the_time( 'F Y' )
						); ?></h6>
					<?php elseif( is_year() ) : ?>
						<h1><?php printf( __('Archives for: %s','skoty'), get_the_time( 'Y' ) ); ?></h1>
						<h6><?php printf(
							_n( 'There was just <span>one post</span> published in <span>%2$s</span>.', 'There were <span>%1$s posts</span> published in <span>%2$s</span>.', $wp_query->found_posts, 'skoty' ),
							number_format_i18n( $wp_query->found_posts ),
							get_the_time( 'Y' )
						); ?></h6>
					<?php elseif( is_day() ) : ?>
						<h1><?php printf( __('Archive for: %s','skoty'), get_the_time( ' F d, Y' ) ); ?></h1>
						<h6><?php printf(
							_n( 'There was just <span>one post</span> published on <span>%2$s</span>.', 'There were <span>%1$s posts</span> published on <span>%2$s</span>.', $wp_query->found_posts, 'skoty' ),
							number_format_i18n( $wp_query->found_posts ),
							get_the_time( 'F d, Y' )
						); ?></h6>

					<?php elseif( is_category() ) : ?>
						<h1><?php printf( __('%s','skoty'), single_cat_title('', false) ); ?></h1>
						<h6><?php
							$tax_desc = category_description();
							if( !empty($tax_desc) ) :
								echo $tax_desc;
							else:
								printf( _n( 'There is just <span>one post</span> published under <span>%2$s</span>.', 'There are <span>%1$s posts</span> published under <span>%2$s</span>.', $wp_query->found_posts, 'skoty' ),
									number_format_i18n( $wp_query->found_posts ),
									single_cat_title('', false)
								);
							endif;
						?></h6>

					<?php elseif( is_tag() ) : ?>
						<h1><?php printf( __('Tag: %s','skoty'), single_tag_title('', false) ); ?></h1>
						<h6><?php
							$tax_desc = tag_description();
							if( !empty($tax_desc) ) :
								echo $tax_desc;
							else:
								printf( _n( 'There is just <span>one post</span> tagged as <span>%2$s</span>.', 'There are <span>%1$s posts</span> tagged as <span>%2$s</span>.', $wp_query->found_posts, 'skoty' ),
									number_format_i18n( $wp_query->found_posts ),
									single_tag_title('', false)
								);
							endif;
						?></h6>

					<?php elseif( is_author() ) : ?>
						<h1><?php printf( __('Posts by %s', 'skoty'), get_the_author_meta( 'display_name', get_queried_object()->ID ) ); ?> </h1>
						<h6><?php printf(
							_n( 'The author has just <span>one post</span> so far.', 'The author has <span>%1$s posts</span> in total.', $wp_query->found_posts, 'skoty' ),
							number_format_i18n( $wp_query->found_posts ),
							single_cat_title('', false)
						); ?></h6>
					<?php elseif( is_404() ) : ?>
						<h1><?php echo EasyFramework::EasyOption( 'error-404-heading', __('404', 'skoty') ); ?> </h1>
						<h6><?php echo EasyFramework::EasyOption( 'error-404-sub-heading', __('It looks like nothing was found at this location. Maybe try a search?', 'skoty') ); ?> </h6>
					<?php elseif( is_search() ) : ?>
						<h1><?php printf( __('Search results for: %s', 'skoty'), get_search_query() ); ?> </h1>
						<h6><?php printf( _n(
								'There is just <span>one post</span> matching your search query.', 
								'There are <span>%1$s posts</span> matching your search query.',
								$wp_query->found_posts, 'skoty' ),
								number_format_i18n( $wp_query->found_posts )
							); ?> </h6>
					<?php else: ?>

					<?php endif; ?>
				</div>
			<?php endif; ?>
			
			<div class="content-preloader-wrapper">
				<span class="content-preloader preloader"></span>
			</div>