<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php get_header(); ?>

<section class="content-wrapper">
	<div class="content">
		<?php get_search_form(); ?>
	</div>
</section>

<?php get_footer(); ?>