<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<article <?php post_class(); ?> title="<?php the_title_attribute(); ?>">
	<div class="entry-wrapper">
		<?php $split_content = EasyFramework::EasySplitPostContent(); ?>
		<a class="entry-link" target="_blank" href="<?php echo $split_content['content']; ?>"><?php 
			echo $split_content['thumbnail'];
		?></a>
	</div>
	<?php EasyFramework::EasyPostLinks(); ?>
	<hr class="post-content-separator">
</article>