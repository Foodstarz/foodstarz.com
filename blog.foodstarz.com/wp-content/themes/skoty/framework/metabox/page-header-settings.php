<?php $header_options = array(
	'no-data' => __('Small Header', 'skoty'),
	'site-data' => __('Site Title &amp; Tagline', 'skoty'),
	'page-data' => __('Page Title &amp; Excerpt', 'skoty')
); ?>
<p><strong><?php _e('Header', 'skoty'); ?></strong></p>
<select style="width: 100%;" name="<?php echo $meta_field; ?>[header-settings]">
	<?php foreach ($header_options as $key => $value) { ?>
		<?php $selected = $meta && isset($meta['header-settings']) && $key === $meta['header-settings'] ? 'selected' : ''; ?>
		<option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
	<?php } ?>
</select>
<br/>
<p>
	<?php $checked = $meta && isset($meta['remove-title']) ? 'checked' : ''; ?>
	<input type="checkbox" name="<?php echo $meta_field; ?>[remove-title]" <?php echo $checked; ?> />
	<strong><?php _e('Remove Page Title', 'skoty'); ?></strong>
</p>