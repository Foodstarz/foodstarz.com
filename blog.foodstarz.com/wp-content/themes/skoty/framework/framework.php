<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php

if( !class_exists('EasyFramework') ) {

	Class EasyFramework {
		public static $custom_image_sizes;






		public static function EasyPageDataHeading( $p ) {
			global $post;
			$post = get_post( $p->ID );
			?><div class="header-content-wrapper">
				<?php if( isset($p) && !empty($p->post_title) ) : ?>
					<h1><?php the_title(); ?></h1>
					<!-- end H1 title -->
				<?php endif; ?>

				<?php if( get_post_type( $p ) === 'atp_project' ) : ?>
						<div class="post-details">
							<div class="post-date">
								<a href="<?php echo get_month_link( get_the_time('Y'), get_the_time('m') ); ?>">
									<span class="fa fa-clock-o"></span><?php echo get_the_date( 'j M, Y' ); ?>
								</a>
							</div>
							<?php EasyFramework::EasyViews(); ?>
							<!-- views -->
							<?php EasyFramework::EasyLikes(); ?>
							<!-- likes -->
							<?php EasyFramework::EasyShare(); ?>
							<!-- share -->
							<?php if( get_comments_number() > 0 ) : ?>
								<div class="comments">
									<a href="<?php comments_link(); ?>">
										<span class="fa fa-comment-o"></span><?php echo get_comments_number(); ?>
									</a>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>

				<?php if( isset($p) && !empty($p->post_excerpt) ) : ?>
					<div class="sub-heading"><?php echo do_shortcode($p->post_excerpt); ?></div>
					<!-- end sub-heading -->
				<?php endif; ?>
			</div>
			<!-- end header-content-wrapper --><?php
			wp_reset_postdata();
		}


		public static function EasySiteDataHeading() {
			$about_page_id = EasyFramework::EasyOption('about-page', 0);
			$bio = get_the_author_meta( 'description', EasyFramework::EasyOption('avatar-user-id', 1) );
			$about_label = EasyFramework::EasyOption('about-button-label', false);
			$about_link = EasyFramework::EasyOption('about-button-link', false);

			?><div class="header-content-wrapper">
				<h1><?php
					if( EasyFramework::EasyOption('header-heading') ) :
						echo EasyFramework::EasyOption('header-heading');
					else :
						echo bloginfo('name');
					endif;
				?></h1>
				<!-- end H1 title -->

				<div class="sub-heading"><?php
					if( EasyFramework::EasyOption('header-text') ) :
						echo EasyFramework::EasyOption('header-text');
					else :
						echo bloginfo('description');
					endif;
				?></div>
				<!-- end sub-heading -->

				<?php
				if( $about_label && ($about_page_id || $bio || $about_link) ) : ?>
					<?php if( $about_link ) : ?>
						<a href="<?php echo esc_url($about_link); ?>" class="link-button button-primary"><?php echo $about_label; ?></a>
					<?php else: ?>
						<a href="<?php echo get_permalink($about_page_id); ?>" class="about-button button-primary"><?php echo $about_label; ?></a>
					<?php endif; ?>
				<?php endif; ?>
			</div>
			<!-- end header-content-wrappper --><?php
		}








		public static function EasySplitPostContent( $set = array() ) {
			global $post, $alternate_content_width, $content_width;

			if( !isset($set['ID']) && !isset($post->ID) ) return false;

			$settings = array_merge( array(
				'ID' => $post->ID,
				'read_more' => '',
				'use_featured_image' => true,
				'thumb_size' => 'thumbnail',
				'link_thumb' => true
			), $set );

			$temp_content_width;
			$post_thumbnail;

			if( isset($set['ID']) ) {
				$post = get_post( $settings['ID'] );
			}

			setup_postdata( $post );

			$link_before = '<a href="'.get_permalink().'" title="'.the_title_attribute(array('echo'=>false)).'">';
			$link_after = '</a>';

			

			if( !is_single() ) {
				$temp_content_width = $content_width;
				$content_width = $alternate_content_width;
			}

			$post_content = apply_filters( 'the_content', get_the_content( $settings['read_more'] ) );
			$post_format = get_post_format( $settings['ID'] );

			$html = new simple_html_dom();
			$html->load( $post_content, true, false );

			if( !is_single() ) {
				$content_width = $temp_content_width;
			}

			if( $html ) {
				switch( $post_format ) {
					case 'image':
						$element = $html->find('img', 0);
						if( $element && !has_post_thumbnail() ) {
							$parent = $element->parent();
							if( $parent && $parent->parent() ) {
								if( $parent->parent()->tag === 'p' ) {
									$element = $parent;
									$parent->parent()->outertext = '';
								} elseif( $parent->parent()->tag === 'root' ) {
									$element = $parent;
								} else {
									$element = $parent->parent();
								}
							} elseif( $parent && !$parent->parent() ) {
								$element = $parent;
							}
							//clear all unnecessary attributes
							$element->style = null; $element->class = null; $element->id = null;

							//store the thumbnail html string
							$post_thumbnail = $element->outertext;
							//remove the thumbnail from the content...
							$element->outertext = '';
							//...store the content in a variable
							$post_content = $html->outertext;
						}
					break;

					case 'video':
						$element = $html->find('iframe, embed, .wp-video-shortcode, .wp-video', 0);
						if( $element ) {
							if( $element->tag === 'iframe' ) {
								$element->webkitallowfullscreen = null;
								$element->mozallowfullscreen = null;
								$element->frameborder = null;
							}
							$parent = $element->parent();
							if( $parent ) {
								if( $parent->tag === 'p' ) {
									$parent->outertext = '';
								} else {
									//$element = $parent;
								}
							}
							//clear all unnecessary attributes
							$element->style = null;

							//store the thumbnail html string
							$post_thumbnail = $element->outertext;
							//remove the thumbnail from the content...
							$element->outertext = '';
							//...store the content in a variable
							$post_content = $html->outertext;
						}
					break;

					case 'audio':
						$element = $html->find('.wp-audio-shortcode, .wp-audio-playlist, iframe', 0);
						if( $element ) {
							//add the thumbnail html string to the post featured image
							if( has_post_thumbnail() ) {
								$post_thumbnail = '<div class="audio-cover-wrapper">';
								$post_thumbnail .= get_the_post_thumbnail( $post->ID, $settings['thumb_size'] );
								$post_thumbnail .= '</div>';
								$post_thumbnail .= $element->outertext;
							} else {
								$post_thumbnail = $element->outertext;
							}
							//remove the thumbnail from the content...
							if( $element->parent()->tag == 'p' ) {
								$element->parent()->outertext = '';
							}
							$element->outertext = '';
							//...store the content in a variable
							$post_content = $html->outertext;
						}


					break;

					case 'gallery':
						$element = $html->find('.gallery-shortcode', 0);
						if( $element ) {
							$element->style = null;
							//store the thumbnail html string
							$post_thumbnail = $element->outertext;
							//remove the thumbnail from the content...
							$element->outertext = '';
							//...store the content in a variable
							$post_content = $html->outertext;
						}
					break;

					case 'link':
						$element = $html->find('a', 0);
						if( $element ) {
							$element->style = null;
							//store the thumbnail html string
							$post_thumbnail = $element->innertext;
							//...store the content in a variable
							$post_content = $element->href;
						} else {
							$post_content = wp_strip_all_tags( $post_content );
							if( preg_match('/(((ftp|https?):\/\/)(www\.)?|www\.)([\da-z-_\.]+)([a-z\.]{2,7})([\/\w\.-_\?\&]*)*\/?/', $post_content, $matches) ) {

								if( strpos($matches[0], 'http://') === 0 ) {
									$post_thumbnail = $matches[0];
									$post_content = $matches[0];
								} else {
									$post_thumbnail = $matches[0];
									$post_content = 'http://' . $matches[0];
								}

							} //end if preg_match
						}
					break;
				}//end switch

				if( isset($element) ) { /**/ }
			}//end if

			/* if has post thumbnail */
			$try_use_featured_image = true;
			//error_log( (isset($post_thumbnail) ? 'true' : 'false') . ' : ' . get_the_title($post->ID) );
			if( isset($post_thumbnail) ) {
				$try_use_featured_image = false;
			}
			if( $settings['use_featured_image'] ) {
				$try_use_featured_image = true;
			}

			if( isset($post->ID) && has_post_thumbnail() && $try_use_featured_image ) {
				if( $settings['link_thumb'] ) {
					$post_thumbnail = $link_before . get_the_post_thumbnail( $post->ID, $settings['thumb_size'] ) . $link_after;
				} else {
					$post_thumbnail = get_the_post_thumbnail( $post->ID, $settings['thumb_size'] );
				}

				if( is_single() ) {
					$post_content = apply_filters( 'the_content', get_the_content( $settings['read_more'] ) );
				}
			}

			if( EasyFramework::EasyOption('posts-content-style', 'read-more') === 'exerpt' ) {
				//error_log($post_content);
				$post_content = strip_shortcodes( $post_content );

				$excerpt_length = apply_filters( 'excerpt_length', 55 );
				$excerpt_more = apply_filters( 'excerpt_more', ' ' . '[&hellip;]' );

				$post_content = wp_trim_words( $post_content, $excerpt_length, $excerpt_more );
			}

			if( !empty($post->post_excerpt) && !is_single() ) {
				$output = array(
					'thumbnail' => isset($post_thumbnail) ? $post_thumbnail : '',
					'content' => apply_filters( 'the_content', $post->post_excerpt )
				);
			} else {
				$output = array(
					'thumbnail' => isset($post_thumbnail) ? $post_thumbnail : '',
					'content' => $post_content
				);
			}

			wp_reset_postdata();
			return $output;
		}













		static function EasyRelatedPosts() {
			if( !EasyFramework::EasyOption('show-related-posts', false) ) return;
			
			global $post;
			if( isset($post) && isset($post->ID) && is_single() ) {
				$cat_ids = wp_get_post_categories( $post->ID );
				//$tag_ids = wp_get_post_tags( $post->ID, array('fields' => 'ids') );
				//
				$related_query = new WP_Query( array(
						'category__in' => $cat_ids,
						//'tag__in' => $tag_ids,
						'post__not_in' => array( $post->ID ),
						'posts_per_page' => 4,
						'ignore_sticky_posts' => true
					)
				);
				//
				if( $related_query->have_posts() ) {
					?><div class="related-posts">
						<h5 class="related-posts-title"><?php _e('Related Posts', 'skoty'); ?></h5><?php
						while( $related_query->have_posts() ) { $related_query->the_post();
							$post_class = !has_post_thumbnail() ? 'no-related-post-thumbnail' : '';
							$post_format = get_post_format();
							$post_class .= $post_format ? ' related-format-' . $post_format : ' related-format-standard';
							$post_format_icon = EasyFramework::EasyPostFormatIcon( $post_format );

							?><article class="<?php echo $post_class; ?>">
								<div class="related-post-thumbnail">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" data-post-format-icon="<?php echo $post_format_icon['html']; ?>" ><?php
										//the_post_thumbnail( 'medium' );

										$attachment_id = get_post_thumbnail_id( $related_query->post->ID );
										$attachment_src = wp_get_attachment_image_src( $attachment_id, 'medium' );

										if( isset($attachment_src[0]) ) :
											?><div class="related-post-img" style="background-image: url(<?php echo $attachment_src[0]; ?>)"></div><?php
										endif;
									?></a>
								</div>
								<div class="related-post-content-wrapper">
									<a class="related-post-title" href="<?php the_permalink(); ?>"><?php
										the_title();
									?></a>
									<!-- <div class="related-post-content"><?php the_excerpt(); ?></div> -->
								</div>
							</article><?php
						} //end while
					?><hr></div><?php
				}
				wp_reset_postdata();
				//error_log( print_r($tag_ids, true) );
			}
		}






		public static function EasyPostFormatIcon( $format = 'standard' ) {
			$icon = array(
				'class' => '',
				'unicode' => ''
			);

			switch ($format) {
				case 'audio':
					$icon = array(
						'class' => 'fa-music',
						'html' => '&#xf001;',
						'unicode' => 'f001'
					);
					break;

				case 'video':
					$icon = array(
						'class' => 'fa-play-circle-o',
						'html' => '&#xf01d;',
						'unicode' => 'f01d'
					);
					break;

				case 'link':
					$icon = array(
						'class' => 'fa-link',
						'html' => '&#xf0c1;',
						'unicode' => 'f0c1'
					);
					break;

				case 'quote':
					$icon = array(
						'class' => 'fa-quote-left',
						'html' => '&#xf10d;',
						'unicode' => 'f10d'
					);
					break;

				case 'aside':
					$icon = array(
						'class' => 'fa-align-center',
						'html' => '&#xf037;',
						'unicode' => 'f037'
					);
					break;

				case 'chat':
					$icon = array(
						'class' => 'fa-comment',
						'html' => '&#xf075;',
						'unicode' => 'f075'
					);
					break;
				
				default:
					$icon = array(
						'class' => 'fa-file-text',
						'html' => '&#xf15c;',
						'unicode' => 'f15c',
					);
					break;
			}

			return $icon;
		}







		public static function EasyPostLinks( $post_type = null ) {
			$type = isset($post_type) ? $post_type : 'post';
			$count_posts = wp_count_posts( $type );

			if( $type !== get_post_type() ) return;

			if( is_single() && isset($count_posts->publish) && $count_posts->publish > 1 ) {
				switch ( get_post_type() ) {
					case 'post':
						?><div class="next-prev-post-links"><hr class="post-content-separator"><?php
						next_post_link(
							'<div class="prev-post-link">%link</div>',
							'<span class="prev-post-label">'.__('Previous post', 'skoty').'</span><span class="fa fa-caret-right"></span>'
						);
						
						previous_post_link(
							'<div class="next-post-link">%link</div>',
							'<span class="fa fa-caret-left"></span><span class="next-post-label">'.__('Next post', 'skoty').'</span>'
						);
						?></div><?php
					break;

					case 'atp_project':
						?><div class="next-prev-post-links"><div class="next-prev-post-links-wrapper"><?php
						next_post_link(
							'<div class="prev-post-link">%link</div>',
							'<span class="prev-post-label">'.__('Previous project', 'skoty').'</span><span class="fa fa-caret-right"></span>'
						);
						
						previous_post_link(
							'<div class="next-post-link">%link</div>',
							'<span class="fa fa-caret-left"></span><span class="next-post-label">'.__('Next project', 'skoty').'</span>'
						);
						?></div></div><?php
					break;
				}
			}
		}



		public static function EasyTitle() {
			if ( !defined('WPSEO_FILE') ) {
				return wp_title('|', false, 'right') . get_bloginfo('name');
			} else {
				return wp_title('', false);
			}
		}



		public static function EasyYoastBreadcumbs() {
			if ( function_exists('yoast_breadcrumb') ) {
				return yoast_breadcrumb('','',false);
			} else {
				return false;
			}
		}



		public static function EasyViews() {
			global $post;
			if( !class_exists('AtticThemes_BlogExtender') ) return;

			if( !isset($post) && !isset($post->ID) ) return;
			if( is_page() ) return;

			$views = AtticThemes_BlogExtender::GetCount( $post->ID, 'views' );

			?><div class="views atbe-views">
				<span class="fa fa-eye"></span><span class="atbe-count"><?php echo $views ? $views : 0; ?></span>
			</div><?php
		}

		public static function EasyLikes() {
			global $post;
			if( !class_exists('AtticThemes_BlogExtender') ) return;
			
			if( !isset($post) && !isset($post->ID) ) return;

			if( is_page() ) return;

			$likes = AtticThemes_BlogExtender::GetCount( $post->ID, 'likes' );
			$liked = AtticThemes_BlogExtender::IsReturning( $post->ID, 'likes' ) ? 'atbe-liked' : '';

			?><div class="likes atbe-likes <?php echo $liked; ?>" data-post-id="<?php echo $post->ID; ?>">
				<span class="fa fa-heart-o"></span><span class="atbe-count"><?php echo $likes ? $likes : 0; ?></span>
			</div><?php
		}

		public static function EasyShare() {
			global $post;
			if( !isset($post) && !isset($post->ID) ) return;
			$post_id = $post->ID;

			if( class_exists('AtticThemes_BlogExtender') ) {
				$icons = array();
				if( self::EasyOption( 'use-facebook-sharing' ) ) {
					$icons[] = 'facebook';
				}
				if( self::EasyOption( 'use-twitter-sharing' ) ) {
					$icons[] = 'twitter';
				}
				if( self::EasyOption( 'use-google-plus-sharing' ) ) {
					$icons[] = 'google-plus';
				}
				if( self::EasyOption( 'use-pinterest-sharing' ) ) {
					$icons[] = 'pinterest';
				}
				if( !empty($icons) ) {
					?><div class="share atbe-share"><span class="fa fa-share-alt"></span><?php
						echo AtticThemes_BlogExtender::GetShareIcons( $post_id, $icons );
					?></div><?php
				}
			}
		}




		public static function EasyLoadMore() {
			if ( !isset($_REQUEST['nonce']) || (isset($_REQUEST['nonce']) && !wp_verify_nonce( $_REQUEST['nonce'], 'Skoty-Load-More-Nonce-Ajax')) ) {
				exit('Do not even try!');
			}

			if( isset($_REQUEST['page']) && !empty($_REQUEST['page']) ) {
				$page = $_REQUEST['page'];

				$load_more_query = new WP_Query( array(
						'paged'=> $page,
						'post_type' => 'post'
					) 
				);
				if( $load_more_query->have_posts() ) {
					while( $load_more_query->have_posts() ) { 
						$load_more_query->the_post();
						get_template_part( 'content', get_post_format() );
					}
					wp_reset_postdata();
				}
			}
			die;
		}






		public static function EasyPagination( $echo = true, $custom_query = null ) {
			global $wp_query;

			if( !isset( $custom_query ) ) {
				$custom_query = $wp_query;
			}
			
			$big = 999999999;
			$args = array(
				'base'=> str_replace( $big, '%#%', esc_url(get_pagenum_link($big)) ),
				'format' => '?page=%#%',
				'total' => $custom_query->max_num_pages,
				'current' => max( 1, $custom_query->get('paged') ),
				'show_all' => false,
				'end_size' => 1,
				'mid_size' => 2,
				'next_text' => '',
				'prev_text' => '',
				'classes' => array(
					'previous' => 'prev',
					'next' => 'next',
					'numbers' => 'page-numbers',
					'current' => 'current',
					'dots' => 'dots',
				),
			);

			if( $echo ) {
				?><div class="pagination"><?php echo paginate_links( $args ); ?></div><?php
			} else {
				return paginate_links( $args );
			}
		}

		public static function HasPagination() {
			global $wp_query;
			if ( $wp_query->max_num_pages > 1 ) {
				return true;
			} else {
				return false;
			}
		}

		public static function EasyCategories( $post_id = null ) {
			global $post;

			if( !isset($post->ID) && !isset($post_id) ) return;

			$post_categories = wp_get_post_categories( isset($post_id) ? $post_id : $post->ID );
			if ( $post_categories ) {
				?><ul class="post-categories"><?php
					foreach( $post_categories as $c ) {
						$cat = get_category( $c );
						?><li>
							<a href="<?php echo get_category_link( $cat->term_id ); ?>"><?php echo $cat->name; ?></a>
						</li><?php
					}
				?></ul><?php
			}
		}

		public static function EasyTags() {
			global $post;
			if( !isset($post) ) return;
			the_tags('<ul class="post-tags"><li>',',</li><li>','</li></ul>');
		}


		public static function EasyLinkPages() {
			wp_link_pages( array(
					'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'skoty' ) . '</span>',
					'after' => '</div>',
					'link_before' => '<span>',
					'link_after' => '</span>'
				)
			);
		}



		public static function EasyNav( $theme_location = null, $depth = 6, $echo = true, $class = 'navigation' ) {

			$args = array(
				'theme_location' => $theme_location,
				'container' => false,
				'menu_class' => $class,
				'echo' => false,
				//'fallback_cb' => 'wp_page_menu',
				'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth' => $depth
			);
			if ( isset( $theme_location ) && has_nav_menu( $theme_location ) ) {
				if( $echo ) {
					echo wp_nav_menu( $args );
				} else {
					return wp_nav_menu( $args );
				}
			} else {
				return false;
			}

		} //END easyNav()





		public static function EasyOptionsExport() {
			$options = get_option( ATTIC_OPTIONS_NAMESPACE );
			return json_encode( $options );
		}





		public static function EasyOption( $key = null, $default = false ) {
			$options = get_option( ATTIC_OPTIONS_NAMESPACE );
			$options = apply_filters( 'attic_easy_option', $options, $options );

			if( isset($key) && isset($options[$key]) && !empty($options[$key]) ) {
				$json_str = json_decode( $options[$key], true );
				if( $json_str ) {
					return $json_str;
				} else {
					return $options[$key];
				}
			} else {
				return $default;
			}
		}





		public static function EasyUsers( $role = 'administrator' ) {
			$users = get_users( array(
					'role' => $role,
				)
			);
			$users_array = array();
			
			foreach( $users as $user ) {
				$data = $user->data;
				$users_array[$data->ID] = $data->display_name.' ('. $data->user_login .')';
			}

			return $users_array;
		}



		public static function EasyAvatarImage( $user_id = null, $size = 100 ) {
			if( !isset($user_id) ) return false;
			
			$user = get_userdata( $user_id );
			$user_data = $user->data;
			
			return get_avatar( $user_id, $size, '', $user_data->display_name );
		}









		public static function EasyImageSizes( $sizes = null ) {
			if( !isset($sizes) ) return;

			self::$custom_image_sizes = $sizes;

			foreach ( $sizes as $name => $data ) {
				if( isset($data['width']) && isset($data['height']) ) {
					add_image_size( 
						$name, 
						$data['width'], 
						$data['height'], 
						$data['crop'] ? $data['crop'] : false
					);
				} //END if
			} //END for each

			add_filter ( 'wp_prepare_attachment_for_js',  array( 'EasyFramework', 'add_image_sizes_to_uploader' ), 10, 3  );
		}

		public static function add_image_sizes_to_uploader( $response, $attachment, $meta ) {
			if( !isset(self::$custom_image_sizes) ) return $response;

			$sizes = array();

			foreach ( self::$custom_image_sizes as $name => $data ) {
				$sizes[] = $name;
			}

			foreach ( $sizes as $size ) {
				if ( isset( $meta['sizes'][ $size ] ) ) {
					$attachment_url = wp_get_attachment_url( $attachment->ID );
					$base_url = str_replace( wp_basename( $attachment_url ), '', $attachment_url );
					$size_meta = $meta['sizes'][ $size ];

					$response['sizes'][ $size ] = array(
						'height' => $size_meta['height'],
						'width' => $size_meta['width'],
						'url' => $base_url . $size_meta['file'],
						'orientation' => $size_meta['height'] > $size_meta['width'] ? 'portrait' : 'landscape',
					);
				}
			}

			return $response;
		}



		public static function EasyColorLevel( $colour, $per ) {
			$colour = substr( $colour, 1 );
			$rgb = '';
			$per = $per/100*255;

			if($per < 0 ) { // Check to see if the percentage is a negative number 
				// DARKER 
				$per = abs($per); // Turns Neg Number to Pos Number 
				for($x=0;$x<3;$x++) { 
					$c = hexdec(substr($colour,(2*$x),2)) - $per;
					$c = ($c < 0) ? 0 : dechex($c);
					$rgb .= (strlen($c) < 2) ? '0'.$c : $c;
				}
			} else { 
				// LIGHTER
				for ($x=0;$x<3;$x++) {
						$c = hexdec(substr($colour,(2*$x),2)) + $per;
						$c = ($c > 255) ? 'ff' : dechex($c);
						$rgb .= (strlen($c) < 2) ? '0'.$c : $c;
					}
				} 
			return '#'.$rgb;
		}


		public static function EasyHexToRgba($color, $opacity = null) {

			$default = 'rgb(0,0,0)';

			//Return default if no color provided
			if(empty($color)) {
				return $default; 
			}

			//Sanitize $color if "#" is provided 
			if ($color[0] == '#' ) {
				$color = substr( $color, 1 );
			}

			//Check if color has 6 or 3 characters and get values
			if (strlen($color) == 6) {
					$hex = array( $color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5] );
			} elseif ( strlen( $color ) == 3 ) {
					$hex = array( $color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2] );
			} else {
					return $default;
			}

			//Convert hexadec to rgb
			$rgb =  array_map('hexdec', $hex);

			//Check if opacity is set(rgba or rgb)
			if( $opacity || $opacity == 0 ) {
				if(floatval($opacity) > 1) {
					$opacity = 1;
				}
				$output = 'rgba('. implode(',',$rgb) .','.$opacity.')';
			} else {
				$output = 'rgb('. implode(',',$rgb) .')';
			}

			//Return rgb(a) color string
			return $output;
		}
























		public static function EasyFontList( $names_only = false ) {
			$fonts = array(
				'arial' => array(
					'name' => 'Arial',
					'import' => '',
					'stack' => 'Arial, Helvetica, sans-serif'
				),
				'arial-black' => array(
					'name' => 'Arial Black',
					'import' => '',
					'stack' => 'Arial, Helvetica, sans-serif'
				),
				'impact' => array(
					'name' => 'Impact',
					'import' => '',
					'stack' => 'Impact, Charcoal, sans-serif'
				),
				'tahoma' => array(
					'name' => 'Tahoma',
					'import' => '',
					'stack' => 'Tahoma, Geneva, sans-serif'
				),
				'open-sans' => array(
					'name' => 'Open Sans',
					'import' => '//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,400,300,600,700',
					'stack' => '"Open Sans", Arial, sans-serif'
				),
				'lato' => array(
					'name' => 'Lato',
					'import' => '//fonts.googleapis.com/css?family=Lato:300,400,700,400italic',
					'stack' => '"Lato", sans-serif'
				),
				'noto-sans' => array(
					'name' => 'Noto Sans',
					'import' => '//fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic',
					'stack' => '"Noto Sans", sans-serif'
				),
				'ubuntu' => array(
					'name' => 'Ubuntu',
					'import' => '//fonts.googleapis.com/css?family=Ubuntu:300,400,500,700,400italic',
					'stack' => '"Ubuntu", sans-serif'
				),
				'droid-sans' => array(
					'name' => 'Droid Sans',
					'import' => '//fonts.googleapis.com/css?family=Droid+Sans:400,700',
					'stack' => '"Droid Sans", sans-serif'
				),
				'droid-serif' => array(
					'name' => 'Droid Serif',
					'import' => '//fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic',
					'stack' => '"Droid Serif", serif'
				),
				'pt-mono' => array(
					'name' => 'PT Mono',
					'import' => '//fonts.googleapis.com/css?family=PT+Mono',
					'stack' => '"PT Mono", monospace'
				),
				'pt-sans' => array(
					'name' => 'PT Sans',
					'import' => '//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic',
					'stack' => '"PT Sans", sans-serif'
				),
				'josefin-slab' => array(
					'name' => 'Josefin Slab',
					'import' => '//fonts.googleapis.com/css?family=Josefin+Slab:300,400,600,700',
					'stack' => '"Josefin Slab", serif'
				),
				'Aarvo' => array(
					'name' => 'Arvo',
					'import' => '//fonts.googleapis.com/css?family=Arvo:400,700,400italic',
					'stack' => '"Arvo", serif'
				),
				'vollkorn' => array(
					'name' => 'Vollkorn',
					'import' => '//fonts.googleapis.com/css?family=Vollkorn:400italic,400,700',
					'stack' => '"Vollkorn", serif'
				),
				'abril-fatface' => array(
					'name' => 'Abril Fatface',
					'import' => '//fonts.googleapis.com/css?family=Abril+Fatface',
					'stack' => '"Abril Fatface", cursive;'
				),
				'old-standard-tt' => array(
					'name' => 'Old Standard TT',
					'import' => '//fonts.googleapis.com/css?family=Old+Standard+TT:400,400italic,700',
					'stack' => '"Old Standard TT", serif'
				),
				'source-sans-pro' => array(
					'name' => 'Source Sans Pro',
					'import' => '//fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,400italic',
					'stack' => '"Source Sans Pro", sans-serif'
				),
				'roboto' => array(
					'name' => 'Roboto',
					'import' => '//fonts.googleapis.com/css?family=Roboto:400,300,400italic,500,700',
					'stack' => '"Roboto", sans-serif'
				),
				'montserrat' => array(
					'name' => 'Montserrat',
					'import' => '//fonts.googleapis.com/css?family=Montserrat:400,700',
					'stack' => '"Montserrat", sans-serif'
				),
				'domine' => array(
					'name' => 'Domine',
					'import' => '//fonts.googleapis.com/css?family=Domine:400,700',
					'stack' => '"Domine", serif'
				),
				'oswald' => array(
					'name' => 'Oswald',
					'import' => '//fonts.googleapis.com/css?family=Oswald:400,700,300',
					'stack' => '"Oswald", sans-serif'
				),
				'rokkitt' => array(
					'name' => 'Rokkitt',
					'import' => '//fonts.googleapis.com/css?family=Rokkitt:400,700',
					'stack' => '"Rokkitt", serif'
				),
				'yanone-kaffeesatz' => array(
					'name' => 'Yanone Kaffeesatz',
					'import' => '//fonts.googleapis.com/css?family=Yanone+Kaffeesatz:400,300,700',
					'stack' => '"Yanone Kaffeesatz", sans-serif'
				),
				'lora' => array(
					'name' => 'Lora',
					'import' => '//fonts.googleapis.com/css?family=Lora:400,700,400italic',
					'stack' => '"Lora", serif'
				),
				'raleway' => array(
					'name' => 'Raleway',
					'import' => '//fonts.googleapis.com/css?family=Raleway:400,300,600,700',
					'stack' => '"Raleway", sans-serif'
				),
				'bitter' => array(
					'name' => 'Bitter',
					'import' => '//fonts.googleapis.com/css?family=Bitter:400,700,400italic',
					'stack' => '"Bitter", serif'
				),
				'cabin' => array(
					'name' => 'Cabin',
					'import' => '//fonts.googleapis.com/css?family=Cabin:400,600,700,400italic',
					'stack' => '"Cabin", sans-serif'
				),
				'cuprum' => array(
					'name' => 'Cuprum',
					'import' => '//fonts.googleapis.com/css?family=Cuprum:400,400italic,700',
					'stack' => '"Cuprum", sans-serif'
				),
				'lobster' => array(
					'name' => 'Lobster',
					'import' => '//fonts.googleapis.com/css?family=Lobster',
					'stack' => '"Lobster", cursive'
				),
				'playfair-display' => array(
					'name' => 'Playfair Display',
					'import' => '//fonts.googleapis.com/css?family=Playfair+Display:400,700,400italic',
					'stack' => '"Playfair Display", serif'
				),
				'bree-serif' => array(
					'name' => 'Bree Serif',
					'import' => '//fonts.googleapis.com/css?family=Bree+Serif',
					'stack' => '"Bree Serif", serif'
				),
				'cookie' => array(
					'name' => 'Cookie',
					'import' => '//fonts.googleapis.com/css?family=Cookie',
					'stack' => '"Cookie", cursive'
				),
			);

			if( $names_only ) {
				$font_names = array();
				foreach ($fonts as $key => $value) {
					$font_names[$key] = $value['name'];
				}
				return $font_names;
			} else {
				return $fonts;
			}
		}

		public static function EasyFontData( $fontname ) {
			$fonts = self::EasyFontList();
			if( isset($fontname) && isset($fonts[$fontname]) ) {
				return $fonts[$fontname];
			} else {
				return false;
			}
		}
	}
}

add_action( 'wp_ajax_easyframework_load_more', array( 'EasyFramework', 'EasyLoadMore') );












if( !class_exists('EasyMetaBox') ) {

	class EasyMetaBox {
		public $ID;
		public $title;
		public $content;
		public $post_type;
		public $meta_field;

		function __construct( $id, $title, $content, $post_type, $context = 'normal', $priority = 'high' ) {
			$this->ID = $id;
			$this->title = $title;
			$this->content = $content;
			$this->post_type = $post_type;

			$this->meta_field = '_' . $this->ID;

			add_meta_box( $this->ID, $this->title, array($this, 'callback'), $this->post_type, $context, $priority );
			add_action( 'save_post', array($this, 'save') );
		}

		function callback( $post ) {
			if( file_exists($this->content) ) {
				wp_nonce_field( $this->ID . '-nonce-action', $this->ID . '-nonce-name' );

				$meta = get_post_meta( $post->ID, $this->meta_field, true );
				$meta_field = $this->meta_field;

				include( $this->content );
			} else {
				echo 'The specified file does not exist.';
			}
		}

		function save( $post_id ) {
			if( !isset($_POST[$this->ID . '-nonce-name']) ) return;

			if( !wp_verify_nonce($_POST[$this->ID . '-nonce-name'], $this->ID . '-nonce-action') ) return;

			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) return;

			if( !isset($_POST[ $this->meta_field ]) ) {
				delete_post_meta( $post_id, $this->meta_field );
				return;
			}
			//get the new submited data
			$new_data = $_POST[ $this->meta_field ];
			//clear keys of the data array if the values are not set ot empty
			$this->clear_metabox( $new_data );

			//update the meta data of the post
			update_post_meta( $post_id, $this->meta_field, $new_data );
		}

		private function clear_metabox( &$arr ) {
			if ( is_array( $arr ) ) {
				foreach ( $arr as $i => $v ) {
					if ( is_array( $arr[ $i ] ) ) {
						$this->clear_metabox( $arr[ $i ] );
						if ( !count( $arr[ $i ] ) ) {
							unset( $arr[ $i ] );
						}
					} else {
						if ( trim( $arr[ $i ] ) == '' ) {
							unset( $arr[ $i ] );
						}
					}
				}
				if ( !count( $arr ) ) {
					$arr = null;
				}
			}
		}
	} //end class
}

?>