<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly


function attic_add_customize_style() {
	wp_register_style( 'customize-chosen-style', ATTIC_FRAMEWORK_URL . '/customizer/css/chosen.min.css', array(), ATTIC_THEME_VERSION, 'all' );
	wp_enqueue_style( 'customize-chosen-style' );

	wp_register_style( 'customize-style', ATTIC_FRAMEWORK_URL . '/customizer/css/customize.css', array(), ATTIC_THEME_VERSION, 'all' );
	wp_enqueue_style( 'customize-style' );
}

function attic_add_customize_script() {
	wp_enqueue_media();

	wp_register_script( 'customize-chosen-script', ATTIC_FRAMEWORK_URL . '/customizer/javascript/chosen.jquery.min.js', array('jquery'), ATTIC_THEME_VERSION, 'all' );
	wp_enqueue_script( 'customize-chosen-script' );

	wp_register_script( 'customize-script', ATTIC_FRAMEWORK_URL . '/customizer/javascript/customize.js', array('jquery', 'jquery-ui-sortable', 'jquery-ui-slider', 'wp-color-picker', 'customize-chosen-script'), ATTIC_THEME_VERSION, 'all' );
	wp_enqueue_script( 'customize-script' );
}

function attic_customize_css() {
	require_once( ATTIC_FRAMEWORK_DIR . '/customizer/customize-style.php' );
}

add_action( 'customize_register', 'attic_register_and_init_customizer' );
add_action( 'customize_controls_enqueue_scripts', 'attic_add_customize_style' );
add_action( 'customize_controls_enqueue_scripts', 'attic_add_customize_script' );

add_action( 'wp_head', 'attic_customize_css');

/* ============================================================================================================== */

function attic_register_and_init_customizer( $wp_customize ) {
	global $attic_wp_customize;
	$attic_wp_customize = $wp_customize;


	Class EasyCustomizer {
		private static $priority = 0;
		public static $wp_customize;
		private static $ready = false;
		private static $sep_counter = 0;

		public static function Init( $wp_customize ) {
			self::$wp_customize = $wp_customize;
			self::$ready = true;
		}

		/* SANITIZE FUNCTIONS */
		public static function sanitize_callback( $input ) {
			$output = $input;
			return $output;
		}
		
		public static function sanitize_callback_slider( $input ) {
			return sanitize_text_field( $input );
		}

		public static function sanitize_callback_color( $input ) {
			return sanitize_hex_color( $input );
		}

		public static function sanitize_callback_checkbox( $input ) {
			return sanitize_text_field($input) ? true : '';
		}

		public static function sanitize_callback_text( $input ) {
			$output = wp_kses($input, array(
					'a' => array( 'href' => true, 'title' => true, 'target' => true ),
					'br' => array(),
					'em' => array(),
					'strong' => array(),
				)
			);
			return $output;
		}
		public static function sanitize_callback_textarea( $input ) {
			$output = wp_kses($input, array(
					'a' => array( 'href' => true, 'title' => true, 'target' => true ),
					'br' => array(),
					'em' => array(),
					'strong' => array(),
					'p' => array()
				)
			);
			return $output;
		}

		public static function sanitize_callback_dropdown_pages( $input ) {
			return intval( $input );
		}

		public static function sanitize_callback_select( $input ) {
			return sanitize_key( $input );
		}

		public static function sanitize_callback_layout_select( $input ) {
			return sanitize_key( $input );
		}

		public static function sanitize_callback_media( $input ) {
			$decode = json_decode( $input );
			return $decode ? $input : '';
		}

		/* ------------------ */


		protected static function Setting( $name, $default = '', $control = false ) {

			if( method_exists('EasyCustomizer', 'sanitize_callback_' . $control) ) {
				$callback = array( 'EasyCustomizer', 'sanitize_callback_' . $control );
			} else {
				$callback = array( 'EasyCustomizer', 'sanitize_callback' );
			}

			self::$wp_customize->add_setting( $name, array(
					'default' => $default,
					'transport' => 'refresh',
					'type' => 'option',
					'sanitize_callback' => $callback
				)
			);
		}

		public static function Panel( $settings ) {
			if( !self::$ready ) return;
			if( !isset($settings['id']) || empty($settings['id']) ) return;

			self::$wp_customize->add_panel( $settings['id'],
				array(
					'title' => isset($settings['title']) ? $settings['title'] : '',
					'description' => isset($settings['description']) ? $settings['description'] : '',
					'priority' => isset($settings['priority']) ? $settings['priority'] : 0,
				)
			);

			return $settings['id'];
		} //end AddSection

		public static function Section( $settings ) {
			if( !self::$ready ) return;
			if( !isset($settings['id']) || empty($settings['id']) ) return;

			self::$wp_customize->add_section( $settings['id'],
				array(
					'title' => isset($settings['title']) ? $settings['title'] : '',
					'description' => isset($settings['description']) ? $settings['description'] : '',
					'priority' => isset($settings['priority']) ? $settings['priority'] : 0,
					'panel' => isset($settings['panel']) ? $settings['panel'] : '',
				)
			);

			if( isset($settings['controls']) && is_array($settings['controls']) ) {
				foreach ($settings['controls'] as $control) {
					if( isset($control['type']) && method_exists('EasyCustomizer', $control['type']) ) {
						call_user_func( array('EasyCustomizer', $control['type']), $settings['id'], $control );
					}
				}//end foreach
			}

			return $settings['id'];
		} //end AddSection

		public static function Separator( $section_id, $args ) {
			if( !self::$ready ) return;
			$name = 'separator-' . self::$sep_counter;

			self::Setting( $name, '', 'separator' );

			self::$wp_customize->add_control(
				new WP_Customize_Separator( self::$wp_customize, $name . '-control',
					array(
						'label' => isset($args['label']) ? $args['label'] : '',
						'description' => isset($args['description']) ? $args['description'] : '',
						'section' => $section_id,
						'settings' => $name,
						'type' => 'attic-separator',
						'priority' => self::$priority,
					)
				)
			);
			self::$sep_counter++;
			self::$priority++;
		} //AddSeparator

		public static function Color( $section_id, $settings = '' ) {
			if( !self::$ready ) return;

			self::Setting( $settings['name'], isset($settings['default']) ? $settings['default'] : '', 'color' );

			self::$wp_customize->add_control(
				new WP_Customize_Color_Control(
					self::$wp_customize,
					$settings['name'] . '-control',
					array(
						'label' => isset($settings['label']) ? $settings['label'] : null,
						'description' => isset($settings['description']) ? $settings['description'] : null,
						'section' => $section_id,
						'settings' => $settings['name'],
						'priority' => self::$priority,
					)
				)
			);
			self::$priority++;
		} //AddColor

		public static function Text( $section_id, $settings = '' ) {
			if( !self::$ready ) return;

			self::Setting( $settings['name'], isset($settings['default']) ? $settings['default'] : '', 'text' );

			self::$wp_customize->add_control( $settings['name'],
				array(
					'label' => isset($settings['label']) ? $settings['label'] : null,
					'description' => isset($settings['description']) ? $settings['description'] : null,
					'section' => $section_id,
					'settings' => $settings['name'],
					'type' => 'text',
					'priority' => self::$priority,
				)
			);
			self::$priority++;
		} //addText

		public static function Checkbox( $section_id, $settings = '' ) {
			if( !self::$ready ) return;

			self::Setting( $settings['name'], isset($settings['default']) ? $settings['default'] : false, 'checkbox' );

			self::$wp_customize->add_control( $settings['name'],
				array(
					'label' => isset($settings['label']) ? $settings['label'] : null,
					'description' => isset($settings['description']) ? $settings['description'] : null,
					'section' => $section_id,
					'settings' => $settings['name'],
					'type' => 'checkbox',
					'std' => $settings['default'] ? $settings['default'] : null,
					'priority' => self::$priority,
				)
			);
			self::$priority++;
		} //AddCheckbox

		public static function Textarea( $section_id, $settings = '' ) {
			if( !self::$ready ) return;

			self::Setting( $settings['name'], isset($settings['default']) ? $settings['default'] : '', 'textarea' );

			$wp_customize_textarea = new WP_Customize_Textarea(
				self::$wp_customize, 
				$settings['name'],
				array(
					'label' => isset($settings['label']) ? $settings['label'] : null,
					'description' => isset($settings['description']) ? $settings['description'] : null,
					'section' => $section_id,
					'settings' => $settings['name'],
					'type' => 'attic-textarea',
					'priority' => self::$priority,
				)
			);

			$wp_customize_textarea->rows = isset($settings['rows']) ? $settings['rows'] : 2;
			self::$wp_customize->add_control( $wp_customize_textarea );

			self::$priority++;
		} //AddText

		public static function DropdownPages( $section_id, $settings = '' ) {
			if( !self::$ready ) return;

			self::Setting( $settings['name'], '', 'dropdown_pages' );

			self::$wp_customize->add_control(
				$settings['name'],
				array(
					'label' => isset($settings['label']) ? $settings['label'] : null,
					'description' => isset($settings['description']) ? $settings['description'] : null,
					'section' => $section_id,
					'settings' => $settings['name'],
					'type' => 'dropdown-pages',
					'priority' => self::$priority,
				)
			);
			self::$priority++;
		} //AddDropdownPages

		public static function Slider( $section_id, $settings = '' ) {
			if( !self::$ready ) return;

			$unit = isset($settings['unit']) ? $settings['unit'] : '';
			$default = isset($settings['default']) ? $settings['default'] . $unit : '0' . $unit;

			self::Setting( $settings['name'], $default, 'slider' );

			$wp_customize_slider = new WP_Customize_Slider(
				self::$wp_customize, 
				$settings['name'],
				array(
					'label' => isset($settings['label']) ? $settings['label'] : null,
					'description' => isset($settings['description']) ? $settings['description'] : null,
					'section' => $section_id,
					'settings' => $settings['name'],
					'type' => 'attic-slider',
					'priority' => self::$priority,
				)
			);

			
			$wp_customize_slider->options = array(
				'min' => isset($settings['min']) ? $settings['min'] : 0,
				'max' => isset($settings['max']) ? $settings['max'] : 10,
				'step' => isset($settings['step']) ? $settings['step'] : 1,
				'default' => $default,
				'unit' => $unit,
			);
			self::$wp_customize->add_control( $wp_customize_slider );

			self::$priority++;
		} //AddSlider

		public static function Media( $section_id, $settings = '' ) {
			if( !self::$ready ) return;

			self::Setting( $settings['name'], '', 'media' );

			$wp_customize_media = new WP_Customize_Media(
				self::$wp_customize,
				$settings['name'],
				array(
					'label' => isset($settings['label']) ? $settings['label'] : null,
					'description' => isset($settings['description']) ? $settings['description'] : null,
					'section' => $section_id,
					'settings' => $settings['name'],
					'type' => 'attic-media',
					'priority' => self::$priority,
				)
			);

			$wp_customize_media->options = array(
				'uploader-title' => isset($settings['uploader-title']) ? $settings['uploader-title'] : 'Select Media',
				'button-label' => isset($settings['button-label']) ? $settings['button-label'] : 'Add Media',
				'multiple' => isset($settings['multiple']) ? $settings['multiple'] : false,
				'bg-color' => isset($settings['bg-color']) ? $settings['bg-color'] : '#f8f8f8',
				'library-type' => isset($settings['library-type']) ? $settings['library-type'] : 'image',
			);
			self::$wp_customize->add_control( $wp_customize_media );

			self::$priority++;
		} //AddMedia

		public static function Select( $section_id, $settings = '' ) {
			if( !self::$ready ) return;

			self::Setting( $settings['name'], isset($settings['default']) ? $settings['default'] : '', 'select' );

			self::$wp_customize->add_control(
				$settings['name'],
				array(
					'label' => isset($settings['label']) ? $settings['label'] : null,
					'description' => isset($settings['description']) ? $settings['description'] : null,
					'section' => $section_id,
					'type' => 'select',
					'choices' => $settings['choices'],
					'priority' => self::$priority,
				)
			);
			self::$priority++;
		} //AddSelect

		public static function LayoutSelect( $section_id, $settings = '' ) {
			if( !self::$ready ) return;

			self::Setting( $settings['name'],isset($settings['default']) ? $settings['default'] : '', 'layout_select' );

			$wp_layout_select = new WP_Customize_LayoutSelect(
				self::$wp_customize,
				$settings['name'],
				array(
					'label' => isset($settings['label']) ? $settings['label'] : null,
					'description' => isset($settings['description']) ? $settings['description'] : null,
					'section' => $section_id,
					'settings' => $settings['name'],
					'type' => 'attic-layout-select',
					'priority' => self::$priority,
				)
			);
			$wp_layout_select->options = isset($settings['options']) ? $settings['options'] : array();
			$wp_layout_select->layouts = isset($settings['layouts']) ? $settings['layouts'] : array();
			$wp_layout_select->default = isset($settings['default']) ? $settings['default'] : '';

			self::$wp_customize->add_control( $wp_layout_select );

			self::$priority++;
		} //AddLayoutSelect

	} //End Class

	



	/* Custom control classes */
	class WP_Customize_LayoutSelect extends WP_Customize_Control {
		public $type = 'attic-layout-select';

		public function render_content() {
			?><span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
			<div class="customize-layout-select">
				<div class="customize-layout-select-selected" title="<?php _e('Select Layout', 'skoty'); ?>"><?php echo isset($this->layouts[$this->value()]) ? $this->layouts[$this->value()] : $this->layouts[$this->default]; ?></div>
				<ul class="customize-layout-select-list">
					<?php foreach( $this->layouts as $class => $layout ) { ?>
						<li data-class="<?php echo esc_attr($class); ?>"><?php echo $layout; ?></li>
					<?php } ?>
				</ul>
			</div>
			<input class="customize-layout-select-field" type="hidden" <?php $this->link(); ?> value="<?php echo $this->value(); ?>"/><?php
		}
	}

	class WP_Customize_Media extends WP_Customize_Control {
		public $type = 'attic-media';

		public function render_content() {
			$multiple = isset($this->options['multiple']) && $this->options['multiple'] ? 'true' : 'false';
			$value = json_decode( $this->value(), true );
			$media_src = null;
			$bg_color_style = isset($this->options['bg-color']) ? 'style="background-color: '.$this->options['bg-color'].'"' : '';

			if( $value && isset($value['ID']) ) {
				if( isset($value['type']) && $value['type'] === 'image' ) {
					$img_size = isset($this->options['image-size']) ? $this->options['image-size'] : 'medium';
					$img_data = wp_get_attachment_image_src( $value['ID'], $img_size );
					$media_src = isset($img_data[0]) ? $img_data[0] : null;
				} elseif( isset($value['type']) && $value['type'] === 'video' ) {
					$media_src = $value['url'];
				}
			}
			?>
			<div class="customize-media-wrapper">
				<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<div class="customize-media <?php echo $media_src ? 'has-media-thumb' : ''; ?> <?php echo ($media_src && isset($value['type']) && $value['type'] === 'video') ? 'is-video' : ''; ?>" data-uploader-title="<?php echo isset($this->options['uploader-title']) ? $this->options['uploader-title'] : __('Select Media', 'skoty'); ?>" data-button-label="<?php echo isset($this->options['button-label']) ? $this->options['button-label'] : __('Select Media', 'skoty'); ?>" data-fallback-title="<?php echo isset($this->options['fallback-title']) ? $this->options['fallback-title'] : __('Select Fallback Media', 'skoty'); ?>" data-fallback-button-label="<?php echo isset($this->options['fallback-button-label']) ? $this->options['fallback-button-label'] : __('Select Fallback Media', 'skoty'); ?>" data-multiple="<?php echo $multiple; ?>" data-type="<?php echo isset($this->options['library-type']) ? $this->options['library-type'] : 'image' ?>" data-image-size="<?php echo isset($this->options['image-size']) ? $this->options['image-size'] : 'medium'; ?>">

					<div class="customize-media-preview" <?php echo $bg_color_style; ?>>
						<?php if( $media_src && isset($value['type']) && $value['type'] === 'image' ) { ?>
							<img class="image-attachment-preview" src="<?php echo $media_src; ?>" alt="" />
						<?php } elseif( $media_src && isset($value['type']) && $value['type'] === 'video' ) {

							if( $value && isset($value['fallback']) && isset($value['fallback']['ID']) ) {
								$fallback_img_data = wp_get_attachment_image_src( $value['fallback']['ID'], 'thumbnail' );
								$fallback_img_src = isset($fallback_img_data[0]) ? $fallback_img_data[0] : null;
								?><img class="fallback-media-preview" src="<?php echo $fallback_img_src; ?>" alt="" /><?php
							}

							?><video src="<?php echo $media_src; ?>" loop muted autoplay></video><?php
						} ?>

						<a class="customize-media-remove"></a>
						<a class="customize-media-set-fallback" title="<?php echo isset($this->options['fallback-title']) ? $this->options['fallback-title'] : __('Select Fallback Media', 'skoty'); ?>"></a>

						<a class="customize-media-select"><?php echo isset($this->options['uploader-title']) ? $this->options['uploader-title'] : __('Select Media', 'skoty'); ?></a>
					</div>
					<textarea class="customize-media-url-field hidden" type="hidden" <?php $this->link(); ?>><?php echo $this->value(); ?></textarea>
				</div>
			</div>
			<?php
		}
	}

	class WP_Customize_Separator extends WP_Customize_Control {
		public $type = 'attic-separator';

		public function render_content() {
		?>
			<?php if( !empty($this->label) ) { ?>
				<span class="customize-control-title separator-title"><?php echo esc_html( $this->label ); ?></span>
			<?php } ?>
			<hr class="custom-separator">
			<?php if( !empty($this->description) ) { ?>
				<span class="customize-control-description description"><?php echo esc_html( $this->description ); ?></span>
			<?php } ?>
			<input type="hidden" <?php $this->link(); ?> value=""/>
		<?php
		}
	}

	class WP_Customize_Slider extends WP_Customize_Control {
		public $type = 'attic-slider';

		public function render_content() {
		?>
			<div class="customize-slider-container">
				<label>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>

					<input type="text" class="sider-value" <?php $this->link(); ?> value="<?php echo sanitize_text_field( $this->value() ); ?>" />

					<div class="customize-slider" data-unit="<?php echo isset($this->options['unit']) ? esc_attr($this->options['unit']) : ''; ?>" data-min="<?php echo esc_attr($this->options['min']);?>" data-max="<?php echo esc_attr($this->options['max']);?>" data-val="<?php echo esc_attr($this->options['default']);?>" data-step="<?php echo esc_attr($this->options['step']); ?>"></div>
				</label>
			</div>
		<?php
		}
	}

	class WP_Customize_Textarea extends WP_Customize_Control {
		public $type = 'attic-textarea';

		public function render_content() {
		?>
			<label>
				<?php if( !empty( $this->label ) ) { ?>
					<span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
				<?php } ?>
				<?php if( !empty( $this->description ) ) { ?>
					<span class="description customize-control-description"><?php echo esc_html($this->description); ?></span>
				<?php } ?>
				<textarea rows="<?php echo esc_attr($this->rows); ?>" type="text" class="sider-value" <?php $this->link(); ?> ><?php echo esc_textarea( $this->value() ); ?></textarea>
			</label>
		<?php
		}
	}



	/* init the customizer */
	EasyCustomizer::Init( $wp_customize );
	//include the settings PHP file
	include_once( ATTIC_FRAMEWORK_DIR . '/customizer/settings.php' );
}
?>