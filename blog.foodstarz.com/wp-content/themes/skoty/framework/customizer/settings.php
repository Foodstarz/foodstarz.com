<?php
	/* $wp_customize is available */

	//delete_option(ATTIC_OPTIONS_NAMESPACE);




	/*=================================================================*/
	/*=================================================================*/
	/*=====================ADDITIONAL NAV SETTINGS=====================*/
	/*=================================================================*/
	/*=================================================================*/
	EasyCustomizer::Section( array(
			'id' => 'nav-settings',
			'title' => __('Navigation Style', 'skoty'),
			'description' => '',
			'priority' => 110,
			'controls' => array(
				array(
					'type' => 'Select',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[nav-style]',
					'label' => __('Style', 'skoty'),
					'default' => 'side',
					'choices' => array(
						'side' => __('Sidebar', 'skoty'),
						'horizontal' => __('Horizontal', 'skoty'),
						'drop-down' => __('Drop Down', 'skoty')
					),
				),
				array( 'type' => 'Separator' ),
				array(
					'type' => 'Checkbox',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[show-home-icon]',
					'label' => __('Show Home Icon.', 'skoty'),
					'default' => false
				),
				array( 'type' => 'Separator' ),
				array(
					'type' => 'Checkbox',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[disable-search-icon]',
					'label' => __('Remove Search Icon.', 'skoty'),
					'default' => false
				),

			) //end controls
		)
	);



	




	/*=================================================================*/
	/*=================================================================*/
	/*==========================LOGO SETTINGS==========================*/
	/*=================================================================*/
	/*=================================================================*/
	EasyCustomizer::Section( array(
			'id' => 'logo-settings',
			'title' => __('Logo Settings', 'skoty'),
			'description' => '',
			'priority' => 200,
			'controls' => array(
				array(
					'type' => 'Media',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[logo-image]',
					'label' => __('Logo', 'skoty'),
					'uploader-title' => 'Select Logo Image',
					'button-label' => 'Set Logo Image',
					'multiple' => false,
					'bg-color' => '#C6D0D8',
					'library-type' => 'image',
				),
				array( 'type' => 'Separator' ),
				array(
					'type' => 'Slider',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[top-offset]',
					'label' => __('Logo Top Offset', 'skoty'),
					'min' => -100,
					'max' => 100,
					'step' => 1,
					'default' => 0,
					'unit' => 'px'
				),
				array(
					'type' => 'Slider',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[left-offset]',
					'label' => __('Logo Left Offset', 'skoty'),
					'min' => -100,
					'max' => 100,
					'step' => 1,
					'default' => 0,
					'unit' => 'px'
				),
				array( 'type' => 'Separator' ),
				array(
					'type' => 'Textarea',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[header-logo-adjacent-content]',
					'default' => '',
					'label' => __('Logo Adjacent Content', 'skoty'),
					'rows' => 4
				),

			) //end controls
		)
	);






	

	/*=================================================================*/
	/*=================================================================*/
	/*=========================HEADER SETTINGS=========================*/
	/*=================================================================*/
	/*=================================================================*/
	EasyCustomizer::Section( array(
			'id' => 'header-settings',
			'title' => __('Header Settings', 'skoty'),
			'description' => '',
			'priority' => 201,
			'controls' => array(
				array(
					'type' => 'Media',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[header-media]',
					'label' => __('Header Media', 'skoty'),
					'uploader-title' => __('Select Header Media', 'skoty'),
					'button-label' => __('Add Header Media', 'skoty'),

					'fallback-title' => __('Select Header Fallback', 'skoty'),
					'fallback-button-label' => __('Add Header Fallback', 'skoty'),

					'library-type' => 'image, video',
					'multiple' => false
				),
				array(
					'type' => 'Slider',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[header-media-opacity]',
					'label' => __('Header Media Opacity', 'skoty'),
					'min' => 0,
					'max' => 100,
					'step' => 1,
					'default' => 40,
					'unit' => '%'
				),
				array(
					'type' => 'Media',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[header-pattern]',
					'label' => __('Header Pattern', 'skoty'),
					'uploader-title' => 'Select Header Pattern',
					'button-label' => 'Add Header Pattern',
					'multiple' => false,
					'bg-color' => '#C6D0D8'
				),
				array(
					'type' => 'Slider',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[header-pattern-opacity]',
					'label' => __('Header Pattern Opacity', 'skoty'),
					'min' => 0,
					'max' => 100,
					'step' => 1,
					'default' => 10,
					'unit' => '%'
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[header-media-bg-color]',
					'default' => '#31323a',
					'label' => __('Header Media Background Color', 'skoty'),
				),
				array(
					'type' => 'Select',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[header-height-style]',
					'label' => __('Select Header Height Style', 'skoty'),
					'default' => 'default',
					'choices' => array(
						'default' => __('Window Height', 'skoty'),
						'content' => __('Header Content Height', 'skoty'),
					),
				),
				array(
					'type' => 'Checkbox',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[disable-parallax-header]',
					'label' => __('Disable Parallax Effect', 'skoty'),
					'default' => false
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Textarea',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[header-heading]',
					'default' => '',
					'label' => __('Heading / Title', 'skoty'),
					'rows' => 2
				),
				array(
					'type' => 'Textarea',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[header-text]',
					'default' => '',
					'label' => __('Sub Heading / Tagline', 'skoty'),
					'rows' => 4
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Media',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[avatar-image]',
					'label' => __('Avatar Image', 'skoty'),
					'uploader-title' => 'Select Avatar Image',
					'button-label' => 'Set Avatar',
					'multiple' => false,
					'image-size' => 'skoty-avatar',
				),
				array(
					'type' => 'Checkbox',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[remove-avatar]',
					'label' => __('Disable Avatar Completely.', 'skoty'),
					'default' => false
				),
				array(
					'type' => 'Text',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[about-button-label]',
					'default' => '',
					'label' => __('"About Me" Button Label', 'skoty')
				),
				array(
					'type' => 'Text',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[about-button-link]',
					'default' => '',
					'description' => __('If left empty, default behavior will be used.', 'skoty'),
					'label' => __('"About Me" Button Link', 'skoty')
				),
				array(
					'type' => 'Select',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[avatar-user-id]',
					'label' => __('User To Get The Gravatar And About Data From', 'skoty'),
					'choices' => EasyFramework::EasyUsers(),
				),
				array(
					'type' => 'DropdownPages',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[about-page]',
					'label' => __('Page To Use as About Page', 'skoty')
				),

			) //end controls
		)
	);










	/*=================================================================*/
	/*=================================================================*/
	/*=========================COLOR SETTINGS==========================*/
	/*=================================================================*/
	/*=================================================================*/
	EasyCustomizer::Section( array(
			'id' => 'color-settings',
			'title' => __('Color Settings', 'skoty'),
			'description' => '',
			'priority' => 202,
			'controls' => array(
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[body-background-color]',
					'default' => '#fff',
					'label' => __('Background Color', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[body-text-color]',
					'default' => '#474851',
					'label' => __('Body Text Color', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[heading-color]',
					'default' => '#1b1c23',
					'label' => __('Heading Color', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[heading-inverse-color]',
					'default' => '#fff',
					'label' => __('Heading Inverse Color', 'skoty'),
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[primary-color]',
					'default' => '#31323a',
					'label' => __('Primary Color', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[secondary-color]',
					'default' => '#43444d',
					'label' => __('Secondary Color', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[sidebar-bg-color]',
					'default' => '#232329',
					'label' => __('Sidebar Background Color', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[accent-color]',
					'default' => '#42d18c',
					'label' => __('Accent Color', 'skoty'),
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[horizontal-nav-accent-color]',
					'default' => '#42d18c',
					'label' => __('Horizontal Navigation Accent Color', 'skoty'),
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[footer-copyright-text-color]',
					'default' => '#b6b8c1',
					'label' => __('Footer Copyright Text Color', 'skoty'),
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[post-bg-color]',
					'default' => '#f8f8f8',
					'label' => __('Post Background Color (Aside, Quote, etc...)', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[service-icon-color]',
					'default' => '#31323a',
					'label' => __('Services Icon Color', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[post-icon-color]',
					'default' => '#bebfc9',
					'label' => __('Post Icons/Info Color', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[border-color]',
					'default' => '#f0f1f6',
					'label' => __('Border Color', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[border-color-inverse]',
					'default' => '#4b4c54',
					'label' => __('Border Color on Dark Backgrounds', 'skoty'),
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[input-placeholder-color]',
					'default' => '#bebfc9',
					'label' => __('Input Placeholder Color', 'skoty'),
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Color',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[button-bg-color]',
					'default' => '#31323a',
					'label' => __('Buttons Background Color', 'skoty'),
				),
			) //end controls
		)
	);











	/*=================================================================*/
	/*=================================================================*/
	/*=======================BLOG SETTINGS===========================*/
	/*=================================================================*/
	/*=================================================================*/
	EasyCustomizer::Section( array(
			'id' => 'blog-settings',
			'title' => __('Blog Settings', 'skoty'),
			'description' => '',
			'priority' => 203,
			'controls' => array(
				array(
					'type' => 'Select',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[post-gallery-type]',
					'default' => 'default',
					'label' => __('Gallery Posts Style', 'skoty'),
					'choices' => array(
						'slider' => __('Slider Galleries', 'skoty'),
						'default' => __('Default Galleries', 'skoty'),
					),
				),
				array(
					'type' => 'Checkbox',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[show-related-posts]',
					'label' => __('Show related posts under post content.', 'skoty'),
					'default' => false
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Checkbox',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[show-related-posts]',
					'label' => __('Show Related Posts', 'skoty'),
					'default' => false
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Slider',
					'name' => 'posts_per_page',
					'label' => __('Articles Per Page (Same as in Settings->Reading)', 'skoty'),
					'min' => 1,
					'max' => 100,
					'step' => 1,
					'default' => 10,
					'unit' => ''
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Select',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[pagination-system]',
					'default' => 'pagination',
					'label' => __('Article Pagination System', 'skoty'),
					'choices' => array(
						'pagination' => __('Standart Pagination', 'skoty'),
						'load-more' => __('Load More via AJAX', 'skoty'),
						'infinite-scroll' => __('Infinite Scroll', 'skoty'),
					),
				),
				array(
					'type' => 'Select',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[load-more-label-style]',
					'default' => 'pagination',
					'label' => __('Load More Label Style', 'skoty'),
					'choices' => array(
						'plus' => __('+ Icon', 'skoty'),
						'text' => __('Text Label', 'skoty'),
					),
				),
				array(
					'type' => 'Text',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[load-more-label]',
					'default' => __('Load More Articles', 'skoty'),
					'label' => __('Load More Label', 'skoty')
				),
			) //end controls
		)
	);
















	/*=================================================================*/
	/*=================================================================*/
	/*========================GENERAL SETTINGS=========================*/
	/*=================================================================*/
	/*=================================================================*/
	EasyCustomizer::Section( array(
			'id' => 'general-settings',
			'title' => __('General Settings', 'skoty'),
			'description' => '',
			'priority' => 204,
			'controls' => array(
				array(
					'type' => 'Select',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[spec-animation-easing]',
					'label' => __('Search Form & Sidebar Animation Easing', 'skoty'),
					'default' => 'elastic',
					'choices' => array(
						'elastic' => __('Elastic', 'skoty'),
						'ease' => __('Ease', 'skoty'),
						/*'ease-in' => __('Ease In', 'skoty'),
						'ease-out' => __('Ease Out', 'skoty'),
						'ease-in-out' => __('Ease In Out', 'skoty'),*/
						'linear' => __('Linear', 'skoty')
					),
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Checkbox',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[use-smoothscroll]',
					'label' => __('Use SmoothScroll for Google Chrome', 'skoty'),
					'default' => false
				),
			) //end controls
		)
	);












	/*=================================================================*/
	/*=================================================================*/
	/*=========================FONT SETTINGS===========================*/
	/*=================================================================*/
	/*=================================================================*/
	EasyCustomizer::Section( array(
			'id' => 'nav-settings',
			'id' => 'font-settings',
			'title' => __('Typography Settings', 'skoty'),
			'description' => __('Change body and heading font families and sizes.', 'skoty'),
			'priority' => 205,
			'controls' => array(
				array(
					'type' => 'Select',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[body-font-family]',
					'label' => __('Select Body Text Font Family', 'skoty'),
					'default' => 'open-sans',
					'choices' => EasyFramework::EasyFontList( true ),
				),
				array(
					'type' => 'Slider',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[body-text-size]',
					'label' => __('Body Text Size', 'skoty'),
					'min' => 8,
					'max' => 24,
					'step' => 1,
					'default' => 16,
					'unit' => 'px'
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Select',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[heading-font-family]',
					'label' => __('Select Font Family for Headings', 'skoty'),
					'default' => 'open-sans',
					'choices' => EasyFramework::EasyFontList( true ),
				),
			) //end controls
		)
	);


















	/*=================================================================*/
	/*=================================================================*/
	/*=======================FOOTER SETTINGS===========================*/
	/*=================================================================*/
	/*=================================================================*/
	EasyCustomizer::Section( array(
			'id' => 'footer-settings',
			'title' => __('Footer Settings', 'skoty'),
			'description' => '',
			'priority' => 206,
			'controls' => array(
				array(
					'type' => 'LayoutSelect',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[accomplishment-layout]',
					'default' => 'one-fourths',
					'label' => __('Accomplishments Layout', 'skoty'),
					'layouts' => array(
						'full-width' => '<span class="full-width"></span>',
						'one-halves' => '<span class="one-half"></span><span class="one-half"></span>',
						'one-thirds' => '<span class="one-third"></span><span class="one-third"></span><span class="one-third"></span>',
						'one-fourths' => '<span class="one-fourth"></span><span class="one-fourth"></span><span class="one-fourth"></span><span class="one-fourth"></span>',
					),
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'LayoutSelect',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[footer-layout]',
					'default' => 'one-half-one-fourths',
					'label' => __('Footer Widgets Layout', 'skoty'),
					'layouts' => array(
						'full-width' => '<span class="full-width"></span>',
						'one-halves' => '<span class="one-half"></span><span class="one-half"></span>',
						'one-thirds' => '<span class="one-third"></span><span class="one-third"></span><span class="one-third"></span>',
						'two-thirds-one-third' => '<span class="two-thirds"></span><span class="one-third">',
						'one-third-two-thirds' => '<span class="one-third"></span><span class="two-thirds">',
						'one-fourths' => '<span class="one-fourth"></span><span class="one-fourth"></span><span class="one-fourth"></span><span class="one-fourth"></span>',
						'one-half-one-fourths' => '<span class="one-half"></span><span class="one-fourth"></span><span class="one-fourth"></span>',
						'one-fourth-one-half-one-fourth' => '<span class="one-fourth"></span><span class="one-half"></span><span class="one-fourth"></span>',
						'one-fourths-one-half' => '<span class="one-fourth"></span><span class="one-fourth"></span><span class="one-half"></span>',
					),
				),
				array(
					'type' => 'Textarea',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[footer-end-text]',
					'default' => 'Thanks for the visit.',
					'label' => __('Footer End Text', 'skoty')
				),
				array(
					'type' => 'Textarea',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[copyright-notice]',
					'default' => '&copy; Copyright 2014. All rights reserved. Designed with passion by atticthemes.',
					'label' => __('Copyright Notice Text', 'skoty'),
					'rows' => 3
				),

			) //end controls
		)
	);










	/*=================================================================*/
	/*=================================================================*/
	/*==========================404 SETTINGS===========================*/
	/*=================================================================*/
	/*=================================================================*/
	EasyCustomizer::Section( array(
			'id' => '404-settings',
			'title' => __('404 Page Settings', 'skoty'),
			'description' => '',
			'priority' => 207,
			'controls' => array(
				array(
					'type' => 'Media',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[error-404-header-media]',
					'label' => __('404 Header Media', 'skoty'),
					'options' => array(
						'uploader-title' => 'Select Header Media',
						'button-label' => 'Add Header Media',
						
						'fallback-title' => __('Select Header Fallback', 'skoty'),
						'fallback-button-label' => __('Add Header Fallback', 'skoty'),

						'library-type' => 'image, video',
						'multiple' => false
					)
				),
				array(
					'type' => 'Separator',
				),
				array(
					'type' => 'Text',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[error-404-heading]',
					'default' => __('404', 'skoty'),
					'label' => __('404 Heading', 'skoty'),
				),
				array(
					'type' => 'Textarea',
					'name' => ATTIC_OPTIONS_NAMESPACE . '[error-404-sub-heading]',
					'default' => __('It looks like nothing was found at this location. Maybe try a search?', 'skoty'),
					'label' => __('404 Sub Heading', 'skoty'),
					'rows' => 2
				),
			) //end controls
		)
	);







	/*=================================================================*/
	/*=================================================================*/
	/*=======================SOCIAL ICON SETTINGS=======================*/
	/*=================================================================*/
	/*=================================================================*/
	if( class_exists('AttichThemes_Social') ) {
		EasyCustomizer::Section( array(
				'id' => 'social-icon-settings',
				'title' => __('Social Icon Settings', 'skoty'),
				'description' => __('This section allows you to set the social icons colors or change the style to the plugin\'s style.', 'skoty'),
				'priority' => 208,
				'controls' => array(
					array(
						'type' => 'Color',
						'name' => ATTIC_OPTIONS_NAMESPACE . '[social-icon-background-color]',
						'default' => '#1b1c23',
						'label' => __('Icon Background Color', 'skoty'),
					),
					array(
						'type' => 'Slider',
						'name' => ATTIC_OPTIONS_NAMESPACE . '[social-icon-background-opacity]',
						'label' => __('Icon Background Opacity', 'skoty'),
						'min' => 0,
						'max' => 100,
						'step' => 1,
						'default' => 100,
						'unit' => '%'
					),
					array(
						'type' => 'Color',
						'name' => ATTIC_OPTIONS_NAMESPACE . '[social-icon-color]',
						'default' => '#fff',
						'label' => __('Icon Color', 'skoty'),
					),
				) //end controls
			)
		);
	} //END if social icons plugin exists












	/*=================================================================*/
	/*=================================================================*/
	/*=========================SHARING SETTINGS========================*/
	/*=================================================================*/
	/*=================================================================*/

	if( class_exists('AtticThemes_BlogExtender') ) {
		EasyCustomizer::Section( array(
				'id' => 'sharing-settings',
				'title' => __('Sharing Settings', 'skoty'),
				'description' => __('Ckeck the boxes next to the social networks that you wish to enable sharing for.', 'skoty'),
				'priority' => 209,
				'controls' => array(
					array(
						'type' => 'Checkbox',
						'name' => ATTIC_OPTIONS_NAMESPACE . '[use-facebook-sharing]',
						'label' => __('Use Facebook Sharing', 'skoty'),
						'default' => false
					),
					array(
						'type' => 'Checkbox',
						'name' => ATTIC_OPTIONS_NAMESPACE . '[use-twitter-sharing]',
						'label' => __('Use Twitter Sharing', 'skoty'),
						'default' => false
					),
					array(
						'type' => 'Checkbox',
						'name' => ATTIC_OPTIONS_NAMESPACE . '[use-google-plus-sharing]',
						'label' => __('Use Google Plus Sharing', 'skoty'),
						'default' => false
					),
					array(
						'type' => 'Checkbox',
						'name' => ATTIC_OPTIONS_NAMESPACE . '[use-pinterest-sharing]',
						'label' => __('Use Pinterest Sharing', 'skoty'),
						'default' => false
					),
				) //end controls
			)
		);

	}  //END if social icons plugin exists















/*

*****Adding a new section*****
	
	//@$arguments = Array() of key value pairs, see below for more details.
	//@returns the ID of the section (string).
	
	$arguments = array(
		'id' => 'section-unique-id', //Set the ID of the section. must be unique.
		'title' => __('Section Title', 'text_domain'), //Set the Title of the section.
		'description' => __('Section description', 'text_domain'), //Write a description for the section.
		'priority' => 201 //Set a priority for the section. This determines the position in the list of sections.
	);

	$section_id = EasyCustomizer::Section( $arguments ); //Instantiate the CustomizeSection class with the $arguments as argument.



	
*****Adding Controls/Settings to a section by calling a Class method of EasyCustomizer.*****

	//@$control_arguments = Array() of key value pairs, see below for more details.

		//@name - Set the name of the option/setting by concatinating the "options namespace" (the array name that is being saved in the options table) value to the [option-key-name] (in Square Brackets as a string).

		//@default - set the default value of the setting
	
		$control_arguments = array(
			'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]', 
			'default' => '',
			'label' => __('Heading / Title', 'text_domain'), //Set the label/title of the setting
			'rows' => 2 //set the number of the rows of the textarea
		);
	
	//Call the Class method of the EasyCustomizer class to add the setting/control to the section (first argument, section id as a string) with the $control_arguments as 2nd argument.

	EasyCustomizer::Textarea( $section_id, $control_arguments );



*****Adding Controls/Settings directly to a section by adding providing the list of controls as an array.*****
	

	$arguments = array(
		'id' => 'section-unique-id', //Set the ID of the section. must be unique.
		'title' => __('Section Title', 'text_domain'), //Set the Title of the section.
		'description' => __('Section description', 'text_domain'), //Write a description for the section.
		'priority' => 201 //Set a priority for the section. This determines the position in the list of sections.
		'controls' => array(
			array(
				'type' => 'Textarea', //Control type. See the control types for more details
				'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]',
				'default' => '',
				'label' => __('Heading / Title', 'text_domain'), //Set the label/title of the setting
				'rows' => 2 //set the number of the rows of the textarea
			),
			array(
				'type' => 'Color', //Control type. See the control types for more details
				'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]',
				'default' => '#ff0000', //Set the default color value as a Hexidecimal number
				'label' => __('Color Title', 'text_domain'), //Set the label/title of the setting
			),
		) //array of the controls, where each array represents a single control
	);

	EasyCustomizer::Section( $arguments );





*****Available controls and their argument structure.*****

//Textarea (a Text area)
	
	EasyCustomizer::Textarea( $section_id, array(
			'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]', 
			'default' => '',
			'label' => __('Heading / Title', 'text_domain'), //Set the label/title of the setting
			'rows' => 2 //set the number of the rows of the textarea
		)
	); 






//Text (A single text input/field)

	EasyCustomizer::Text( $section_id, array(
			'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]', 
			'default' => '',
			'label' => __('Heading / Title', 'text_domain'), //Set the label/title of the setting
		)
	); 





//Color

	EasyCustomizer::Color( $section_id, array(
			'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]',
			'default' => '#ff0000', //Set the default color value as a Hexidecimal number
			'label' => __('Color Title', 'text_domain'), //Set the label/title of the setting
		)
	);





//Media

	EasyCustomizer::Media( $section_id, array(
			'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]',
			'label' => __('Media Select Control Title', 'text_domain'), //Set the label/title of the setting
			'options' => array(
				'uploader-title' => __('Media Window Title', 'text_domain'), //Set the media window title
				'button-label' => __('Media Window Button Label', 'text_domain'), //Set the media window button label
				
				'fallback-title' => __('Media Fallback Window Title', 'skoty'),  //Set the falback media window title
				'fallback-button-label' => __('Media Fallback Window Button Label', 'skoty'), //Set the falback media window button label

				'library-type' => 'image, video, audio', //Set the library type by listing the media types allowed in the library, seperated by comma.
				'multiple' => false //Set the selection type allowed. True will allow multiple media to be selected, where false will allow only one media to be selected.
			)
		)
	);





//Slider

	EasyCustomizer::Slider( $section_id, array(
			'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]',
			'label' => __('Slider Title', 'text_domain'),
			'options' => array(
				'min' => 1, //minimum value that the slider can have.
				'max' => 100, //maximum value the the slider can have.
				'step' => 1, //the incriment number
				'default' => 10, //default value.
				'unit' => '' //the usnit representing the value 'px', '%', 'stones' for example.
			)
		)
	);





//Select

	EasyCustomizer::Select( $section_id, array(
			'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]',
			'label' => __('Select Box Title', 'text_domain'), //Set the label/title of the setting
			'default' => 'option-two-value',
			'choices' => array(
				'option-one-value' => __('Option One Display Text', 'text_domain'),
				'option-two-value' => __('Option Two Display Text', 'text_domain'),
			),
		)
	);





//LayoutSelect

	EasyCustomizer::LayoutSelect( $section_id, array(
			'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]',
			'default' => 'one-half-one-fourths', //set the default layout
			'label' => __('Layout Select Title', 'text_domain'), //Set the label/title of the setting
			'layouts' => array(
				'full-width' => '<span class="full-width"></span>',
				'one-halves' => '<span class="one-half"></span><span class="one-half"></span>',
				'one-thirds' => '<span class="one-third"></span><span class="one-third"></span><span class="one-third"></span>',
				'two-thirds-one-third' => '<span class="two-thirds"></span><span class="one-third">',
				'one-third-two-thirds' => '<span class="one-third"></span><span class="two-thirds">',
				'one-fourths' => '<span class="one-fourth"></span><span class="one-fourth"></span><span class="one-fourth"></span><span class="one-fourth"></span>',
				'one-half-one-fourths' => '<span class="one-half"></span><span class="one-fourth"></span><span class="one-fourth"></span>',
				'one-fourth-one-half-one-fourth' => '<span class="one-fourth"></span><span class="one-half"></span><span class="one-fourth"></span>',
				'one-fourths-one-half' => '<span class="one-fourth"></span><span class="one-fourth"></span><span class="one-half"></span>',
			),
		)
	);





//Checkbox

	EasyCustomizer::Checkbox( $section_id, array(
			'name' => ATTIC_OPTIONS_NAMESPACE . '[option-key-name]',
			'label' => __('Checkbox Title', 'text_domain'), //Set the label/title of the setting
			'default' => false //set the default state, where false is unchecked and true is checked. NOTICE: Avoid setting the true, still has some bugs.
		)
	);
	




//Seperator

	EasyCustomizer::Separator( $section_id, array(
			'title' => 'Hello'
		)
	);
*/
?>