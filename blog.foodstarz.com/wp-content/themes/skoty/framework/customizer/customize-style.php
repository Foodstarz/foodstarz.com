<style type="text/css">
	/*typography*/
	<?php $body_font_name = EasyFramework::EasyOption( 'body-font-family', 'open-sans' ); ?>
	<?php $body_font_data = EasyFramework::EasyFontData( $body_font_name ); ?>
	<?php $heading_font_name = EasyFramework::EasyOption( 'heading-font-family', 'open-sans' ); ?>
	<?php $heading_font_data = EasyFramework::EasyFontData( $heading_font_name ); ?>
	<?php if( $body_font_data ) : ?>
		<?php if( !empty($body_font_data['import']) ) : ?>
			@import url(<?php echo $body_font_data['import'];?>);
		<?php endif; ?>
	<?php endif; ?>
	<?php if( $heading_font_data ) : ?>
		<?php if( !empty($heading_font_data['import']) ) : ?>
			@import url(<?php echo $heading_font_data['import'];?>);
		<?php endif; ?>
	<?php endif; ?>
	<?php if( $body_font_data ) : ?>
		body, input, textarea, select {
			font-family: <?php echo $body_font_data['stack']; ?>;
		}
	<?php endif; ?>
	body, input, textarea, select {
		font-size: <?php echo EasyFramework::EasyOption( 'body-text-size', '16px' ); ?>;
	}
	<?php if( $heading_font_data ) : ?>
		h1, h2, h3, h4, h5, h6, .post-title, .widget-title, .footer-content {
			font-family: <?php echo $heading_font_data['stack']; ?>;
		}
	<?php endif; ?>
	/* logo */
	.logo-container a {
		top: <?php echo EasyFramework::EasyOption('top-offset', '0'); ?>;
		margin-left: <?php echo EasyFramework::EasyOption('left-offset', '0'); ?>;
	}
	/* colors */
	/* body & such... */
	body, .drop-down-navigation .navigation li a, .skoty .wp-playlist-item .wp-playlist-caption, .skoty .skoty .wp-playlist-item-length, .skoty .wp-playlist-light .wp-playlist-playing, .post.format-chat a.more-link, .widgets .widget_recent_comments, #lang_sel_footer a, .page-template-archive-page-php .archive-recent-posts-wrapper ul li a, .page-template-archive-page-php .archive-categories-wrapper ul li a, .page-template-archive-page-php .archive-months-wrapper ul li a {
		color: <?php echo EasyFramework::EasyOption('body-text-color', '#474851'); ?>;
	}
	body, #header, .drop-down-navigation .navigation li, .drop-down-navigation .navigation li a, .drop-down-navigation .navigation li:after, .header-avatar-wrapper:before, .posts:after, .main-content, .header-search-form, .skoty .mejs-controls .mejs-time-rail .mejs-time-current, .skoty .mejs-controls .mejs-horizontal-volume-slider .mejs-horizontal-volume-current, .tab-box .tab-btns li, .tab-box .tabs, .flex-control-nav>li a, .our-clients .atbb-grid-gallery>li figure, .atbb-team-members-wrapper .flex-direction-nav>li a, .horizontal-navigation .navigation ul>li {
		background-color: <?php echo EasyFramework::EasyOption('body-background-color', '#fff'); ?>;
	}
	.drop-down-navigation .navigation li:first-child, .drop-down-navigation .navigation li:last-child, .drop-down-navigation .navigation:before, .has-header-media .header-avatar-wrapper:after, .horizontal-navigation .navigation ul::before {
		border-color: <?php echo EasyFramework::EasyOption('body-background-color', '#fff'); ?>;
	}
	.tab-box .tab-btns li.active-tab {
		border-bottom-color: <?php echo EasyFramework::EasyOption('body-background-color', '#fff'); ?>;
	}
	/* headings & such... */
	h1, h2, h2, h3, h4, h5, h6, strong, .footer-content, .post-title, .widget-title, .post-title a, .post-date a:hover, .post-details a:hover, .post-details .likes:hover, .post-taxonomies a:hover, .comment-reply-link:hover, .skoty .wp-playlist-item.wp-playlist-playing, .skoty .wp-playlist-item.wp-playlist-playing a, .page-links a span, .page-links span.page-links-title, .comment-author a, .comment-pagination a, .drop-cap:first-letter, .toggle>li>a, .tab-box .tab-btns li a, .atbb-team-member-name, .main-sidebar-opener, input[type="text"], .content.seach-results article .search-result-title a, .post-details .share:hover>.fa, .footer-content-wrapper .to-top, .atbb-team-members-wrapper .flex-direction-nav>li a, .next-prev-post-links a, .related-posts article .related-post-title, .horizontal-navigation .navigation ul>li a, .atp-grid-portfolio .atp-project-title, .atp-grid-portfolio .atp-filter-label, .atp-grid-portfolio .atp-project-categories li span:hover, .atp-carousel-portfolio .flex-direction-nav>li a {
		color: <?php echo EasyFramework::EasyOption('heading-color', '#1b1c23'); ?>;
	}

	/* text colors inverse */
	/*body text*/
	.header-wrapper, .widgets, .widgets a, .blog .content article figure .wp-caption-text, .post.format-chat a.more-link:hover, .about-content, .widget_search .search-form input.search-field, p.wp-caption-text, .horizontal-navigation .navigation li a, .atp-carousel-portfolio .atp-project-details, .atp-slider-portfolio .atp-project-excerpt, .atp-slider-portfolio .atp-project-categories span, .atp-grid-portfolio.ato-nogutter .atp-project-categories li, .main-sidebar select {
		color: <?php echo EasyFramework::EasyOption('body-text-inverse-color', '#e8e8e8'); ?>;
	}

	.boxbuilder-content .atbb-section.section-background-dark p,
	.boxbuilder-content .atbb-section.section-background-dark strong,
	.boxbuilder-content .atbb-section.section-background-dark blockquote {
		color: <?php echo EasyFramework::EasyOption('body-text-inverse-color', '#e8e8e8'); ?>;
	}

	/*header text*/
	.header-wrapper h1, .header-wrapper h2, .header-wrapper h3, .header-wrapper h4, .header-wrapper h5, .header-wrapper h6, .about h1, .about h2, .about h3, .about h4, .about h5, .about h6, .header-wrapper strong, .widget-title, .drop-down-navigation .navigation li:hover>a, .slider-gallery-direction-nav li, .post.format-link a.entry-link, .more-link:hover, .page-links>span, .bypostauthor .comment-avatar::before, input[type="submit"], input[type="button"], button, #navigation, .sidebar-navigation a, .scroll-down-indicator, .button, .button-primary, .about-title, .drop-cap.square:first-letter, .drop-cap.round:first-letter, .post.sticky.format-link .entry-wrapper::before, .flex-direction-nav>li a, .atbb-slider-gallery-caption, .atbb-skills li span.atbb-skill-title, .main-sidebar-wrapper table tbody tr td a:hover, .main-sidebar-opener.icons-hover .fa:nth-child(3), .breadcrumbs-wrapper, .breadcrumbs-wrapper strong, .has-header-media .header-wrapper .fa, .close-about .fa, .home-link, .section-background-image input, .section-background-image textarea, .section-background-image strong, .highlight, .main-sidebar .tagcloud a:hover, .post-details .share .atbe-share-box, .horizontal-navigation .navigation ul>li:hover a, .horizontal-navigation .navigation li.current-menu-item:not(.menu-item-type-custom):hover>a, .horizontal-navigation .navigation li.current-menu-item.menu-item-home:hover>a, .atp-grid-portfolio .atp-active-filter .atp-filter-label:hover, .atp-grid-portfolio .atp-active-filter .atp-filter-label, .atp-grid-portfolio.atp-nogutter .atp-project-categories li span:hover, .atp-slider-portfolio .atp-project-title, .atp-grid-portfolio.atp-nogutter .atp-project-title, .atp-grid-portfolio.atp-nogutter .atp-project-details, .atp-carousel-portfolio .atp-project-title, .header-content-wrapper .post-details a, .header-content-wrapper .post-details span,
		.header-content-wrapper .post-details .share:hover>.fa {
		color: <?php echo EasyFramework::EasyOption('heading-inverse-color', '#fff'); ?>;
		-webkit-text-stroke: 0;
	}

	.boxbuilder-content .atbb-section.section-background-dark h1, .atbb-section.section-background-dark h2,
	.boxbuilder-content .atbb-section.section-background-dark h3, .atbb-section.section-background-dark h4,
	.boxbuilder-content .atbb-section.section-background-dark h5, .atbb-section.section-background-dark h6,
	.boxbuilder-content .atbb-section.section-background-dark .atbb-services-block-icon {
		color: <?php echo EasyFramework::EasyOption('heading-inverse-color', '#fff'); ?>;
		-webkit-text-stroke: 0;
	}

	.footer-widgets-wrapper, .slider-gallery-direction-nav li, .skoty .mejs-container .mejs-controls, .skoty .mejs-controls .mejs-volume-button .mejs-volume-slider, .posts article figure.post-thumbnail, .preloader.content-preloader:before, .preloader.content-preloader:after, .page-links>span, .main-content section.about, .flex-direction-nav>li a, .boxbuilder-content .atbb-section.section-background, .atbb-grid-gallery>li figure, .atbb-team-members li figure, .atbe-share-box div.fa, .related-posts article .related-post-thumbnail, .atp-grid-portfolio .atp-project-thumb, .atp-carousel-portfolio .atp-project-thumb, .single.single-atp_project article .post-thumbnail {
		background-color: <?php echo EasyFramework::EasyOption('primary-color', '#31323a'); ?>;
	}
	input[type="submit"], input[type="button"], button, .button {
		background-color: <?php echo EasyFramework::EasyOption('button-bg-color', '#31323a'); ?>;
	}
	.skoty .wp-playlist .mejs-audio {
		border-color: <?php echo EasyFramework::EasyOption('primary-color', '#31323a'); ?>;
	}
	.atbe-share-box::after {
		border-top-color: <?php echo EasyFramework::EasyOption('primary-color', '#31323a'); ?>;
	}
	.post.sticky.format-quote .entry-wrapper::before, .post.sticky.format-aside .aside-post-content-wrapper::before, .post.sticky .post-date::before, .related-posts article .related-post-thumbnail a {
		color: <?php echo EasyFramework::EasyOption('primary-color', '#31323a'); ?>;
	}
	.skoty .mejs-controls .mejs-time-rail .mejs-time-loaded, .footer-accomplishments-wrapper {
		background-color: <?php echo EasyFramework::EasyOption('secondary-color', '#43444d'); ?>;
	}
	.post.format-quote h5.post-title, .pagination .page-numbers, .footer-load-more-wrapper a {
		color: <?php echo EasyFramework::EasyOption('secondary-color', '#43444d'); ?>;
	}
	.main-sidebar, .sicky-header {
		background-color: <?php echo EasyFramework::EasyOption('sidebar-bg-color', '#232329'); ?>;
	}
	.post.format-quote .entry-wrapper, .post.format-aside .aside-post-content-wrapper, .footer-pagination-wrapper, .footer-load-more-wrapper, .message-box, .atbb-skills li, .main-sidebar-opener, .related-posts article.no-related-post-thumbnail .related-post-thumbnail, #footer .next-prev-post-links, .invert-icons a.atsi {
		background-color: <?php echo EasyFramework::EasyOption('post-bg-color', '#f8f8f8'); ?>;
	}
	/*footer*/
	.footer-copyright {
		color: <?php echo EasyFramework::EasyOption('footer-copyright-text-color', '#b6b8c1'); ?>;
	}
	/* borders & separators */
	hr, .post.format-chat .post-content p {
		background-color: <?php echo EasyFramework::EasyOption('border-color', '#f0f1f6'); ?>;
	}
	.footer-copyright-wrapper, .no-footer-widgets #footer, .header-avatar-wrapper:after, .skoty .wp-playlist, .skoty .wp-playlist-item, input, textarea, select, .tab-box .tab-btns li, .tab-box .tabs, .tab-box .tabs li, .toggle>li, .toggle>li:last-child, #lang_sel_footer, .atp-grid-portfolio .atp-project-details, .open-comments.single.single-atp_project .post-comments-wrapper {
		border-color: <?php echo EasyFramework::EasyOption('border-color', '#f0f1f6'); ?>;
	}
	.tab-box .tab-btns li:last-child, .footer-accomplishments-wrapper .widgets>li .widget-title::after {
		border-right-color: <?php echo EasyFramework::EasyOption('border-color', '#f0f1f6'); ?>;
	}
	.widget-title::before, .sidebar-navigation li {
		border-color: <?php echo EasyFramework::EasyColorLevel(
			EasyFramework::EasyOption('sidebar-bg-color', '#232329'),
			5
		); ?>;
	}
	.sidebar-navigation li.menu-item-has-children::before {
		color: <?php echo EasyFramework::EasyOption('border-color-inverse', '#4b4c54'); ?>;
	}
	/* icons */
	.atbb-services-block-icon {
		color: <?php echo EasyFramework::EasyOption('service-icon-color', '#31323a'); ?>;
	}

	.post-date, .post-details, .post-date a, .post-details a, .post-taxonomies .fa, .post-taxonomies a, .post-taxonomies, .comment-edit-link, .comment-date, .comment-form p, .form-allowed-tags, .archives-heading h6, .archives-heading h6, .atp-grid-portfolio .atp-project-categories li, .invert-icons a.atsi {
		color: <?php echo EasyFramework::EasyOption('post-icon-color', '#bebfc9'); ?>;
	}
	.post.format-quote h5.post-title:after {
		background-color: <?php echo EasyFramework::EasyOption('post-icon-color', '#bebfc9'); ?>;
	}
	.post-details .likes.atbe-liked {
		color: <?php echo EasyFramework::EasyOption('post-liked-color', '#ff5656'); ?>;
	}
	/* acent colors */
	a, .to-top:hover, .widgets a:hover, .post-title a:hover, a:hover strong, .skoty .wp-playlist-item:hover, .skoty .wp-playlist-item:hover a, .page-links a:hover span, .comment-pagination a:hover, .pagination .page-numbers.current, .pagination a.page-numbers:hover, .footer-load-more-wrapper a:hover, .comment-pagination span, .toggle>li.open>a, .tab-box .tab-btns li.active-tab a, .atbb-services-block:hover .atbb-services-block-icon, .sidebar-navigation a:hover, .content.seach-results article .search-result-title a:hover, .page-template-archive-page-php .archive-recent-posts-wrapper ul li a:hover, .page-template-archive-page-php .archive-categories-wrapper ul li a:hover, .page-template-archive-page-php .archive-months-wrapper ul li a:hover, .current-menu-item>a, .next-prev-post-links a:hover, .atp-grid-portfolio .atp-project-title:hover, .atp-grid-portfolio .atp-filter-label:hover, .atp-slider-portfolio .atp-project-title:hover, .atp-carousel-portfolio .atp-project-title:hover {
		color: <?php echo EasyFramework::EasyOption('accent-color', '#42d18c'); ?>;
	}
	.drop-down-navigation .navigation li:hover>a, .slider-gallery-direction-nav li:hover, .post.format-link .entry-wrapper, .more-link:hover, .bypostauthor .comment-avatar:before, .bypostauthor .comment-avatar:after, input[type="submit"]:hover, input[type="button"]:hover, button:hover, .button-primary, .button:hover, .drop-cap.square:first-letter, .drop-cap.round:first-letter, .atbb-skills li span.atbb-skill-level, .flex-control-nav>li a.flex-active, .flex-control-nav>li a:hover, .main-sidebar-wrapper table tbody tr td a, .main-sidebar .tagcloud a:hover, .highlight, .atbe-share-box div.fa:hover, .atp-grid-portfolio .atp-active-filter .atp-filter-label {
		background-color: <?php echo EasyFramework::EasyOption('accent-color', '#42d18c'); ?>;
	}
	blockquote, .atbb-skills li span.atbb-skill-level {
		border-color: <?php echo EasyFramework::EasyOption('accent-color', '#42d18c'); ?>;
	}
	.button-primary:hover, .post.format-link a.entry-link:hover {
		background-color: <?php echo EasyFramework::EasyColorLevel(
			EasyFramework::EasyOption('accent-color', '#42d18c'),
			8
		); ?>;
	}

	.horizontal-navigation .navigation>li>a:hover, .horizontal-navigation .navigation li.current-menu-item>a, .horizontal-navigation .navigation li.current-menu-item.menu-item-home>a, .current-menu-item>a, .horizontal-navigation .navigation li.current-menu-item:not(.menu-item-type-custom)>a {
		color: <?php echo EasyFramework::EasyOption('horizontal-nav-accent-color', '#42d18c'); ?>;
	}

	.horizontal-navigation .navigation ul>li:hover a {
		background-color: <?php echo EasyFramework::EasyOption('horizontal-nav-accent-color', '#42d18c'); ?>;
	}

	/* selections */
	::selection {
		background: <?php echo EasyFramework::EasyOption('accent-color', '#42d18c'); ?>;
		color: #fff;
	}
	::-moz-selection {
		background: <?php echo EasyFramework::EasyOption('accent-color', '#42d18c'); ?>;
		color: #fff;
	}
	/* inputs */
	::-webkit-input-placeholder {
		color: <?php echo EasyFramework::EasyOption('input-placeholder-color', '#bebfc9'); ?>;
	}
	:-moz-placeholder {
		color: <?php echo EasyFramework::EasyOption('input-placeholder-color', '#bebfc9'); ?>;
	}
	::-moz-placeholder {
		color: <?php echo EasyFramework::EasyOption('input-placeholder-color', '#bebfc9'); ?>;
	}
	:-ms-input-placeholder {  
		color: <?php echo EasyFramework::EasyOption('input-placeholder-color', '#bebfc9'); ?>;
	}
	.header-search-form .fa {
		color: <?php echo EasyFramework::EasyOption('input-placeholder-color', '#bebfc9'); ?>;
	}
	/* header media */
	.header-media,
	.header-media-holder {
		background-color: <?php echo EasyFramework::EasyOption('header-media-bg-color', '#31323a'); ?>;
	}
	/* header pattern */
	<?php $header_pattern_data = EasyFramework::EasyOption('header-pattern'); ?>
	<?php if( $header_pattern_data && isset($header_pattern_data['url']) ) : ?>
	.header-pattern {
		background-image: url(<?php echo $header_pattern_data['url'] ?>);
		opacity: <?php echo intval(EasyFramework::EasyOption('header-pattern-opacity', '5%')) / 100; ?>
	}
	<?php endif; ?>
	/* social icons */
	a.atsi, .tagcloud a {
		background-color: <?php echo EasyFramework::EasyColorLevel(
			EasyFramework::EasyOption('social-icon-background-color', '#31323a'), 2
		); ?>;
		color: <?php echo EasyFramework::EasyOption('social-icon-color', '#ffffff'); ?>;
	}
	a.atsi {
		background-color: <?php echo EasyFramework::EasyHexToRgba(
			EasyFramework::EasyColorLevel( EasyFramework::EasyOption('social-icon-background-color', '#31323a'), 2 ),
			intval(EasyFramework::EasyOption('social-icon-background-opacity', '100%')) / 100
		); ?>;
	}
	.main-sidebar-wrapper a.atsi, .main-sidebar-wrapper a.atsi, .main-sidebar-wrapper .tagcloud a {
		background-color: <?php echo EasyFramework::EasyColorLevel(
			EasyFramework::EasyOption('sidebar-bg-color', '#232329'), 1
		); ?>;
		color: <?php echo EasyFramework::EasyOption('social-icon-color', '#ffffff'); ?>;
	}
	a.atsi:hover, a.atsi:hover {
		background-color: <?php echo EasyFramework::EasyOption('accent-color', '#42d18c'); ?>;
		color: <?php echo EasyFramework::EasyOption('social-icon-color', '#ffffff'); ?>;
	}
	/* spec anim easing */
	<?php 
		$easing = EasyFramework::EasyOption('spec-animation-easing', 'elastic');
		$easing_val = 'linear';
		$anim_time = '0.35s';
		if( $easing === 'elastic' ) :
			$easing_val = 'cubic-bezier(.42,.66,.14,1.24)';
			$anim_time = '0.5s';
		else:
			$easing_val = $easing;
		endif;
	?>
	.header-search-form, .blog.show-avatar .header-avatar-wrapper, .home.show-avatar .header-avatar-wrapper, .has-header-content.show-avatar .header-avatar-wrapper, .widget_atbe_accomplishments .widget-wrapper, .icons-hover .fa, .main-content, .search-opener>.fa-times {
		-webkit-transition-timing-function: <?php echo $easing_val; ?>;
		-moz-transition-timing-function: <?php echo $easing_val; ?>;
		-o-transition-timing-function: <?php echo $easing_val; ?>;
		transition-timing-function: <?php echo $easing_val; ?>;

		-webkit-transition-duration: <?php echo $anim_time; ?>;
		-moz-transition-duration: <?php echo $anim_time; ?>;
		-o-transition-duration: <?php echo $anim_time; ?>;
		transition-duration: <?php echo $anim_time; ?>;
	}
</style>