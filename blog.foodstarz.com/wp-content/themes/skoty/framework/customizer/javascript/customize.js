/*global jQuery:false */
//console.log('customize script');
jQuery(document).ready(function($) {
	'use strict';

	$( '.customize-slider' ).each(function() {
		var slider = $( this );
		var unit = slider.attr('data-unit');
		slider.slider({
			range: 'min',
			min: parseFloat( slider.attr('data-min') ),
			max: parseFloat( slider.attr('data-max') ),
			value: slider.siblings('.sider-value').val() === '' ? parseFloat( slider.attr('data-val') ) : parseFloat(slider.siblings('.sider-value').val()),
			step: parseFloat( slider.attr('data-step') ),
			slide: function( event, ui ) {
				$( this ).siblings('.sider-value').attr( 'value', ui.value + unit );
				$( this ).siblings('.sider-value').change();
			}
		});

		if(slider.siblings('.sider-value').val() === '') {
			slider.siblings('.sider-value').attr( 'value', slider.slider( 'value' ) + unit );
		}
		
		slider.siblings('.sider-value').change(function() {
			slider.slider( 'value', parseInt(slider.siblings('.sider-value').val()) );
		});
	});




	if( $().chosen ) {
		var selector = '[data-customize-setting-link*="font-family"]';
		$(selector).chosen({
			width: '100%'
		});
	}
	





	$('.customize-layout-select').each(function() {
		var element = $(this);
		var opener = element.find('.customize-layout-select-selected');
		var list = element.find('.customize-layout-select-list');
		var layouts = element.find('.customize-layout-select-list>li');
		var field = element.parent().find('.customize-layout-select-field');

		//console.log(field);

		opener.on('click', function() {
			if( element.hasClass('open-layout-select-list') ) {
				close();
			} else {
				open();
			}
		});

		function close() {
			list.slideUp('fast', 'swing');
			element.removeClass('open-layout-select-list');
		}

		function open() {
			list.slideDown('fast', 'swing');
			element.addClass('open-layout-select-list');
		}

		element.on('click', '.customize-layout-select-list>li', function() {
			var layout = $(this);
			opener.empty();

			layout.children().clone().appendTo( opener );
			close();

			field.val( layout.attr('data-class') ).trigger('change');
		});
	});








	$('.customize-media-wrapper').on('click', '.customize-media-preview .customize-media-remove', function() {
		var container = $('.customize-media-wrapper').has(this).find('.customize-media');

		container.removeClass('has-media-thumb');
		container.removeClass('is-video');

		$('.customize-media-preview img, .customize-media-preview video', container).remove();
		$('.customize-media-url-field', container).val('').trigger('change');
	});

	$('.customize-media-wrapper').on('click', '.customize-media-select, .customize-media-preview img.image-attachment-preview, .customize-media-preview video', function() {
		var container = $('.customize-media-wrapper').has(this).find('.customize-media');

		var media_window = new MediaWindow({
			'button-label': container.attr('data-button-label'),
			'uploader-title': container.attr('data-uploader-title'),
			multiple: container.attr('data-multiple'),
			type: container.attr('data-type'),
		});

		media_window.off('mediawindow.select', mediaWidnowSelect );
		media_window.on('mediawindow.select', mediaWidnowSelect );

		function mediaWidnowSelect(e, attachment) {
			//console.log( attachment );
			var image_size = container.attr('data-image-size');
				container.removeClass('is-video');

			$('.customize-media-preview img.image-attachment-preview, .customize-media-preview video', container).remove();

			if( attachment.type === 'image' ) {
				var image_url = attachment.url;
				if( attachment.sizes && attachment.sizes[image_size] && attachment.sizes[image_size].url ) {
					image_url = attachment.sizes[image_size].url;
				}

				$('<img/>', {
					'class': 'image-attachment-preview',
					src: image_url
				}).prependTo( $('.customize-media-preview', container) );

				if( !container.hasClass('has-media-thumb') ) {
					container.addClass('has-media-thumb');
				}
			} else if( attachment.type === 'video' ) {
				var video = $('<video/>', {src: attachment.url, muted: true, loop: true, autoplay: true });
					video.prependTo( $('.customize-media-preview', container) );

				if( !container.hasClass('has-media-thumb') ) {
					container.addClass('has-media-thumb');
				}

				if( !container.hasClass('is-video') ) {
					container.addClass('is-video');
				}
			}

			var saved_data = $('.customize-media-url-field', container).val();
			var media_data = JSON.parse( saved_data !== '' ? saved_data : '{}' );

				media_data.ID = attachment.id,
				media_data.url = attachment.url,
				media_data.type = attachment.type,
				media_data.mime = attachment.mime,
				media_data.orientation = attachment.orientation

			//container.data( 'media-data', JSON.stringify(media_data) );
			$('.customize-media-url-field', container).val( JSON.stringify(media_data) ).trigger('change');
		}
	});


	$('.customize-media-wrapper').on('click', '.customize-media-set-fallback, .fallback-media-preview', function() {
		var container = $('.customize-media-wrapper').has(this).find('.customize-media');

		var media_window = new MediaWindow({
			'button-label': container.attr('data-fallback-button-label'),
			'uploader-title': container.attr('data-fallback-title'),
			multiple: false,
			type: 'image'
		});

		media_window.off('mediawindow.select', mediaWidnowSelect );
		media_window.on('mediawindow.select', mediaWidnowSelect );

		function mediaWidnowSelect(e, attachment) {

			$('.customize-media-preview img.fallback-media-preview', container).remove();
			
			var image_url = attachment.url;
			if( attachment.sizes && attachment.sizes.thumbnail && attachment.sizes.thumbnail.url ) {
				image_url = attachment.sizes.thumbnail.url;
			}
			$('<img/>', {
				'class': 'fallback-media-preview',
				src: image_url
			}).appendTo( $('.customize-media-preview', container) );


			var media_data = JSON.parse( $('.customize-media-url-field', container).val() );

				media_data.fallback = {
					ID: attachment.id,
					url: attachment.url,
					type: attachment.type,
					mime: attachment.mime,
					orientation: attachment.orientation
				};

			$('.customize-media-url-field', container).val( JSON.stringify(media_data) ).trigger('change');

			//console.log( attachment );
		}
	});




	function MediaWindow( settings ) {
		var file_frame;
		var media_window = $('<span/>');
		
		// If the media frame already exists, reopen it.
		if ( file_frame !== undefined ) {
			file_frame.open();
			return media_window;
		}
		// Create the media frame.
		file_frame = wp.media.frames.skoty_file_frame = wp.media({
			title: settings['uploader-title'],
			library: {
				type: settings.type ? settings.type : 'image, video'
			},
			button: {
				text: settings['button-label']
			},
			multiple: false, // Set to true to allow multiple files to be selected
			/*frame: 'post',*/
		});
		

		// When an image is selected, run a callback.
		file_frame.on( 'select', function() {
			// We set multiple to false so only get one image from the uploader
			var attachment = file_frame.state().get('selection').first().toJSON();
			//console.log( attachment );
			media_window.trigger( 'mediawindow.select', [attachment, ] );
			//console.log(file_frame);
		});

		// Finally, open the modal
		file_frame.open();
		return media_window;
	}
});