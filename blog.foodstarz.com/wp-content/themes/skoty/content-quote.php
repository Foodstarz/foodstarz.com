<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<article <?php post_class(); ?>>
	<div class="entry-wrapper">
		<?php if( has_post_thumbnail() ) : ?>
			<figure class="post-thumbnail"><?php the_post_thumbnail(); ?></figure>
		<?php endif; ?>
		<div class="quote-post-content-wrapper">
			<h5 class="post-title"><?php the_title(); ?></h5>
			<div class="post-content"><?php the_content(); ?></div>
		</div>
	</div>
	<?php EasyFramework::EasyPostLinks(); ?>
	<hr class="post-content-separator">
</article>