/* global console:false, jQuery:false, SKOTY_DATA:false, atbb:false, MediaBox:false, skotyData:true */
(function($) {
	'use strict';

	window.skotyData = {};

	skotyData = SKOTY_DATA;
	skotyData.options = JSON.parse(skotyData.options);
	skotyData.paged_urls = JSON.parse(skotyData.paged_urls);
	skotyData.easing = 'ease';
	skotyData.animationTime = 0.5;
	skotyData.is_mobile = false;
	skotyData.paged = 0;
	skotyData.loading_posts = false;
	skotyData.prevScrollPos = 0;

	//console.log(skotyData);

	if ( !$.support.transition || $().transition === undefined ) {
		$.fn.transition = $.fn.animate;
		skotyData.easing = 'easeInOutSine';
	}

	if( window.location.hash ) {
		//scroll to top for further smooth scrolling when window is loaded
		smoothScrollTo( 0, 0 );
	}

	setHeaderHeight();

	$(document).ready(function() {
		/* alter options if mobile */
		if( $('body').hasClass('mobile') ) {
			skotyData.is_mobile = true;
			if( window.atbb ) {
				atbb.use_parallax = false;
			}
		}

		setHeaderHeight();
		showAvatar();

		/* functions to be executed on window scroll */
		$(window).scroll( function() {
			if( $('body').hasClass('has-header-content') ) {
				showAvatar();
			}
			showSidebarOpener();
		});

		/* functions to be executed on window resize */
		$(window).resize(function() {
			setHeaderHeight();
			//window.requestAnimationFrame( initSidebarScroller );
		});
		
		$('.header-media').headerMedia();

		$('.search-opener').on('click', function() {
			if( skotyData.blurclick ) return;
			//console.log( 'click' );
			if( !$('body').hasClass('open-search-form') ) {
				openHeaderSearchForm();
			}
			return false;
		});



		/* sidebar navigation */
		$('.sidebar-navigation a').on('click', function() {
			var a = $(this);
			var li = a.parent( 'li' );
			var sub = li.children( '.sub-menu' );
			var sub_height = 0;

			if( sub.length > 0 ) {
				sub.children().each(function() {
					sub_height += $(this).outerHeight(true);
				});
				/* open submenu here */
				if( li.hasClass('submenu-open') ) {
					if( !skotyData.is_mobile ) {
						sub.css({ height: sub.height() });
						setTimeout(function(){
							sub.css({ height: 0 });
						}, 0);
					} else {
						sub.css({ height: 0 });
					}
					li.removeClass('submenu-open');
				} else {
					if( !skotyData.is_mobile ) {
						sub.css({ height: sub_height });
						setTimeout(function(){
							sub.css({ height: 'auto' });
						}, 200);
					} else {
						sub.css({ height: 'auto' });
					}
					li.addClass('submenu-open');
				}
				return false;
			} else {
				return true;
			}
		});


		$('.main-content').on('click', function() {
			if( $('body').hasClass('open-sidebar') ) {
				$('body').removeClass('open-sidebar');
				blurSearchForm();
			}
		});


		/* sidebar openners */
		if( $('.main-sidebar').length > 0 ) { 
			$('.navigation-opener>span').on('click', function() {
				if( !$('body').hasClass('open-sidebar') ) {
					setTimeout(function(){
						$('body').addClass('open-sidebar');
					}, 10);
				}
				blurSearchForm();
			});
		}

		if( $('.main-sidebar').length > 0 ) { 
			$('.main-sidebar-opener').on('click', function() {
				$('body').toggleClass('open-sidebar');
				blurSearchForm();
			});
		}


		$('.sidebar-navigation a, .navigation a').on('click', function(){
			var anchor = this;
			var hash = anchor.hash;
			var href = anchor.href;
			var loc = window.location.href;
			var clean_href = href.split( '#' )[0] ? href.split( '#' )[0] : '';
			var clean_loc = loc.split( '#' )[0] ? loc.split( '#' )[0] : '';

			//console.log(hash, clean_href, clean_loc);

			if( hash && clean_href === clean_loc ) {
				scrollToHash( hash );
				return false;
			}
		});


		/* add smoothscroll when clicked on the comments post icon */
		$('.comments a').on('click', function() {
			var to_hash = $(this).get(0).hash;
			var to = $(to_hash);

			//console.log(to);

			if( to.length > 0 ) {
				smoothScrollTo( to.offset().top, 1000 );
				return false;
			} else {
				return true;
			}
		});


		/* Initiate about section controls */
		initAboutControls();

		/* smoothscroll to top */
		$('.to-top').on('click', function() {
			smoothScrollTo( 0, 1000 );
			return false;
		});

		/* smoothscroll to content */
		$('.scroll-down-indicator').on('click', function() {
			var to = 0;
			var avatar_wrapper = $('.header-avatar-wrapper');

			if( avatar_wrapper.length > 0 ) {
				to = $('.header-avatar-wrapper').offset().top - 42;
			} else {
				to = $('#header').height() + 56;
			}
			
			//console.log( to );
			if( avatar_wrapper.css('display') !== 'none' ) {
				smoothScrollTo( to, 1000 );
			} else {
				smoothScrollTo( $('#header').height() + 2, 1000 );
			}
			return false;
		});
		
		/**/

		function remind() {
			if( !skotyData.is_mobile ) {
				setTimeout(function() {
					window.requestAnimationFrame(remind);
					//remind
					//console.log( 'remind' );

					$('body').addClass('reminder');
					setTimeout(function(){
						$('body').removeClass('reminder');
					}, 500);
				}, 8000);
			}
		}
		remind();
		
		/* initiate content preloader positioning script */
		preloaderPositioning();

		/* initiate blog pagination key navigation */
		initKeyNav();
		//
		//console.log( 'ready' );



		/* vimeo iframe window load block fix*/
		if( skotyData.is_mobile ) {
			var vimeo_iframes = $('iframe[src*="vimeo.com"]');
			vimeo_iframes.each(function() {
				var src = $(this).attr('src');

				$(this).attr( 'data-src', src );
				$(this).attr( 'src', '' );

				//$(this).on( 'load', initFitVids );
			});
		}









		if( skotyData.options['pagination-system'] === 'infinite-scroll' ) {

			$(window).on('scroll.skoty', function() {
				var offset = $('#footer').offset().top - ($(window).height() - 56);
				var scrollTop = $(window).scrollTop();
				//console.log(offset, scrollTop, skotyData.prevScrollPos);

				if( scrollTop > offset && scrollTop > skotyData.prevScrollPos ) {
					preLoadMore();
				}

				skotyData.prevScrollPos = scrollTop;
			});
		}


	});







	function preLoadMore() {
		if( skotyData.loading_posts ) return false;

		var paged = parseInt( skotyData.paged );
		var page_url = skotyData.paged_urls[ paged + 1 ];

		if( !page_url ) { return false; }

		paged++;

		skotyData.paged = paged;
		loadMore( paged );
	}
	




	$(window).on('load', function() {
		/* vimeo iframe window load block fix*/
		if( skotyData.is_mobile ) {
			var vimeo_iframes = $('iframe[data-src*="vimeo.com"]');
			vimeo_iframes.each(function() {
				var src = $(this).attr('data-src');
				$(this).attr( 'src', src );
			});
		}

		//scrollOnBlogPage();
		initFitVids();

		$('.load-more').on('click', function() {
			preLoadMore();
			return false;
		});

		
		

		/* slider galleries */
		var slider_galleries = $('.slider-gallery');

		if( slider_galleries.length === 0 ) {
			masonry();
			//scrollToHash();
		}

		slider_galleries.each(function() {
			var gallery = $(this);
			if( !gallery.flexslider ) return;

			gallery.flexslider({
				animation: 'slide',
				selector: '.gallery > li',
				smoothHeight: true,
				controlNav: false,
				directionNav: false,
				prevText: '',
				nextText: '',
				slideshow: false,
				start: function( slider ) {
					$('.prev-slide', gallery).on('click', function(e) {
						e.preventDefault();
						slider.flexAnimate( slider.getTarget('prev'), true );
					});

					$('.next-slide', gallery).on('click', function(e) {
						e.preventDefault();
						slider.flexAnimate( slider.getTarget('next'), true );
					});

					slider_galleries.find('.gallery > li.clone').find('a').addClass('atbb-no-lightbox');

					masonry();
					//scrollToHash();
				},
				after: function() {
					masonry();
				}
			});
			
		});
		



		/* lightbox/mediabox */
		if( window.atbb !== undefined && atbb.lightbox !== undefined ) {
			atbb.lightbox.addGallery( '.media-gallery,.lightbox-enabled,.atbb-grid-gallery,.atbb-slider-gallery' );
		} else {
			skotyData.lightbox = new MediaBox({
				//element: 'a.show_review_form',
				gallery: '.media-gallery,.lightbox-enabled,.atbb-grid-gallery,.atbb-slider-gallery',
				duration: 500,
				easing: skotyData.easing,
				exclude: '.atbb-no-lightbox'
			});
		}



		/* the page (+images) is loaded */
		$('body').addClass('page-loaded');
		showSidebarOpener();
		showAvatar();
		masonryGalleries();
		scrollToHash();

		//console.log( 'loaded' );
		initSidebarScroller();

		setTimeout(initAccomplishments, 1000);
	});












	function initKeyNav() {
		$(window).one( 'keydown', function( e ) {
			var url = '';

			switch( e.keyCode ) {
				case 37: //left
					url = $('.pagination a.prev').attr('href');
					if( url ) {
						window.location = url;
					}
				break;
				case 39: //right
					url = $('.pagination a.next').attr('href');
					if( url ) {
						window.location = url;
					}
				break;
			}

			//console.log( 'key down', e.keyCode, url );
		});
	}










	function initSidebarScroller() {
		if( skotyData.is_mobile ) return;

		var scroller = $('.main-sidebar-scroller');
		var sidebar = $('.main-sidebar');

		scroller.on('mousedown', function( e ) {
			e.preventDefault();

			scroller.addClass('main-sidebar-scroller-scrolling');
			$(window).off('mousemove', onSidebarMouseMove);
			$(window).on('mousemove', onSidebarMouseMove);
		});

		$(window).on('mouseup', function() {
			scroller.removeClass('main-sidebar-scroller-scrolling');
			$(window).off('mousemove', onSidebarMouseMove);
		});

		sidebar.on( 'scroll', function() {
			window.requestAnimationFrame( onSidebarScroll );
		});
	}

	function onSidebarMouseMove( e ) {
		var scroller = $('.main-sidebar-scroller');
		var sidebar = $('.main-sidebar');
		//var sidebar_wrapper = $('.main-sidebar-wrapper');

		//var sidebar_height = $(window).height();
		//var wrapper_height = sidebar_wrapper.outerHeight();
		
		var scroller_height = scroller.outerHeight();
		//var max_scroll = sidebar_height - scroller_height;

		var scroll_height = sidebar.get(0).scrollHeight - $(window).height();
		//
		var scrollTop = sidebar.scrollTop();
		var scroll_perc = Math.round( scrollTop * 100 / scroll_height );

		var spec_perc = (scroller_height * scroll_perc) / 100;

		if( e.clientY >= 0 ) {
			sidebar.scrollTop( e.clientY + spec_perc );
		}
		
		//console.log( 'mouse move', e.clientY, spec_perc );
	}

	function onSidebarScroll() {
		var scroller = $('.main-sidebar-scroller');
		var sidebar = $('.main-sidebar');
		//var sidebar_wrapper = $('.main-sidebar-wrapper');

		var sidebar_height = $(window).height();
		//var wrapper_height = sidebar_wrapper.outerHeight();
		
		var scroller_height = scroller.outerHeight();
		//var max_scroll = sidebar_height - scroller_height;

		var scroll_height = sidebar.get(0).scrollHeight - $(window).height();
		//
		var scrollTop = sidebar.scrollTop();
		var scroll_perc = Math.round( scrollTop * 100 / scroll_height );

		var spec_perc = (scroller_height * scroll_perc) / 100;
		var scroller_pos = Math.round( (sidebar_height * scroll_perc / 100) - spec_perc );

		scroller.css({
			y: scroller_pos
		});

		//console.log( scrollTop );
	}
















	

	
	function initAccomplishments() {
		if( skotyData.accop_init || skotyData.is_mobile ) return;

		//console.log('accomplishments');

		var accops = $('.atbe-accomplishment-value').text('0');
		var offset = $('.footer-accomplishments').offset();

		if( !offset || !offset.top ) return;
		var top = offset.top - $(window).height() + 84;

		//console.log(offset);

		new ScrollToggle( top, function () {
			if( skotyData.accop_init ) return;

			$('.footer-accomplishments-wrapper').addClass('popup-accomplishments');
			//
			accops.each(function() {
				var accop = $(this);
				var value = parseInt( accop.attr('data-value') );
				var obj = {}; obj.anim = 0;

				$(obj).animate({ anim: value }, {
					duration: 2000,
					step: function( now ) {
						accop.text( now.toFixed() );
					}
				});
			});
			skotyData.accop_init = true;

			//console.log('accomplished');
		});

	} //end function
	skotyData.accop_init = false;







	/* if the blog is set via a page */
	/*function scrollOnBlogPage() {
		if( $('body').hasClass('blog') && !$('body').hasClass('home') ) {
			var to = $('.header-avatar-wrapper').offset().top - 42;
			setTimeout(function() {
				smoothScrollTo( to, 1000 );
			}, 500);
		}
	} //end function*/







	function setHeaderHeight() {
		/* set the header height */
		if(!$('body').hasClass( 'default-header-height' )) return;

		if( $('body').hasClass('has-header-content') ) {
			$('.header-wrapper').css( 'height', $(window).height() - $('#wpadminbar').height() );
		}
	}



	function showSidebarOpener() {
		var scrollTop = $(window).scrollTop();
		var offset = $('#header').height() + 2;
		var body = $('body');

		if( scrollTop >= offset ) {
			if( !body.hasClass('show-sidebar-opener') ) {
				body.addClass('show-sidebar-opener');
			}
		} else {
			body.removeClass('show-sidebar-opener');
		}
	}


	function showAvatar() {
		var scrollTop = $(window).scrollTop();
		var avatar_offset = $('.header-avatar-wrapper').height() / 2;
		var body = $('body');
		if( scrollTop >= avatar_offset - 28 ) {
			if( !body.hasClass('show-avatar') ) {
				body.addClass('show-avatar');
			}
		} else {
			body.removeClass('show-avatar');
		}
	}


	function scrollToHash( hash ) {
		var to = 0;

		if( hash ) {
			to = $(hash);
		} else if( !hash && window.location.hash ) {
			to = $(window.location.hash);
		}

		if( to.length > 0 ) {
			//smoothScrollTo( 0, 0 );
			smoothScrollTo( to.offset().top - 56, 1000 );
		}
	}






	function closeHeaderSearchForm() {
		$('body').removeClass('open-search-form');
		setTimeout( function() { 
			skotyData.blurclick = false;
		}, 500 );
	}

	function blurSearchForm( callback ) {
		$('.header-search-form-wrapper input[name="s"]').trigger('blur');

		if( callback ) { setTimeout(callback, 500); }
	}

	function openHeaderSearchForm() {
		$('body').removeClass('open-sidebar').addClass('open-search-form');
		$('.header-search-form-wrapper input[name="s"]').trigger('focus');
		if( !skotyData.is_mobile ) {
			$(window).one( 'scroll', function(){
				blurSearchForm();
			});
		}
	}

	$('.header-search-form-wrapper input[name="s"]').on('blur', function() {
		//console.log( 'blur' );
		skotyData.blurclick = true;

		closeHeaderSearchForm();
	});







	function smoothScrollTo( to, duration, callback ) {
		$('body, html').animate({
			scrollTop: to
		}, duration !== undefined ? duration : skotyData.animationTime*1000, 'easeInOutCubic', function() {
			if(callback){
				callback();
			}
		});
	}


	function preloaderPositioning() {
		var preloader = $('.content-preloader-wrapper');
		
		var offsetTop = preloader.offset().top;

		function position() {
			var scrollTop = $(window).scrollTop();
			var winMiddle = $(window).height() / 2;
			var maxScroll = $('.content-wrapper').offset().top + $('.content-wrapper').height();

			if( scrollTop + winMiddle > offsetTop ) {
				preloader.addClass('to-fixed');
			} else {
				preloader.removeClass('to-fixed');
			}

			if( scrollTop + winMiddle > maxScroll ) {
				preloader.addClass('fade-out');
			} else {
				preloader.removeClass('fade-out');
			}

			//console.log(scrollTop + winMiddle, offsetTop, maxScroll);
		}

		position();
		$(window).scroll( position );
	}








	function initAboutControls() {
		var button = $('.about-button');
		var close_icon = $('<span/>', {'class': 'fa fa-times'});

		/* open about when about button is clicked */
		button.on('click', function() {
			if( !$('body').hasClass('open-about-section') ) {
				if( $('body').hasClass('open-search-form') ) {
					blurSearchForm( openAboutSection );
				} else {
					openAboutSection();
				}
			} else {
				closeAboutSection();
			}
			return false;
		});

		$('.close-about').on('click', function( e ) {
			e.preventDefault();
			closeAboutSection();
		});

		function openAboutSection() {
			var avatar_wrapper = $('.header-avatar-wrapper');
			var to = $('#header').height();

			if( avatar_wrapper.length > 0 ) {
				to = $('.header-avatar-wrapper').offset().top - 42;
			}

			if( avatar_wrapper.css('display') !== 'none' ) {
				smoothScrollTo( to, 1000 );
			} else {
				smoothScrollTo( $('#header').height(), 1000 );
			}

			button.append( close_icon );

			$('.about').css({
				height: $('.about>.section-wrapper').outerHeight(true)
			});
			$('body').addClass('open-about-section');
		}

		function closeAboutSection() {
			$('.about').css({
				height: 0
			});
			$('body').removeClass('open-about-section');
			button.find('.fa').remove();
		}
	}


	function initFitVids() {
		$('.post').each(function() {
			var figure = $('figure.post-thumbnail', this).not('figure.fitVids').has('iframe');
			figure.addClass('fitVids').fitVids({
				customSelector: 'iframe[src^="http://videohive.net"]',
			});
		});
	}




	function masonryGalleries() {
		var galleries = $('.gallery.masonry-gallery');
		if( galleries.length === 0 || !galleries.isotope ) return;

		galleries.each(function() {
			var gallery = $(this);
			gallery.isotope({
				isOriginLeft: $('body').hasClass('rtl') ? false : true,
				itemSelector: 'li.gallery-item',
				layoutMode : 'masonry'
			});

			$(window).off('smartresize', resize);
			$(window).on('smartresize', resize);

			function resize() {
				gallery.isotope();
			}
		});
	}




	/* masonry */
	function masonry() {
		var posts = $('.content.posts:not(.single .content.posts, .page .content.posts)');
		if( posts.length === 0 || !posts.isotope ) return;

		$( posts ).isotope({
			isOriginLeft: $('body').hasClass('rtl') ? false : true,
			itemSelector: 'article.hentry',
			layoutMode : 'masonry'
		});

		$( posts ).isotope('once', 'layoutComplete', function(){
			initAccomplishments();
		});

		$(window).off('smartresize', resize);
		$(window).on('smartresize', resize);

		function resize() {
			//$( posts ).masonry('reloadItems').masonry();
			$( posts ).isotope();
		}
	}




	function loadMore( page ) {
		var page_url = skotyData.paged_urls[ page ];
		if( !page_url ) return false;

		var content = $('.content.posts');
		$('body').addClass('loading-articles');

		skotyData.loading_posts = true;

		$.ajax({
			type : 'get',
			dataType : 'text',
			url : page_url,
			/*data : {
				action: 'easyframework_load_more',
				nonce: skotyData.loadmore_nonce,
				page: page + 1
			},*/
			success: function( response ) {

				if( !response ) return false;
				var html = $.parseHTML( response, undefined, true );
				var articles = $(html).find('.posts>article');

				articles.imagesLoaded(function() {
					content.isotope().append(articles);
					/* init the mediaelementplayer on videos and audio */
					content.find('.post').each(function() {
						if( $().mediaelementplayer ) {
							$(this).find('audio, video, .wp-audio-playlist').mediaelementplayer();
						}
					});

					/* listen to layoutComplete event */
					$( content ).isotope( 'on', 'layoutComplete', complete );

					function complete() {
						initFitVids();
						var to = articles.eq(0).offset().top - 42;
						if( to ) {
							setTimeout( function(){
								//smoothScrollTo( to, 1000 );
							}, 500);
						}
						//console.log(to, 'layoutComplete');
						$( content ).isotope( 'off', 'layoutComplete', complete );
						articles.css({opacity: 1});
					}
					/* trigger the appended method of Masonry */
					articles.css({
						opacity: 0
					});
					content.isotope('appended', articles.get()).isotope('layout');

					window.Retina.reinit( articles.get() );

					skotyData.loading_posts = false;

					//console.log( masonry.appended );
					if( !skotyData.paged_urls[ page + 1 ] ) {
						$('body').addClass('all-articles-loaded').addClass('no-pagination');
					}

					$('body').removeClass('loading-articles');
				});
				
			},
			error: function(jqXHR, textStatus, errorThrown) {
				console.log(jqXHR, textStatus, errorThrown);
			}
		});
		//console.log( page, page_url );
	}





	var ScrollToggle = function(top, callbackShow, callbackHide) {
		this.ontop = 0;
		this.hontop = 0;
		this.top = top;
		this.show = callbackShow;
		this.hide = callbackHide;
		var self = this;

		(function () {
			$(window).scroll(function () {
				var y = $(window).scrollTop();
				if (y >= self.top) {
					self.ontop = 1;
				} else {
					self.ontop = 0;
				}
				if (self.ontop !== self.hontop) {
					if (self.ontop) {
						if( self.show ) {
							self.show();
						}
					} else {
						if( self.hide ) {
							self.hide();
						}
					}
				}
				self.hontop = (self.ontop * 1);
			});
		})();
	};





	$.fn.headerMedia = function() {
		var element = this;
			element.type = this.attr('data-media-type');
			element.url = this.attr('data-media-url');
			element.mime = this.attr('data-media-mime');
			element.fallback_url = this.attr('data-media-fallback-url');
			element.target_opacity = parseFloat( this.attr('data-media-opacity') );

		var disable_header_parallax = skotyData.options && skotyData.options['disable-parallax-header'] ? skotyData.options['disable-parallax-header'] : false;

		if( $('body').hasClass('mobile') ) {
			disable_header_parallax = true;
		}

		if( disable_header_parallax ) {
			element.use_parallax = false;
		} else {
			element.use_parallax = true;
		}

		if( !element.url ) {
			fadeIn();
		}
		//console.log(element.url);

		function __construct() {
			var mime = element.mime;
			var supports_video = supports_media(mime, 'video');

			//console.log( element.mime );

			/* switch to fallback image if mobile or unsupported format */
			if( element.type === 'video' && (skotyData.is_mobile || !supports_video) && element.fallback_url ) {
				element.type = 'image';
				element.url = element.fallback_url;
			}

			switch( element.type ) {
				case 'image':
					var image = $('<img/>').hide();
						image.one('load', function() {
							element.css({
								'background-image': 'url(' + element.url + ')'
							});
							fadeIn();
							//console.log('image loaded');
							image.remove();
						});
						image.attr({ src: element.url });
						//console.log(image);
				break;
				case 'video':
					var video = $('<video/>', {
						muted: true,
						loop: true,
						autoplay: true,
						src: element.url
					});

					video.appendTo( element ).get( 0 ).addEventListener( 'canplaythrough', function() {
						fadeIn();
						if( element.type ) {
							fitVideo();
						}
					});
				break;
			}
		}

		function parallax() {
			if( element.parent().length === 0 ) return;
			
			var offsetTop = element.parent().offset().top;
			var cont_height = element.parent().height();
			var element_height = element.outerHeight();
			var admin_bar_height = $('#wpadminbar').height();

			var scrollTop = $(window).scrollTop() + (admin_bar_height ? admin_bar_height : 0);

			if( scrollTop > cont_height ) { return; }

			var zeroed_offset = scrollTop - (offsetTop - cont_height);
			var perc = zeroed_offset * 100 / (cont_height + element_height );
			var pos = (cont_height * perc / 100) - element_height / 2;

			if( scrollTop > offsetTop - cont_height && scrollTop < offsetTop + element_height  ) {
				if( pos > 0 ) {
					element.css({ 
						/*y: pos.toFixed(2)*/
						'-webkit-transform': 'translate3d(0, '+pos.toFixed(2)+'px, 0)',
						'transform': 'translate3d(0, '+pos.toFixed(2)+'px, 0)'
					});
				} else {
					element.css({ 
						/*y: 0 */
						'-webkit-transform': 'translate3d(0, 0, 0)',
						'transform': 'translate3d(0, 0, 0)'
					});
				}
			}
		}


		function fadeIn() {
			element.css({
				opacity: element.target_opacity
			});

			$('body').addClass('header-media-loaded');
		}

		function fitVideo() {
			var cont_width = element.parent().width();
			var video_width = element.find('video').width();

			element.find('video').css({
				left: cont_width / 2 - video_width / 2
			});
		}

		if( element.type ) {
			$(window).resize( fitVideo );
		}

		if( element.use_parallax ) {
			$(window).on( 'scroll', parallax );
			parallax();
		}

		__construct();
		//console.log( element );
		return element;
	};


	/*function get_mime( filetype ) {
		var mimetype = '';
		var media_container = 'video';
		switch(filetype) {
			case 'mp4':
				mimetype = 'video/mp4; codecs="avc1.42E01E, mp4a.40.2"';
			break;
			case 'ogg':
				mimetype = 'video/ogg; codecs="theora, vorbis"';
			break;
			case 'webm':
				mimetype = 'video/webm; codecs="vp8, vorbis"';
			break;
			case 'mp3':
				mimetype = 'audio/mpeg';
				media_container = 'audio';
			break;
		}
		return {'mimetype':mimetype,'container':media_container};
	}*/

	function supports_media( mimetype, container ) {
		var elem = document.createElement(container);
		if(typeof elem.canPlayType == 'function'){
			var playable = elem.canPlayType(mimetype);
			if((playable.toLowerCase() == 'maybe') || (playable.toLowerCase() == 'probably')){
				return true;
			}
		}
		return false;
	}

	

})(jQuery);


//requestAnimationFrame && cancelAnimationFrame polyfill
(function() {
	'use strict';

	var lastTime = 0;
	var vendors = ['ms', 'moz', 'webkit', 'o'];
	for(var x = 0; x < vendors.length && !window.requestAnimationFrame; ++x) {
		window.requestAnimationFrame = window[vendors[x]+'RequestAnimationFrame'];
		window.cancelAnimationFrame = window[vendors[x]+'CancelAnimationFrame'] || window[vendors[x]+'CancelRequestAnimationFrame'];
	}
 
	if (!window.requestAnimationFrame)
		window.requestAnimationFrame = function(callback) {
			var currTime = new Date().getTime();
			var timeToCall = Math.max(0, 16 - (currTime - lastTime));
			var id = window.setTimeout(function() { callback(currTime + timeToCall); }, 
			  timeToCall);
			lastTime = currTime + timeToCall;
			return id;
		};

	if (!window.cancelAnimationFrame)
		window.cancelAnimationFrame = function(id) {
			clearTimeout(id);
		};
}());