<?php
if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$attic_theme = wp_get_theme();

/* Define constants */
define( 'ATTIC_OPTIONS_NAMESPACE', 'skoty_options' );
define( 'ATTIC_THEME_URI', get_template_directory_uri() );
define( 'ATTIC_THEME_RESOURCES_URI', get_template_directory_uri() . '/resources' );
define( 'ATTIC_THEME_VERSION', $attic_theme->Version );

define( 'ATTIC_FRAMEWORK_DIR', get_template_directory() . '/framework' );
define( 'ATTIC_FRAMEWORK_URL', get_template_directory_uri() . '/framework' );

define( 'ATTIC_AJAX_NONCE', '_' . $attic_theme->Name . '_AJAX_NONCE_' );

define( 'ATTIC_IS_DEV', false );
define( 'ATTIC_MIN_SUFFIX', !ATTIC_IS_DEV ? '.min' : '' );

global $alternate_content_width;

$content_width = 800;
$alternate_content_width = 310;


/* Include the framework */
require_once( ATTIC_FRAMEWORK_DIR . '/simple-html-dom.php' );
require_once( ATTIC_FRAMEWORK_DIR . '/framework.php' );
require_once( ATTIC_FRAMEWORK_DIR . '/customizer/customizer.php' );

//delete_option(ATTIC_OPTIONS_NAMESPACE);


/*function skoty_preview_kill_customizer_save() {
	wp_send_json_error();
}
add_action( 'customize_save', 'skoty_preview_kill_customizer_save' );*/


add_editor_style( 'editor.css' );


if( class_exists('AtticThemes_BlogExtender') ) {
	if( method_exists('AtticThemes_BlogExtender', 'addPostTypeSupport') ) {
		AtticThemes_BlogExtender::addPostTypeSupport( 'atp_project' );
	}
	AtticThemes_BlogExtender::$settings = array( 'display' => false );
}
//

function skoty_add_metabox() {
	new EasyMetaBox(
		'skoty-page-header-settings',
		'Page Heading Settings',
		ATTIC_FRAMEWORK_DIR . '/metabox/page-header-settings.php',
		'page', 'side', 'high'
	);
}
add_action( 'admin_init', 'skoty_add_metabox' );



/* add image sizes */
if ( function_exists( 'add_image_size' ) ) {
	add_image_size( 'skoty-blog-post', 357, 9999, false );
	add_image_size( 'skoty-blog-post-single', 800, 9999, false );
	
	if( class_exists('EasyFramework') ) {
		EasyFramework::EasyImageSizes( array(
			'skoty-avatar' => array(
				'width' => 235,
				'height' => 235,
				'crop' => true
			),
		));
	}
}

/* Adding Theme Support */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-formats', array( 'aside', 'quote', 'link', 'image', 'gallery', 'audio', 'video', 'chat' ) );

	add_theme_support( 'post-thumbnails', array( 'atp_project', 'post', 'page', 'forum', 'topic' ) );
}


function skoty_init() {
	add_post_type_support( 'page', 'excerpt' );
	/*bbPress*/
	//add_post_type_support( 'forum', array('thumbnail', 'excerpt') );
	//add_post_type_support( 'topic', array('thumbnail', 'excerpt') );
}
add_action( 'init', 'skoty_init' );





function skoty_theme_setup() {
	load_theme_textdomain('skoty', get_template_directory() . '/languages');
}
add_action('after_setup_theme', 'skoty_theme_setup');





/* register navigation menu location */
register_nav_menu( 'header-location', __('Menu will appear in the header or the main sidebar of the theme.', 'skoty') ); 

/* register sidebars */
register_sidebar( array(
		'name' => __( 'Main Sidebar', 'skoty' ),
		'id' => 'skoty-main-sidebar',
		'description' => __( 'Main sidebar taht appears on the right of all pages.', 'skoty' ),
		'class' => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s"><div class="widget-wrapper">',
		'after_widget' => '</div></li>',
		'before_title' => '<span class="widget-title">',
		'after_title' => '</span>'
	)
);

register_sidebar( array(
		'name' => __( 'Footer Area', 'skoty' ),
		'id' => 'skoty-footer-sidebar',
		'description' => __( 'This will appear in the footer of all pages.', 'skoty' ),
		'class' => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s"><div class="widget-wrapper">',
		'after_widget' => '</div></li>',
		'before_title' => '<span class="widget-title">',
		'after_title' => '</span>'
	)
);

register_sidebar( array(
		'name' => __( 'Accomplishments Area', 'skoty' ),
		'id' => 'skoty-accomplishment-sidebar',
		'description' => __( 'This will appear above the footer in all pages.', 'skoty' ),
		'class' => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s"><div class="widget-wrapper">',
		'after_widget' => '</div></li>',
		'before_title' => '<span class="widget-title">',
		'after_title' => '</span>'
	)
);

/*register_sidebar( array(
		'name' => __( 'Sidebar Left', 'skoty' ),
		'id' => 'skoty-default-sidebar-left',
		'description' => __( 'Appears in in pages that support sidebar on the left side of the content.', 'skoty' ),
		'class' => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s"><div class="widget-wrapper">',
		'after_widget' => '</div></li>',
		'before_title' => '<span class="widget-title">',
		'after_title' => '</span>'
	)
);

register_sidebar( array(
		'name' => __( 'Sidebar Right', 'skoty' ),
		'id' => 'skoty-default-sidebar-right',
		'description' => __( 'Appears in pages that support a sidebar on the right side of the content.', 'skoty' ),
		'class' => '',
		'before_widget' => '<li id="%1$s" class="widget %2$s"><div class="widget-wrapper">',
		'after_widget' => '</div></li>',
		'before_title' => '<span class="widget-title">',
		'after_title' => '</span>'
	)
);*/









function skoty_enqueue_resources() {
	global $post, $wp_query;
	/* enqueue styles */

	//Font-Awesome
	wp_register_style( 'font-awesome', ATTIC_THEME_RESOURCES_URI . '/css/font-awesome'.ATTIC_MIN_SUFFIX.'.css', array(), ATTIC_THEME_VERSION, 'all' );
	wp_enqueue_style( 'font-awesome' );

	/* main style */
	wp_register_style( 'main-style', ATTIC_THEME_RESOURCES_URI . '/css/main-style'.ATTIC_MIN_SUFFIX.'.css', array(), ATTIC_THEME_VERSION, 'all' );
	wp_enqueue_style( 'main-style' );

	/* theme style */
	wp_register_style( 'theme-stylesheet', get_stylesheet_uri(), array('main-style'), ATTIC_THEME_VERSION );
	wp_enqueue_style( 'theme-stylesheet' );

	



	/* ----------------------------------------------------- */

	/* equeue scripts */
	wp_enqueue_script( 'jquery' );
	//wp_enqueue_script( 'jquery-masonry' );
	wp_enqueue_script( 'comment-reply' );
	wp_enqueue_script( 'wp-mediaelement' );

	
	//smoothscroll
	if( !wp_is_mobile() && EasyFramework::EasyOption('use-smoothscroll', false) ) {
		wp_register_script( 'smoothscroll', ATTIC_THEME_RESOURCES_URI . '/js/smooth.scroll.min.js', array(), ATTIC_THEME_VERSION, true );
		wp_enqueue_script( 'smoothscroll' );
	}


	wp_register_script( 'helpers',
		ATTIC_THEME_RESOURCES_URI . '/js/helpers'.ATTIC_MIN_SUFFIX.'.js',
		array( 'jquery' ),
		ATTIC_THEME_VERSION,
		true
	);
	wp_enqueue_script( 'helpers' );


	/*wp_register_script( 'jquery-easing', ATTIC_THEME_RESOURCES_URI . '/js/jquery.easing.1.3'.ATTIC_MIN_SUFFIX.'.js', array( 'jquery' ), ATTIC_THEME_VERSION, true );
	wp_enqueue_script( 'jquery-easing' );*/

	//transit
	/*if( !wp_script_is( 'atbb-transit-script', 'enqueued' ) ) {
		//transit
		wp_register_script( 'jquery-transit', ATTIC_THEME_RESOURCES_URI . '/js/jquery.transit.min.js', array( 'jquery' ), ATTIC_THEME_VERSION, true );
		wp_enqueue_script( 'jquery-transit' );
	}*/

	//retina.js
	/*wp_register_script( 'retina-js',
		ATTIC_THEME_RESOURCES_URI . '/js/retina'.ATTIC_MIN_SUFFIX.'.js',
		array( 'jquery' ),
		ATTIC_THEME_VERSION,
		true
	);
	wp_enqueue_script( 'retina-js' );*/

	//fitvids
	/*wp_register_script(
		'fitvids',
		ATTIC_THEME_RESOURCES_URI . '/js/jquery.fitvids'.ATTIC_MIN_SUFFIX.'.js',
		array( 'jquery' ),
		ATTIC_THEME_VERSION,
		true
	);
	wp_enqueue_script( 'fitvids' );*/


	if( wp_is_mobile() ) {
		//hammer.js
		wp_register_script(
			'hammer',
			ATTIC_THEME_RESOURCES_URI . '/js/hammer'.ATTIC_MIN_SUFFIX.'.js',
			array( 'jquery' ),
			ATTIC_THEME_VERSION,
			true
		);
		wp_enqueue_script( 'hammer' );
	}

	//isotope
	wp_register_script(
		'isotope',
		ATTIC_THEME_RESOURCES_URI . '/js/jquery.isotope.min.js',
		array( 'jquery' ),
		ATTIC_THEME_VERSION,
		true
	);
	wp_enqueue_script( 'isotope' );

	//flexslider
	wp_register_script(
		'flexslider',
		ATTIC_THEME_RESOURCES_URI . '/js/jquery.flexslider'.ATTIC_MIN_SUFFIX.'.js',
		array( 'jquery' ),
		ATTIC_THEME_VERSION,
		true
	);
	wp_enqueue_script( 'flexslider' );

	//mediabox
	wp_register_script(
		'mediabox',
		ATTIC_THEME_RESOURCES_URI . '/js/jquery.mediabox'.ATTIC_MIN_SUFFIX.'.js',
		array( 'jquery' ),
		ATTIC_THEME_VERSION,
		true
	);
	wp_enqueue_script( 'mediabox' );

	/* main script */
	wp_register_script(
		'main-script', 
		ATTIC_THEME_RESOURCES_URI . '/js/script'.ATTIC_MIN_SUFFIX.'.js', 
		array( 'jquery', 'isotope', 'mediabox', 'flexslider' ),
		ATTIC_THEME_VERSION,
		true
	);
	wp_enqueue_script( 'main-script' );

	/* get urls of paged archive */
	$paged_urls = array();
	if( isset($wp_query->max_num_pages) ) {
		for($p = 1; $p <= $wp_query->max_num_pages; $p++ ) {
			$paged_urls[] = get_pagenum_link( $p );
		}
	}

	wp_localize_script( 'main-script', 'SKOTY_DATA', array(
			'ajax_url' => admin_url( 'admin-ajax.php' ),
			'contact_nonce' => wp_create_nonce( 'Skoty-Contacts-Nonce-Ajax' ),
			'loadmore_nonce' => wp_create_nonce( 'Skoty-Load-More-Nonce-Ajax' ),
			'post_id' => isset($post) && isset($post->ID) ? $post->ID : null,
			'options' => json_encode( get_option(ATTIC_OPTIONS_NAMESPACE) ),
			'paged_urls' => json_encode( $paged_urls ),
			/*'woocommerce' => array(
				'enable_lightbox' => get_option('woocommerce_enable_lightbox', 'no') === 'no' ? false : true
			)*/
		)
	);
}
add_action( 'wp_enqueue_scripts', 'skoty_enqueue_resources' );


/* Deregister unnecessary atticthemes plugin styles and scripts */
function skoty_deregister_scripts() {
	/*JS*/
	/* deregister box-builder scripts */
	wp_deregister_script( 'atbb-transit-script' );
	wp_dequeue_script( 'atbb-transit-script' );

	wp_deregister_script( 'atbb-flex-slider-script' );
	wp_dequeue_script( 'atbb-flex-slider-script' );

	wp_deregister_script( 'atbb-media-box-script' );
	wp_dequeue_script( 'atbb-media-box-script' );

	/* deregister attic portfolio scripts */
	wp_deregister_script( 'atp-isotope' );
	wp_dequeue_script( 'atp-isotope' );

	wp_deregister_script( 'atp-flexslider' );
	wp_dequeue_script( 'atp-flexslider' );


	/*CSS*/
	/* deregister box-builder styles */
	wp_deregister_style( 'atbb-style' );
	wp_dequeue_style( 'atbb-style' );

	/* deregister attic portfolio styles */
	wp_deregister_style( 'atp-style' );
	wp_dequeue_style( 'atp-style' );
	
	if( EasyFramework::EasyOption('social-icons-style', 'theme') === 'theme' ) {
		//remove social icons CSS file
		//wp_deregister_style( 'atsi-style' );
		//wp_dequeue_style( 'atsi-style' );
	}
}
add_action( 'wp_enqueue_scripts', 'skoty_deregister_scripts', 20 );




















/* --------------------------Customizations ------------------------- */

function skoty_atpprojectthumb( $thumb_size, $post_id, $settings ) {
	if(isset($settings['columns'])) {
		switch ( intval($settings['columns']) ) {
			case 1:
				$thumb_size = 'full';
			break;
			case 2:
				$thumb_size = 'skoty-blog-post-single';
			break;
			case 3:
				$thumb_size = 'skoty-blog-post-single';
			break;
			default:
				$thumb_size = 'skoty-blog-post';
			break;
		}
	}

	return $thumb_size;
}
add_filter( 'atp_project_thumb_size', 'skoty_atpprojectthumb', 10, 3 );


function skoty_atp_meta_before_thumb( $meta, $namespace ) {
	
	$header_options = array(
		'no-data' => __('No Heading', 'skoty'),
		'site-data' => __('Site Title &amp; Tagline', 'skoty'),
		'page-data' => __('Project Title &amp; Excerpt', 'skoty')
	);

	?><div class="atp-meta-option atp-select">
		<p class="atp-option-title"><?php _e('Header Style', 'skoty'); ?></p>
		<em class="atp-option-description howto"><?php _e('Select the header style.', 'skoty'); ?></em>
		<select style="width: 100%;" name="<?php echo $namespace; ?>[header-settings]">
			<?php foreach ($header_options as $key => $value) { ?>
				<?php $selected = $meta && isset($meta['header-settings']) && $key === $meta['header-settings'] ? 'selected' : ''; ?>
				<option <?php echo $selected; ?> value="<?php echo $key; ?>"><?php echo $value; ?></option>
			<?php } ?>
		</select>
	</div><?php
}
add_action( 'atp_meta_before_thumb', 'skoty_atp_meta_before_thumb', 10, 2 );


















add_filter( 'widget_text', 'do_shortcode' );



function skoty_widget_tag_cloud_args() {
	$args['number'] = 20;
	$args['largest'] = 14;
	$args['smallest'] = 14;
	$args['unit'] = 'px';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'skoty_widget_tag_cloud_args' );


function skoty_excerpt_length( $length ) {
	return 10;
}
add_filter( 'excerpt_length', 'skoty_excerpt_length', 999 );

function skoty_excerpt_more( $more ) {
	return '&hellip;';
}
add_filter( 'excerpt_more', 'skoty_excerpt_more' );

/* more link */
function skoty_more_link( $more_link, $more_link_text ) {
	global $post;

	if( isset($post) && has_post_format('chat') ) {
		$more_html = str_get_html( $more_link );
		$more_link = $more_html->find('a', 0);
		$more_link->innertext = '&hellip;';
		return '<p class="more-link-wrapper">'. $more_link->outertext .'</p>';
	} else {
		return $more_link;
	}
	
}
add_filter( 'the_content_more_link', 'skoty_more_link', 10, 2 );


/* add markdown to widget titles */
function skoty_html_widget_title( $title ) {
	$title = str_replace( '[', '<', $title );
	$title = str_replace( '[/', '</', $title );
	$title = str_replace( ']', '>', $title );
	return $title;
}
add_filter( 'widget_title', 'skoty_html_widget_title' );



function skoty_filter_content( $content ) {
	if ( has_post_format( 'aside' ) && !is_singular() ) {
		$content .= ' <a class="aside-infinite" href="' . get_permalink() . '">&#8734;</a>';
	}
	return $content;
}
add_filter( 'the_content', 'skoty_filter_content' );





function skoty_img_caption_shortcode( $empty, $attr, $content ){
	$attr = shortcode_atts( array(
		'id'      => '',
		'align'   => 'alignnone',
		'width'   => '',
		'caption' => ''
	), $attr );

	if ( 1 > (int) $attr['width'] || empty( $attr['caption'] ) ) {
		return '';
	}

	if ( $attr['id'] ) {
		$attr['id'] = 'id="' . esc_attr( $attr['id'] ) . '" ';
	}

	return '<div ' . $attr['id']
	. 'class="wp-caption ' . esc_attr( $attr['align'] ) . '" '
	. 'style="max-width: ' . ( (int) $attr['width'] ) . 'px;">'
	. do_shortcode( $content )
	. '<p class="wp-caption-text">' . $attr['caption'] . '</p>'
	. '</div>';

}
add_filter( 'img_caption_shortcode', 'skoty_img_caption_shortcode', 10, 3 );





function skoty_password_form() {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	return '<form class="post-password-form" action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post"><span class="post-password-form-note">' . __( 'To view this protected post, enter the password below:', 'skoty' ) . '</span><div class="post-password-form-inputs-wrapper"><input class="post-password-form-text-field" placeholder="'.__( 'Password', 'skoty' ).'" name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" /><input class="post-password-form-submit" type="submit" name="Submit" value="' . esc_attr__( 'Submit', 'skoty' ) . '" /></div></form>';
}
add_filter( 'the_password_form', 'skoty_password_form' );







/* VideoHive embed handeler */
function skoty_embed_handler_videohive( $matches, $attr, $url, $rawattr ) {
	global $content_width;

	$tag = 'iframe';
	$embed = '<'.$tag.' src="'.$matches[1].'/embed/'.$matches[2].'" width="'.$content_width.'" height="'.round($content_width * 0.56).'" frameborder="0"></'.$tag.'>';
	return apply_filters( 'embed_videohive', $embed, $matches, $attr, $url, $rawattr );
}

wp_embed_register_handler( 'videohive', '#(http://[www\.]?videohive\.net/item/[\S]+)/([\d]+)*#i', 'skoty_embed_handler_videohive');











/* body class */
function skoty_body_class_names( $classes ) {
	global $post;
	$classes[] = 'skoty';

	if( is_rtl() ) {
		$classes[] = 'rtl';
	}

	if( EasyFramework::EasyOption('remove-avatar', false) ) {
		$classes[] = 'no-avatar';
	}

	$classes[] = EasyFramework::EasyOption( 'header-height-style', 'default' ) . '-header-height';

	$header_media_data = EasyFramework::EasyOption( 'header-media' );
	if( $header_media_data && isset($header_media_data['url']) ) {
		$classes[] = 'has-header-media';
	}

	if( isset($post) && isset($post->ID) && !comments_open($post->ID) && get_comments_number($post->ID) == 0 ) {
		$classes[] = 'no-comments';
	}

	if( isset($post) && isset($post->ID) && comments_open($post->ID) ) {
		$classes[] = 'open-comments';
	}

	if( isset($post) && isset($post->ID) && comments_open($post->ID) && get_comments_number($post->ID) > 0 ) {
		$classes[] = 'has-comments';
	}

	if( !is_active_sidebar('skoty-footer-sidebar') ) {
		$classes[] = 'no-footer-widgets';
	}

	if( wp_is_mobile() ) {
		$classes[] = 'mobile';
	} else {
		$classes[] = 'not-mobile';
	}

	if( !EasyFramework::HasPagination() ) {
		$classes[] = 'no-pagination';
	}

	

	$page_for_posts = get_option( 'page_for_posts', false );
	$header_settings = 'no-data';

	if( is_page() && isset($post) && isset($post->ID) ) {

		$page_settings = isset($post->ID) ? get_post_meta( $post->ID, '_skoty-page-header-settings', true ) : array(); 
		$header_settings = isset($page_settings['header-settings']) ? $page_settings['header-settings'] : false;
		/* --------------------------------- */
	} elseif( is_singular('atp_project') && isset($post) && isset($post->ID) ) {

		$page_settings = isset($post->ID) ? get_post_meta( $post->ID, '_atp_project_metabox', true ) : array(); 
		$header_settings = isset($page_settings['header-settings']) ? $page_settings['header-settings'] : false;
		/* --------------------------------- */
	} elseif( is_home() && $page_for_posts ) {

		$page_settings = $page_for_posts ? get_post_meta( $page_for_posts, '_skoty-page-header-settings', true ) : array(); 
		$header_settings = isset($page_settings['header-settings']) ? $page_settings['header-settings'] : false;
		/* --------------------------------- */
	} else if( is_home() && !$page_for_posts ) {
		$header_settings = 'site-data';
	}

	if( $header_settings !== 'no-data' ) {
		$classes[] = 'has-header-content';
	}



	$nav_style = EasyFramework::EasyOption('nav-style', 'side');
	$classes[] = $nav_style . '-navigation';

	return $classes;
}
add_filter('body_class','skoty_body_class_names');


















/* Customizing default gallery shortcode */
function attic_post_gallery( $output, $attr ) {
	global $post, $wp_locale;

	if ( class_exists( 'Jetpack', false ) && Jetpack::is_module_active('tiled-gallery') ) {
		//print_r(Jetpack::get_active_modules());
		if( isset($attr['type']) || get_option('tiled_galleries', false) ) {
			return $output;
		}
	}

	static $instance = 0;
	$instance++;

	if ( isset( $attr['orderby'] ) ) {
		$attr['orderby'] = sanitize_sql_orderby( $attr['orderby'] );
		if ( !$attr['orderby'] ) {
			unset( $attr['orderby'] );
		}
	}

	extract(shortcode_atts(array(
		'order' => 'ASC',
		'orderby' => 'menu_order ID',
		'id' => $post->ID,
		'itemtag' => 'li',
		'icontag' => '',
		'captiontag' => '',
		'columns' => 3,
		'size' => 'large',
		'include' => '',
		'exclude' => ''
	), $attr));

	$id = intval($id);
	if( 'RAND' == $order ) {
		$orderby = 'none';
	}


	if ( !empty($include) ) {
		$include = preg_replace( '/[^0-9,]+/', '', $include );
		$_attachments = get_posts( array('include' => $include, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );

		$attachments = array();
		foreach ( $_attachments as $key => $val ) {
			$attachments[$val->ID] = $_attachments[$key];
		}
	} elseif ( !empty($exclude) ) {
		$exclude = preg_replace( '/[^0-9,]+/', '', $exclude );
		$attachments = get_children( array('post_parent' => $id, 'exclude' => $exclude, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	} else {
		$attachments = get_children( array('post_parent' => $id, 'post_status' => 'inherit', 'post_type' => 'attachment', 'post_mime_type' => 'image', 'order' => $order, 'orderby' => $orderby) );
	}

	if( empty($attachments) ) {
		return '';
	}

	if ( is_feed() ) {
		$output = "\n";
		foreach ( $attachments as $att_id => $attachment ) {
			$output .= wp_get_attachment_link($att_id, $size, true) . "\n";
		}
		return $output;
	}

	$itemtag = tag_escape($itemtag);
	$captiontag = tag_escape($captiontag);
	$columns = intval($columns);
	$margins = 100 - ( 2.5 * ($columns - 1) );

	$itemwidth = ($columns > 0 ? round(100 / $columns, 2) - 0.1 : 100);
	$float = is_rtl() ? 'right' : 'left';

	$selector = "gallery-{$instance}";

	$media_gallery = isset($attr['link']) && 'file' == $attr['link'] ? 'media-gallery' : 'attachment-gallery';
	$slider_gallery = $columns === 1 || EasyFramework::EasyOption('post-gallery-type', 'default') !== 'default' ? 'slider-gallery' : '';
	$masonry_gallery = empty($slider_gallery) ? 'masonry-gallery' : '';


	$output = "<div class='gallery-wrapper gallery-shortcode {$slider_gallery}'>";

	$output .= apply_filters('gallery_style', "<style type='text/css' scoped>#{$selector} {margin: auto;}#{$selector} .gallery-item {
float: {$float};text-align: center;width: {$itemwidth}%;}#{$selector} img {display: block;}#{$selector} .gallery-caption {margin-left: 0;}</style><!-- see gallery_shortcode() in wp-includes/media.php -->"
	);

	$output .= "<ul id='$selector' class='gallery galleryid-{$id} gallery-columns-{$columns} {$media_gallery} {$masonry_gallery}' >";

	$counter = 0;
	foreach( $attachments as $id => $attachment ) {
		$image = wp_get_attachment_image( $id, $size, false );
		$link_url = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_url( $id ) : get_attachment_link( $id );

		$to_attachment = isset($attr['link']) && 'file' == $attr['link'] ? '' : 'to-attachment';

		$link = '<a class="'.$to_attachment.'" href="'.$link_url.'" title="'.esc_attr( $attachment->post_excerpt ).'">'.$image.'</a>';
		//$link = isset($attr['link']) && 'file' == $attr['link'] ? wp_get_attachment_link($id, $size, false, false) : wp_get_attachment_link($id, $size, true, false);
		$title_attr = esc_attr( get_the_title($id) );

		if( $columns > 0 && ++$counter == $columns ) {
			$output .= "<{$itemtag} data-post-id='{$id}' data-post-title='{$title_attr}' class='gallery-item last'>";
			$counter = 0;
		} else {
			if( $counter == 1 ) {
				$output .= "<{$itemtag} data-post-id='{$id}' data-post-title='{$title_attr}' class='gallery-item first'>";
			} else {
				$output .= "<{$itemtag} data-post-id='{$id}' data-post-title='{$title_attr}' class='gallery-item'>";
			}
		}

		$output .= $link;

		if( $captiontag && trim($attachment->post_excerpt) ) {
			//$output .= "<{$captiontag} class='gallery-caption'>" . wptexturize($attachment->post_excerpt) . "</{$captiontag}>";
		}
		$output .= "</{$itemtag}>";

		/*if( $columns > 0 && ++$i % $columns == 0 ) {
			//$output .= '<br style="clear: both" />';
		}*/

		//$i++;
	}

	$output .= "</ul>\n";

	if( !empty($slider_gallery) ) {
		$output .= '<ul class="slider-gallery-direction-nav"><li class="prev-slide fa fa-caret-left"></li><li class="next-slide fa fa-caret-right"></li></ul>';
	}

	$output .= "</div>\n";

	return $output;

}
add_filter( 'post_gallery', 'attic_post_gallery', 10, 2 );


















/* custom comment structure */
function skoty_custom_comment_structure($comment, $args, $depth) {

	global $post; $GLOBALS['comment'] = $comment; extract($args, EXTR_SKIP);
	
	?>
	<li <?php comment_class('response'); ?> id="comment-<?php comment_ID() ?>" >
		<div class="comment-wrapper" id="comment-wrapper-<?php echo $comment->comment_ID; ?>">
			<div class="comment-avatar-wrap">
				<div class="comment-avatar">
					<?php echo get_avatar( $comment->comment_author_email, 110 ); ?>
				</div>
			</div>
			<div class="comment-wrap">
				<div class="comment-heading">
					<h5 class="comment-author"><?php printf(__('%s', 'skoty'), get_comment_author_link() ) ?></h5>
					<?php edit_comment_link('', '', ''); ?>
					<div class="comment-date">
						<span class="fa fa-clock-o"></span><?php printf(__('%1$s at %2$s', 'skoty'), get_comment_date(),  get_comment_time()) ?>
					</div>
				</div>
				<div class="comment-body">
					<?php if ($comment->comment_approved == '0') { ?>
						<em><?php _e('Your comment is awaiting moderation.', 'skoty') ?></em>
						<br/>
					<?php } ?>
					<?php comment_text() ?>
				</div>
				<?php 
					comment_reply_link( array_merge( $args, array( 
						'add_below' => 'comment-wrapper', 
						'depth' => $depth, 
						'max_depth' => $args['max_depth'] 
						)
					), $comment->comment_ID, $post->ID );
				?>
			</div>
		</div>
	<?php
}

function skoty_custom_comment_structure_end($comment, $args, $depth) {
	?>
	</li>
	<?php
}


/* custom pingback/trackback structure */
function skoty_custom_pingback_structure($comment, $args, $depth) {
	//echo $comment;
	?>
	<li <?php comment_class('response'); ?> id="comment-<?php comment_ID() ?>" >
		<div class="comment-wrap" id="comment-wrap-<?php echo $comment->comment_ID; ?>">
			<div class="comment-heading">
				<span class="comment-author"><?php printf(__('%s', 'skoty'), get_comment_author_link() ) ?></span> <?php //edit_comment_link(__('edit', 'skoty'), '', ''); ?>
			</div>
			
		</div>
	<?php
}
function skoty_custom_pingback_structure_end($comment, $args, $depth) {
	?>
	</li>
	<?php
}





























/* retina image creation functions */
function skoty_retina_support_attachment_meta( $metadata, $attachment_id ) {
	foreach ( $metadata as $key => $value ) {
		if ( is_array( $value ) ) {
			foreach ( $value as $image => $attr ) {
				if ( is_array( $attr ) ) {
					skoty_retina_support_create_images( get_attached_file( $attachment_id ), $attr['width'], $attr['height'], true );
				}
			}
		}
	}
	return $metadata;
}
add_filter( 'wp_generate_attachment_metadata', 'skoty_retina_support_attachment_meta', 10, 2 );

function skoty_retina_support_create_images( $file, $width, $height, $crop = false ) {
	if ( $width || $height ) {
		$resized_file = wp_get_image_editor( $file );
		if ( ! is_wp_error( $resized_file ) ) {
			$filename = $resized_file->generate_filename( $width . 'x' . $height . '@2x' );

			$resized_file->resize( $width * 2, $height * 2, $crop );
			$resized_file->save( $filename );

			$info = $resized_file->get_size();

			return array(
				'file' => wp_basename( $filename ),
				'width' => $info['width'],
				'height' => $info['height'],
			);
		}
	}
	return false;
}

function skoty_delete_retina_support_images( $attachment_id ) {
	$meta = wp_get_attachment_metadata( $attachment_id );
	if( $meta ) {
		$upload_dir = wp_upload_dir();
		$path = pathinfo( $meta['file'] );
		foreach ( $meta as $key => $value ) {
			if ( 'sizes' === $key ) {
				foreach ( $value as $sizes => $size ) {
					$original_filename = $upload_dir['basedir'] . '/' . $path['dirname'] . '/' . $size['file'];
					$retina_filename = substr_replace( $original_filename, '@2x.', strrpos( $original_filename, '.' ), strlen( '.' ) );
					if ( file_exists( $retina_filename ) ) {
						unlink( $retina_filename );
					}
				}
			}
		}
	}
}
add_filter( 'delete_attachment', 'skoty_delete_retina_support_images' );





























/* Include the TGM_Plugin_Activation class. */
require_once( get_template_directory() . '/framework/class-tgm-plugin-activation.php' );
/* add required plugins via TGM_Plugin_Activation class */
function skoty_theme_register_required_plugins() {
	$plugins = array(
		array(
			'name' => 'AtticThemes: Blog Extender', // The plugin name
			'slug' => 'atticthemes-blog-extender', // The plugin slug (typically the folder name)
			'source' => get_stylesheet_directory() . '/plugins/atticthemes-blog-extender.zip', // The plugin source
			'required' => false, // If false, the plugin is only 'recommended' instead of required
			'version' => '1.0.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' => '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name' => 'AtticThemes: Box Builder', // The plugin name
			'slug' => 'atticthemes-box-builder', // The plugin slug (typically the folder name)
			'source' => get_stylesheet_directory() . '/plugins/atticthemes-box-builder.zip', // The plugin source
			'required' => false, // If false, the plugin is only 'recommended' instead of required
			'version' => '2.3.0', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' => '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name' => 'AtticThemes: Portfolio', // The plugin name
			'slug' => 'atticthemes-portfolio', // The plugin slug (typically the folder name)
			'source' => get_stylesheet_directory() . '/plugins/atticthemes-portfolio.zip', // The plugin source
			'required' => false, // If false, the plugin is only 'recommended' instead of required
			'version' => '1.0.3', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' => '', // If set, overrides default API URL and points to an external URL
		),
		array(
			'name' => 'AtticThemes: Social Icons',
			'slug' => 'atticthemes-social-icons',
			'required' => false,
		),
		array(
			'name' => 'Envato WordPress Toolkit', // The plugin name
			'slug' => 'envato-wordpress-toolkit-master', // The plugin slug (typically the folder name)
			'source' => get_stylesheet_directory() . '/plugins/envato-wordpress-toolkit-master.zip', // The plugin source
			'required' => false, // If false, the plugin is only 'recommended' instead of required
			'version' => '1.7.1', // E.g. 1.0.0. If set, the active plugin must be this version or higher, otherwise a notice is presented
			'force_activation' => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins
			'external_url' => 'https://github.com/envato/envato-wordpress-toolkit/archive/master.zip', // If set, overrides default API URL and points to an external URL
			'network' => true
		),
	);

	//

	$config = array(
		'domain' => 'skoty',
		'menu' => 'install-required-plugins'
	);

	tgmpa( $plugins, $config );
}
/* add the tgm register action */
add_action( 'tgmpa_register', 'skoty_theme_register_required_plugins' );
?>










