<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php get_header(); ?>

<section class="content-wrapper">
	<div class="content">
		<?php if( have_posts() ) : ?>
			<?php while( have_posts() ) : the_post(); ?>
				<?php get_template_part( 'content', 'atp_project' ); ?>
				<?php comments_template('', true); ?>
			<?php endwhile; ?>
		<?php else: ?>
			<?php _e('Sorry, there are no projects matching your query.', 'skoty'); ?>
		<?php endif; ?>
	</div>
</section>

<?php get_footer(); ?>