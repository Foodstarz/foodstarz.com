<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<article <?php post_class(); ?> title="<?php the_title_attribute(); ?>">
	<?php if( has_post_thumbnail() ) : ?>
		<figure class="post-thumbnail"><?php the_post_thumbnail(); ?></figure>
	<?php endif; ?>
	<div class="aside-post-content-wrapper">
		<div class="post-content"><?php the_content(); ?></div>
	</div>
	<?php EasyFramework::EasyPostLinks(); ?>
	<hr class="post-content-separator">
</article>