<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
	<label>
		<span class="screen-reader-text"><?php _e('Search for:', 'skoty'); ?></span>
		<input type="text" class="search-field" placeholder="<?php _e('Type and hit enter', 'skoty'); ?>" value="<?php the_search_query(); ?>" name="s" title="<?php _e('Search for:', 'skoty'); ?>" />
	</label>
	<input type="submit" class="search-submit" value="Search" />
</form>