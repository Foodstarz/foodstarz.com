<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>

<?php if( is_page() ) : ?><div class="page-comments"><?php endif; ?>
<div id="comments" class="post-comments-wrapper">
	<?php if ( !post_password_required( $post->ID ) ) : ?>

		<?php if( is_page() && (comments_open() || have_comments()) ) : ?><hr class="page-comments-separator"><?php endif; ?>

		<?php if ( have_comments() ) : ?>


			<h5 class="comments-title-heading"><?php
				printf(
					_n( 'One Comment', '%1$s Comments',
					get_comments_number( $post->ID ), 'skoty' ),
					number_format_i18n( get_comments_number( $post->ID ) )
				);
			?></h5>

			<ul class="comment-list">
				<?php
					$comments = get_comments( array( 'post_id' => $post->ID, ) );
					wp_list_comments( array(
						'type' => 'comment',
						'callback' => 'skoty_custom_comment_structure', 
						'end-callback' => 'skoty_custom_comment_structure_end' 
					), $comments
				); ?>
			</ul>

			<div class="comment-pagination"><?php 
				paginate_comments_links( array( 
						'next_text' => '',
						'prev_text' => '',
						'add_fragment' => '',
						'classes' => array(
							'previous' => 'prev',
							'next' => 'next',
							'numbers' => 'page-numbers',
							'current' => 'current',
							'dots' => 'dots',
						),
					)
				);
			?></div>

		<?php endif; ?>

		<?php if( !empty($wp_query->comments_by_type['pingback']) || !empty($wp_query->comments_by_type['trackback']) ) : ?>
			
			<div class="pingbak-trackback-wrapper">
				<?php if( !empty($wp_query->comments_by_type['pingback']) ) :?>
					<hr class="pingbacks-form-separator">
					<h5 class="pingback-heading"><?php
						echo __( 'Pingbacks', 'skoty' );
					?></h5>

					<ul class="pingback-list">
						<?php
							$comments = get_comments( array( 'post_id' => $post->ID, ) );
							wp_list_comments( array(
								'type' => 'pingback',
								'callback' => 'skoty_custom_pingback_structure', 
								'end-callback' => 'skoty_custom_pingback_structure_end' 
							), $comments
						); ?>
					</ul>
				<?php endif; ?>

				<?php if( !empty($wp_query->comments_by_type['trackback']) ) :?>
					<h5 class="trackback-heading"><?php
						echo __( 'Trackbacks', 'skoty' );
					?></h5>
					<ul class="trackback-list">
						<?php
							$comments = get_comments( array( 'post_id' => $post->ID, ) );
							wp_list_comments( array(
								'type' => 'trackback',
								'callback' => 'skoty_custom_pingback_structure', 
								'end-callback' => 'skoty_custom_pingback_structure_end'
							), $comments
						); ?>
					</ul>
				<?php endif; ?>
			</div>
		<?php endif; ?>

		<?php if ( comments_open( $post->ID ) ) : ?>
			<?php if ( get_comments_number( $post->ID ) > 0 ) : ?>
				<hr class="comment-form-separator">
			<?php endif; ?>
			<div class="comment-form-wrapper">
				<?php comment_form( array(
						'comment_field' => '<label for="email-comment">' . __('Comment*', 'skoty') . '</label><textarea placeholder="' . __('Comment*', 'skoty') . '" rows="4" id="message-comment" name="comment"></textarea>',
						'fields' => array(
							'author' => '<label for="name-comment">' . __('Name*', 'skoty') . '</label><input placeholder="' . __('Name*', 'skoty') . '" id="name-comment" type="text" name="author" value="">',
							'email' => '<label for="email-comment">' . __('Email*', 'skoty') . '</label><input placeholder="' . __('Email*', 'skoty') . '" id="email-comment" type="text" name="email" value="">',
							'url' => '<label for="email-comment">' . __('Website', 'skoty') . '</label><input placeholder="' . __('Website', 'skoty') . '" id="website-comment" type="text" name="url" value="">',
						),
						'comment_notes_before' => '<p>' . __('Your email address will not be published. Required fields are marked with *', 'skoty') . '</p>',
						'comment_notes_after' => '<small class="form-allowed-tags">' . sprintf( __( 'You may use these <abbr title="HyperText Markup Language">HTML</abbr> tags and attributes: %s' ), ' <code>' . allowed_tags() . '</code>' ) . '</small>',

						'title_reply' => __( 'Leave a Comment', 'skoty' ),
						'title_reply_to' => __( 'Leave a Reply to %s', 'skoty' ),
						'cancel_reply_link' => __( 'Cancel Reply', 'skoty' ),
						'id_submit' => 'submit-comment',
					) 
				); ?>
			</div>
			<!--END COMMENT-FORM-WRAP-->
		<?php endif; ?>
	<?php else: ?>
		<?php _e('This post is password protected. Enter the password to view any comments.', 'skoty'); ?>
	<?php endif; ?>
</div>
<?php if( is_page() ) : ?></div><?php endif; ?>