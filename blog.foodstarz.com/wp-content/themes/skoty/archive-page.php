<?php if( !defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php 
/*
Template Name: Archive Template
*/
?>
<?php get_header(); ?>

<section class="content-wrapper">
	<div class="content">

		<?php if( have_posts() ) : ?>
			<?php while( have_posts() ) : the_post(); ?>
				<article <?php post_class(); ?>>
					<?php
						$page_settings = get_post_meta( $post->ID, '_skoty-page-header-settings', true ); 
						$header_settings = isset($page_settings['header-settings']) ? $page_settings['header-settings'] : false;
					?>
					<?php if( $header_settings !== 'page-data' ) : ?>
						<h1 class="post-title"><?php the_title(); ?></h1>
					<?php endif; ?>

					<div class="page-content-wrapper">
						<?php the_content(); wp_link_pages(); ?>
					</div>


				<div class="archive-content-wrapper">
					<div class="archive-recent-posts-wrapper block one-half-column">
						<?php $recent_posts = get_posts( array('posts_per_page' => 10/*get_option('posts_per_page', 10)*/) ); ?>
						<?php if( $recent_posts ) : ?>
							<h5 class="archive-lists-title"><?php printf( __(' %s Recent Posts', 'skoty'), count($recent_posts)/*get_option('posts_per_page', 10)*/ ); ?></h5>
							<ul class="archive-recent-posts">
								<?php foreach( $recent_posts as $recent_post ) : ?>
									<li><a href="<?php echo get_permalink( $recent_post->ID ); ?>"><?php echo get_the_title( $recent_post->ID ); ?></a></li>
								<?php endforeach; ?>
							</ul>
						<?php endif; ?>
					</div>

					<div class="archive-categories-wrapper block one-fourth-column">
						<?php $categories = get_categories(); ?>
						<?php if( $categories ) : ?>
							<h5 class="archive-lists-title"><?php _e('By Category', 'skoty' ); ?></h5>
							<ul class="archive-categories">
								<?php foreach( $categories as $category ) : ?>
									<li><a href="<?php echo get_category_link( $category->term_id ); ?>"><?php echo $category->name; ?></a></li>
								<?php endforeach; ?>
							</ul>
						<?php endif ?>
					</div>

					<div class="archive-months-wrapper block one-fourth-column last">
						<h5 class="archive-lists-title"><?php _e('By Month', 'skoty' ); ?></h5>
						<ul class="archive-months">
							<?php 
								wp_get_archives();
							?>
						</ul>
					</div>
				</div>

				</article>
			<?php endwhile; ?>
		<?php else: ?>
			<?php _e('Sorry, there are no posts matching your query.', 'skoty'); ?>
		<?php endif; ?>

	</div>
</section>

<?php get_footer(); ?>